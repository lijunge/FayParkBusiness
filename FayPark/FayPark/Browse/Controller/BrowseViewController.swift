//
//  BrowseViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/25.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit
import SDCycleScrollView
import JTSImageViewController
import AFNetworking
import SwiftyJSON
import SDWebImage
import MBProgressHUD
import MJRefresh
import FBSDKCoreKit
import AppsFlyerLib

class BrowseViewController: UIViewController{
    //侧边栏
    var leftView = UIView()
    var leftBackView = UIView()
    var leftContentView:LeftContentView!

    //顶部banner
    var browseProductInfo: BrowseActivityInfoList?
    //倒计时商品
    var browseCountDownInfo: BrowseCountDownInfo?
    //顶部折扣商品list
    var headerProductArr = Array<BrowseActivityInfo>()
    //商品列表
    var productList = Array<BrowseActivityInfo>()
    var categoryArr = Array<BrowseHotType>()
    //请求商品分类页数
    var pageIndex = 1
    
    lazy var collectionView: UICollectionView = {
        let layout = ZJFlexibleLayout(delegate: self)
        let collectionFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let tmpView = UICollectionView.init(frame: collectionFrame, collectionViewLayout: layout)
        tmpView.showsVerticalScrollIndicator = false
        tmpView.backgroundColor = .white
        tmpView.dataSource = self
        tmpView.delegate = self
        tmpView.register(UINib.init(nibName: "FayParkCollectionCell", bundle: nil), forCellWithReuseIdentifier: "fayparkcell")
        view.addSubview(tmpView)
        return tmpView
    }()
    
    lazy var browseHeaderView: BrowseHeaderView = {
        let tmpBrowseView = BrowseHeaderView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 100))
        tmpBrowseView.delegate = self
        return tmpBrowseView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(requestBrowseAllData), name: Notification.Name(rawValue: "alreadyLogin"), object: nil)
        if UserManager.shareUserManager.getUserID() == "0" || UserManager.shareUserManager.getUserID().isEmpty {
            self.loginTourist()
        } else {
            requestBrowseAllData()
        }
        //设置UI
        setupNavigationItem()
        setupLeftMenuView()
        configEventMonitor()
     }
    //MARK:游客登陆
    func loginTourist() {
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let params = ["Type": "1","CountrySName": countryCode,"CountryCurrency":countryPay]
        FPHttpManager().manager.post(FayParkGetUserTourist, parameters: params, progress: nil, success: { (task, data) in
            let json = JSON(data as! Data)
            if json["Code"].stringValue == "200" {
                let user = User.init(jsonData: json["Data"])
                UserManager.shareUserManager.setUser(user: user)//缓存用户登录信息
                self.requestBrowseAllData()
            }
        }) { (task, error) in
            
        }
    }
    //MARK: - 请求所有数据
    @objc func requestBrowseAllData() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        requestBannerListFromServer()
    }
    //MARK: - 请求bannerlist
    func requestBannerListFromServer() {
        let userID = UserManager.shareUserManager.getUserID()
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,"CountrySName":countryCode,"CountryCurrency":countryPay]
        //let bannersStart = CACurrentMediaTime()
        FPHttpManager().manager.post(BrowseActivityListInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            self.browseProductInfo = BrowseActivityInfoList(jsonData: json["Data"]["ActivityStreetList"][0])
            self.headerProductArr = self.browseProductInfo!.BrowseActivityList
            self.browseCountDownInfo = BrowseCountDownInfo(jsonData: json["Data"]["ActivityCountDownList"][0])
            self.categoryArr.removeAll()
            for item in json["Data"]["HotTypeList"].arrayValue {
                self.categoryArr.append(BrowseHotType(jsonData: item))
            }
            //设置瀑布流
            self.collectionViewInit()
            self.requestBrowseDataFromServer()
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.collectionView.mj_footer.endRefreshing()
        }
    }
    //MARK: - 请求首页数据
    func requestBrowseDataFromServer() {
        let userID = UserManager.shareUserManager.getUserID()
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "Index":self.pageIndex,
                     "Count":10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay] as [String : Any]
        FPHttpManager().manager.post(ProductListInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            let proArray = json["Data"]["IndexProductList"]
            for i in 0..<proArray.count {
                var tempProduct = BrowseActivityInfo(jsonData: proArray[i])
                let x = arc4random() % 10
                tempProduct.randomNumber = Int(x)
                self.productList.append(tempProduct)
            }
            self.collectionView.reloadData()
            if self.collectionView.mj_header != nil {
                self.collectionView.mj_header.endRefreshing()
            }
            if self.collectionView.mj_footer != nil {
                self.collectionView.mj_footer.endRefreshing()
            }
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func collectionViewInit() {
        if let browseProductInfo = self.browseProductInfo, let browseCountDownInfo = self.browseCountDownInfo{
            browseHeaderView.headerImgUrl = browseProductInfo.MainImgLink
            browseHeaderView.bannerProductArr = self.headerProductArr
            browseHeaderView.limitProductInfo = browseCountDownInfo
            browseHeaderView.setupCategoryView(categoryArr: self.categoryArr)
            browseHeaderView.setupRecommendView()
            let maxY = browseHeaderView.getHeaderHeight()
            browseHeaderView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: maxY)
            let layout = ZJFlexibleLayout(delegate: self)
            layout.collectionHeaderView = browseHeaderView
            collectionView.collectionViewLayout = layout
        }
        //添加刷新控件
        self.addRefreshControl()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - 添加刷新控件
    func addRefreshControl() {
        //下拉刷新
        collectionView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.pageIndex = 1
            self.productList.removeAll()
//            self.headerProductArr.removeAll()
//            self.categoryArr.removeAll()
            self.requestBrowseDataFromServer()
        })
        
        //上拉加载更多
        collectionView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {
            self.pageIndex += 1
            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.requestBrowseDataFromServer()
        })
    }
}

// MARK: - 设置控制器UI
extension BrowseViewController {
    
    func setupNavigationItem() {
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        let labelForTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        labelForTitle.font = UIFont(name: kGothamRoundedBold, size: kSizeFrom750(x: 40))
        labelForTitle.center = titleView.center
        labelForTitle.text = "FAYPARK"
        labelForTitle.textAlignment = .center
        titleView.addSubview(labelForTitle)
        self.navigationItem.titleView = titleView
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        //设置navigationbar左item
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 41, height: 44)
        
        let leftBtn = UIButton(type: .custom)
        leftBtn.frame = CGRect(x: 0, y: 4, width: 41, height: 36)
        leftBtn.setImage(UIImage.init(named: "list_icon"), for: .normal)
        leftBtn.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -20, bottom: 0, right: 0)
        leftBtn.addTarget(self, action: #selector(leftItemClick), for: .touchUpInside)
        leftView.addSubview(leftBtn)
        let menuItem = UIBarButtonItem(customView: leftView)
       
        let rightItemView = BrowseCountryView(frame: CGRect(x: 0, y: 0, width: 70, height: 44))
        if let countryDic = UserDefaults.standard.value(forKey: kSelectedCountry) as? [String: String]{
            let countryImg = countryDic["CountryImg"]!
            rightItemView.leftImageView.image = UIImage(named: countryImg)
        } else {
            let locationInstance = LocationManager.shareInstance
            locationInstance.locationSuccessBlock = {(_ address: String) -> Void in
                var codeArr = [String]()
                for i in 0 ..< kAPPCountryInfoArr.count {
                    let countryDic = kAPPCountryInfoArr[i]
                    let countryCode = countryDic["CountryCode"]
                    codeArr.append(countryCode!)
                    if countryCode == address {
                        UserDefaults.standard.setValue(countryDic, forKey: kSelectedCountry)
                        UserDefaults.standard.synchronize()
                        let imageString = countryDic["CountryImg"]
                        rightItemView.leftImageView.image = UIImage(named: imageString!)
                        return
                    }
                }
                if !codeArr.contains(address) {
                    //默认设置美国
                    let defultDic = ["CountryName":"United States",
                                     "CountryCode": "US",
                                     "CountryLanguage":"en",
                                     "CountryPayment":"USD",
                                     "CountryImg": "browse_unitedstates_country"]
                    UserDefaults.standard.setValue(defultDic, forKey: kSelectedCountry)
                    UserDefaults.standard.synchronize()
                    rightItemView.leftImageView.image = UIImage(named: "browse_unitedstates_country")
                }
            }
            locationInstance.locationFailureBlock = { (_ error: String) -> Void in
                //默认设置美国
                let defultDic = ["CountryName":"United States",
                                 "CountryCode": "US",
                                 "CountryLanguage":"en",
                                 "CountryPayment":"USD",
                                 "CountryImg": "browse_unitedstates_country"]
                UserDefaults.standard.setValue(defultDic, forKey: kSelectedCountry)
                UserDefaults.standard.synchronize()
                rightItemView.leftImageView.image = UIImage(named: "browse_unitedstates_country")
            }
        }
        rightItemView.didTappedCountryViewBlock = { () -> Void in
            let countryVC = CountrySelectViewController()
            countryVC.didSelectedCountryBlock = { (_ selectedCountry: [String: String]) -> Void in
                let selectedLanguage = selectedCountry["CountryLanguage"]
                LanguageHelper.standardLanguager().currentLanguage = selectedLanguage!
                
                let mainVC = MainViewController()
                let window = UIApplication.shared.keyWindow
                window?.rootViewController = mainVC
                mainVC.selectedIndex = 0
                
            }
            self.present(countryVC, animated: true, completion: nil)
        }
        let rightNaviItem = UIBarButtonItem(customView: rightItemView)
        
        navigationItem.leftBarButtonItem = menuItem
        navigationItem.rightBarButtonItem = rightNaviItem
        
        //消除导航栏的黑线
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @objc func leftItemClick() {
        //点击弹出侧边栏,并加深右边遮罩
        showLeftView()
    }
    @objc func rightDoneButtonItemTapped() {
        let countryVC = CountrySelectViewController()
        countryVC.didSelectedCountryBlock = { (_ selectedCountry: [String: String]) -> Void in
           
            let selectedLanguage = selectedCountry["CountryLanguage"]
            LanguageHelper.standardLanguager().currentLanguage = selectedLanguage!
            
            let mainVC = MainViewController()
            let window = UIApplication.shared.keyWindow
            window?.rootViewController = mainVC
            mainVC.selectedIndex = 0
        }
        self.present(countryVC, animated: true, completion: nil)
    }
    //隐藏左侧边栏
    @objc func hideLeftView() {
        UIView.animate(withDuration: 0.3) {
            self.leftView.frame = CGRect(x: -UIScreen.main.bounds.width, y: 0, width: kSizeFrom750(x: 580), height: UIScreen.main.bounds.height)
            self.leftBackView.alpha = 0.0
        }
    }
    @objc func topDidImage() {
        self.hideLeftView()
        self.navigationController?.pushViewController(ProfileViewController(), animated: true)
    }
    //显示左侧View
    func showLeftView() {
        UIView.animate(withDuration: 0.5) {
            self.leftView.frame = CGRect(x: 0, y: 0, width: kSizeFrom750(x: 580), height: UIScreen.main.bounds.height)
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(self.topDidImage))
            
            if (UserDefaults.standard.object(forKey: "kUserName") as! String).count == 0{
                self.leftContentView.headBtn.isHidden = true
                self.leftContentView.userNameLabel.isHidden = true
                self.leftContentView.loginBtn.isHidden = false
                self.leftContentView.loginInfoLable.isHidden = false
                self.leftContentView.rightDetailImgView.isHidden = true
                self.leftContentView.loginBtn.addTarget(self, action: #selector(self.loginTouch), for: .touchUpInside)
                tap.isEnabled = false
            }else{
                self.leftContentView.loginBtn.isHidden = true
                self.leftContentView.loginInfoLable.isHidden = true

                self.leftContentView.headBtn.isHidden = false
                self.leftContentView.userNameLabel.isHidden = false
                self.leftContentView.tableView.tableHeaderView?.addGestureRecognizer(tap)
                if let firstName = UserDefaults.standard.object(forKey: "kFirstName") {
                    self.leftContentView.headBtn.setTitle(getFirstLetterFromString(aString: (firstName as! String)), for: .normal)
                }
                if let name = UserDefaults.standard.object(forKey: "kUserName") {
                    self.leftContentView.userNameLabel.text = (name as! String)
                }
                if UserDefaults.standard.object(forKey: "kUserImg") != nil {
                    self.leftContentView.updateHeadViewInfo()
                }
                
            }
            self.leftContentView.updateHeadViewInfo()
            self.leftBackView.alpha = 0.5
        }
    }
    @objc func loginTouch(){
        self.hideLeftView()
        self.present(NewLoginViewController(), animated: true) {}
    }
    func setupLeftMenuView() {
        let window = UIApplication.shared.keyWindow!
        //初始化leftView
        leftView = UIView(frame: CGRect(x: -UIScreen.main.bounds.width, y: 0, width: kSizeFrom750(x: 580), height: UIScreen.main.bounds.height))
        
        //初始化leftBackView
        leftBackView = UIView(frame: UIScreen.main.bounds)
        leftBackView.backgroundColor = .black
        leftBackView.alpha = 0.0
        window.addSubview(leftBackView)
        
        //添加leftBackView
        let rightGes = UITapGestureRecognizer.init(target: self, action: #selector(hideLeftView))
        leftBackView.addGestureRecognizer(rightGes)
        
        leftContentView = LeftContentView.init(frame: CGRect(x: 0, y: 0, width: leftView.bounds.width, height: leftView.bounds.height))
        leftContentView.hideOrJump = { (num:IndexPath) -> Void in
            let email =  UserDefaults.standard.object(forKey: "kUserName") as! String
            
            if num.row == 0 && num.section == 1 {
                let webVC = SupportWebController()
                webVC.urlString = AboutUSUrlString
                webVC.titleString = kAccountListAboutUsString
                self.navigationController?.pushViewController(webVC, animated: true)
            }else if num.row == 1 && num.section == 1 {
                UIApplication.shared.canOpenURL(URL(string: fayParkAppStoreString)!)
            }else if num.row == 1 && num.section == 0 {
                
                let webVC = SupportWebController()
                webVC.urlString = ShippingUrlString
                webVC.titleString = kAccountListShippingInfoString
                self.navigationController?.pushViewController(webVC, animated: true)
                
            }else if num.row == 2 && num.section == 0 {
                
                let webVC = SupportWebController()
                webVC.urlString = ReturnUrlString
                webVC.titleString = kAccountListRefundPolicyString
                self.navigationController?.pushViewController(webVC, animated: true)
            }else if num.row == 2 && num.section == 1 {
                let contactVC = ContactUsViewController()
                self.navigationController?.pushViewController(contactVC, animated: true)
            }else{
                if email.count == 0 {
                    self.present(NewLoginViewController(), animated: true, completion: nil)
                }else{
                    if num.row == 0 && num.section == 0 {
                        self.navigationController?.pushViewController(OrderHistoryViewController(), animated: true)
                    }else if num.row == 3 && num.section == 0 {
                        self.navigationController?.pushViewController(MyFavoritesViewController(), animated: true)
                    }else if num.row == 3 && num.section == 1 {
                        let settingVC = SettingViewController()
                        self.navigationController?.pushViewController(settingVC, animated: true)
                    }
                }
            }
            self.hideLeftView()
        }
        leftView.addSubview(leftContentView)
        window.addSubview(leftView)
    }
    
    func configEventMonitor() {
        configCommonEventMonitor(monitorString: kFayParkHomePage, type: 1)
    }
}
//MARK: - CollectionView 数据源
extension BrowseViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fayparkcell", for: indexPath) as! FayParkCollectionCell
        let pro = productList[indexPath.item]
        cell.imgUrl = URL.init(string: pro.MainImgLink)
        cell.configBrowseCollectionCell(productInfo: pro)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productVC = NewProductDetailViewController()
        productVC.requestProID = productList[indexPath.item].ProductID
        navigationController?.pushViewController(productVC, animated: true)
    }
}

//MARK: - 瀑布流代理
extension BrowseViewController: ZJFlexibleDataSource {
    //26
    func sizeOfItemAtIndexPath(at indexPath: IndexPath) -> CGSize {
        let pro = productList[indexPath.item]
        let itemWidth = (UIScreen.main.bounds.width - 10) / 2
        let proString = pro.Proportion
        var proFloat: CGFloat = 0
        if let doubleValue = Double(proString){
            proFloat = CGFloat(doubleValue)
        }
        let itemHeight = itemWidth / proFloat
        return CGSize(width: itemWidth, height: itemHeight + 80 + 62)
    }
    
    func heightOfAdditionalContent(at indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func numberOfCols(at section: Int) -> Int {
        return 2
    }
    //13
    func spaceOfCells(at section: Int) -> CGFloat{
        return 5
    }
    //13
    func sectionInsets(at section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 13, left: 5, bottom: 80, right: 5)
    }
}

//MARK: - BrowseHeaderViewDelegate
extension BrowseViewController: BrowseHeaderViewDelegate {
    // 顶部轮播图点击事件
    func browseHeaderImageViewTappedDelegate() {
        configCommonEventMonitor(monitorString: kHomeBannerTop, type: 2)
        let freeGiftVC = FreeGiftViewController()
        freeGiftVC.isHomePush = true
        self.navigationController?.pushViewController(freeGiftVC, animated: true)
//        let recommendVC = RecommendProductViewController()
//        recommendVC.recommendType = .RecommendSaleProduct
//        recommendVC.typeId = (self.browseProductInfo?.TypeId)!
//        self.navigationController?.pushViewController(recommendVC, animated: true)
    }
    // 顶部轮播图详情点击事件
    func browseHeaderImageDetailViewTappedDelegate(indexpath: IndexPath) {
        let productVC = NewProductDetailViewController()
        productVC.requestProID = self.headerProductArr[indexpath.row].ProductID
        navigationController?.pushViewController(productVC, animated: true)
    }
    // 中间限时商品点击事件
    func browseLimitImageViewTappedDelegate() {
        configCommonEventMonitor(monitorString: kHomeLimitBanner, type: 2)
        let recommendVC = RecommendProductViewController()
        recommendVC.recommendType = .RecommendLimitTimeProduct
        if let browseInfo = self.browseCountDownInfo {
            recommendVC.typeId = browseInfo.TypeId
        }
        self.navigationController?.pushViewController(recommendVC, animated: true)
    }
    // 底部品类点击事件
    func browseCategoryButtonTappedDelegate(sender: UIButton) {
        let new = NewCateDetailViewController()
        new.OneTypeID = self.categoryArr[sender.tag-100].TypeId
        new.cateType = .CateDetailHomeSaleProduct
        new.title = self.categoryArr[sender.tag-100].TypeName
        self.navigationController?.pushViewController(new, animated: true)
        
    }
}
