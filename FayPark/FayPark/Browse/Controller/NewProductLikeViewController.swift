//
//  NewProductLikeViewController.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SDCycleScrollView
import JTSImageViewController
import AFNetworking
import SwiftyJSON
import SDWebImage
import MBProgressHUD
import MJRefresh
import FBSDKCoreKit
import AppsFlyerLib

class NewProductLikeViewController: BaseViewController {
    var collectionView : UICollectionView!
//    商品列表
    var productList = Array<BrowseActivityInfo>()
    var groupEnter: DispatchGroup = DispatchGroup.init()
    var cateHederTitle = Array<JSON>.init()
    var tuochNumber:Int = 0
    
    var requestProID:String = ""
    var pageIndex:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = kPDAlsoLikeTitleString
        self.view.backgroundColor = UIColor.white
        self.addLoadView()
        self.addLodaDate(indx: self.pageIndex)
        self.setFootRefreshControl()
        self.setHederRefreshControl()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func addLoadView(){
        
        let layout = ZJFlexibleLayout(delegate: self )
        let collectionFrame = CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight)
        collectionView = UICollectionView(frame: collectionFrame, collectionViewLayout: layout)
        collectionView.showsVerticalScrollIndicator = false
        view.addSubview(collectionView)
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "FayParkCollectionCell", bundle: nil), forCellWithReuseIdentifier: "fayparkcell")
    }
    //MARK:___________下拉刷新______________
    func setHederRefreshControl() {
        self.collectionView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            //            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.productList = Array<BrowseActivityInfo>()
            self.addLodaDate(indx: 1)
            self.collectionView.mj_header.endRefreshing()
        })
    }
    //MARK:___________上拉加载______________
    func setFootRefreshControl() {
        self.collectionView.mj_footer = MJRefreshAutoFooter.init(refreshingBlock:{
            //            MBProgressHUD.showAdded(to: self.view, animated: true)
            self.pageIndex += 1
            self.addLodaDate(indx: self.pageIndex)
            self.collectionView.mj_footer.endRefreshing()
        })
    }
    func addLodaDate(indx:Int){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.groupEnter.enter()
        let userID = UserManager.shareUserManager.getUserID()
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "ProductID":requestProID,
                     "Index":self.pageIndex,
                     "Count":10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay] as [String : Any]
        FPHttpManager().manager.post(ProductWaterfallInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON(result as! Data)
            //print(json)
            MBProgressHUD.hide(for: self.view, animated: true)
            let proArray = json["Data"]["ProductDetailsList"]
            for i in 0..<proArray.count {
                var tempProduct = BrowseActivityInfo(jsonData: proArray[i])
                let x = arc4random() % 10
                tempProduct.randomNumber = Int(x)
                self.productList.append(tempProduct)
            }
            self.collectionView.reloadData()
            self.groupEnter.leave()
        }) { (task, error) in
            //print(error)
            self.groupEnter.leave()
        }
    }
    
}

//MARK: - CollectionView 数据源
extension NewProductLikeViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fayparkcell", for: indexPath) as! FayParkCollectionCell
        let pro = productList[indexPath.item]
        cell.imgUrl = URL.init(string: pro.MainImgLink)
        cell.configBrowseCollectionCell(productInfo: pro)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if UserDefaults.standard.object(forKey: "kUserID") != nil && UserDefaults.standard.object(forKey: "kUserID") as! Int != 0 {
            let productVC = NewProductDetailViewController()
            productVC.requestProID = productList[indexPath.item].ProductID
            navigationController?.pushViewController(productVC, animated: true)
            
//        } else {
//            let loginVC = UserLoginViewController()
//            let navi = FPNavigationController.init(rootViewController: loginVC)
//            //让present的navicontroller背景不会变成黑色
//            navi.modalPresentationStyle = .custom
//            self.present(navi, animated: true, completion: nil)
//        }
        
    }
}

//MARK: - 瀑布流代理
extension NewProductLikeViewController: ZJFlexibleDataSource {
    
    func sizeOfItemAtIndexPath(at indexPath: IndexPath) -> CGSize {
        let pro = productList[indexPath.item]
        let itemWidth = (UIScreen.main.bounds.width - 24) / 2
        //let itemHeight = (imageSize!.height/imageSize!.width * itemWidth)
        let proString = pro.Proportion
        var proFloat: CGFloat = 0
        if let doubleValue = Double(proString){
            proFloat = CGFloat(doubleValue)
        }
        let itemHeight = itemWidth / proFloat
        return CGSize(width: itemWidth, height: itemHeight + 80 + 62)
    }
    
    func heightOfAdditionalContent(at indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func numberOfCols(at section: Int) -> Int {
        return 2
    }
    
    func spaceOfCells(at section: Int) -> CGFloat{
        return 13
    }
    
    func sectionInsets(at section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 13, left: 13, bottom: 80, right: 13)
    }
}
