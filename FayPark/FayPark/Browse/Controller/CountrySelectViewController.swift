//
//  CountrySelectViewController.swift
//  FayPark
//
//  Created by faypark on 2018/8/1.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
class CountrySelectViewController: BaseViewController {
    var tableView: UITableView!
    let cellIdentifier = "CountryTableViewCell"
    typealias SelectedCountryBlock = ((_ selectedDic: [String: String]) -> Void)
    var didSelectedCountryBlock: SelectedCountryBlock?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        self.title = localized("kHomeCountrySelect")
        setupNavigationView()
        setupTableView()
    }
    func setupNavigationView() {
        
        let statusHeight = UIApplication.shared.statusBarFrame.height
        let naviView = UIView(frame: CGRect(x: 0, y: statusHeight, width: KScreenWidth, height: 44))
        view.addSubview(naviView)
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 43))
        titleLabel.text = localized("kHomeCountrySelect")
        titleLabel.font = UIFont(name: kMSReference, size: 17)
        titleLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        titleLabel.textAlignment = .center
        naviView.addSubview(titleLabel)
        
        let rightButton = UIButton(type: UIButtonType.custom)
        rightButton.setImage(UIImage(named: "cart_icon_close"), for: .normal)
        rightButton.titleLabel?.isHidden = true
        rightButton.contentHorizontalAlignment = .center
        let backW: CGFloat = kScreenWidth > 375.0 ? 50 : 44
        rightButton.frame = CGRect(x: KScreenWidth - 10 - backW, y: 0, width: backW, height: 43)
        rightButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        naviView.addSubview(rightButton)
        
        let lineView = UIView(frame: CGRect(x: 0, y: 43, width: KScreenWidth, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        naviView.addSubview(lineView)
    }
    func setupTableView() {
        let statusHeight = UIApplication.shared.statusBarFrame.height + 44
        tableView = UITableView(frame: CGRect(x: 0, y: statusHeight, width: KScreenWidth, height: kScreenHeight - 64), style: .plain)
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        tableView.rowHeight = 56
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        tableView.separatorStyle = .none
        tableView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
    }
    @objc func dismissButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension CountrySelectViewController: UITableViewDataSource,UITableViewDelegate {
    //MARK: - tableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return kAPPCountryInfoArr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CountryTableViewCell
        let countryDic = kAPPCountryInfoArr[indexPath.row]
        cell.countryLabel.text = countryDic["CountryName"]
        cell.leftImageView.image = UIImage(named: countryDic["CountryImg"]!)
        cell.detailImgView.isHidden = true
        cell.textLabel?.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        cell.textLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 35))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! CountryTableViewCell
        cell.backgroundColor = kRGBColorFromHex(rgbValue: 0xfff4f2)
        cell.detailImgView.isHidden = false
        let selectedDic = kAPPCountryInfoArr[indexPath.row]
        let code: String = selectedDic["CountryCode"]!
        configCommonEventMonitor(monitorString: "Home-\(code)", type: 2)
        if self.didSelectedCountryBlock != nil {
            self.didSelectedCountryBlock!(selectedDic)
        }
        //保存国家信息
        UserDefaults.standard.setValue(selectedDic, forKey: kSelectedCountry)
        //删除上次保存的freegift
        UserDefaults.standard.removeObject(forKey: "SelectFreeInfo")
        UserDefaults.standard.synchronize()
//        let countryCode = selectedDic["CountryCode"]!
//        let userID = UserManager.shareUserManager.getUserID()
//        let param = ["UserID":userID, "CountrySName":countryCode]
//        MBProgressHUD.showAdded(to: self.view, animated: true)
//        FPHttpManager().manager.post(SettingUserCountryInterface, parameters: param, progress: nil, success: { (task, result) in
//            MBProgressHUD.hide(for: self.view, animated: true)
//            let json = JSON(result as! Data)
//            print("SettingUserCountryInterface\(json)")
//            if json["Code"].stringValue == "200" {
//                if self.didSelectedCountryBlock != nil {
//                    self.didSelectedCountryBlock!(selectedDic)
//                }
//                UserDefaults.standard.setValue(selectedDic, forKey: kSelectedCountry)
//                UserDefaults.standard.synchronize()
//            } else {
//                showToast(json["Msg"].stringValue)
//            }
//            self.dismiss(animated: true, completion: nil)
//        }) { (task, error) in
//           MBProgressHUD.hide(for: self.view, animated: true)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
}
