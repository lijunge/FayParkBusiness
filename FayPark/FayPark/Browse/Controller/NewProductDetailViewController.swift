//
//  NewProductDetailViewController.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDCycleScrollView
import MJRefresh
import JTSImageViewController
import AFNetworking
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import AppsFlyerLib
import FBSDKShareKit
import PinterestSDK

class NewProductDetailViewController: BaseViewController {

    var requestProID: String = ""//商品详情ID
    var productDetailInfo: ProductDetailInfo!
    
    var productSkuID: String = "-1"//用户选择的商品型号的skuid
    var productSkuCount: Int = 1// 选择的sku数量
    var isShare: Int = 0         //商品是否已分享
    
    var blackView:UIView!
    var selectView:NewSelectProductView!
    var circleLabel:UILabel!
    var rigghtButton:UIButton!
    var viewSize:NewSizeTableView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "ProductDetailDisappera"), object: nil, userInfo: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.loadProductDetailFromServer()
        setNavigationBarItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func makeConstraints() {
        bottomView.mas_makeConstraints { (mark) in
            mark?.bottom.right().left().equalTo()(self.view)
            mark?.height.equalTo()(kSizeFrom750(x: 102))
        }
        productDetailView.mas_makeConstraints { (mark) in
            mark?.top.left().right().equalTo()(self.view)
            mark?.bottom.equalTo()(bottomView.mas_top)
        }
    }
    
    //MARK: - 设置导航栏右上角
    func setNavigationBarItem() {
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        let labelForTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        labelForTitle.font = UIFont(name: kGothamRoundedBold, size: kSizeFrom750(x: 40))
        labelForTitle.center = titleView.center
        labelForTitle.text = "FAYPARK"
        labelForTitle.textAlignment = .center
        titleView.addSubview(labelForTitle)
        self.navigationItem.titleView = titleView
        
        let rightView = UIView()
        rightView.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
        
        rigghtButton = UIButton(type: .custom)
        rigghtButton.frame = CGRect(x: 30, y: 10, width: 30, height: 30)
        rigghtButton.setImage(UIImage.init(named: "cart_icon"), for: .normal)
        rigghtButton.addTarget(self, action: #selector(rightCartClick), for: .touchUpInside)
        rightView.addSubview(rigghtButton)
        
        circleLabel = UILabel(frame: CGRect(x: 60-16, y: 10, width: 16, height: 16))
        circleLabel.font = UIFont.systemFont(ofSize: 10)
        circleLabel.textAlignment = .center
        circleLabel.textColor = UIColor.white
        circleLabel.layer.cornerRadius = 8
        circleLabel.backgroundColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        circleLabel.layer.masksToBounds = true
        if let carCount = UserManager.shareUserManager.carProductCount {
            if let number = Int(carCount) {
                if number != 0 {
                    circleLabel.text = String(number+1)
                } else {
                    circleLabel.text = "1"
                }
            }
        }
        rightView.addSubview(circleLabel)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightView)
    }
    @objc func rightCartClick(){
        let userID = UserManager.shareUserManager.getUserID()
        if userID.isEmpty {
            self.present(NewLoginViewController(), animated: true, completion: nil)
            return
        }
        //跳转到购物车
        let tabbarVC = parent?.parent as! UITabBarController
        tabbarVC.selectedIndex = 2
        //顶部菜单栏添加购物车监控
        if let tmpProductDetailInfo = productDetailInfo {
            self.configPageMonitor(monitorString: kDetailTopToCartEvent, staticsType: 2, productID: tmpProductDetailInfo.ProductID)
        }
        self.navigationController?.popViewController(animated: false)
    }
    
    lazy var bottomView: NewDetailcBottomView = {
        let tempBottomView = NewDetailcBottomView.init(frame: CGRect.init())
        tempBottomView.backgroundColor = kSpacingColor
        tempBottomView.cartBtn.addTarget(self, action: #selector(buyButtonClick), for: .touchUpInside)//添加购物车
        tempBottomView.likeBtn.addTarget(self, action: #selector(likeBtnClick), for: .touchUpInside)
        return tempBottomView
    }()
    
    lazy var productDetailView: NewProductDetailcView = {
        let tempProductView = NewProductDetailcView.init(frame: CGRect.init(), style: .grouped)
        tempProductView.selectInfoBlock(block: { (indx) in
            let newView = NewProductInfoViewController()
            newView.type = indx
            //查看更多评论监控
            self.configPageMonitor(monitorString: kDetailMoreReviewEvent, staticsType: 2, productID: self.productDetailInfo.ProductID)
            self.navigationController?.pushViewController(newView, animated: true)
        })
        tempProductView.selectLike = {(indx)in
            let newView = NewProductLikeViewController()
            newView.requestProID = self.requestProID
            //查看推荐商品监控
            self.configPageMonitor(monitorString: kDetailMoreLikeEvent, staticsType: 2, productID: self.productDetailInfo.ProductID)
            self.navigationController?.pushViewController(newView, animated: true)
        }
        tempProductView.selectViewMore = {()in
            let commentVC = CommentViewController()
            commentVC.productID = self.productDetailView.productDetailInfo.ProductID
            self.navigationController?.present(commentVC, animated: true, completion: nil)
        }
        return tempProductView
    }()
    
    
    //MARK:-------- 加入购物处，弹出选择框
    @objc func buyButtonClick(){
        self.configPageMonitor(monitorString: kDetailAddToCartEvent, staticsType: 2, productID: self.requestProID)
        let userID = UserManager.shareUserManager.getUserID()
        if userID.isEmpty {
            self.present(NewLoginViewController(), animated: true, completion: nil)
            return
        }
        blackView = UIView.init(frame: self.view.bounds)
        blackView.backgroundColor = UIColor.black
        blackView.alpha = 0.5
        self.view.window?.addSubview(blackView)
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(touch))
        blackView.addGestureRecognizer(tap)
        
        selectView = NewSelectProductView.init(frame: CGRect.init(x: 0, y:kScreenHeight-kSizeFrom750(x: 850) , width: KScreenWidth, height: kSizeFrom750(x: 850)), style: .grouped)
        if productDetailInfo != nil {
            selectView.setSkuProductItemList(data: productDetailInfo)
            let selectHeight = selectView.getSelectProductHeight() + kSizeFrom750(x: 50)
            selectView.frame = CGRect.init(x: 0, y:kScreenHeight-selectHeight , width: KScreenWidth, height: selectHeight)
        }
        selectView.selectColorAction { (data,number) in
            //SKU确认添加进入购物车监控
            self.configPageMonitor(monitorString: kDetailConfirmAddToCartEvent, staticsType: 2, productID: self.productDetailInfo.ProductID)
            self.requestShareAndCartFromServer(skuID: data.ProductSkuId!, productCount: number)
            self.selectView.removeFromSuperview()
            self.blackView.removeFromSuperview()
        }
        
        selectView.selectSizeAction {
            self.viewSize = NewSizeTableView.init(frame: CGRect.init(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight))
            self.viewSize.closeBtn.addTarget(self, action: #selector(self.touchRemove), for: .touchUpInside)
            self.view.window?.addSubview(self.viewSize)
        }
        
        selectView.selectCloseAction {
            self.selectView.removeFromSuperview()
            self.blackView.removeFromSuperview()
        }
        self.view.window?.addSubview(selectView)
        
    }
    @objc func touchRemove(){
        self.viewSize.removeFromSuperview()
    }
    @objc func touch() {
        selectView.removeFromSuperview()
        blackView.removeFromSuperview()
    }
    //MARK: - 请求商品详情数据(商品id)
    func loadProductDetailFromServer() {
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let userID = UserDefaults.standard.object(forKey: "kUserID")
        let param = ["UserID":userID, "ProductID":requestProID,"CountrySName": countryCode,"CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FPHttpManager().manager.post(ProductDetailInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("loadProductDetailFromServer\(json)")
            if json["Code"].stringValue == "200" {
                let prodetail = ProductDetailInfo.init(jsonData: json["Data"]["ProductDetailsList"][0])
                prodetail.IsTime = ProductDetailInfo.init(jsonData: json["Data"]).IsTime
                prodetail.StartTime = ProductDetailInfo.init(jsonData: json["Data"]).StartTime
                prodetail.EndTime  = ProductDetailInfo.init(jsonData: json["Data"]).EndTime
                prodetail.CommentCount = ProductDetailInfo.init(jsonData: json["Data"]).CommentCount
                prodetail.CommentAvg = ProductDetailInfo.init(jsonData: json["Data"]).CommentAvg
                prodetail.Number = ProductDetailInfo.init(jsonData: json["Data"]).Number
                prodetail.ComName = ProductDetailInfo.init(jsonData: json["Data"]).ComName
                self.view.addSubview(self.productDetailView)
                self.view.addSubview(self.bottomView)
                self.makeConstraints()
                self.productDetailView.productDetailInfo = prodetail
                self.productDetailView.productCommentInfo = ProductCommentList(json: json["Data"])//评论数组
                if prodetail.IsColl == "1" {
                    self.bottomView.likeBtn.isSelected = true
                }
                self.productDetailInfo = prodetail
                self.productDetailView.reloadData()
                //页面监控 和 FSB页面监控
                self.configPageMonitor(monitorString: kDetailPage, staticsType: 2, productID: prodetail.ProductID)
                self.configPageFBKContentMonitor(productID: prodetail.ProductID, price: prodetail.SellingPrice)
            }else{
                showToast(json["Msg"].stringValue)
            }
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }

    //MARK: - 点击收藏
    @objc func likeBtnClick(sender:UIButton){
        let email =  UserDefaults.standard.object(forKey: "kUserName") as! String
        if email.count == 0 {
            self.present(NewLoginViewController(), animated: true, completion: nil)
            return
        }
        sender.isSelected = !sender.isSelected
        var likeCount = 0
        if sender.isSelected {
            //添加收藏
            if self.productDetailInfo.IsColl == "0"{
                likeCount = productDetailInfo.CollCount + 1
                self.productDetailInfo.IsColl = "1"
                self.productDetailInfo.CollCount = likeCount
            }
        } else {
            //取消收藏
            if self.productDetailInfo.IsColl == "1" {
                likeCount = productDetailInfo.CollCount - 1
                self.productDetailInfo.IsColl = "0"
                self.productDetailInfo.CollCount = likeCount
            }
        }
        for cell in self.productDetailView.subviews{//便利出label赋值 收藏数量
            if cell.tag == 10010 {
                for view in (cell as! NewProductDetailcCell).subviews {
                    if view.tag == 10086 {
                        (view as! UILabel).text = "\(likeCount) \(kProductDetailLikeString)"
                    }
                }
            }
        }
        //点击收藏监控
        self.configPageMonitor(monitorString: kDetailCollectionEvent, staticsType: 2, productID: productDetailInfo.ProductID)
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.mode = .indeterminate
        let userID = UserDefaults.standard.object(forKey: "kUserID")
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID, "ProductID":requestProID,"CountrySName": countryCode,"CountryCurrency": countryPay]
        FPHttpManager().manager.post(ProductCollectionInterface, parameters: param, progress: nil, success: { (task, result) in //商品收藏接口
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            if json["Code"].stringValue != "200"{
                showToast(json["Msg"].stringValue)
            }
            //收藏成功的监听
            self.configCollectMonitor(monitorString: kAddToWishlistSuccess, productID: self.productDetailInfo.ProductID, staticsType: 2,price: self.productDetailInfo.SellingPrice)
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    //MARK:-------- 分享
    var dict = Dictionary<AnyHashable, Any>()
    @objc func shareBtnClick(){

        //服务器分享,更改商品是否分享状态
        configPageMonitor(monitorString: kDetailShareEvent, staticsType: 2, productID: productDetailInfo.ProductID)
        
        let textString = "测试分享"
        let imageToShare = UIImage(named: "like_icon")
        let urlToShare = URL(string: "https://www.baidu.com")
        let activityItems = [textString,imageToShare,urlToShare] as [Any]
        
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityType.print, UIActivityType.copyToPasteboard,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll]
        self.present(activityVC, animated: true) {
            //activityVC.completionWithItemsHandler =

        }
//        //第三方分享
//        UMSocialUIManager.showShareMenuViewInWindow { (platformType, dict) in
//            self.shareWebPageToPlatformType(platformType: platformType)
//        }
    }
    
    // MARK: - 请求服务器分享接口
    func requestServerShare() {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID, "ProductID":productDetailInfo.ProductID,"CountrySName": countryCode,"CountryCurrency": countryPay]
        FPHttpManager().manager.post(ProductShareInterface, parameters: param, progress: nil, success: { (task, result) in
            //右上角分享后,解决重复弹出分享框
            self.isShare = 1
        }) { (task, error) in
            
        }
    }
    //分享行为
    func shareWebPageToPlatformType(platformType: UMSocialPlatformType) {
        
        /*
         let imgStr = "https://mobile.umeng.com/images/pic/home/social/img-1.png"
         
         let webObj = UMShareWebpageObject.shareObject(withTitle: "faypark", descr: "faypark-earing-necklace-bracelet", thumImage: imgStr)
         messageObj.shareObject = webObj
         
         UMSocialManager.default().share(to: platformType, messageObject: messageObj, currentViewController: self) { (data, error) in
         //let resp: UMSocialShareResponse = data as! UMSocialShareResponse
         //print(resp.message)
         //print(resp.originalResponse as! String)
         print(data)
         }
         
         
         */
        
        if platformType == .pinterest {
            /*
             let imageURL = URL.init(string: (self.productDetailInfo.ImgLinksList.first?.Img)!)
             let linURL = URL.init(string: "https://www.faypark.com/advertising/index.html?MediumSource=facebook-Share&id=\(productDetailInfo.ProductID!)")
             
             PDKPin.pin(withImageURL: imageURL, link: linURL, suggestedBoardName: "faypark001", note: "soeimnefi", withSuccess: {
             print("sharesuccesspin")
             }) { (error) in
             print(error)
             }
             
             return
             */
            let pinObj = UMSocialMessageObject.init()
            pinObj.title = self.productDetailInfo.ProductShareURL
            let shareobj = UMShareImageObject.init()
            shareobj.shareImage = self.productDetailInfo.ImgLinksList.first?.Img
            pinObj.shareObject = shareobj
            
            UMSocialManager.default().share(to: .pinterest, messageObject: pinObj, currentViewController: self) { (response, error) in
                if error != nil {
                    print("error\(String(describing: error))")
                } else {
                    print(" pinterest share succedd")
                }
            }
            //服务器分享,更改商品是否分享状态
            self.requestServerShare()
            //直接分享,选择好型号后分享
            if self.productSkuID != "-1" {
                self.requestShareAndCartFromServer(skuID: self.productSkuID, productCount: self.productSkuCount)
                //self.updateCartButtonNum()
            }
            self.configPageMonitor(monitorString: kPinterestShareEvent, staticsType: 2, productID: self.productDetailInfo.ProductID)
        } else if platformType == .facebook {
            
            let shareContent = FBSDKShareLinkContent.init()
            shareContent.contentTitle = "faypark"
            shareContent.contentDescription = "accerrosics"
            shareContent.contentURL = URL.init(string: "https://www.faypark.com/advertising/index.html?MediumSource=facebook-Share&id=\(productDetailInfo.ProductID)")
            shareContent.imageURL = URL.init(string: (productDetailInfo.ImgLinksList.first?.Img!)!)
            
            let shareDialog = FBSDKShareDialog.init()
            shareDialog.delegate = self
            shareDialog.mode = .automatic
            shareDialog.shareContent = shareContent
            shareDialog.show()
            self.configPageMonitor(monitorString: kFaceBookShareEvent, staticsType: 2, productID: self.productDetailInfo.ProductID)
            /*
             return
             
             let messageObj = UMSocialMessageObject.init()
             
             let shareString = "https://www.faypark.com/advertising/index.html?MediumSource=facebook-Share&id=\(productDetailInfo.ProductID!)"
             //print(shareString)
             messageObj.text = "\(self.productDetailInfo.ProductTitle!)" + "\n" + "\(shareString)"
             
             let shareImageObj = UMShareImageObject.init()
             shareImageObj.shareImage = productDetailInfo.ImgLinksList.first?.Img!
             messageObj.shareObject = shareImageObj
             
             UMSocialManager.default().share(to: platformType, messageObject: messageObj, currentViewController: self) { (response, error) in
             if error != nil {
             //print("error======%@",error)
             } else {
             //print("share succedd")
             let sharehud = MBProgressHUD.showAdded(to: self.view, animated: true)
             sharehud.mode = .text
             sharehud.label.text = "Share Success"
             sharehud.hide(animated: true, afterDelay: 1.5)
             //服务器分享,更改商品是否分享状态
             self.requestServerShare()
             //分享成功,是否继续购买还是前往购物车
             //直接分享,选择好型号后分享
             if self.productSkuID != -1 {
             self.requestShareAndCartFromServer(skuID: self.productSkuID, productCount: self.productSkuCount)
             self.updateCartButtonNum()
             }
             
             }
             }
             
             */
            /*
             let imageView = UIImageView.init()
             imageView.sd_setImage(with: URL.init(string: (productDetailInfo.ImgLinksList.first?.Img)!), completed: { (image, error, caheType, url) in
             let pinObj = UMSocialMessageObject.init()
             //pinObj.text = self.productDetailInfo.ProductShareURL
             
             let shareobj = UMShareWebpageObject.init()
             shareobj.title = "sdoejife"
             shareobj.descr = "fauelrakr"
             shareobj.thumbImage = UIImage.init(named: "freegift")
             shareobj.webpageUrl = self.productDetailInfo.ProductShareURL!
             pinObj.shareObject = shareobj
             UMSocialManager.default().share(to: platformType, messageObject: pinObj, currentViewController: self) { (response, error) in
             if error != nil {
             print("error======%@",error)
             } else {
             //print("share succedd")
             //服务器分享,更改商品是否分享状态
             //已分享改商品,应提示用户
             
             
             self.requestServerShare()
             
             //直接分享,选择好型号后分享
             if self.productSkuID != -1 {
             self.requestShareAndCartFromServer(skuID: self.productSkuID, productCount: self.productSkuCount)
             self.updateCartButtonNum()
             }
             }
             }
             
             
             })
             */
            
        } else if platformType == .instagram {
            //instagram
            let imageView = UIImageView.init()
            imageView.sd_setImage(with: URL.init(string: (productDetailInfo.ImgLinksList.first?.Img)!), completed: { (image, error, caheType, url) in
                let pinObj = UMSocialMessageObject.init()
                let shareobj = UMShareImageObject.init()
                
                shareobj.shareImage = image!
                
                pinObj.shareObject = shareobj
                
                UMSocialManager.default().share(to: platformType, messageObject: pinObj, currentViewController: self) { (response, error) in
                    if error != nil {
                        
                    } else {
                        //服务器分享,更改商品是否分享状态
                        self.requestServerShare()
                        
                        //直接分享,选择好型号后分享
                        if self.productSkuID != "-1" {
                            self.requestShareAndCartFromServer(skuID: self.productSkuID, productCount: self.productSkuCount)
                            //self.updateCartButtonNum()
                        }
                        self.configPageMonitor(monitorString: kInstagramShareEvent, staticsType: 2, productID: self.productDetailInfo.ProductID)
                    }
                }
            })
            
            
        } else if platformType == .googlePlus {
            //googleplus
            let urlComponents = NSURLComponents.init(string: "https://plus.google.com/share")
            //落地页:https://www.faypark.com/advertising/index.html
            let googleStr = "https://www.faypark.com/advertising/index.html"
            //"https://www.faypark.com/advertising/index.html?MediumSource=google-Share&id=\(productDetailInfo.ProductID!)"
            
            urlComponents?.queryItems = [NSURLQueryItem.init(name: "url", value: googleStr) as URLQueryItem]
            
            let url = urlComponents?.url
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url!)
            }
            //服务器分享,更改商品是否分享状态
            self.requestServerShare()
            self.configPageMonitor(monitorString: kGooglePlusShareEvent, staticsType: 2, productID: self.productDetailInfo.ProductID)
            //直接分享,选择好型号后分享
            if self.productSkuID != "-1" {
//                self.requestShareAndCartFromServer(skuID: self.productSkuID, productCount: self.productSkuCount)
//                self.updateCartButtonNum()
            }
        }
    }
    
    func addAnimation() {
        let scaleBigAnimation = createAnimation(keyPath: "transform.scale", fromValue: 1.0, toValue: 1.3)
        self.rigghtButton.layer.add(scaleBigAnimation, forKey: "scale")
        self.circleLabel.layer.add(scaleBigAnimation, forKey: "scale")
    }
    
    func createAnimation(keyPath: String,fromValue: CGFloat,toValue: CGFloat) -> CABasicAnimation {
        let animation = CABasicAnimation.init(keyPath: keyPath)
        animation.fromValue = fromValue
        animation.toValue = toValue
        animation.duration = 1
        animation.autoreverses = true
        animation.isRemovedOnCompletion = true
        animation.repeatCount = 1
        animation.fillMode = kCAFillModeForwards
        animation.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionLinear)
        return animation
    }
    
    // MARK: - 分享并添加进购物车
    func requestShareAndCartFromServer(skuID: String, productCount: Int) {
        
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID, "ProductID":requestProID, "ProductSKUID":skuID, "ProductSKUCount":productCount,"CountrySName":countryCode,"CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FPHttpManager().manager.post(AddCartInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            self.isShare = json["Data"]["IsShare"].intValue
            let tabbarVC = UIApplication.shared.keyWindow?.rootViewController
            if json["Data"]["CarProductCount"] == 0 {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "1"
                self.circleLabel.text = "1"
            } else {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "\(json["Data"]["CarProductCount"])"
                self.circleLabel.text = "\(json["Data"]["CarProductCount"].intValue + 1)"
            }
            UserManager.shareUserManager.carProductCount = json["Data"]["CarProductCount"].stringValue
            self.addAnimation()
            //将skuid置为-1,防止点击分享按钮直接添加进购物车
            self.productSkuID = "-1"
            self.configAddCartSuccessMonitor(monitorString: kAddToCartSuccess, productID: self.requestProID, staticsType: 3, price: self.productDetailInfo.SellingPrice)
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    //产品详情页面增加这个监测 在获取到数据后监控
    //页面监控 收藏（Detail-Collection） 分享（Detail-Share） 购买（Detail-AddToCart）确认（Detail-Confirm-AddToCar）
    func configPageMonitor(monitorString: String,staticsType: Int,productID: String) {
        let userID = UserManager.shareUserManager.getUserID()
        FBSDKAppEvents.logEvent(monitorString)
        AppsFlyerTracker.shared().trackEvent(monitorString, withValues: nil)
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: String(staticsType), name: monitorString, productID: productID)
    }
    //产品详情页面增加这个监测
    func configPageFBKContentMonitor(productID: String,price: Double) {
        let priceString = decimalNumberWithDouble(convertValue: price)
        let currencyString = getCurrentPaymentCodeString()
        FBSDKAppEvents.logEvent(FBSDKAppEventNameViewedContent, parameters: [FBSDKAppEventParameterNameContentID : productID,FBSDKAppEventParameterNameContentType : "product",FBSDKAppEventParameterNameCurrency: currencyString,"price": priceString])
    }
    //收藏商品监测 1后台监控AddToWishlist-Success 2FSB监控FBSDKAppEventNameAddedToWishlist 3APPFlayer AddToWishlist-Success
    func configCollectMonitor(monitorString: String,productID: String,staticsType: Int,price: Double) {
        let userID = UserManager.shareUserManager.getUserID()
        let priceString = decimalNumberWithDouble(convertValue: price)
        let currencyString = getCurrentPaymentCodeString()
        AppsFlyerTracker.shared().trackEvent(AFEventAddToWishlist, withValues: [AFEventParamPrice:priceString, AFEventParamCurrency: currencyString, AFEventParamContentType: "product", AFEventParamContentId:productID, AFEventContent: ""])
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: String(staticsType), name: monitorString, productID: productID)
        FBSDKAppEvents.logEvent(FBSDKAppEventNameAddedToWishlist, parameters: [FBSDKAppEventParameterNameContentID : productID,FBSDKAppEventParameterNameContentType : "product",FBSDKAppEventParameterNameCurrency: currencyString,"price": priceString])
    }
    //添加购物车成功事件监测 1 后台监控AddToCart-Success 2 FBSDKAppEventNameAddedToCart 3 AppFlyer AddToCart-Success
    func configAddCartSuccessMonitor(monitorString: String,productID: String,staticsType: Int,price: Double) {
        let userID = UserManager.shareUserManager.getUserID()
        let priceString = decimalNumberWithDouble(convertValue: price)
        let currencyString = getCurrentPaymentCodeString()
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: String(staticsType), name: monitorString, productID: productID)
        FBSDKAppEvents.logEvent(FBSDKAppEventNameAddedToCart, parameters: [FBSDKAppEventParameterNameContentID : productID,FBSDKAppEventParameterNameContentType : "product",FBSDKAppEventParameterNameCurrency: currencyString,"price": priceString])
        AppsFlyerTracker.shared().trackEvent(AFEventAddToCart, withValues: [AFEventParamPrice:priceString, AFEventParamCurrency: currencyString, AFEventParamContentType: "product", AFEventParamContentId:productID, AFEventContent: ""])
    }
}

extension NewProductDetailViewController: FBSDKSharingDelegate{
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        let sharehud = MBProgressHUD.showAdded(to: self.view, animated: true)
        sharehud.mode = .text
        sharehud.label.text = "Share Success"
        sharehud.hide(animated: true, afterDelay: 1.5)
        //服务器分享,更改商品是否分享状态
        self.requestServerShare()
        //分享成功,是否继续购买还是前往购物车
        //直接分享,选择好型号后分享
        if self.productSkuID != "-1" {
            self.requestShareAndCartFromServer(skuID: self.productSkuID, productCount: self.productSkuCount)
            //self.updateCartButtonNum()
        }
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        
    }
}
