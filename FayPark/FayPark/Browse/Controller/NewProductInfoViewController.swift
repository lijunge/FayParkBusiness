//
//  NewProductInfoViewController.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewProductInfoViewController: BaseViewController {

    var type : Int = 0
    
    var modelStr : String? {
        didSet {
            print(modelStr!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        switch type {
        case 2:
            view.addSubview(self.shippingView)
            self.title = kPDShippingInfoString
        case 3:
            view.addSubview(self.paymentView)
            self.title = kPDSecurityPaymentString
        case 4:
            view.addSubview(self.refoundView)
            self.title = kPDRefundPolicyString
        default: break
            
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        switch type {
        case 2:
            self.shippingView.mas_makeConstraints { (mark) in
                mark?.top.left().bottom().right().equalTo()(self.view)
            }
        case 3:
            self.paymentView.mas_makeConstraints { (mark) in
                mark?.top.left().bottom().right().equalTo()(self.view)
            }
        case 4:
            self.refoundView.mas_makeConstraints { (mark) in
                mark?.top.left().bottom().right().equalTo()(self.view)
            }
        default: break
        }
    }
    
    lazy var paymentView : PaymentView = {
        let tmpPayV = PaymentView.init()
        return tmpPayV
    }()
    
    lazy var shippingView: ShippingView = {
        let tmpShipV = ShippingView.init()
        return tmpShipV
    }()
    
    lazy var refoundView: RefoundView = {
        let tmpRefoundV = RefoundView.init()
        return tmpRefoundV
    }()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
