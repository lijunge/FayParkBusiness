//
//  RecommendProductViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/6.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import MJRefresh
import AFNetworking
import SwiftyJSON
import FBSDKCoreKit
import AppsFlyerLib

enum RecommendProductType {
    case RecommendSaleProduct
    case RecommendLimitTimeProduct
    case RecommendDefultProduct
}

class RecommendProductViewController: BaseViewController {

    var collectionView : UICollectionView!
    var headImgView: UIImageView!
    var limitTimeView: UIView!
    var limitTimeLabel: UILabel!

    //请求商品分类页数
    var pageIndex = 1
    var typeId = ""
    var circleLabel:UILabel!
    var recommendType: RecommendProductType!
    var recommendProductInfo: RecommendProductCountDownInfo!
    var recommendProuctArr = Array<BrowseActivityInfo>()
    var currentIdentifier = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setupSubViews()
    }
    func configEventMonitor(activityName: String) {
        configCommonEventMonitor(monitorString: activityName, type: 1)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    //MARK:- setupUI
    func setupSubViews() {
        setNavigationBarItem()
        switch recommendType {
        case .RecommendSaleProduct:
            //折扣商品 1导航栏title 2隐藏倒计时view 停止计时器
            title = ""
            setupHeaderImgView()
            if (limitTimeView) != nil {
                limitTimeView!.isHidden = true
             }
        case .RecommendLimitTimeProduct:
            //限时商品
            setupLimitTimeView()
            title = kRecommendTitleString
            if (headImgView) != nil {
                headImgView.isHidden = true
            }
         default:
            print("default")
        }
        setupCollectionView()
        requestRecommendProductList()
    }
    
    func setNavigationBarItem() {
        let rightView = UIView()
        rightView.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
        
        let rigghtButton = UIButton(type: .custom)
        rigghtButton.frame = CGRect(x: 30, y: 10, width: 30, height: 30)
        rigghtButton.setImage(UIImage.init(named: "cart_icon"), for: .normal)
        rigghtButton.addTarget(self, action: #selector(rightCartClick), for: .touchUpInside)
        rightView.addSubview(rigghtButton)
        
        circleLabel = UILabel(frame: CGRect(x: 60-16, y: 10, width: 16, height: 16))

        circleLabel.font = UIFont.systemFont(ofSize: 10)
        circleLabel.textAlignment = .center
        circleLabel.textColor = UIColor.white
        circleLabel.layer.cornerRadius = 8
        circleLabel.layer.masksToBounds = true
        circleLabel.backgroundColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        if let carCount = UserManager.shareUserManager.carProductCount {
            if let number = Int(carCount) {
                if number != 0 {
                    circleLabel.text = String(number+1)
                } else {
                    circleLabel.text = "1"
                }
            }
        }
        rightView.addSubview(circleLabel)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightView)
    }
    
    func setupHeaderImgView() {
        
        headImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: kSizeFrom750(x: 210)))
        headImgView.backgroundColor = UIColor.white
        view.addSubview(headImgView)
    }
    
    func setupLimitTimeView() {

        limitTimeView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: kSizeFrom750(x: 95)))
        
        let topLineView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: kSizeFrom750(x: 1)))
        topLineView.backgroundColor = kSpacingColor
        limitTimeView.addSubview(topLineView)
        
        let bottomLineView = UIView(frame: CGRect(x: 0, y: kSizeFrom750(x: 93), width: view.frame.size.width, height: kSizeFrom750(x: 1)))
        bottomLineView.backgroundColor = kSpacingColor
        limitTimeView.addSubview(bottomLineView)
        
        limitTimeLabel = UILabel(frame: CGRect(x: 0, y: kSizeFrom750(x: 2), width: view.frame.size.width, height: kSizeFrom750(x: 95)))
        limitTimeLabel.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        limitTimeLabel.font = UIFont.init(name: kMSReference, size: kSizeFrom750(x: 25))
        limitTimeLabel.textAlignment = .center
       
        limitTimeView.addSubview(limitTimeLabel)
        
        view.addSubview(limitTimeView)
    }
    
    func setupCollectionView() {
        let layout = ZJFlexibleLayout(delegate: self)
        switch recommendType {
        case .RecommendSaleProduct:
            layout.collectionHeaderView = headImgView
            break
        case .RecommendLimitTimeProduct:
            layout.collectionHeaderView = limitTimeView
            break
        default:
            print("default")
        }
        
        let collectionFrame = CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight)
        collectionView = UICollectionView(frame: collectionFrame, collectionViewLayout: layout)
        collectionView.showsVerticalScrollIndicator = false
        view.addSubview(collectionView)
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "FayParkCollectionCell", bundle: nil), forCellWithReuseIdentifier: "fayparkcell")
        addRefreshControl()
    }
    
    //MARK: - Private Method
    @objc func rightCartClick() {
        //跳转到购物车
        let email =  UserDefaults.standard.object(forKey: "kUserName") as! String
        if email.count == 0 {
            self.present(NewLoginViewController(), animated: true, completion: nil)
            return
        }
        let tabbarVC = parent?.parent as! UITabBarController
        tabbarVC.selectedIndex = 2
        self.navigationController?.popViewController(animated: false)
    }
    
    private func changeRecommendProductType(productType: RecommendProductType) {
        print("changeRecommendProductType")
    }

    func requestRecommendProductList() {
        /*******获取限时商品***********/
        MBProgressHUD.showAdded(to: self.view, animated: true)
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID, "Index":pageIndex, "Count":10, "TypeId": typeId,"CountrySName": countryCode,"CountryCurrency": countryPay]
        FPHttpManager().manager.post(BrowseRecommendProductListInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON(result as! Data)
            if json["Code"].stringValue == "200"{
                self.recommendProductInfo = RecommendProductCountDownInfo(jsonData: json["Data"])
                if self.pageIndex == 1 {
                    self.setupLimitTimeStamp(recommendProductInfo: self.recommendProductInfo)
                    if self.recommendType == RecommendProductType.RecommendSaleProduct {
                        self.configEventMonitor(activityName: self.recommendProductInfo.ActivityName)
                    }
                }
                let recommendProuctArr = self.recommendProductInfo.BrowseActivityList
                for i in 0..<recommendProuctArr.count {
                    let x = arc4random() % 10
                    var pr = recommendProuctArr[i]
                    pr.randomNumber = Int(x)
                    self.recommendProuctArr.append(pr)
                }
                MBProgressHUD.hide(for: self.view, animated: true)
                self.collectionView.reloadData()
                //加载完数据结束刷新,在afn的网络回调完成里使用
                self.collectionView.mj_footer.endRefreshing()
                self.collectionView.mj_header.endRefreshing()
            }else{
                showToast(json["Msg"].stringValue)
            }
        }) { (task, error) in
        }
    }
    func addRefreshControl() {
        //下拉刷新
        self.collectionView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.pageIndex = 1
            self.recommendProuctArr.removeAll()
            self.requestRecommendProductList()
        })
        
        //上拉加载更多
        self.collectionView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {
            self.pageIndex += 1
            self.requestRecommendProductList()
        })
    }
    
    func setupLimitTimeStamp(recommendProductInfo: RecommendProductCountDownInfo) {
        self.recommendProductInfo = recommendProductInfo
        switch recommendType {
        case .RecommendSaleProduct:
            headImgView.setImageWith(URL.init(string: recommendProductInfo.MainImgLink)!, placeholderImage: UIImage.init())
            break
        case .RecommendLimitTimeProduct:
            
            if Int(recommendProductInfo.Number)! < 0 {
                // 活动暂未开始或者结束
                limitTimeLabel.attributedText = changeTextChange(regex: "\\d+", text: String(format: "%@  %@ : %@ : %@",kRecommendFinishInString, "00","00","00"), color: kRGBColorFromHex(rgbValue: 0xe9668f),font: UIFont(name: kMSReference, size: kSizeFrom750(x: 28))!)
            } else {
                updateRandomCountDownManager()
            }
            break
        default:
            print("default")
        }

    }
    func updateRandomCountDownManager() {
        //有倒计时商品
        let countDownInter = TimeInterval(recommendProductInfo.Number)
        currentIdentifier = "RecommentCountdownIdentifier"
        TimeCountDownManager.manager.scheduledCountDownWith(currentIdentifier, timeInteval: countDownInter!, countingDown: { (timeInterval) in
            var hourTime:String
            var minuteTime:String
            var minute:String
            let time = Int(timeInterval)
            
            if time / 3600  < 10 {
                hourTime = String(format: "0%ld", time / 3600)
            }else{
                hourTime = String(format: "%ld", time / 3600)
            }
            if time / 60 % 60 < 10 {
                minuteTime = String(format: "0%ld", time / 60 % 60)
            } else {
                minuteTime = String(format: "%ld", time / 60 % 60)
            }
            if time % 60 < 10 {
                minute = String(format: "0%ld", time % 60)
            } else {
                minute = String(format: "%ld", time % 60)
            }
            
            self.limitTimeLabel.attributedText = self.changeTextChange(regex: "\\d+", text: String(format: "%@  %@ : %@ : %@",kRecommendFinishInString,hourTime,minuteTime,minute), color: kRGBColorFromHex(rgbValue: 0xe9668f),font: UIFont(name: kMSReference, size: kSizeFrom750(x: 28))!)
        }) { (timeInterval) in
            //倒计时时间到的话，需要更新倒计时商品的价格
            self.limitTimeLabel.attributedText = self.changeTextChange(regex: "\\d+", text: String(format: "%@  %@ : %@ : %@",kRecommendFinishInString, "00","00","00"), color: kRGBColorFromHex(rgbValue: 0xe9668f),font: UIFont(name: kMSReference, size: kSizeFrom750(x: 28))!)
            TimeCountDownManager.manager.cancelTask(self.currentIdentifier)
        }
    }
    
    //根据正则表达式改变文字颜色
    func changeTextChange(regex: String, text: String, color: UIColor,font: UIFont) -> NSMutableAttributedString {
        let attributeString = NSMutableAttributedString(string: text)
        do {
            let regexExpression = try NSRegularExpression(pattern: regex, options: NSRegularExpression.Options())
            let result = regexExpression.matches(in: text, options: NSRegularExpression.MatchingOptions(), range: NSMakeRange(0, text.count))
            for item in result {
                attributeString.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: item.range)
                attributeString.addAttribute(NSAttributedStringKey.font, value: font, range: item.range)
            }
        } catch {
            print("Failed with error: \(error)")
        }
        return attributeString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
extension RecommendProductViewController: ZJFlexibleDataSource {
    func sizeOfItemAtIndexPath(at indexPath: IndexPath) -> CGSize {
        let pro = recommendProuctArr[indexPath.item]
        let itemWidth = (UIScreen.main.bounds.width - 24) / 2
        let proString = pro.Proportion
        var proFloat: CGFloat = 0
        if let doubleValue = Double(proString){
            proFloat = CGFloat(doubleValue)
        }
        let itemHeight = itemWidth / proFloat
        return CGSize(width: itemWidth, height: itemHeight + 80 + 62)
    }
    
    func heightOfAdditionalContent(at indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func numberOfCols(at section: Int) -> Int {
        return 2
    }
    
    func spaceOfCells(at section: Int) -> CGFloat{
        return 13
    }
    
    func sectionInsets(at section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 13, left: 13, bottom: 80, right: 13)
    }
}
//MARK: -- UICollectionViewDelegate UICollectionViewDataSource
extension RecommendProductViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recommendProuctArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fayparkcell", for: indexPath) as! FayParkCollectionCell
        let pro = recommendProuctArr[indexPath.item]
        cell.configBrowseCollectionCell(productInfo: pro)
        cell.freeShippingImgView.isHidden = false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pro = recommendProuctArr[indexPath.item]
        let productVC = NewProductDetailViewController()
        productVC.requestProID = pro.ProductID
        navigationController?.pushViewController(productVC, animated: true)
        
    }
}

