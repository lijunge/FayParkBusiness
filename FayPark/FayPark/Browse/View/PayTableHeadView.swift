//
//  PayTableHeadView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class PayTableHeadView: UIView {
    
    let cardTitles : [String] = [kPDVisaString,kPDMasterString,kPDAmexString]
    let cardImages : [String] = ["card_visa_icon","card_mastercard_icon","card_americanexpress_icon"]//存储在本地对应的图片名字
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        makeConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        
        self.backgroundColor = UIColor.white
        self.addSubview(titleLb)
        self.addSubview(textLb)
        
        for i in 0 ... cardTitles.count - 1 {
            let imageV = UIImageView.init(image: UIImage.init(named: cardImages[i]))
            self.addSubview(imageV)
            
            let lb = UILabel.init()
            lb.text = cardTitles[i]
            lb.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
            lb.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 25))
            self.addSubview(lb)
            
            let indexX = i%3
            let imgW = kSizeFrom750(x: 81)
            let imgH = kSizeFrom750(x: 57)
            let startX = kSizeFrom750(x: 40 + CGFloat(indexX) * (81 + 58))
            imageV.mas_makeConstraints { (mark) in
                mark?.left.equalTo()(self)?.offset()(startX)
                mark?.size.mas_equalTo()(CGSize.init(width: imgW, height: imgH))
                mark?.top.equalTo()(self.textLb.mas_bottom)?.offset()(kSizeFrom750(x: 30))
            }
            
            lb.mas_makeConstraints { (mark) in
                mark?.top.equalTo()(imageV.mas_bottom)
                mark?.centerX.height().equalTo()(imageV)
            }
        }
    }
    func makeConstraint() {
        titleLb.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 40))
            mark?.left.equalTo()(kSizeFrom750(x: 34))
        }
        textLb.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(titleLb)?.offset()(kSizeFrom750(x: 8))
            mark?.top.equalTo()(titleLb)?.offset()(kSizeFrom750(x: 60))
        }
    }
    
    lazy var titleLb : UILabel = {
        let tmpTitleLb = UILabel.init()
        tmpTitleLb.text = kPDPaymentMethodString
        tmpTitleLb.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tmpTitleLb.font = kGothamBoldFontSize(size: kSizeFrom750(x: 28))
        return tmpTitleLb
    }()
    
    lazy var textLb : UILabel = {
        let tmptextLb = UILabel.init()
        tmptextLb.text = kPDCreditCardString
        tmptextLb.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tmptextLb.font = kHelveticaFontSize(size: kSizeFrom750(x: 28))
        return tmptextLb
    }()
    
    lazy var cardLb : UILabel = {
        let tmpcardLb = UILabel.init()
        tmpcardLb.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        tmpcardLb.font = kMSReferenceFontSize(size: kSizeFrom750(x: 25))
        return tmpcardLb
    }()
    
}
