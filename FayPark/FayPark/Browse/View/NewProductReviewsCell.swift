//
//  NewProductReviewsCell.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewProductReviewsCell: UITableViewCell {
    
    var rateView:FPStartRateView!

    class func ReviewsCell(tableView:UITableView)->NewProductReviewsCell {
        let cell = NewProductReviewsCell.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.addSubview(headerIMG)
        self.addSubview(nameLable)
        self.addSubview(reviewsInfoLable)
        self.addSubview(timeLable)
        self.addSubview(viewlines)
        makeConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func makeConstraint() {
        self.headerIMG.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 30))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
            mark?.width.height().equalTo()(kSizeFrom750(x: 80))
        }
        
        self.nameLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 30))
            mark?.left.equalTo()(self.headerIMG.mas_right)?.offset()(kSizeFrom750(x: 28))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 200))
            mark?.height.equalTo()(kSizeFrom750(x: 30))
        }
        self.nameLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.reviewsInfoLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.nameLable.mas_bottom)?.offset()(kSizeFrom750(x: 28))
            mark?.left.equalTo()(self.nameLable)
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 28))
        }
        self.reviewsInfoLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.timeLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.reviewsInfoLable.mas_bottom)?.offset()(kSizeFrom750(x: 30))
            mark?.left.right().equalTo()(self.reviewsInfoLable)
            mark?.height.equalTo()(kSizeFrom750(x: 30))
        }
        self.timeLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.viewlines.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.timeLable.mas_bottom)?.offset()(kSizeFrom750(x: 28))
            // mark?.bottom.equalTo()(self)
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 32))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 32))
            mark?.height.equalTo()(kSizeFrom750(x: 2))
        }
    }
   
    lazy var headerIMG: UIButton = {
        let tempHeaderIMG = UIButton.init()
        tempHeaderIMG.layer.cornerRadius = kSizeFrom750(x: 40)
        tempHeaderIMG.layer.masksToBounds = true
        return tempHeaderIMG
    }()
    
    lazy var nameLable: UILabel = {
        let tempName = UILabel.init()
        tempName.text = "Alice Wang"
        tempName.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 27))
        tempName.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        tempName.numberOfLines = 0
        return tempName
    }()
    
    lazy var reviewsInfoLable: UILabel = {
        let tempReviews = UILabel.init()
        tempReviews.textColor = kRGBColorFromHex(rgbValue: 0x444444)
        tempReviews.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 27))
        tempReviews.text = "A witty woman is a  treasure; a witty beauty is a power."
        tempReviews.numberOfLines = 0
        return tempReviews
    }()
    
    lazy var timeLable: UILabel = {
        let tempTime = UILabel.init()
        tempTime.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        tempTime.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 22))
        tempTime.text = "1 day ago"
        return tempTime
    }()

    lazy var viewlines: UIView = {
        let tempView = UIView.init()
        return tempView
    }()
    
    func getCommentCellHeight(comment: CommentInfo) -> CGFloat {
        let nameHeight = 2 * kSizeFrom750(x: 30)
        let detailW = KScreenWidth - 2 * kSizeFrom750(x: 28)
        let detailHeight = comment.CommentContent.getHeightWithConstrainedWidth(width: detailW, font: UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 27))!) + kSizeFrom750(x: 28)
        let timeHeight = 2 * kSizeFrom750(x: 30) + kSizeFrom750(x: 2) + kSizeFrom750(x: 28)
        return nameHeight + detailHeight + timeHeight + kSizeFrom750(x: 20)
    }
    func updateCommentCellData(comment: CommentInfo) {
        if comment.UserImg.isEmpty {
            let character = getFirstLetterFromString(aString: comment.UserName)
            var titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
            if character == "A" || character == "B" || character == "C" || character == "D" || character == "E" {
                titleColor = kRGBColorFromHex(rgbValue: 0xe9668f)
            } else if character == "F" || character == "G" || character == "H" || character == "I" || character == "J" {
                titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
            } else if character == "K" || character == "L" || character == "M" || character == "N" || character == "O" {
                titleColor = kRGBColorFromHex(rgbValue: 0x00acf0)
            } else if character == "P" || character == "Q" || character == "R" || character == "S" || character == "T" {
                titleColor = kRGBColorFromHex(rgbValue: 0xc45cac)
            } else {
                titleColor = kRGBColorFromHex(rgbValue: 0xf97f46)
            }
            headerIMG.layer.borderWidth = 0.5
            headerIMG.layer.borderColor = kRGBColorFromHex(rgbValue: 0xababab).cgColor
            headerIMG.backgroundColor = kRGBColorFromHex(rgbValue: 0xcdcdcd)
            headerIMG.setTitleColor(titleColor, for: .normal)
            headerIMG.setTitle(character, for: .normal)
            headerIMG.titleLabel?.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 50))
            
        } else {
            headerIMG.layer.borderWidth = 0.0
            headerIMG.backgroundColor = UIColor.white
            headerIMG.setTitleColor(UIColor.white, for: .normal)
            headerIMG.sd_setImage(with: URL(string: comment.UserImg), for: .normal, placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        }
        if !comment.UserName.isEmpty{
            let firstC = comment.UserName.first
            let lastC = comment.UserName.last
            if let firstString = firstC, let lastString = lastC{
                self.nameLable.text = "\(firstString)***\(lastString)"
            }
        }
        self.reviewsInfoLable.text = comment.CommentContent
        self.timeLable.text = comment.CommentTime
        let score = comment.CommentLevel
        rateView = FPStartRateView(frame: CGRect(x: KScreenWidth - 90 - 14, y: 16, width: 90, height: 15), starCount: 5, score: Float(score))
        rateView.isUserInteractionEnabled = false//不支持用户操作
        rateView.usePanAnimation = true
        rateView.allowUserPan = true//滑动评星
        contentView.addSubview(rateView)
    }
}
