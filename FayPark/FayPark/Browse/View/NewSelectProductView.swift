//
//  NewSelectProductView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewSelectProductView: UITableView,UITableViewDelegate,UITableViewDataSource {
    
    var productDetailInfo: ProductDetailInfo!
    var sizeArray = Array<String>.init()
    var colorArray = Array<String>.init()
    var sameSizeDic : Dictionary<String , [SkuProductItem]> = Dictionary()//按size分类的组合
    var sizeArr : [String] = [String]()//存放不同的size
    
    var sizeIndx:Int = 0
    var colorIndx:Int = 0
    
    var selectBtn = UIButton()
    
    typealias SelectProductColor =  (SkuProductItem,_ number:Int) -> Void
    var selectBlock:SelectProductColor?
    
    func selectColorAction(block:@escaping SelectProductColor) {
        self.selectBlock = block;
    }
    
    typealias SelectSize =  () -> Void
    var selectSizeBlock:SelectSize?
    
    func selectSizeAction(block:@escaping SelectSize) {
        self.selectSizeBlock = block;//展示size
    }
    
    typealias SelectCloseBtn =  () -> Void
    var selectCloseBlock:SelectCloseBtn?
    
    func selectCloseAction(block:@escaping SelectCloseBtn) {
        self.selectCloseBlock = block;//删除页面
    }
    
    func setSkuProductItemList(data:ProductDetailInfo) {
        self.productDetailInfo = data
        for item in data.SkuProductItemList {//获取 size color的种类并对应存放
            if sizeArr.contains(item.Size!) {
                continue
            } else {
                sizeArr.append(item.Size!)
                if item.Size != nil && item.Size != "" {
                    sizeArray.append(item.Size!)
                }
            }
        }
        
        for str in sizeArr {//创建不同size的dic
            sameSizeDic[str] = [SkuProductItem]()
        }
        for item in data.SkuProductItemList {
            for str in sizeArr {
                if item.Size == str {
                    var itemArr = sameSizeDic[str]//取出size作为key对应的数组
                    itemArr?.append(item)//添加size对应的dic到数组
                    sameSizeDic[str] = itemArr //更新size作为key对应的数组
                }
            }
        }
        
        let defaultSize = sizeArr[sizeIndx]
        self.colorArray = getColorList(size: defaultSize)
        let indx = NSIndexPath.init(row: 0, section: 4)
        self.selectRow(at: indx as IndexPath, animated: true, scrollPosition: .top)//将cell移动到顶端
    }
    
    @objc func confirmBtn(btn:UIButton){
        
        var lableText:String!
        let cell = self.cellForRow(at: NSIndexPath.init(row: 0, section: 3) as IndexPath) //数量quantity
        for lable in (cell?.subviews)! {
            if lable.tag == 300 {
               lableText = (lable as! UILabel).text
            }
        }
        let defaultSize = self.sizeArr[self.sizeIndx]
        let arr:[SkuProductItem] = self.sameSizeDic[defaultSize]!
        
        if self.selectBlock != nil {
            self.selectBlock!(arr[self.colorIndx],Int(lableText)!)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sizeArray.count == 0 {
            if section ==  1 {
                return 0
            }
        }
        if colorArray.count == 0 {
            if section ==  2 {
                return 0
            }
        }
        return 1
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    @objc func closeView(){
        if self.selectCloseBlock != nil{
            self.selectCloseBlock!()
        }
    }
    @objc func sizeView(){
        if self.selectSizeBlock != nil {
            self.selectSizeBlock!()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = ProductImageView.ImageCell(tableView: tableView)
            cell.closeBtn.addTarget(self, action: #selector(closeView), for: .touchUpInside)
            if self.productDetailInfo != nil {
                cell.productImage.setImageWith(URL.init(string: self.productDetailInfo.ProductImgs)!)
                var SellingNumber = self.productDetailInfo.SharePrice
                if self.productDetailInfo.IsTime == 0 {
                    SellingNumber = self.productDetailInfo.SellingPrice
                }
                cell.productPriceLabel.attributedText = FPSalePriceText.fayparkFontColorAttribute(price: String(format: "%@%.2f  ",getCurrentPayCharacter(),SellingNumber), oldPrice: String(format: "%@%.2f",getCurrentPayCharacter(),self.productDetailInfo.Msrp), priceColor: kRGBColorFromHex(rgbValue: 0xe9668f), oldPriceColor: kRGBColorFromHex(rgbValue: 0xbbbbbb), priceFont: kSizeFrom750(x: 50), oldPriceFont: kSizeFrom750(x: 24))// 显示优惠价格
                if self.colorArray.count != 0 {
                    cell.productColorLable.text = "\(kPDColorString):"+self.colorArray[0]
                }
                if self.sizeArray.count != 0 {
                    cell.productSizeLable.text = "\(kPDSizeString):"+self.sizeArray[0]
                }
            }
            return cell
            
        } else if indexPath.section == 4 {
            let cell = ConfirmView.ConfirmCell(tableView: tableView)
            cell.btn.addTarget(self, action: #selector(confirmBtn), for: .touchUpInside)
            return cell
        } else if indexPath.section == 2 { //颜色
            let cell = SizeView.SizeCell(tableView: tableView)
            cell.type = 101
            cell.colorLable.text = kPDColorCapitalString
            cell.sizeIcon.isHidden = true
            cell.updateColorOrSizeView(arr: colorArray)
            cell.selectColorBlock = {(indx)in
                self.colorIndx = indx - 200
                cell.colorSelectedIndex = self.colorIndx
                self.reloadData()
            }
            return cell
        } else if indexPath.section == 3 {
            let cell = QuantityView.QuantityCell(tableView: tableView)
            return cell
        } else{ //size
            let cell = SizeView.SizeCell(tableView: tableView)
            cell.type = 100
            cell.sizeIcon.addTarget(self, action: #selector(sizeView), for: .touchUpInside)
            cell.updateColorOrSizeView(arr: sizeArray)
            cell.selectSizeBlock = {(indx)in
                self.sizeIndx = indx - 200
                let defaultSize = self.sizeArr[self.sizeIndx]
                self.colorArray = self.getColorList(size: defaultSize)
                cell.sizeSelectedIndex = self.sizeIndx
                if let colorCell = self.cellForRow(at: IndexPath(row: 0, section: 2)) {
                    let tmpCell = colorCell as! SizeView
                    tmpCell.colorSelectedIndex = 0
                }
                self.reloadData()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
           return kSizeFrom750(x: 275)
        } else if indexPath.section == 1  {
            if sizeArray.count != 0 {
                return SizeView().getColorOrSizeHeight(arr: sizeArray)
            }
            return kSizeFrom750(x: 400)
        } else if indexPath.section == 2  {
            if colorArray.count != 0 {
                return SizeView().getColorOrSizeHeight(arr: colorArray)
            }
            return kSizeFrom750(x: 400)
        } else if indexPath.section == 3 {
            return kSizeFrom750(x: 215)
        } else{
            return kSizeFrom750(x: 119)
        }
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.backgroundColor = UIColor.white
        self.dataSource = self
        self.delegate = self
        self.estimatedSectionFooterHeight = 0
        self.estimatedSectionHeaderHeight = 0
        self.separatorStyle = .none
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func getSelectProductHeight() -> CGFloat {
        let height: CGFloat = kSizeFrom750(x: 275) + kSizeFrom750(x: 215) + kSizeFrom750(x: 119)
        var sizeHeight: CGFloat = 0
        if sizeArr.count != 0 {
            sizeHeight = SizeView().getColorOrSizeHeight(arr: sizeArray)
        }
        var colorHeight: CGFloat = 0
        if colorArray.count != 0 {
            colorHeight = SizeView().getColorOrSizeHeight(arr: colorArray)
        }
        return height + sizeHeight + colorHeight
    }
    
}
//MARK:-------  确认按钮 ---------------
class ConfirmView: UITableViewCell {

    class func ConfirmCell(tableView:UITableView)->ConfirmView {
        let cell = ConfirmView.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.contentView.addSubview(self.lineView)
        self.contentView.addSubview(self.btn)
        makeConstraints()
    }
    func makeConstraints() {
        self.lineView.mas_makeConstraints { (mark) in
            mark?.top.right().left().equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 1))
        }
        self.btn.mas_makeConstraints { (mark) in
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 34))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 34))
            mark?.bottom.equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 85))
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var lineView: UIView = {
        let view = UIView.init()
        view.backgroundColor = kSpacingColor
        return view
    }()

    lazy var btn: UIButton = {
        let tempBtn = UIButton.init()
        tempBtn.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        tempBtn.setTitle(kPDConfirmString, for: .normal)
        tempBtn.titleLabel?.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 36))
        return tempBtn
    }()
}


//MARK:--------------- 大小和颜色 ---------------------
class SizeView: UITableViewCell {

    var type : Int = 0// 10001 sizeCell 1002 colorCell
    var arrTitle = Array<String>.init()
    var selectBtn = UIButton()
    
    typealias SelectColor =  (Int) -> Void
    var selectColorBlock:SelectColor?
    
    typealias SelectSize =  (Int) -> Void
    var selectSizeBlock:SelectSize?
    var colorSelectedIndex : Int = 0 //记录当前选中color的index
    var sizeSelectedIndex : Int = 0 //记录当前选中size的index
    func selectColorAction(block:@escaping SelectColor) {
        self.selectColorBlock = block;
    }
    
    func selectSizeAction(block:@escaping SelectSize) {
        self.selectSizeBlock = block;
    }

    func textHeight(title:String,classWidth:CGFloat)->CGSize{
        let size = CGSize(width: CGFloat(MAXFLOAT), height: classWidth)
        let textFont = kGothamRoundedFontSize(size: kSizeFrom750(x: 27))
        let textLabelSize = textSize(text:title , font: textFont, maxSize: size)
        return textLabelSize
    }
    
    func textSize(text : String , font : UIFont , maxSize : CGSize) -> CGSize{
        
        let options : NSStringDrawingOptions = .usesLineFragmentOrigin
        let dic = NSDictionary(object: font, forKey: NSAttributedStringKey.font as NSCopying)
        return text.boundingRect(with: maxSize, options: options, attributes: dic as? [NSAttributedStringKey : Any], context: nil).size
    }
    
    class func SizeCell(tableView:UITableView) -> SizeView {
        var cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(self))
        if cell == nil {
            cell = SizeView.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self));
        }
        return cell! as! SizeView
    }
    
    func getColorOrSizeHeight(arr: [String]) -> CGFloat {
        var width:CGFloat = KScreenWidth-kSizeFrom750(x: 60)
        var newArray = [[String]]()
        var btnY:CGFloat = 0
        var oneArray = [String]()
        for indx in 0..<Int(arr.count){
            let size = textHeight(title: arr[indx], classWidth: CGFloat.greatestFiniteMagnitude)
            if width > (size.width + kSizeFrom750(x: 90)) {
                width = width-(size.width+kSizeFrom750(x: 90))
                oneArray.append(arr[indx])
            } else {
                width = KScreenWidth - kSizeFrom750(x: 60)
                width = width - (size.width + kSizeFrom750(x: 90))
                newArray.append(oneArray)
                oneArray = [String]()
                oneArray.append(arr[indx])
            }
            if indx == arr.count-1 {
                newArray.append(oneArray)
            }
        }
        let btnTop:CGFloat = kSizeFrom750(x: 36)
        for indx in 0 ..< Int(newArray.count){
            btnY = CGFloat(indx) * (btnTop + kSizeFrom750(x: 66)) + kSizeFrom750(x: 90) + kSizeFrom750(x: 66)
        }
        return btnY + kSizeFrom750(x: 40)
    }
    func updateColorOrSizeView(arr: [String]) {
        self.arrTitle = arr
        var width:CGFloat = KScreenWidth-kSizeFrom750(x: 60)
        var newArray = [[String]]()
        var oneArray = [String]()
        for indx in 0..<Int(arrTitle.count){
            let size = textHeight(title: arrTitle[indx], classWidth: CGFloat.greatestFiniteMagnitude)
            if width > (size.width+kSizeFrom750(x: 90)) {
                width = width-(size.width+kSizeFrom750(x: 90))
                oneArray.append(arrTitle[indx])
            }else{
                width = KScreenWidth-kSizeFrom750(x: 60)
                width = width-(size.width+kSizeFrom750(x: 90))
                newArray.append(oneArray)
                oneArray = Array<String>.init()
                oneArray.append(arrTitle[indx])
            }
            if indx == arrTitle.count-1 {
                newArray.append(oneArray)
            }
        }
        let btnTop:CGFloat = kSizeFrom750(x: 36)
        var btnWidth:CGFloat = kSizeFrom750(x: 30)
        for indx in 0 ..< newArray.count {
            let arr: [String] = newArray[indx]
            var topArr = [String]()
            if indx != 0 {
                topArr = newArray[indx - 1]
            }
            for count in  0 ..< arr.count {
                let currentString = arr[count]
                let size = textHeight(title: currentString, classWidth: CGFloat.greatestFiniteMagnitude)
                if indx*topArr.count+count == 0 || count == 0 {
                    btnWidth = kSizeFrom750(x: 30)
                }
                let btn = UIButton.init(frame: CGRect.init(x: btnWidth, y: CGFloat(indx) * (btnTop + kSizeFrom750(x: 66))+kSizeFrom750(x: 90), width: kSizeFrom750(x: 60)+size.width, height: kSizeFrom750(x: 66)))
                btn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
                btn.backgroundColor = UIColor.white
                btn.setTitle(currentString, for: .normal)
                btn.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 27))
                btn.isSelected = false
                self.addSubview(btn)
                btnWidth = btn.mj_origin.x + btn.mj_w + kSizeFrom750(x: 30)
                if arrTitle.contains(currentString) {
                    if let arrIndex = arrTitle.index(of: currentString) {
                        btn.tag = 200 + arrIndex
                    }
                }
                if self.type == 101 { //颜色
                    btn.addTarget(self, action: #selector(touchColorBtn), for: .touchUpInside)
                    if btn.tag == colorSelectedIndex + 200 {
                        btn.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
                        btn.setTitleColor(UIColor.white, for: .normal)
                    }else {
                        btn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
                        btn.layer.borderWidth = kSizeFrom750(x: 1)
                    }
                } else {
                    //Size
                    btn.addTarget(self, action: #selector(touchSizeBtn), for: .touchUpInside)
                    if btn.tag == sizeSelectedIndex + 200 {
                        btn.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
                        btn.setTitleColor(UIColor.white, for: .normal)
                    }else {
                        btn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
                        btn.layer.borderWidth = kSizeFrom750(x: 1)
                    }
                }
            }
        }
        
    }
    
    @objc func touchColorBtn(btn:UIButton){

        if (self.selectColorBlock != nil) {
            self.selectColorBlock!(btn.tag)
        }
        
    }
    @objc func touchSizeBtn(btn:UIButton){

        if (self.selectSizeBlock != nil) {
            self.selectSizeBlock!(btn.tag)
        }
        
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(self.colorLable)
        self.addSubview(self.lineView)
        self.addSubview(self.sizeIcon)
        self.selectionStyle = .none
        makeConstraint()
    }
    
    func makeConstraint() {
        self.colorLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 36))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 20))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 20))
        }
        self.colorLable.setContentHuggingPriority(.required, for: .horizontal)
        self.lineView.mas_makeConstraints { (mark) in
            mark?.top.left().right().equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 1))
        }
        self.sizeIcon.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.colorLable)
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 34))
            mark?.width.equalTo()(kSizeFrom750(x: 230))
            mark?.height.equalTo()(kSizeFrom750(x: 30))
        }
        
        self.sizeIcon.superview?.layoutIfNeeded()
        self.sizeIcon.imageEdgeInsets = UIEdgeInsetsMake(0, kSizeFrom750(x: 200), 0, 0)
        self.sizeIcon.contentHorizontalAlignment = .left
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var colorLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempLable.text = kPDSizeCapitalString
        return tempLable
    }()
    
    lazy var lineView: UIView = {
        let tempLine = UIView.init()
        tempLine.backgroundColor = kSpacingColor
        return tempLine
    }()
    lazy var sizeIcon:UIButton = {
        let tempBtn = UIButton.init()
        tempBtn.setTitle(kPDSizeGuideString, for: .normal)
        tempBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x7c7c7c), for: .normal)
        tempBtn.titleLabel?.font = UIFont.init(name: kMSReference, size: kSizeFrom750(x: 28))
        tempBtn.setImage(kImage(iconName: "arrowrighticon"), for: .normal)
        return tempBtn
    }()
}

//MARK:-----------头部视图--------------------
class ProductImageView: UITableViewCell {
    
    class func ImageCell(tableView:UITableView)->ProductImageView {
        let cell = ProductImageView.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(self.productImage)
        self.addSubview(self.productSizeLable)
        self.addSubview(self.productPriceLabel)
        self.addSubview(self.productColorLable)
        self.addSubview(self.productliensView)
        self.addSubview(self.closeBtn)
        self.selectionStyle = .none
        makeConstraint()
    }
    func makeConstraint() {
        self.productImage.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 34))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 24))
            mark?.width.height().equalTo()(kSizeFrom750(x: 222))
        }
        self.closeBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 34))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 48))
            mark?.width.equalTo()(kSizeFrom750(x: 40))
            mark?.height.equalTo()(kSizeFrom750(x: 40))
        }
        self.productPriceLabel.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 74))
            mark?.left.equalTo()(self.productImage.mas_right)?.offset()(kSizeFrom750(x: 64))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 48))
        }
        self.productPriceLabel.setContentHuggingPriority(.required, for: .horizontal)
        self.productColorLable.mas_makeConstraints { (mark) in
            mark?.left.right().equalTo()(self.productPriceLabel)
            mark?.top.equalTo()(self.productPriceLabel.mas_bottom)?.offset()(kSizeFrom750(x: 30))
        }
        self.productColorLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.productSizeLable.mas_makeConstraints { (mark) in
            mark?.right.left().equalTo()(self.productColorLable)
            mark?.top.equalTo()(self.productColorLable.mas_bottom)?.offset()(kSizeFrom750(x: 30))
        }
        self.productSizeLable.setContentHuggingPriority(.required, for: .horizontal)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var productImage: UIImageView = {
        let tempProductImage = UIImageView.init()
        tempProductImage.backgroundColor = kSpacingColor
        return tempProductImage
    }()
    lazy var productSizeLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = UIFont.init(name: kMSReference, size: kSizeFrom750(x: 28))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        return tempLable
    }()
    lazy var productPriceLabel: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 50))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        return tempLable
    }()
    lazy var productColorLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = UIFont.init(name: kMSReference, size: kSizeFrom750(x: 28))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        tempLable.text = "hello"
        return tempLable
    }()
    lazy  var productliensView: UIView = {
        let tempView = UIView.init()
        tempView.backgroundColor = kSpacingColor
        return tempView
    }()
    lazy var closeBtn: UIButton = {
        let tempBtn = UIButton.init()
        tempBtn.setBackgroundImage(kImage(iconName: "cart_icon_close"), for: .normal)
        return tempBtn
    }()
}

class QuantityView: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(self.lineView)
        self.addSubview(self.quantityLable)
        self.addSubview(self.plusBtn)
        self.addSubview(self.LessBtn)
        self.addSubview(self.numberLable)
        self.selectionStyle = .none
        makeConstraint()
    }
    func makeConstraint() {
        self.lineView.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 28))
            mark?.height.equalTo()(kSizeFrom750(x: 1))
        }
        self.quantityLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 36))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 20))
        }
        self.quantityLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.plusBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 90))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 40))
            mark?.width.equalTo()(kSizeFrom750(x: 82))
            mark?.height.equalTo()(kSizeFrom750(x: 80))
        }
        self.numberLable.mas_makeConstraints { (mark) in
            mark?.top.height().equalTo()(self.plusBtn)
            mark?.right.equalTo()(self.plusBtn.mas_left)
            mark?.width.equalTo()(kSizeFrom750(x: 120))
        }
        self.LessBtn.mas_makeConstraints { (mark) in
            mark?.top.width().height().equalTo()(self.plusBtn)
            mark?.right.equalTo()(self.numberLable.mas_left)
        }
    }
    class func QuantityCell(tableView:UITableView)-> QuantityView {
        let cell = QuantityView.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var lineView: UIView = {
        let tempLine = UIView.init()
        tempLine.backgroundColor = kSpacingColor
        return tempLine
    }()
    
    lazy var quantityLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempLable.text = kPDQuantityCaptialString
        return tempLable
    }()
    
    lazy var plusBtn: UIButton = {
        let tempBtn = UIButton.init()
        tempBtn.setTitle("+", for: .normal)
        tempBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
        tempBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        tempBtn.layer.borderWidth = kSizeFrom750(x: 1)
        tempBtn.addTarget(self, action: #selector(selectedPushBtn), for: .touchUpInside)
        return tempBtn
    }()
    @objc func selectedPushBtn(btn:UIButton){
        self.numberLable.text = String(format: "%d", Int(self.numberLable.text!)!+1)
    }
    @objc func selectedLessBtn(btn:UIButton){
        if Int(self.numberLable.text!)! > 1 {
            self.numberLable.text = String(format: "%d", Int(self.numberLable.text!)!-1)
        }
    }
    lazy var LessBtn: UIButton = {
        let tempBtn = UIButton.init()
        tempBtn.setTitle("-", for: .normal)
        tempBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
        tempBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        tempBtn.layer.borderWidth = kSizeFrom750(x: 1)
        tempBtn.addTarget(self, action: #selector(selectedLessBtn), for: .touchUpInside)
        return tempBtn
    }()
    lazy var numberLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.text = "1"
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 40))
        tempLable.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        tempLable.layer.borderWidth = kSizeFrom750(x: 1)
        tempLable.textAlignment = .center
        tempLable.tag = 300
        return tempLable
    }()
    
}

extension NewSelectProductView {
    
    func getColorList(size:String) -> [String] {
        let sizeKey = size
        let sizeArr = sameSizeDic[sizeKey]!
        var colorArr = [String]()
        for item in sizeArr {
            if item.Color?.count != 0 {
                colorArr.append(item.Color!)
            }
        }
        return colorArr
    }
}
