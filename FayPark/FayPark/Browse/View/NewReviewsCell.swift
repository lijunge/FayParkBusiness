//
//  NewReviewsCell.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewReviewsCell: UITableViewCell {

    class func ReviewsViewCell(tableView:UITableView)->NewReviewsCell {
        let cell = NewReviewsCell.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        //MARK:加载评论头部时图
        self.addSubview(reviewsLable)
        self.addSubview(scoreImg)
        self.addSubview(scoreLable)
        self.addSubview(moreBtn)
        makeConstraint()
    }
    
    func makeConstraint() {
        //MARK:加载评论头部时图----------------------------------------------------
        reviewsLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 62))
            mark?.centerX.equalTo()(self)
        }
        reviewsLable.setContentHuggingPriority(.required, for: .horizontal)
        
        scoreImg.mas_remakeConstraints { (mark) in
            mark?.top.equalTo()(reviewsLable.mas_bottom)?.offset()(kSizeFrom750(x: 28))
            mark?.height.equalTo()(kSizeFrom750(x: 30))
            mark?.width.equalTo()(kSizeFrom750(x: 150))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 275))
        }
        scoreLable.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(scoreImg.mas_right)?.offset()(kSizeFrom750(x: 14))
            mark?.centerY.equalTo()(scoreImg.mas_centerY)
        }
        scoreLable.setContentHuggingPriority(.required, for: .horizontal)
        //----------------------------------------------------------------------
        
        moreBtn.mas_makeConstraints { (mark) in
            //mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 54))
            mark?.centerX.equalTo()(self)
            mark?.centerY.equalTo()(self)
            mark?.width.equalTo()(kSizeFrom750(x: 406))
            mark?.height.equalTo()(kSizeFrom750(x: 88))
        }
    }
    
    func setCommentAvg(str1:String) {
        if str1.count == 0  {
            self.scoreLable.text = "0"
        }else{
            self.scoreLable.text = str1
        }
        self.scoreImg.superview?.layoutIfNeeded()
        var score: Float = 0
        if let tmp = Float(str1) {
            score = tmp
        }
        self.scoreImg.setCreateStarView(frame: self.scoreImg.frame, starCount: 5, score: score)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   //MARK:加载评论头部时图----------------------------------------------------
    lazy var reviewsLable: UILabel = {
        let tempReviewsLable = UILabel.init()
        tempReviewsLable.font = UIFont.init(name:kGothamRoundedBold, size: kSizeFrom750(x: 40))
        tempReviewsLable.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        tempReviewsLable.textAlignment = .center
        tempReviewsLable.text = "Reviews"
        return tempReviewsLable
    }()
    
    lazy var scoreImg: FPStartRateView = {
        let tempScoreImage = FPStartRateView.init(frame: CGRect.init(), starCount: 5, score: 0)
        tempScoreImage.isUserInteractionEnabled = false//不支持用户操作
        tempScoreImage.usePanAnimation = true
        tempScoreImage.allowUserPan = true//滑动评星
        return tempScoreImage
    }()
   
    lazy var scoreLable: UILabel = {
        let tempScoreLable = UILabel.init()
        tempScoreLable.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 26))
        tempScoreLable.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        tempScoreLable.text = "3.3"
        return tempScoreLable
    }()
    // -----------------------------------------------------------------------------
    
    //MARK:加载评论尾部时图----------------------------------------------------
    
    lazy var moreBtn: UIButton = {
        let tempMoreBtn = UIButton.init()
        tempMoreBtn.titleLabel?.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        tempMoreBtn.setTitle(kPDViewMoreString, for: .normal)
        tempMoreBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x000000), for: .normal)
        return tempMoreBtn
    }()
    
}
