//
//  NewDetailcBottomView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewDetailcBottomView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.cartBtn)
        self.addSubview(self.likeBtn)
        makeConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func makeConstraint() {
        self.likeBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.setOffset(kSizeFrom750(x: 2))
            mark?.left.equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 100))
            mark?.right.equalTo()(self.mas_centerX)
        }
        
        self.cartBtn.mas_makeConstraints { (mark) in
            mark?.height.equalTo()(kSizeFrom750(x: 102))
            mark?.right.top().equalTo()(self)
            mark?.left.equalTo()(self.mas_centerX)
        }
    }

    lazy var likeBtn: UIButton = {
        let tempLikeBtn = UIButton.init()
        tempLikeBtn.setImage(UIImage.init(named: "like_icon"), for: .normal)
        tempLikeBtn.setImage(kImage(iconName: "heartlike_icon_pressed"), for: .selected)
        tempLikeBtn.backgroundColor = UIColor.white
        return tempLikeBtn
    }()
    
    lazy var cartBtn: UIButton = {
        let tempCartBtn = UIButton.init()
        tempCartBtn.setTitle(kPDAddToCartString, for: .normal)
        tempCartBtn.backgroundColor =  kRGBColorFromHex(rgbValue: 0xe9668f)
        tempCartBtn.titleLabel?.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        return tempCartBtn
    }()

}
