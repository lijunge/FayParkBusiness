//
//  BrowseCountryView.swift
//  FayPark
//
//  Created by faypark on 2018/8/2.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class BrowseCountryView: UIView {
    typealias CountryViewBlock = (() -> Void)
    var didTappedCountryViewBlock: CountryViewBlock?
    let rightBackView = UIView()
    let leftImageView = UIImageView()
    var leftImageString: String = "browse_unitedstates_country"{
        didSet {
            leftImageView.image = UIImage(named: leftImageString)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        let countryW: CGFloat = 30
        let countryH: CGFloat = 25
        let countryY = frame.height / 2.0 - countryH / 2.0
        
        leftImageView.frame = CGRect(x: 0, y: countryY, width: frame.width - countryW, height: countryH)
        addSubview(leftImageView)
        
        rightBackView.frame = CGRect(x: frame.width - countryW, y: countryY, width: countryW, height: countryH)
        rightBackView.layer.borderWidth = 0.5
        rightBackView.layer.borderColor = kRGBColorFromHex(rgbValue: 0xe8e8e8).cgColor
        let arrowImgView = UIImageView(frame: CGRect(x: countryW / 2.0 - 12 / 2.0, y: countryH / 2.0 - 12 / 2.0, width: 12, height: 12))
        arrowImgView.image = UIImage(named: "states_arrowdownicon")
        rightBackView.addSubview(arrowImgView)
        
        addSubview(rightBackView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(countryButtonTapped))
        addGestureRecognizer(tapGesture)
    }
    @objc func countryButtonTapped() {
        if didTappedCountryViewBlock != nil {
            didTappedCountryViewBlock!()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
