//
//  NewProductDetailcCell.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SDCycleScrollView
class NewProductDetailcCell: UITableViewCell {

    typealias SelectReviews =  () -> Void
    typealias SelectLoadTimer = () -> Void
    var selectBlock:SelectReviews?
    var selectTimerBlock:SelectLoadTimer?
    var productData:ProductDetailInfo!
    var detailc:NewProductDetailViewController!
  
    var productImgArray : [String]!
    let productTimeCountIdentifier = "productCountDownIdentifier"
    let randomTimeCountIdentifier = "productRandomCountDownIdentifier"
    let activityTimeCountIdentifier = "productActivityCountDownIdentifier"
    var currentTimeCountIdentifier = ""
   
    func selectBlockAction(block:@escaping SelectReviews) {
        self.selectBlock = block;
    }
//    func selectTimerAction(block:@escaping SelectLoadTimer) {
//        self.selectBlock = block;
//    }
    
    class func DetalicCell(tableView:UITableView) -> NewProductDetailcCell {
        let cell = NewProductDetailcCell.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.addSubview(banner)
        self.addSubview(normalCountdownView)
        self.addSubview(randomCountdownView)
        self.addSubview(shareBtn)
        self.addSubview(priceLabel)
        self.addSubview(likeImgView)
        self.addSubview(likeLable)
        self.addSubview(cartImgView)
        self.addSubview(cartLable)
        self.addSubview(shoppingImgView)
        self.addSubview(shoppingLable)
        self.addSubview(storeNameLabel)
        self.addSubview(storeActLabel)
        self.addSubview(reviewsLable)
        self.addSubview(scoreImg)
        self.addSubview(scoreLable)
        self.addSubview(titleSlogan)
        self.addSubview(nameSlogan)
        self.addSubview(rightImg)
        self.addSubview(pageCountLable)
        self.addSubview(reviewsView)
        banner.addSubview(discountBtn)
        self.addSubview(headerBtmView)
        
        self.tag = 10010
        self.detailc = NewProductDetailViewController()
        NotificationCenter.default.addObserver(self, selector: #selector(removeTimerCount), name: Notification.Name(rawValue: "ProductDetailDisappera"), object: nil)
        makeConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func makeConstraints() {
        self.banner.mas_makeConstraints { (mark) in
            mark?.left.top().right().equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 580))
        }
        self.discountBtn.mas_makeConstraints { (mark) in
            mark?.top.left().equalTo()(self.banner)
            mark?.height.equalTo()(kSizeFrom750(x: 70))
            mark?.width.equalTo()(kSizeFrom750(x: 140))
        }
        
        self.pageCountLable.mas_remakeConstraints { (mark) in
            mark?.top.equalTo()(kSizeFrom750(x: 20))
            mark?.right.equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 70))
            mark?.width.equalTo()(kSizeFrom750(x: 130))
        }
        
        self.shareBtn.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self.banner.mas_bottom)
            mark?.width.height().equalTo()(kSizeFrom750(x: 100))
            mark?.right.equalTo()(self.mas_right)?.setOffset(-kSizeFrom750(x: 18))
        }
        
        self.normalCountdownView.mas_remakeConstraints { (mark) in
            mark?.top.equalTo()(self.banner.mas_bottom)
            mark?.left.right().equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 90))
        }
        self.randomCountdownView.mas_remakeConstraints { (mark) in
            mark?.top.equalTo()(self.banner.mas_bottom)
            mark?.left.right().equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 90))
        }
        self.randomCountdownView.isHidden = true
        self.normalCountdownView.isHidden = true
        self.priceLabel.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.banner.mas_bottom)?.offset()(kSizeFrom750(x: 10))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
            mark?.right.equalTo()(self.shareBtn.mas_left)?.offset()(-kSizeFrom750(x: 18))
        }
        self.priceLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        self.likeImgView.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.priceLabel.mas_bottom)?.offset()(kSizeFrom750(x: 30))
            mark?.width.height().equalTo()(kSizeFrom750(x: 30))
            mark?.left.equalTo()(self.priceLabel)
        }
        
        self.likeLable.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(self.likeImgView.mas_right)?.offset()(kSizeFrom750(x: 10))
            mark?.centerY.equalTo()(self.likeImgView)
        }
        self.likeLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.cartImgView.mas_makeConstraints { (mark) in
            mark?.top.with().height().equalTo()(self.likeImgView)
            mark?.left.equalTo()(self.likeLable.mas_right)?.offset()(kSizeFrom750(x: 30))
        }
        self.cartLable.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(self.cartImgView.mas_right)?.offset()(kSizeFrom750(x: 10))
            mark?.centerY.equalTo()(self.cartImgView)
        }
        self.shoppingImgView.mas_makeConstraints { (mark) in
            mark?.top.with().height().equalTo()(self.cartImgView)
            mark?.left.equalTo()(self.cartLable.mas_right)?.offset()(kSizeFrom750(x: 30))
        }
        
        self.shoppingLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self.shoppingImgView)
            mark?.left.equalTo()(self.shoppingImgView.mas_right)?.offset()(kSizeFrom750(x: 10))
        }
        
        self.storeNameLabel.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.likeImgView.mas_bottom)?.offset()(kSizeFrom750(x: 34))
            mark?.left.equalTo()(self.likeImgView)
            mark?.width.equalTo()((KScreenWidth - 32) / 2)
        }
        self.storeActLabel.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.storeNameLabel.mas_bottom)?.offset()(kSizeFrom750(x: 14))
            mark?.left.equalTo()(self.likeImgView)
            mark?.width.equalTo()((KScreenWidth - 32) / 2)
        }
        
        self.reviewsLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.likeImgView.mas_bottom)?.offset()(kSizeFrom750(x: 34))
            mark?.left.equalTo()(self.storeNameLabel.mas_right)?.offset()(kSizeFrom750(x: 3))
            mark?.width.equalTo()((KScreenWidth - 32) / 2)
        }

        self.scoreImg.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.reviewsLable.mas_bottom)?.offset()(kSizeFrom750(x: 14))
            mark?.width.equalTo()(kSizeFrom750(x: 150))
            mark?.height.equalTo()(kSizeFrom750(x: 30))
            mark?.left.equalTo()(self.reviewsLable)
        }
        
        self.scoreLable.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(self.scoreImg.mas_right)?.offset()(kSizeFrom750(x: 14))
            mark?.centerY.equalTo()(self.scoreImg.mas_centerY)
        }
        
        self.rightImg.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.reviewsLable.mas_bottom)
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 32))
            mark?.width.height().equalTo()(kSizeFrom750(x: 30))
        }
        
        self.reviewsView.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.reviewsLable)
            mark?.left.right().equalTo()(self)
            mark?.bottom.equalTo()(self.scoreLable.mas_bottom)
        }
        
        self.headerBtmView.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.reviewsView.mas_bottom)?.offset()(kSizeFrom750(x: 36))
            mark?.left.right().equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 14))
        }
        
        self.titleSlogan.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.headerBtmView.mas_bottom)?.offset()(kSizeFrom750(x: 42))
            mark?.left.equalTo()(self.likeImgView)
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 28))
        }
        self.titleSlogan.setContentHuggingPriority(.required, for: .horizontal)
        
        self.nameSlogan.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.titleSlogan.mas_bottom)?.offset()(kSizeFrom750(x: 30))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 30))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 30))
        }

        self.pageCountLable.superview?.layoutIfNeeded()
        let maskPath = UIBezierPath.init(roundedRect: self.pageCountLable.bounds, byRoundingCorners: [UIRectCorner.bottomLeft,UIRectCorner.topLeft], cornerRadii: CGSize(width: kSizeFrom750(x: 35), height: kSizeFrom750(x: 35)))//切半圆
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.pageCountLable.bounds
        maskLayer.path = maskPath.cgPath
        self.pageCountLable.layer.mask = maskLayer
    }
    func updateCurrentConstraint(data: ProductDetailInfo) {
        //MARK:------根据状态更新UI---------------------------------------------------
        /**
         * isTime = 0 正常商品
         * isTime = 1 随机倒计时商品 randomCountdownView
         * isTime = 2 倒计时商品 normalCountdownView
         * isTime = 3 活动商品（正常显示）
         **/
        if self.productData != nil {
            if self.productData.IsTime == 0  || self.productData.IsTime == 3 {//正常显示//活动
                self.randomCountdownView.isHidden = true
                self.normalCountdownView.isHidden = true
                self.priceLabel.mas_remakeConstraints { (mark) in
                    mark?.top.equalTo()(self.banner.mas_bottom)?.offset()(kSizeFrom750(x: 10))
                    mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
                    mark?.right.equalTo()(self.shareBtn.mas_left)?.offset()(-kSizeFrom750(x: 18))
                }
                self.likeImgView.mas_remakeConstraints { (mark) in
                    mark?.top.equalTo()(self.priceLabel.mas_bottom)?.offset()(kSizeFrom750(x: 30))
                    mark?.width.height().equalTo()(kSizeFrom750(x: 30))
                    mark?.left.equalTo()(self.priceLabel)
                }
            } else if self.productData.IsTime == 2 {//显示倒计时
                self.randomCountdownView.isHidden = true
                self.priceLabel.isHidden = true
                self.normalCountdownView.expiredLable.isHidden = true
                self.normalCountdownView.expiredLable.isEnabled = true
                self.normalCountdownView.isHidden = false
                self.likeImgView.mas_remakeConstraints { (mark) in
                    mark?.top.equalTo()(self.normalCountdownView.mas_bottom)?.offset()(kSizeFrom750(x: 30))
                    mark?.width.height().equalTo()(kSizeFrom750(x: 30))
                    mark?.left.equalTo()(self.normalCountdownView)?.offset()(kSizeFrom750(x: 28))
                }
            } else{ //1 随机倒计时
                self.normalCountdownView.isHidden = true
                self.randomCountdownView.isHidden = false
                self.priceLabel.mas_remakeConstraints { (mark) in
                    mark?.top.equalTo()(self.randomCountdownView.mas_bottom)?.offset()(kSizeFrom750(x: 10))
                    mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
                    mark?.right.equalTo()(self.shareBtn.mas_left)?.offset()(-kSizeFrom750(x: 18))
                }
                self.likeImgView.mas_remakeConstraints { (mark) in
                    mark?.top.equalTo()(self.priceLabel.mas_bottom)?.offset()(kSizeFrom750(x: 30))
                    mark?.width.height().equalTo()(kSizeFrom750(x: 30))
                    mark?.left.equalTo()(self.priceLabel)
                }
            }
        } else {
            self.randomCountdownView.isHidden = true
            self.normalCountdownView.isHidden = true
            self.priceLabel.mas_remakeConstraints { (mark) in
                mark?.top.equalTo()(self.banner.mas_bottom)?.offset()(kSizeFrom750(x: 10))
                mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
                mark?.right.equalTo()(self.shareBtn.mas_left)?.offset()(-kSizeFrom750(x: 18))
            }
            self.likeImgView.mas_remakeConstraints { (mark) in
                mark?.top.equalTo()(self.priceLabel.mas_bottom)?.offset()(kSizeFrom750(x: 30))
                mark?.width.height().equalTo()(kSizeFrom750(x: 30))
                mark?.left.equalTo()(self.priceLabel)
            }
        }
        self.layoutIfNeeded()
    }
    lazy var banner: SDCycleScrollView = {
        let tempBanner = SDCycleScrollView.init(frame: CGRect.init(), imageNamesGroup: self.productImgArray)
        tempBanner?.backgroundColor = UIColor.white
        tempBanner?.bannerImageViewContentMode = .scaleAspectFit//.center
        tempBanner?.showPageControl = false
        tempBanner?.autoScrollTimeInterval = 9999999
        tempBanner?.itemDidScrollOperationBlock = {(indx) in
            self.pageCountLable.text = String(format: "\(indx+1)/%d", self.productImgArray.count)
        }
        return tempBanner!
    }()
    
    lazy var discountBtn: UIButton = {
        let tempLable = UIButton.init()
        tempLable.titleLabel?.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 32))
        tempLable.setBackgroundImage(kImage(iconName: "browse_shape_label"), for: .normal)
        tempLable.contentHorizontalAlignment = .left
        return tempLable
    }()
    
    lazy var randomCountdownView :NewDetailcHeaderView = {
       let tempHeader = NewDetailcHeaderView.init(frame: CGRect.init())
        tempHeader.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        return tempHeader
    }()
    
    lazy var normalCountdownView : NewDetailcExpiredImageView = {
        
        let tempExpired = NewDetailcExpiredImageView.init(frame: CGRect.init())
        tempExpired.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        return tempExpired
    }()
    
    lazy var pageCountLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.textColor = UIColor.white
        tempLable.alpha = 0.3
        tempLable.backgroundColor = kRGBColorFromHex(rgbValue: 0x000000)
        tempLable.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 34))
        tempLable.textAlignment = .center
        return tempLable
    }()
    
    lazy var shareBtn: UIButton = {
        let tempShareBtn = UIButton.init()
        tempShareBtn.backgroundColor = UIColor.clear
        tempShareBtn.setImage(UIImage.init(named: "share_icon"), for: .normal)
        tempShareBtn.addTarget(self.detailc, action: #selector(shareBtnClick), for: .touchUpInside)
        return tempShareBtn
    }()
    
    @objc func shareBtnClick(){
        print("fenxiang")
    }
    lazy var priceLabel: UILabel = {
        let tempPriceLable = UILabel.init()
        tempPriceLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 50))
        return tempPriceLable
    }()
    
    lazy var likeImgView: UIImageView = {
        let tempLikeImg = UIImageView.init()
        tempLikeImg.image = kImage(iconName: "account_wishlist_icon")
        return tempLikeImg
    }()
    
    lazy var likeLable: UILabel = {
        let tempLikeLable = UILabel.init()
        tempLikeLable.tag = 10086
        return styleLable(lable: tempLikeLable)
    }()
    
    lazy var cartImgView: UIImageView = {
        let tempCartImg = UIImageView.init()
        tempCartImg.image = kImage(iconName: "Purchased")
        return tempCartImg
    }()
    
    lazy var cartLable: UILabel = {
        let tempCart = UILabel.init()
        return styleLable(lable: tempCart)
    }()
    
    lazy var shoppingImgView: UIImageView = {
        let tempShopping = UIImageView.init()
        tempShopping.image = kImage(iconName: "Sharefreeshipping")
        return tempShopping
    }()

    lazy var shoppingLable: UILabel = {
        let tempShoppingLable = UILabel.init()
        return styleLable(lable: tempShoppingLable)
    }()
    
    lazy var reviewsView: UIView = {
        let views = UIView.init()
        views.backgroundColor = UIColor.clear
        views.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(reviewsTouch)))
        return views
    }()
    lazy var storeNameLabel: UILabel = {
        let storeLabel = UILabel.init()
        storeLabel.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 34))
        storeLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        storeLabel.text = "Store Name:"
        return storeLabel
    }()
    
    lazy var storeActLabel: UILabel = {
        let tmpActLabel = UILabel.init()
        tmpActLabel.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 34))
        tmpActLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tmpActLabel.text = "Boom Fashion"
        return tmpActLabel
    }()
    
    lazy var reviewsLable: UILabel = {
        let tempReviewsLable = UILabel.init()
        tempReviewsLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 34))
        tempReviewsLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        return tempReviewsLable
    }()
    
    lazy var scoreImg: FPStartRateView = {
        let tempScoreImage = FPStartRateView.init(frame: CGRect.init(), starCount: 5, score: 0)
        tempScoreImage.isUserInteractionEnabled = false//不支持用户操作
        tempScoreImage.usePanAnimation = true
        tempScoreImage.allowUserPan = true//滑动评星
        return tempScoreImage
    }()

    lazy var scoreLable: UILabel = {
        let tempScoreLable = UILabel.init()
        tempScoreLable.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 26))
        tempScoreLable.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        return tempScoreLable
    }()
    
    lazy var rightImg: UIImageView = {
        let tempRightImg = UIImageView.init()
        tempRightImg.image = kImage(iconName: "arrowrighticon")
        return tempRightImg
    }()
    
    
    lazy var headerBtmView: UIView = {
        let tempView = UIView.init()
        tempView.backgroundColor = kSpacingColor
        return tempView
    }()
    
    lazy var titleSlogan: UILabel = {
        let tempTitle = UILabel.init()
        tempTitle.numberOfLines = 0
        tempTitle.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 26))
        tempTitle.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        return tempTitle
    }()
    
    lazy var nameSlogan: UILabel = {
        let tempName = UILabel.init()
        tempName.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 26))
        tempName.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempName.textAlignment = .right
        return tempName
    }()
    
    //MARK:-----------
    func styleLable(lable:UILabel) -> UILabel {
        lable.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 22))
        lable.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        return lable
    }
 
    func getProductCellHeight(data: ProductDetailInfo) -> CGFloat {
        let bannerHeight = kSizeFrom750(x: 580)
        var countdownHeight: CGFloat = 0
        var priceHeight: CGFloat = 0
        var likeHeight: CGFloat = 0
        var reviewsHeight: CGFloat = 0
        var backViewHeight: CGFloat = 0
        var titleHeight: CGFloat = 0
        var nameHeight: CGFloat = 0
        let priceWidth = KScreenWidth - kSizeFrom750(x: 164)
        let priceString = String(format: "%@%.2f  ",getCurrentPayCharacter(),data.SellingPrice)
        priceHeight = kSizeFrom750(x: 10) + priceString.getHeightWithConstrainedWidth(width: priceWidth, font: UIFont(name: "Avenir-Roman", size: kSizeFrom750(x: 50)) ?? UIFont.systemFont(ofSize: kSizeFrom750(x: 50)))

        if data.IsTime == 0 || data.IsTime == 3 { //正常
            countdownHeight = 0
        } else if data.IsTime == 2 { //正常倒计时 没有pricelabel
            countdownHeight = kSizeFrom750(x: 90)
            priceHeight = 0
        } else if data.IsTime == 1 { //随机倒计时
            countdownHeight = kSizeFrom750(x: 90)
        }
        likeHeight = kSizeFrom750(x: 30) * 2
        let reviewString = "\(kPDReviewsString)(\(data.CommentCount))"
        reviewsHeight = reviewString.getHeightWithConstrainedWidth(width: (KScreenWidth - 32) / 2, font: UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 34))!) + kSizeFrom750(x: 34) + kSizeFrom750(x: 30)
        backViewHeight = kSizeFrom750(x: 36) + kSizeFrom750(x: 14)

        let titleWidth = KScreenWidth - kSizeFrom750(x: 28) * 2
        let paragh: NSMutableParagraphStyle = NSMutableParagraphStyle.init()
        paragh.lineSpacing = kSizeFrom750(x: 10)
        let attText: NSAttributedString = NSAttributedString.init(string: (data.CelebrityMottoInfoList.first?.Title)!, attributes: [NSAttributedStringKey.paragraphStyle : paragh, NSAttributedStringKey.font:UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 26))!])
        titleHeight = String.getHeighForAttributeString(str: attText, textWidth: titleWidth) + kSizeFrom750(x: 42)
        let nameFont = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 26))
        let nameWidth = KScreenWidth - kSizeFrom750(x: 30) * 2
        let nameH = data.CelebrityMottoInfoList.first?.ByName?.getHeightWithConstrainedWidth(width: nameWidth, font: nameFont!)
        nameHeight = kSizeFrom750(x: 30) + nameH! + kSizeFrom750(x: 30)
        return countdownHeight + priceHeight + likeHeight + reviewsHeight + backViewHeight + titleHeight + nameHeight + bannerHeight 
    }
    
    func updateProductCell(data: ProductDetailInfo) {
        self.productData = data
        updateCurrentConstraint(data: data)
        //评论
        if data.CommentAvg.count == 0 {
            self.scoreLable.text = "0"
        }else{
            self.scoreLable.text = data.CommentAvg
        }
        var score: Float = 0
        if let tmp = Float(data.CommentAvg) {
            score = tmp
        }
        self.scoreImg.setCreateStarView(frame: self.scoreImg.frame, starCount: 5, score: score)
    
        //banner
        var productImgArray = [String]()
        for item in data.ImgLinksList {
            productImgArray.append(item.Img!)
        }
        self.productImgArray = productImgArray
        DispatchQueue.main.async {
            self.banner.imageURLStringsGroup = self.productImgArray
        }
        self.pageCountLable.text = String(format: "1/%d", self.productImgArray.count)
        
        //价格
        self.priceLabel.attributedText = FPSalePriceText.fayparkFontColorAttribute(price: String(format: "%@%.2f  ",getCurrentPayCharacter(),data.SellingPrice), oldPrice: String(format: "%@%.2f",getCurrentPayCharacter(),data.Msrp), priceColor: kRGBColorFromHex(rgbValue: 0xe9668f), oldPriceColor: kRGBColorFromHex(rgbValue: 0xbbbbbb), priceFont: kSizeFrom750(x: 50), oldPriceFont: kSizeFrom750(x: 24))
        if data.CollCount > 1000 {
            self.likeLable.text = String(format: "%dK %@", data.CollCount/1000,kPDLikesString)
        }else {
            self.likeLable.text = String(format: "%d %@", data.CollCount,kPDLikesString)
        }
        self.cartLable.text = String(format: "%d %@",data.CarCount,kPDPurchasedString)
        if data.Shipping == 0 {
            self.shoppingLable.text = kPDFreeShippingString
        } else {
            self.shoppingLable.text = String(format: "%@ %@%.2f",kPDShippingString,getCurrentPayCharacter(),data.Shipping)
        }
        self.storeActLabel.text = data.ComName
        self.reviewsLable.text = "\(kPDReviewsString)(\(data.CommentCount))"
        
        self.titleSlogan.attributedText = String.getAttributeString(str: (data.CelebrityMottoInfoList.first?.Title)!, lineSpace: kSizeFrom750(x: 10), font: self.titleSlogan.font)
        self.nameSlogan.text = data.CelebrityMottoInfoList.first?.ByName
        var discountStr = (1 - (Float(data.SellingPrice) / Float(data.Msrp))) * 100
        if data.IsTime != 0 {
            discountStr = (1 - (Float(data.SharePrice) / Float(data.Msrp))) * 100
        }
        if discountStr == 100 {
            self.discountBtn.setTitle(String(format: "  \(kHomeFree)", discountStr), for: .normal)
        } else {
            self.discountBtn.setTitle(String(format: "  -%.0f%%", discountStr), for: .normal)
        }
        
        if data.EndTime.count != 0 && data.StartTime.count != 0{
            self.randomCountdownView.infoLable.attributedText = attriStringCheckoutTitle(frontString: kPDCheckNowString, oldString: String(format: "%@%.2f",getCurrentPayCharacter(),Double(data.SharePrice)))
            if data.IsTime == 1 { //随机倒计时
                self.normalCountdownView.priceLabel.attributedText = FPSalePriceText.fayparkFontColorAttribute(price: String(format: "%@%.2f  ",getCurrentPayCharacter(),data.SharePrice), oldPrice: String(format: "%@%.2f",getCurrentPayCharacter(),data.Msrp), priceColor: UIColor.white, oldPriceColor: kRGBColorFromHex(rgbValue: 0xbbbbbb), priceFont: kSizeFrom750(x: 50), oldPriceFont: kSizeFrom750(x: 24))
            } else if data.IsTime == 2 { //正常倒计时
                self.normalCountdownView.priceLabel.attributedText = FPSalePriceText.fayparkFontColorAttribute(price: String(format: "%@%.2f  ",getCurrentPayCharacter(),data.SharePrice), oldPrice: String(format: "%@%.2f",getCurrentPayCharacter(),data.Msrp), priceColor: UIColor.white, oldPriceColor: kRGBColorFromHex(rgbValue: 0xbbbbbb), priceFont: kSizeFrom750(x: 50), oldPriceFont: kSizeFrom750(x: 24))
            } else if data.IsTime == 3 { //正常
                self.priceLabel.attributedText = FPSalePriceText.fayparkFontColorAttribute(price: String(format: "%@%.2f  ",getCurrentPayCharacter(),data.SharePrice), oldPrice: String(format: "%@%.2f",getCurrentPayCharacter(),data.Msrp), priceColor: kRGBColorFromHex(rgbValue: 0xe9668f), oldPriceColor: kRGBColorFromHex(rgbValue: 0xbbbbbb), priceFont: kSizeFrom750(x: 50), oldPriceFont: kSizeFrom750(x: 24))// 显示优惠价格
                let discountStr = (1 - (Float(data.SharePrice) / Float(data.Msrp))) * 100
                if discountStr == 100 {
                    self.discountBtn.setTitle(String(format: "  \(kHomeFree)", discountStr), for: .normal)
                } else {
                    self.discountBtn.setTitle(String(format: "  -%.0f%%", discountStr), for: .normal)
                }
            }
            if (data.Number > 0 || data.IsTime == 1 || data.IsTime == 2) {
                setupCountSownManager()
            }
        }
        self.layoutIfNeeded()
    }

    @objc func removeTimerCount() {
        if let productData = self.productData {
            //随机倒计时 正常倒计时
            if (productData.IsTime == 1 || productData.IsTime == 2) {
                if productData.Number > 0 {
                    if self.selectTimerBlock != nil {
                        self.selectTimerBlock!()
                    }
                    TimeCountDownManager.manager.cancelTask(currentTimeCountIdentifier)
                    NotificationCenter.default.removeObserver(self)
                }
            }
        }
    }
    
    func setupCountSownManager() {
        //倒计时商品倒计时identier
        currentTimeCountIdentifier = productTimeCountIdentifier
        if self.productData.IsTime == 1 {
            currentTimeCountIdentifier = randomTimeCountIdentifier
        }else if self.productData.IsTime == 2 {
            currentTimeCountIdentifier = activityTimeCountIdentifier
        }
        //有倒计时商品
        let countDownInter = TimeInterval(self.productData.Number)
        TimeCountDownManager.manager.scheduledCountDownWith(currentTimeCountIdentifier, timeInteval: countDownInter, countingDown: { (timeInterval) in
            var hourTime:String
            var minuteTime:String
            var minute:String
            let time = Int(timeInterval)
            
            if time / 3600  < 10 {
                hourTime = String(format: "0%ld", time / 3600)
            }else{
                hourTime = String(format: "%ld", time / 3600)
            }
            if self.productData.IsTime == 1 {
                if time / 60 < 10 {
                    minuteTime = String(format: "0%ld", time / 60)
                } else {
                    minuteTime = String(format: "%ld", time / 60)
                }
            }else{
                if time / 60 % 60 < 10 {
                    minuteTime = String(format: "0%ld", time / 60 % 60)
                } else {
                    minuteTime = String(format: "%ld", time / 60 % 60)
                }
            }
            
            if time % 60 < 10 {
                minute = String(format: "0%ld", time % 60)
            } else {
                minute = String(format: "%ld", time % 60)
            }
            self.normalCountdownView.hourLable.text = hourTime
            self.normalCountdownView.minuteLable.text = minuteTime
            self.normalCountdownView.secondLable.text = minute
            self.randomCountdownView.minuteLable.text = minuteTime
            self.randomCountdownView.secondLable.text = minute
            
        }) { (timeInterval) in
            if self.selectTimerBlock != nil {
                self.selectTimerBlock!()
            }
            TimeCountDownManager.manager.cancelTask(self.currentTimeCountIdentifier)
            self.normalCountdownView.hourLable.text = "00"
            self.normalCountdownView.minuteLable.text = "00"
            self.normalCountdownView.secondLable.text = "00"
            self.randomCountdownView.minuteLable.text = "00"
            self.randomCountdownView.secondLable.text = "00"
        }
    }

    @objc func reviewsTouch(){
        if (self.selectBlock != nil) {
            self.selectBlock!()
        }
    }
    
    func textHeight(title:String,classWidth:CGFloat)->CGSize{
        let size = CGSize(width: CGFloat(MAXFLOAT), height: classWidth)
        let textFont = UIFont.systemFont(ofSize: kSizeFrom750(x: 30))
        let textLabelSize = textSize(text:title , font: textFont, maxSize: size)
        return textLabelSize
    }
    
    func textSize(text : String , font : UIFont , maxSize : CGSize) -> CGSize{
        let options : NSStringDrawingOptions = .usesLineFragmentOrigin
        let dic = NSDictionary(object: font, forKey: NSAttributedStringKey.font as NSCopying)
        return text.boundingRect(with: maxSize, options: options, attributes: dic as? [NSAttributedStringKey : Any], context: nil).size
    }
    //根据正则表达式改变文字颜色
    func changeTextChange(regex: String, text: String, color: UIColor,font: UIFont) -> NSMutableAttributedString {
    
        let attributeString = NSMutableAttributedString(string: text)
        do {
            let regexExpression = try NSRegularExpression(pattern: regex, options: NSRegularExpression.Options())
            let result = regexExpression.matches(in: text, options: NSRegularExpression.MatchingOptions(), range: NSMakeRange(0, text.count))
            for item in result {
                attributeString.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: item.range)
                attributeString.addAttribute(NSAttributedStringKey.font, value: font, range: item.range)
            }
        } catch {
            print("Failed with error: \(error)")
        }
        return attributeString
    }
    
    func attriStringCheckoutTitle(frontString:String,oldString:String)-> NSMutableAttributedString{
        
        let frontAttriString = NSMutableAttributedString.init(string: frontString)
        frontAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0xffffff), NSAttributedStringKey.font : kGothamRoundedFontSize(size: kSizeFrom750(x: 28))], range: NSRange(location: 0, length: frontAttriString.length))
        
        let lastAttriString = NSMutableAttributedString.init(string: oldString)
        lastAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0xffffff), NSAttributedStringKey.font : kHelveticaFontSize(size: kSizeFrom750(x: 48))], range: NSRange(location: 0, length: lastAttriString.length))

        let mutableAttributeString = NSMutableAttributedString.init()
        mutableAttributeString.append(frontAttriString)
        mutableAttributeString.append(lastAttriString)

        return mutableAttributeString
    }
}

