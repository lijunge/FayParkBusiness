//
//  NewProductDetailcView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewProductDetailcView: UITableView {

    var productDetailInfo: ProductDetailInfo!
    var commentListArr = [CommentInfo]()
    var productCommentInfo: ProductCommentList?
   
    typealias SelectInfoReviews =  (Int) -> Void
    var selectInfo:SelectInfoReviews?
    
    typealias SelectLikeReviews = (Int) -> Void
    var selectLike:SelectLikeReviews?
    
    typealias SelectViewMore = ()->Void
    var selectViewMore:SelectViewMore?
    
    func selectInfoBlock(block:@escaping SelectInfoReviews) {
        self.selectInfo = block;
    }
    
    func selectLikeBlock(block:@escaping SelectLikeReviews) {
        self.selectLike = block;
    }
    func selectMoreBlock(block:@escaping SelectViewMore) {
        self.selectViewMore = block;
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.dataSource = self
        self.delegate = self
        self.estimatedSectionFooterHeight = 0
        self.estimatedSectionHeaderHeight = 0
        self.separatorStyle = .none
        self.backgroundColor = kSpacingColor
        self.register(UINib.init(nibName: "CommentTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func addReviews(){//加载更多评论
        if selectViewMore != nil {
            self.selectViewMore!()
        }
    }
}

extension NewProductDetailcView: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 5 {
            if productDetailInfo != nil {
                if self.productDetailInfo.CommentCount > 3 {
                    return 2 + 3
                } else {
                    return 2 + Int(self.productDetailInfo.CommentCount)
                }
            }
            return 2
        }
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4 {
            if selectInfo != nil {
                self.selectInfo!(indexPath.section)
            }
        } else if indexPath.section == 6 {
            if selectLike != nil {
                self.selectLike!(indexPath.section)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {//顶部
            if self.productDetailInfo != nil {
                let cell = NewProductDetailcCell()
                return cell.getProductCellHeight(data: productDetailInfo)
            }
            return 0
        }else if indexPath.section == 1 {//商品描述
            if self.productDetailInfo != nil {
                return NewProductStylishCell().getStyleHeight(data: self.productDetailInfo)
            }
            return kSizeFrom750(x: 535)
        } else if indexPath.section == 5 {
            if self.commentListArr.count >= 3 {
                if indexPath.row > 0 && indexPath.row <= 3 { //正常评论cell
                    if self.commentListArr.count != 0 {
                        return NewProductReviewsCell().getCommentCellHeight(comment: self.commentListArr[indexPath.row-1])
                    }
                    return 0
                } else { // 顶部提示评论 和 底部 viewmore
                    return kSizeFrom750(x: 160)
                }
            } else {
                if indexPath.row == 0 || indexPath.row == self.commentListArr.count + 1{
                    return kSizeFrom750(x: 160) //顶部提示
                } else {
                    if self.commentListArr.count != 0 {
                        return NewProductReviewsCell().getCommentCellHeight(comment: self.commentListArr[indexPath.row-1])
                    }
                    return 0
                }
            }
        } else if indexPath.section == 6 {//猜你喜欢
            return kSizeFrom750(x: 175)
        } else { //shipping
            return kSizeFrom750(x: 95)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 { //顶部
            let cell = NewProductDetailcCell.DetalicCell(tableView: tableView)
            cell.selectTimerBlock = {()
                cell.normalCountdownView.isHidden = true
                cell.randomCountdownView.isHidden = true//隐藏视图
                self.productDetailInfo.IsTime = 0
                self.productDetailInfo.Number = 0
                self.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .none)
            }
            if productDetailInfo != nil {
                cell.normalCountdownView.isHidden = false
                cell.randomCountdownView.isHidden = false
                cell.updateProductCell(data: productDetailInfo)
            } else {
                cell.normalCountdownView.isHidden = true
                cell.randomCountdownView.isHidden = true//隐藏视图
            }
            cell.selectBlockAction {
                let indx = NSIndexPath.init(row: 0, section: 5)
                self.selectRow(at: indx as IndexPath, animated: true, scrollPosition: .top)//将cell移动到顶端
            }
            cell.shareBtn.isHidden = true
            return cell
        }else if  indexPath.section == 1 { //商品描述
            let cell = NewProductStylishCell.StylishCell(tableView: tableView)
            if productDetailInfo != nil {
                cell.updateStyleDatas(data: productDetailInfo)
            }
            return cell
        }else if indexPath.section == 5 { //评论
            var rowNumber:Int = 2
            if productDetailInfo != nil {
                if self.productDetailInfo.CommentCount >= 3 {
                    rowNumber =  5
                } else {
                    rowNumber = 2 + Int(self.productDetailInfo.CommentCount)
                }
                if indexPath.row == 0 { //顶部
                    let cell = NewReviewsCell.ReviewsViewCell(tableView: tableView)
                    cell.setCommentAvg(str1: productDetailInfo.CommentAvg)
                    cell.moreBtn.isHidden = true
                    return cell
                } else if indexPath.row == rowNumber - 1{
                    let cell = NewReviewsCell.ReviewsViewCell(tableView: tableView)
                    cell.reviewsLable.isHidden = true
                    cell.scoreLable.isHidden = true
                    cell.scoreImg.isHidden = true
                    if self.productDetailInfo.CommentCount == 0 {
                        cell.moreBtn.setTitle(kPDNoReviewsString, for: .normal)
                        cell.moreBtn.isUserInteractionEnabled = false
                    }else{
                        cell.moreBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
                        cell.moreBtn.layer.borderWidth = kSizeFrom750(x: 1)
                        cell.moreBtn.addTarget(self, action: #selector(addReviews), for: .touchUpInside)
                    }
                    return cell
                } else {
                    let cell = NewProductReviewsCell.ReviewsCell(tableView: tableView)
                    cell.selectionStyle = .none
                    if let productInfo = self.productCommentInfo {
                        self.commentListArr = productInfo.ProductCommentList
                        cell.updateCommentCellData(comment: self.commentListArr[indexPath.row-1])
                    }
                    if (self.productCommentInfo?.CommentCount)! > 3 {
                        if indexPath.row != 3 {
                            cell.viewlines.backgroundColor = kSpacingColor
                        }
                    }else{
                        if indexPath.row != self.productDetailInfo.CommentCount {
                            cell.viewlines.backgroundColor = kSpacingColor
                        }
                    }
                    return cell
                }
            }
            return UITableViewCell.init()
        } else if indexPath.section == 6 {//猜你喜欢
            let cell = NewProductInfoCell.ProductInfo(tableView: tableView)
            cell.titleLable.isHidden = true
            return cell
        } else {
            let cell = NewProductInfoCell.ProductInfo(tableView: tableView)
            cell.accessoryType = .disclosureIndicator
            cell.detailTextLabel?.text = kPDMoreString
            cell.detailTextLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 26))
            if indexPath.section == 3 {
                cell.titleLable.attributedText = changeTextChange(regex: kPDHundredPercendString, text: "\(kPDHundredPercendString)\(kPDSecurityPaymentString)", color: kRGBColorFromHex(rgbValue: 0x000000), font: kGothamRoundedFontSize(size: kSizeFrom750(x: 32)))
                
            }else if indexPath.section == 4 {
                cell.titleLable.attributedText = changeTextChange(regex: kPDSixtyDaysString, text: "\(kPDRefundPolicyString) \(kPDSixtyDaysString)", color: kRGBColorFromHex(rgbValue: 0x000000), font: kGothamRoundedFontSize(size: kSizeFrom750(x: 32)))
            }else {
                cell.titleLable.attributedText = changeTextChange(regex: kPDTenDaysString, text: "\(kPDShippingInfoString)\(kPDTenDaysString)", color: kRGBColorFromHex(rgbValue: 0x000000), font: kGothamRoundedFontSize(size: kSizeFrom750(x: 32)))
            }
            cell.alsoLikeLabel.isHidden = true
            cell.enterLable.isHidden = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 3 || section == 4 || section == 2 {
            return kSizeFrom750(x: 10)
        } else if section == 0 || section == 1 || section == 5{
            return kSizeFrom750(x: 14)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
