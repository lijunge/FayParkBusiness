//
//  NewProductInfoCell.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewProductInfoCell: UITableViewCell {

    class func ProductInfo(tableView:UITableView)->NewProductInfoCell {
        let cell = NewProductInfoCell.init(style: UITableViewCellStyle.value1, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.contentView.addSubview(self.titleLable)
        //self.contentView.addSubview(self.rightImg)
        //-----------------------------------------//
        self.contentView.addSubview(self.alsoLikeLabel)
        self.contentView.addSubview(self.enterLable)
        makeConstraint()
    }
    func makeConstraint() {
        self.titleLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
        }
        self.titleLable.setContentHuggingPriority(.required, for: .horizontal)
        
        //        self.rightImg.mas_makeConstraints { (mark) in
        //            mark?.centerY.equalTo()(self)
        //            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 30))
        //            mark?.width.height().equalTo()(kSizeFrom750(x: 30))
        //        }
        //
        self.alsoLikeLabel.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 42))
            mark?.centerX.equalTo()(self)
        }
        self.alsoLikeLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        self.enterLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.alsoLikeLabel.mas_bottom)?.offset()(kSizeFrom750(x: 28))
            mark?.centerX.equalTo()(self)
        }
        self.enterLable.setContentHuggingPriority(.required, for: .horizontal)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    lazy var titleLable: UILabel = {
        let tempTitle = UILabel.init()
        tempTitle.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 34))
        tempTitle.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempTitle.text = "\(kPDShippingInfoString)\(kPDTenDaysString)"
        return tempTitle
    }()
    
//    lazy var rightImg: UIImageView = {
//        let tempRightImg = UIImageView.init()
//        tempRightImg.image = kImage(iconName: "arrowrighticon")
//        return tempRightImg
//    }()
    
    //MARK:----------添加 “猜你喜欢” 视图
    lazy var alsoLikeLabel: UILabel = {
        let tempAlso = UILabel.init()
        tempAlso.font = UIFont.init(name: kGothamRoundedBold, size: kSizeFrom750(x: 40))
        tempAlso.text = kPDAlsoLikeTitleString
        tempAlso.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        return tempAlso
    }()
    
    lazy var enterLable: UILabel = {
        let tempEnterLable = UILabel.init()
        tempEnterLable.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        tempEnterLable.textColor =  kRGBColorFromHex(rgbValue: 0x0000000)
        tempEnterLable.text = kHomeEnterString
        return tempEnterLable
    }()
    
}
