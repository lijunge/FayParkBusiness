//
//  ShippingView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class ShippingView: UIView {
    
    let title : String = kPDShippingViewTitleString
    let listArr : [[String]] = [["",
                                 kPDShippingViewString2,
                                 kPDShippingViewString3],
                                [kPDShippingViewString4,
                                 kPDShippingViewString5,
                                 kPDShippingViewString6],
                                [kPDShippingViewString7,
                                 kPDShippingViewString8,
                                 "\(kPDShippingViewString9) \(getCurrentPayCharacter())\(kPDShippingViewString10)"]]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configCommonEventMonitor(monitorString: kShippingInfoEvent, type: 2)
        self.addSubview(titleLb)
        self.addSubview(listView)
        
        for i in 0 ..< listArr.count {
            let itemArr = listArr[i];
            let itemH : CGFloat = i == 0 ? kSizeFrom750(x: 82) : (i == 1 ? kSizeFrom750(x: 125) : kSizeFrom750(x: 92))
            let itemW : CGFloat = (KScreenWidth - kSizeFrom750(x: 68) - 4.0)/3.0
            let topY : CGFloat = i == 0 ? 1.0 : (i == 1 ? 2.0 + kSizeFrom750(x: 82) : 3.0 + kSizeFrom750(x: 125) + kSizeFrom750(x: 82))
            for j in 0 ..< itemArr.count {
                let textLb = UILabel.init()
                textLb.numberOfLines = 0
                textLb.textAlignment = .center
                textLb.text = itemArr[j]
                textLb.backgroundColor = i == 0 ? kRGBColorFromHex(rgbValue: 0xf7f7f7) : UIColor.white
                textLb.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 24))
                textLb.textColor = kRGBColorFromHex(rgbValue: 0x000000)
                listView.addSubview(textLb)
                
                textLb.mas_makeConstraints({ (mark) in
                    mark?.top.equalTo()(listView)?.offset()(topY)
                    mark?.left.equalTo()(listView)?.offset()(1.0 + CGFloat(j) * (1.0 + itemW))
                    mark?.size.mas_equalTo()(CGSize.init(width: itemW, height: itemH))
                })
            }
        }
        makeConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func makeConstraint() {
        titleLb.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 34))
            mark?.right.equalTo()(self)?.offset()(kSizeFrom750(x: -34))
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 40))
        }
        
        listView.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(titleLb.mas_bottom)?.offset()(kSizeFrom750(x: 40))
            mark?.left.right().equalTo()(titleLb)
            mark?.height.mas_equalTo()(kSizeFrom750(x: 82+125+92)+4.0)
        }
    }
    
    lazy var titleLb: UILabel = {
        let lb = UILabel.init()
        lb.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        lb.numberOfLines = 0
        lb.attributedText = String.getAttributeString(str: title, lineSpace: kSizeFrom750(x: 20), font: kHelveticaFontSize(size: kSizeFrom750(x: 30)))
        return lb
    }()
    
    lazy var listView: UIView = {
        let tmpV = UIView.init()
        tmpV.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        return tmpV
    }()
}
