//
//  RefoundView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class RefoundView: UIView {
    
    let cellIdentifier = "cellIdentifier"
    
    var contentArr : [String] = [
        kPDRefundViewString1,
        kPDRefundViewString2,
        kPDRefundViewString3,
        kPDRefundViewString4,
        kPDRefundViewString5,
        kPDRefundViewString6,
        kPDRefundViewString7,
        kPDRefundViewString8]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configCommonEventMonitor(monitorString: kRefundPolicyEvent, type: 2)
        self.addSubview(tableView)
        makeConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func makeConstraint() {
        tableView.mas_makeConstraints { (mark) in
            mark?.edges.equalTo()(self)?.insets()(UIEdgeInsetsMake(kSizeFrom750(x: 20), 0, 0, 0))
        }
    }
    
    lazy var tableView : UITableView = {
        let tmpTableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        tmpTableView.delegate = self
        tmpTableView.dataSource = self
        tmpTableView.estimatedSectionFooterHeight = 0
        tmpTableView.estimatedSectionHeaderHeight = 0
        tmpTableView.separatorStyle = .none
        tmpTableView.backgroundColor = UIColor.white
        tmpTableView.showsVerticalScrollIndicator = false
        return tmpTableView
    }()
}

extension RefoundView : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.backgroundColor = UIColor.white
        cell?.selectionStyle = .none
        cell?.imageView?.image = nil
        cell?.textLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 28))
        cell?.textLabel?.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        
        cell?.imageView?.image = DrawPointImage.getBlackPointImage()
        cell!.imageView!.mas_makeConstraints({ (mark) in
            mark?.left.mas_equalTo()(kSizeFrom750(x: 34))
            mark?.centerY.equalTo()(cell)
            mark?.size.mas_equalTo()(CGSize.init(width: kSizeFrom750(x: 8), height: kSizeFrom750(x: 8)))
        })
        cell!.textLabel!.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(cell?.imageView?.mas_right)?.offset()(kSizeFrom750(x: 16))
            mark?.centerY.equalTo()(cell)
        }
        cell?.textLabel?.text = contentArr[indexPath.row]
        
        return cell!
    }
}

extension RefoundView : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kSizeFrom750(x: 70)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return heighForAttributeString(str: kPDRefundViewFooterString) + kSizeFrom750(x: 40)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heighForAttributeString(str: kPDRefundViewHeaderTitleString) + kSizeFrom750(x: 40)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let head = UIView.init()
        let lable = UILabel.init()
        head.addSubview(lable)
        
        lable.attributedText = getAttributeString(str: kPDRefundViewHeaderTitleString)
        lable.numberOfLines = 0
        lable.mas_makeConstraints { (mark) in
            mark?.left.mas_equalTo()(kSizeFrom750(x: 34))
            mark?.right.mas_equalTo()(kSizeFrom750(x: -34))
            mark?.top.mas_equalTo()(kSizeFrom750(x: 20))
        }
        return head
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let foot = UIView.init()
        let lable = UILabel.init()
        foot.addSubview(lable)
        
        lable.attributedText = getAttributeString(str: kPDRefundViewFooterString)
        lable.numberOfLines = 0
        lable.mas_makeConstraints { (mark) in
            mark?.left.mas_equalTo()(kSizeFrom750(x: 34))
            mark?.right.mas_equalTo()(kSizeFrom750(x: -34))
            mark?.top.mas_equalTo()(kSizeFrom750(x: 20))
        }
        return foot
    }
    
    private func getAttributeString(str:String) -> NSAttributedString {
        
        return String.getAttributeString(str: str, lineSpace: kSizeFrom750(x: 20), font: kHelveticaFontSize(size: kSizeFrom750(x: 30)))
    }
    
    private func heighForAttributeString(str:String) -> CGFloat {
        
        return String.getHeighForAttributeString(str: getAttributeString(str: str), textWidth: KScreenWidth - kSizeFrom750(x: 68))
    }
}
