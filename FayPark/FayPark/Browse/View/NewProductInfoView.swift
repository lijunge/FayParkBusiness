//
//  NewProductInfoView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewProductInfoView: UIView {
    var modelStr : String? {
        didSet {
            print(modelStr!)
        }
    }
    init(frame: CGRect,type:Int) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        switch type {
        case 2:
            self.addSubview(paymentView)
        case 3:
            self.addSubview(refoundView)
        case 4:
            self.addSubview(shippingView)
        default: break
            
        }
    }
    lazy var paymentView : PaymentView = {
        let tmpPayV = PaymentView.init(frame: self.bounds)
        return tmpPayV
    }()
    
    lazy var shippingView: ShippingView = {
        let tmpShipV = ShippingView.init(frame: self.bounds)
        return tmpShipV
    }()
    
    lazy var refoundView: RefoundView = {
        let tmpRefoundV = RefoundView.init(frame: self.bounds)
        return tmpRefoundV
    }()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
