//
//  NewDetailcHeaderView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

/**
 * 随机倒计时页面
 */
class NewDetailcHeaderView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.minuteLable)
        self.addSubview(self.symbolLable)
        self.addSubview(self.secondLable)
        self.addSubview(self.infoLable)
        self.addSubview(self.triangleImage)
        makeConstraint()
    }
    func makeConstraint() {
        self.minuteLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 32))
            mark?.width.equalTo()(kSizeFrom750(x: 64))
            mark?.height.equalTo()(kSizeFrom750(x: 46))
        }
        
        self.symbolLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self.minuteLable.mas_right)
            mark?.width.equalTo()(kSizeFrom750(x: 12))
        }
        
        self.secondLable.mas_makeConstraints { (mark) in
            mark?.left.equalTo()(self.symbolLable.mas_right)
            mark?.centerY.equalTo()(self)
            mark?.width.equalTo()(kSizeFrom750(x: 64))
            mark?.height.equalTo()(kSizeFrom750(x: 46))
        }
        
        self.infoLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self.secondLable.mas_right)?.offset()(kSizeFrom750(x: 12))
        }
        self.infoLable.setContentHuggingPriority(.required, for: .horizontal)
        
        
        self.triangleImage.mas_makeConstraints { (mark) in
            mark?.bottom.equalTo()(self.mas_bottom)?.offset()(kSizeFrom750(x: 2))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 235))
            mark?.height.equalTo()(kSizeFrom750(x: 20))
            mark?.width.equalTo()(kSizeFrom750(x: 35))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    lazy var inLable: UILabel = {
        let tempInLable = UILabel.init()
        tempInLable.text = ""
        tempInLable.textAlignment = .center
        return lableInfoStyle(lable: tempInLable)
    }()
    
    lazy var hourLable: UILabel = {
        let tempHourLable = UILabel.init()
        tempHourLable.textAlignment = .center
        return lableTimerStyle(lable: tempHourLable)
    }()
    
    lazy var minuteLable: UILabel = {
        let tmepMinuteLable = UILabel.init()
        tmepMinuteLable.textAlignment = .center
        return lableTimerStyle(lable: tmepMinuteLable)
    }()
    
    lazy var secondLable: UILabel = {
        let tempSecondLable = UILabel.init()
        tempSecondLable.textAlignment = .center
        return lableTimerStyle(lable: tempSecondLable)
    }()
    
    lazy var symbolLable: UILabel = {
        let tempSymbolLable = UILabel.init()
        tempSymbolLable.text = ":"
        tempSymbolLable.textAlignment = .center
        return lableInfoStyle(lable: tempSymbolLable)
    }()
    
    lazy var infoLable: UILabel = {
        let tempInfoLable = UILabel.init()
        return lableInfoStyle(lable: tempInfoLable)
    }()

    lazy var triangleImage: UIImageView = {
        let tempImage = UIImageView.init()
        tempImage.image = kImage(iconName: "product_triangle_img")
        return tempImage
    }()
    
    func lableInfoStyle(lable:UILabel)->UILabel {
        lable.textColor = UIColor.white
        lable.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 27))
        return lable
    }
    func lableTimerStyle(lable:UILabel)->UILabel {
        lable.backgroundColor = UIColor.white
        lable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 35))
        return lable
    }
}
/**
 * 倒计时商品
 */
class NewDetailcExpiredImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.priceLabel)
        self.addSubview(self.infoLable)
        self.addSubview(self.hourLable)
        self.addSubview(self.secondLable)
        self.addSubview(self.symbolLable1)
        self.addSubview(self.symbolLable)
        self.addSubview(self.minuteLable)
        self.addSubview(self.expiredLable)
        makeConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func makeConstraint() {
        self.priceLabel.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 32))
        }
        self.priceLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        self.infoLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self.priceLabel.mas_right)?.offset()(kSizeFrom750(x: 16))
        }
        self.infoLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.hourLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.width.equalTo()(kSizeFrom750(x: 64))
            mark?.height.equalTo()(kSizeFrom750(x: 46))
            mark?.left.equalTo()(self.infoLable.mas_right)?.offset()(kSizeFrom750(x: 10))
        }
        
        self.expiredLable.mas_makeConstraints { (mark) in
            mark?.centerY.left().height().equalTo()(self.hourLable)
            mark?.width.equalTo()(kSizeFrom750(x: 140))
        }
        self.expiredLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.symbolLable.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self.hourLable.mas_right)
        }
        self.symbolLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.minuteLable.mas_makeConstraints { (mark) in
            mark?.centerY.width().height().equalTo()(self.hourLable)
            mark?.left.equalTo()(self.symbolLable.mas_right)
        }
        
        self.symbolLable1.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self)
            mark?.left.equalTo()(self.minuteLable.mas_right)
        }
        self.symbolLable1.setContentHuggingPriority(.required, for: .horizontal)
        
        self.secondLable.mas_makeConstraints { (mark) in
            mark?.width.height().centerY().equalTo()(self.minuteLable)
            mark?.left.equalTo()(self.symbolLable1.mas_right)
        }
    }
    // 价格
    lazy var priceLabel:UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 50))
        return tempLable
    }()
    //  Finish in
    lazy var infoLable: UILabel = {
        let tempInfoLable = UILabel.init()
        tempInfoLable.text = kRecommendFinishInString
        return lableInfoStyle(lable: tempInfoLable)
    }()
    // 时
    lazy var hourLable: UILabel = {
        let tempHourLable = UILabel.init()
        tempHourLable.textAlignment = .center
        return lableTimerStyle(lable: tempHourLable)
    }()
    // 分
    lazy var minuteLable: UILabel = {
        let tmepMinuteLable = UILabel.init()
        tmepMinuteLable.textAlignment = .center
        return lableTimerStyle(lable: tmepMinuteLable)
    }()
    // 秒
    lazy var secondLable: UILabel = {
        let tempSecondLable = UILabel.init()
        tempSecondLable.textAlignment = .center
        return lableTimerStyle(lable: tempSecondLable)
    }()
    
    lazy var symbolLable: UILabel = {
        let tempSymbolLable = UILabel.init()
        tempSymbolLable.text = ":"
        tempSymbolLable.textAlignment = .center
        return lableInfoStyle(lable: tempSymbolLable)
    }()
    
    lazy var symbolLable1: UILabel = {
        let tempSymbolLable = UILabel.init()
        tempSymbolLable.text = ":"
        tempSymbolLable.textAlignment = .center
        return lableInfoStyle(lable: tempSymbolLable)
    }()
    
    lazy var expiredLable: UILabel = {
        let lable = UILabel.init()
        lable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 24))
        lable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        lable.backgroundColor = UIColor.white
        lable.textAlignment = .center
        return lable
    }()
    
    func lableInfoStyle(lable:UILabel)->UILabel {
        lable.textColor = UIColor.white
        lable.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 27))
        return lable
    }
    func lableTimerStyle(lable:UILabel)->UILabel {
        lable.backgroundColor = UIColor.white
        lable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 35))
        return lable
    }
    
}
