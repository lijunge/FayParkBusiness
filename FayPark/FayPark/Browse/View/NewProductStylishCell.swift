//
//  NewProductStylishCell.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewProductStylishCell: UITableViewCell {
    let styleFont = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 28))
    let infoFont = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 40))
    let detailFont = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 38))
    
    class func StylishCell(tableView:UITableView)->NewProductStylishCell {
        let cell = NewProductStylishCell.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        return cell
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.addSubview(infoTitleLable)
        self.addSubview(detailsLable)
        self.addSubview(styleLables)
        makeConstraints()
        self.tag = 10000
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func makeConstraints() {
        infoTitleLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 10))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 28))
        }
        infoTitleLable.setContentHuggingPriority(.required, for: .horizontal)
        
        detailsLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(infoTitleLable.mas_bottom)?.offset()(kSizeFrom750(x: 32))
            mark?.left.right().equalTo()(infoTitleLable)
        }
        detailsLable.setContentHuggingPriority(.required, for: .horizontal)
        
        styleLables.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(detailsLable.mas_bottom)?.offset()(kSizeFrom750(x: 28))
            mark?.left.equalTo()(detailsLable)
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 28))
        }
        styleLables.setContentHuggingPriority(.required, for: .horizontal)
    }

    lazy var infoTitleLable: UILabel = {
        let tempInfo = UILabel.init()
        return styleLable(lable: tempInfo, color:kRGBColorFromHex(rgbValue: 0x333333), font: infoFont)
    }()
    
    lazy var detailsLable: UILabel = {
        let tempDetails = UILabel.init()
        tempDetails.text = kPDDescriptionString
        return styleLable(lable: tempDetails, color: kRGBColorFromHex(rgbValue: 0x555555), font: detailFont)
    }()
    
    lazy var styleLables: UILabel = {
        let tempStyleLable = UILabel.init()
        return styleLable(lable: tempStyleLable, color: kRGBColorFromHex(rgbValue: 0x7c7c7c), font: styleFont)
    }()
    
    //MARK:-----------
    func styleLable(lable:UILabel,color:UIColor,font:UIFont!) -> UILabel {
        lable.font = font
        lable.textColor = color
        lable.numberOfLines = 0
        return lable
    }
    
    func updateStyleDatas(data: ProductDetailInfo) {
        self.styleLables.attributedText = String.getAttributeString(str: (data.ProductDescription.replacingOccurrences(of: "<br>", with: "\n")), lineSpace: kSizeFrom750(x: 16), font: self.styleLables.font)
        self.infoTitleLable.text = data.ProductTitle
        self.styleLables.superview?.layoutIfNeeded()
    }
    
    func getStyleHeight(data: ProductDetailInfo) -> CGFloat {
        let stryleWidth = KScreenWidth - 2 * kSizeFrom750(x: 28)
        let infoHeight = kSizeFrom750(x: 10) + data.ProductTitle.getHeightWithConstrainedWidth(width: stryleWidth, font: infoFont!)
        let detailHeght = infoHeight + kSizeFrom750(x: 32) + kPDDescriptionString.getHeightWithConstrainedWidth(width: stryleWidth, font: detailFont!)
        let attributeText = String.getAttributeString(str: (data.ProductDescription.replacingOccurrences(of: "<br>", with: "\n")), lineSpace: kSizeFrom750(x: 16), font: styleFont!)
        return  detailHeght + kSizeFrom750(x: 28) + kSizeFrom750(x: 30) + String.getHeighForAttributeString(str: attributeText, textWidth: stryleWidth)
    }
}
