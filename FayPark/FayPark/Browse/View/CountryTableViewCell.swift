//
//  CountryTableViewCell.swift
//  FayPark
//
//  Created by faypark on 2018/8/2.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var leftImageView: UIImageView!
    
    @IBOutlet weak var countryLabel: UILabel!
    
    @IBOutlet weak var detailImgView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
