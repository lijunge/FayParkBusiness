//
//  BrowseHeaderCollectionCell.swift
//  FayPark
//
//  Created by faypark on 2018/6/6.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SDWebImage
class BrowseHeaderCollectionCell: UICollectionViewCell {

    @IBOutlet weak var backImgView: UIImageView!
    
    @IBOutlet weak var backBottomView: UIView!
    
    //优惠价
    @IBOutlet weak var preferentialPriceLabel: UILabel!
    //原价
    @IBOutlet weak var originalPriceLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCollectionCell(browseModel: BrowseActivityInfo) {
        let placeHolderImage = creatImageWithColor(color: kSpacingColor)
        self.backImgView.sd_setImage(with: URL(string: browseModel.MainImgLink), placeholderImage: placeHolderImage, options: .avoidAutoSetImage) { (image, error, cacheType, imageUrl) in
                DispatchQueue.main.async(execute: {
                    let tmpImage = image?.resizeImage(reSize: CGSize(width: self.backImgView.frame.size.width, height: self.backImgView.frame.size.height))
                    self.backImgView.contentMode = .scaleAspectFit
                    self.backImgView.image = tmpImage
                })
        }
        let moneyCharacter = getCurrentPayCharacter()
        let sharePriceString = String(format: "%@ %.2f",moneyCharacter,Double(browseModel.SharePrice)!)
        self.preferentialPriceLabel.attributedText = changeTextChange(regex: moneyCharacter, text: sharePriceString, color: kRGBColorFromHex(rgbValue: 0x333333), font: UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 32))!)
        
        let oldString = String(format: "%@ %.2f",moneyCharacter,Double(browseModel.Msrp)!)
        let str2 = NSMutableAttributedString.init(string: oldString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        self.originalPriceLabel.attributedText = str2
        
        self.likeButton.setTitle(browseModel.CollCount, for: .normal)
    
        if browseModel.IsColl == "1" {
            self.likeButton.isSelected = true
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}
