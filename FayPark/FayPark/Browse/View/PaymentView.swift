//
//  PaymentView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
class PaymentView: UIView {
    
    let cellIdentifier = "cellIdentifier"
    
    var contentArr : [String] = [
        kPDPayViewString1,
        kPDPayViewString2,
        kPDPayViewString3,
        kPDPayViewString4,
        kPDPayViewString5,
        kPDPayViewString6]
    
    let titleArr : [String] = [kPDPayViewNoteString,kPDPayViewPayPalString,kPDPayViewNoteString]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configCommonEventMonitor(monitorString: kSecurityPamentEvent, type: 2)
        self.addSubview(tableView)
        makeConstraint()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func makeConstraint() {
        tableView.mas_makeConstraints { (mark) in
            mark?.edges.equalTo()(self)?.insets()(UIEdgeInsets.zero)
        }
    }    
    lazy var tableView : UITableView = {
        let tmpTableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        tmpTableView.delegate = self
        tmpTableView.dataSource = self
        tmpTableView.estimatedSectionFooterHeight = 0
        tmpTableView.estimatedSectionHeaderHeight = 0
        tmpTableView.tableHeaderView = tableHeaderView
        tmpTableView.separatorStyle = .none
        tmpTableView.backgroundColor = UIColor.white
        tmpTableView.showsVerticalScrollIndicator = false
        return tmpTableView
    }()
    
    lazy var tableHeaderView: PayTableHeadView = {
        let headerV = PayTableHeadView.init(frame: CGRect.init(x: 0, y: 0, width: KScreenWidth, height: kSizeFrom750(x: 292 + 22)))
        return headerV
    }()
}

extension PaymentView : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return contentArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.backgroundColor = UIColor.white
        cell?.selectionStyle = .none
        
        cell!.imageView?.image = UIImage.init()
        cell?.textLabel?.text = nil
        cell?.textLabel?.font = kGothamBoldFontSize(size: kSizeFrom750(x: 28))
        cell?.detailTextLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 28))
        cell?.detailTextLabel?.numberOfLines = 0
        cell?.detailTextLabel?.lineBreakMode = .byWordWrapping
        
        
        if indexPath.section < titleArr.count {
            cell?.textLabel?.text = titleArr[indexPath.section]
            cell?.detailTextLabel?.text = contentArr[indexPath.section]
            if indexPath.section == 1 {
                
                cell?.textLabel?.numberOfLines = 0
                
                let mutAttributeStr : NSMutableAttributedString = NSMutableAttributedString.init(string: (cell?.textLabel?.text)!)
                let attchment : NSTextAttachment = NSTextAttachment.init()
                attchment.image = imageWithImage(image: kImage(iconName: "address_paypal"), scaleSize: CGSize.init(width: kSizeFrom750(x: 200), height: kSizeFrom750(x: 50)))
                attchment.bounds = CGRect.init(x: kSizeFrom750(x: 100), y: kSizeFrom750(x: -15), width: kSizeFrom750(x: 200), height: kSizeFrom750(x: 50))
                let attributeStr : NSAttributedString = NSAttributedString.init(attachment: attchment)
                mutAttributeStr.append(attributeStr)
                cell?.textLabel?.attributedText = mutAttributeStr
            }
            
            cell!.textLabel!.mas_makeConstraints({ (mark) in
                mark?.left.mas_equalTo()(kSizeFrom750(x: 34))
                mark?.top.mas_equalTo()(kSizeFrom750(x: 16))
            })
            
            cell!.detailTextLabel!.mas_makeConstraints { (mark) in
                mark?.left.equalTo()(cell?.textLabel?.mas_left)?.offset()(kSizeFrom750(x: 8))
                mark?.top.equalTo()(cell?.textLabel?.mas_bottom)?.offset()(kSizeFrom750(x: 16))
                mark?.width.mas_equalTo()(KScreenWidth * 0.93)
            }
            cell!.imageView!.mas_makeConstraints({ (mark) in
                mark?.centerY.equalTo()(cell?.textLabel?.mas_centerY)
                mark?.left.equalTo()(cell)?.offset()(kSizeFrom750(x: 165))
                mark?.size.equalTo()(CGSize.init(width: kSizeFrom750(x: 200), height: kSizeFrom750(x: 65)))
            })
            
        }else {
            cell?.textLabel?.text = nil
            cell?.imageView?.image = DrawPointImage.getBlackPointImage()
            cell?.detailTextLabel?.text = contentArr[indexPath.section]
            cell!.imageView!.mas_makeConstraints({ (mark) in
                mark?.top.mas_equalTo()(kSizeFrom750(x: 8))
                mark?.left.mas_equalTo()(kSizeFrom750(x: 34))
                mark?.size.mas_equalTo()(CGSize.init(width: kSizeFrom750(x: 8), height: kSizeFrom750(x: 8)))
            })
            cell!.detailTextLabel!.mas_makeConstraints { (mark) in
                mark?.left.equalTo()(cell?.imageView?.mas_right)?.offset()(kSizeFrom750(x: 16))
                mark?.top.equalTo()(cell?.contentView)
                mark?.width.mas_equalTo()(KScreenWidth * 0.9)
            }
        }
        cell?.detailTextLabel?.attributedText = attributeStringFromString(str: (cell?.detailTextLabel?.text!)!)
        
        return cell!
    }
}

extension PaymentView : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section >= titleArr.count {
            
            let height = heighForCell(detailStr: contentArr[indexPath.section], textWidth: KScreenWidth * 0.91)
            return height
        }else {
            
            let detailStr = contentArr[indexPath.section]
            let height = heighForCell(detailStr: detailStr, textWidth: KScreenWidth * 0.95) + kSizeFrom750(x: 88)//88位标题的高度
            return height
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section == 0 ? 0.1 : kSizeFrom750(x: 40)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}

extension PaymentView {
    
    func imageWithImage(image:UIImage, scaleSize:CGSize) -> UIImage {
        let newImage : UIImage?
        UIGraphicsBeginImageContextWithOptions(scaleSize, false, 0.0)
        image.draw(in: CGRect.init(x: 0, y: 0, width: scaleSize.width, height: scaleSize.height))
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func attributeStringFromString(str : String) -> NSAttributedString {
        
        return String.getAttributeString(str: str, lineSpace: kSizeFrom750(x: 20), font: kGothamRoundedFontSize(size: kSizeFrom750(x: 28)))
    }
    
    func heighForCell(detailStr:String, textWidth:CGFloat) -> CGFloat {
        let att = attributeStringFromString(str: detailStr)
        return String.getHeighForAttributeString(str: att, textWidth: textWidth)
        
    }
    
}
