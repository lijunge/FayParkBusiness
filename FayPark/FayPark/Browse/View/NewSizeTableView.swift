//
//  NewSizeTableView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/7/5.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewSizeTableView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.addSubview(self.headerView)
        self.headerView.addSubview(self.infoLable)
        self.headerView.addSubview(self.closeBtn)
        self.headerView.addSubview(self.ringLable)
        self.addSubview(self.scrollView)
        self.scrollView.addSubview(self.imageViews)
        let size = kImage(iconName: "select_insidediameter_img").size
        self.scrollView.contentSize = size
        makeConstraint()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func makeConstraint() {
        self.headerView.mas_makeConstraints { (mark) in
            mark?.top.left().right().equalTo()(self)
            mark?.height.equalTo()(kSizeFrom750(x: 200))
        }
        self.closeBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(headerView)?.offset()(kSizeFrom750(x: 62))
            mark?.width.height().equalTo()(kSizeFrom750(x: 40))
            mark?.right.equalTo()(headerView)?.offset()(-kSizeFrom750(x: 48))
        }
        self.infoLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(headerView)?.offset()(kSizeFrom750(x: 86))
            mark?.centerX.equalTo()(headerView.mas_centerX)
        }
        self.infoLable.setContentHuggingPriority(.required, for: .horizontal)
        self.ringLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(headerView)?.offset()(kSizeFrom750(x: 180))
            mark?.left.equalTo()(headerView)?.offset()(kSizeFrom750(x: 34))
        }
        self.ringLable.setContentHuggingPriority(.required, for: .horizontal)
        
        imageViews.image = kImage(iconName: "select_insidediameter_img")
        imageViews.mas_makeConstraints { (mark) in
            mark?.top.left().equalTo()(self.scrollView)?.offset()(kSizeFrom750(x: 0))
            mark?.right.equalTo()(self.scrollView)?.offset()(-kSizeFrom750(x: 0))
            mark?.bottom.equalTo()(self.scrollView.mas_bottom)?.offset()(-kSizeFrom750(x: 100))
        }
    }

    lazy var headerView: UIView = {
        let tmepHeader = UIView.init()
        tmepHeader.backgroundColor = UIColor.white
        return tmepHeader
    }()
    lazy var infoLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = kGothamBoldFontSize(size: kSizeFrom750(x: 36))
        tempLable.text = kPDSizeGuideString
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        return tempLable
    }()
    lazy var closeBtn: UIButton = {
        let tempBtn = UIButton.init()
        tempBtn.setImage(kImage(iconName: "cart_icon_close"), for: .normal)
        return tempBtn
    }()
    lazy var ringLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = kHelveticaFontSize(size: kSizeFrom750(x: 30))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempLable.text = kPDRingConverterString
        return tempLable
    }()
    
    lazy var scrollView: UIScrollView = {
        let tempView = UIScrollView.init(frame: CGRect.init(x: kSizeFrom750(x: 0), y: kSizeFrom750(x: 255), width: KScreenWidth, height: kScreenHeight-kSizeFrom750(x: 255)))
        tempView.backgroundColor = UIColor.white
        return tempView
    }()
    
    lazy var imageViews: UIImageView = {
        let tempImage = UIImageView.init()
        return tempImage
    }()
    
}
