//
//  BrowseHeaderView.swift
//  FayPark
//
//  Created by faypark on 2018/6/6.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

protocol BrowseHeaderViewDelegate{
    // 顶部轮播图点击事件
    func browseHeaderImageViewTappedDelegate()
    // 顶部轮播图详情点击事件
    func browseHeaderImageDetailViewTappedDelegate(indexpath: IndexPath)
    // 中间限时商品点击事件
    func browseLimitImageViewTappedDelegate()
    // 底部品类点击事件
    func browseCategoryButtonTappedDelegate(sender: UIButton)
}

class BrowseHeaderView: UIView {
    
    var headerView: UIView!
    var categoryView: UIView!
    var headerImgView: UIImageView!
 
    var timeLimitImgView: UIImageView!
    var recommendView: UIView!
    
    var saleLabel: UILabel!
    var notiseLabel: UILabel!
    var enterButton: UIButton!

    var collectionView: UICollectionView!
    let collectionViewCellIdentifier = "BrowseHeaderCollectionCell"

    var categoryImgArr: [String]!
    var bannerProductArr = Array<BrowseActivityInfo>()
    
    var delegate: BrowseHeaderViewDelegate?

    var hourLabel: UILabel!
    var minuteLabel: UILabel!
    var secondLabel: UILabel!
    
    var headerImgUrl: String? {
        get {
            return self.headerImgUrl!
        }
        set {
            self.headerImgView.sd_setImage(with: URL(string: newValue!), placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        }
    }

    var limitProductInfo: BrowseCountDownInfo? {
        didSet {
            setupTimeLimitView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupHeaderImgView()
        setupCollectionView()
    }
    func setupHeaderImgView() {
        
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.width, height: 350))
        headerView.backgroundColor = UIColor.white
        self.addSubview(headerView)
        
        headerImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.width, height: kSizeFrom750(x: 210)))
        headerImgView.backgroundColor = UIColor.white
        headerImgView.isUserInteractionEnabled = true
        headerImgView.contentMode = .scaleAspectFit
        headerView.addSubview(headerImgView)
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(headerImgViewTapped))
        headerImgView.addGestureRecognizer(tapgesture)
    }
    
    func setupCollectionView() {
        
        let itemSpacing: CGFloat = 5
        let imageW = (self.frame.size.width - itemSpacing * 3) / 5
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumInteritemSpacing = itemSpacing
        layout.minimumLineSpacing = 5
        layout.itemSize = CGSize(width: imageW * 2, height: 292)
        
        collectionView = UICollectionView(frame: CGRect(x: itemSpacing, y: headerImgView.frame.maxY + itemSpacing, width: self.frame.size.width - itemSpacing, height: 292), collectionViewLayout: layout)
        collectionView.register(UINib(nibName: collectionViewCellIdentifier, bundle: nil), forCellWithReuseIdentifier: collectionViewCellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        headerView.addSubview(collectionView)
        
    }
    
    func setupTimeLimitView() {
        
        let topBackView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 10))
        topBackView.backgroundColor = kSpacingColor
        if limitProductInfo!.ActivityName.isEmpty {
            timeLimitImgView = UIImageView(frame: CGRect(x: 0, y: collectionView.frame.maxY + 5, width: frame.width, height: 0))
            self.addSubview(timeLimitImgView)
            return
        }
        timeLimitImgView = UIImageView(frame: CGRect(x: 0, y: collectionView.frame.maxY + 5, width: frame.width, height: 160))
        //timeLimitImgView.addSubview(topBackView)
        timeLimitImgView.isUserInteractionEnabled = true
        timeLimitImgView.sd_setImage(with: URL(string: limitProductInfo!.MainImgLink), placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        self.addSubview(timeLimitImgView)
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(timeLimitImgViewTapped))
        timeLimitImgView.addGestureRecognizer(tapgesture)
        
    
        let timeView = UIView(frame: CGRect(x: frame.size.width / 2.0 - kSizeFrom750(x: 240) / 2.0, y: kSizeFrom750(x: 80), width: kSizeFrom750(x: 240), height: kSizeFrom750(x: 80)))
        timeView.backgroundColor = UIColor.orange
        //timeLimitImgView.addSubview(timeView)
        
        let timeArr = [kHomeHourString,kHomeMinString,kHomeSecString]
        for i in 0..<timeArr.count {
            let tmpView = UIView(frame: CGRect(x: CGFloat(i) * kSizeFrom750(x: 80), y: 0, width: kSizeFrom750(x: 80), height: kSizeFrom750(x: 80)))
            tmpView.backgroundColor = UIColor.white
            tmpView.layer.borderWidth = 0.5
            tmpView.layer.borderColor = UIColor.lightGray.cgColor
            timeView.addSubview(tmpView)
        
            let timeLabel = UILabel(frame: CGRect(x: 0, y: kSizeFrom750(x: 5), width: kSizeFrom750(x: 80), height: kSizeFrom750(x: 40)))
            timeLabel.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
            timeLabel.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 32))
            timeLabel.font = UIFont.systemFont(ofSize: kSizeFrom750(x: 32), weight: UIFont.Weight(rawValue: kSizeFrom750(x: 10)))
            
            timeLabel.textAlignment = .center
            tmpView.addSubview(timeLabel)
            
            if i == 0 {
                hourLabel = timeLabel
            } else if i == 1 {
                minuteLabel = timeLabel
            } else {
                secondLabel = timeLabel
            }
            let notiseLabel = UILabel(frame: CGRect(x: 0, y: kSizeFrom750(x: 40), width: kSizeFrom750(x: 80), height: kSizeFrom750(x: 40)))
            notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
            notiseLabel.text = timeArr[i]
            notiseLabel.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 25))
            notiseLabel.textAlignment = .center
            tmpView.addSubview(notiseLabel)
        }
        
        let timeLabel = UILabel(frame: CGRect(x: 0, y: timeView.frame.maxY, width: frame.size.width, height: kSizeFrom750(x: 60)))
        timeLabel.textColor = UIColor.black
        timeLabel.text = limitProductInfo?.ActivityName
        timeLabel.textAlignment = .center
        timeLabel.numberOfLines = 0
        timeLabel.lineBreakMode = .byWordWrapping
        timeLabel.font = UIFont(name: kGothamRoundedBold, size: kSizeFrom750(x: 33))
        timeLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        //timeLimitImgView.addSubview(timeLabel)
        
        let enterButton = UIButton(frame: CGRect(x: 0, y: timeLabel.frame.maxY, width: frame.size.width, height: kSizeFrom750(x: 30)))
        enterButton.setTitle(kHomeEnterString, for: .normal)
        enterButton.setTitleColor(kRGBColorFromHex(rgbValue: 0x000000), for: .normal)
        enterButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 28))
        //timeLimitImgView.addSubview(enterButton)
        
        //setupLimitTimeStamp()
    }
    
    func setupCategoryView(categoryArr: [BrowseHotType]) {
 
        let topBackView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 10))
        topBackView.backgroundColor = kSpacingColor
        
        categoryView = UIView(frame: CGRect(x: 0, y: timeLimitImgView.frame.maxY, width: frame.width, height: 156))
        self.addSubview(categoryView)
        //categoryView.addSubview(topBackView)
        
        let notiseLabel = UILabel(frame: CGRect(x: 0, y: 10, width: frame.size.width, height: 60))
        notiseLabel.backgroundColor = UIColor.white
        notiseLabel.text = kHomePopularString
        notiseLabel.textAlignment = .center
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        notiseLabel.font = UIFont(name: kGothamRoundedBold, size: 18)
        categoryView.addSubview(notiseLabel)
        
        let categoryW = (self.frame.size.width - 20) / 5
        var maxCateY = notiseLabel.frame.maxY
        for i in 0 ..< categoryArr.count {
            let temButton = UIButton(type: .custom)
            let row: Int = i / 5
            let col: Int = i % 5
            let categoryItem: BrowseHotType = categoryArr[i]
            let temX = 10 + categoryW * CGFloat(col)
            let temY = notiseLabel.frame.maxY + categoryW * CGFloat(row)
            maxCateY = temY
            var imgName = "browse_anklets_icon"
            temButton.frame = CGRect(x: temX, y: temY, width: categoryW, height: categoryW)
            if categoryItem.TypeId == 2 {
                imgName = "browse_earrings_icon"
                temButton.tag = 102
            } else if categoryItem.TypeId == 3 {
                imgName = "browse_necklaces_icon"
                temButton.tag = 103
            } else if categoryItem.TypeId == 4 {
                imgName = "browse_rings_icon"
                temButton.tag = 104
            } else if categoryItem.TypeId == 5 {
                imgName = "browse_bracelets_icon"
                temButton.tag = 105
            } else if categoryItem.TypeId == 6 {
                imgName = "browse_hairaccessories_icon"
                temButton.tag = 106
            } else if categoryItem.TypeId == 7 {
                imgName = "browse_watches_icon"
                temButton.tag = 107
            } else if categoryItem.TypeId == 8{
                imgName = "browse_anklets_icon"
                temButton.tag = 108
            } else {
                imgName = "browse_other_icon"
                temButton.tag = 109
            }
            temButton.setTitle(categoryItem.TypeName, for: .normal)
            temButton.setImage(UIImage(named: imgName), for: .normal)
            temButton.setTitleColor(kRGBColorFromHex(rgbValue: 0x000000), for: .normal)
            temButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: 11)
            temButton.tag = 100 + i
            temButton.titleLabel?.numberOfLines = 0
            temButton.titleLabel?.textAlignment = .center
            temButton.addTarget(self, action: #selector(categoryButtonTapped), for: .touchUpInside)
            temButton.imageLabelEdgeInset(imageDirection: .Top, imageTitleSpace: 10)
            categoryView.addSubview(temButton)
        }
        //总共有多少行
        //let categoryCol = categoryArr.count / 5
        categoryView.frame = CGRect(x: 0, y: timeLimitImgView.frame.maxY, width: frame.width, height: maxCateY + 60 + 20)
    }
    
    func setupRecommendView() {
        
        let topBackView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kSizeFrom750(x: 10)))
        topBackView.backgroundColor = kSpacingColor
        
        recommendView = UIView(frame: CGRect(x: 0, y: categoryView.frame.maxY, width: frame.width, height: 70))
        recommendView.backgroundColor = UIColor.white
        self.addSubview(recommendView)
        
        recommendView.addSubview(topBackView)
        
        let recommendLabel = UILabel(frame: CGRect(x: 0, y: 10, width: frame.width, height: 60))
        recommendLabel.text = kHomeRecommendString
        recommendLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        recommendLabel.textAlignment = .center
        recommendLabel.font = UIFont(name: kGothamRoundedBold, size: 18)
        recommendView.addSubview(recommendLabel)
        
    }
    
    func setupLimitTimeStamp() {
        //有倒计时商品
        let countDownInter = TimeInterval((limitProductInfo?.Number)!)
        TimeCountDownManager.manager.scheduledCountDownWith("countDownIdentifier", timeInteval: countDownInter, countingDown: { (timeInterval) in
            var hourTime:String
            var minuteTime:String
            var minute:String
            
            let time = Int(timeInterval)
            if time / 3600  < 10 {
                hourTime = String(format: "0%ld", time / 3600)
            }else{
                hourTime = String(format: "%ld", time / 3600)
            }
            if time / 60 % 60 < 10 {
                minuteTime = String(format: "0%ld", time / 60 % 60)
            } else {
                minuteTime = String(format: "%ld", time / 60 % 60)
            }
            if time % 60 < 10 {
                minute = String(format: "0%ld", time % 60)
            } else {
                minute = String(format: "%ld", time % 60)
            }
            self.hourLabel.text = hourTime
            self.minuteLabel.text = minuteTime
            self.secondLabel.text = minute
        }) { (timeInterval) in
            //倒计时时间到的话，需要更新倒计时商品的价格
            self.hourLabel.text = "00"
            self.minuteLabel.text = "00"
            self.secondLabel.text = "00"
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Private Actions
    @objc func headerImgViewTapped() {
        //顶部轮播图点击事件
        if let delegate = self.delegate {
            delegate.browseHeaderImageViewTappedDelegate()
        }
    }
    @objc func timeLimitImgViewTapped() {
        if let delegate = self.delegate {
            delegate.browseLimitImageViewTappedDelegate()
        }
    }
    @objc func categoryButtonTapped(sender: UIButton) {
        let tag = sender.tag
        if tag == 102 {//earring
            configCommonEventMonitor(monitorString: kHomeEarrings, type: 2)
        } else if tag == 103 { //necklaces
            configCommonEventMonitor(monitorString: kHomeNecklaces, type: 2)
        } else if tag == 104 {//rings
            configCommonEventMonitor(monitorString: kHomeRings, type: 2)
        } else if tag == 105 {//bracelets
            configCommonEventMonitor(monitorString: kHomeBracelets, type: 2)
        } else if tag == 106 {//hairaccessories
            configCommonEventMonitor(monitorString: kHomeHair, type: 2)
        }
        if let delegate = self.delegate {
            delegate.browseCategoryButtonTappedDelegate(sender: sender)
        }
    }
    func getHeaderHeight() -> CGFloat {
        let maxY = recommendView.frame.maxY
        return maxY
    }
}

//MARK: - CollectionView 数据源
extension BrowseHeaderView: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerProductArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellIdentifier, for: indexPath) as! BrowseHeaderCollectionCell
        let productModel = bannerProductArr[indexPath.item]
        cell.configCollectionCell(browseModel: productModel)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let delegate = self.delegate {
            delegate.browseHeaderImageDetailViewTappedDelegate(indexpath: indexPath)
        }
    }
}
