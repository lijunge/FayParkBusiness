//
//  FayParkCollectionCell.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/2.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SDWebImage.UIImageView_WebCache
import MBProgressHUD

class FayParkCollectionCell: UICollectionViewCell {
    @IBOutlet weak var cartBtn: UIButton!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var oldPrice: UILabel!
    @IBOutlet weak var nowPrice: UILabel!
//    @IBOutlet weak var discountLabel: UILabel!
    
    @IBOutlet weak var freeShippingImgView: UIImageView!
    @IBOutlet weak var discountBtn: UIButton!
    typealias DeleteButtonTappedBlock = (_ cell : FayParkCollectionCell) -> Void
    
    var deleteButtonTapped: DeleteButtonTappedBlock?
    
    var imgUrl : URL? {
        get {
            return self.imgUrl!
        }
        set {
            self.imgView.sd_setImage(with: newValue!, placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        }
    }
    //获取的数据模型
    override func awakeFromNib() {
        super.awakeFromNib()
        self.favBtn.isUserInteractionEnabled = false
        self.imgView.addSubview(self.almostView)
        self.almostView.addSubview(self.almostLable)
        self.almostView.addSubview(self.soldLable)
        self.imgView.addSubview(self.hotLable)
        self.discountBtn.setBackgroundImage(kImage(iconName: "browse_shape_label"), for: .normal)
        self.discountBtn.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 25))
        self.discountBtn.contentHorizontalAlignment = .left
        self.discountBtn.isUserInteractionEnabled = false
        self.freeShippingImgView.isHidden = true
        self.almostView.mas_makeConstraints { (mark) in
            mark?.bottom.right().equalTo()(self.imgView)
            mark?.size.equalTo()(CGSize.init(width: kSizeFrom750(x: 142), height: kSizeFrom750(x: 74)))
        }
        
        self.almostLable.mas_makeConstraints { (mark) in
            mark?.top.left().right().equalTo()(self.almostView)
            mark?.bottom.equalTo()(self.almostView.mas_centerY)
        }
        self.soldLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.almostLable.mas_bottom)
            mark?.left.equalTo()(self.almostView)?.offset()(kSizeFrom750(x: 3))
            mark?.bottom.right().equalTo()(self.almostView)?.offset()(-kSizeFrom750(x: 3))
        }
        self.hotLable.mas_makeConstraints { (mark) in
            mark?.bottom.right().equalTo()(self.imgView)
            mark?.size.equalTo()(CGSize.init(width: kSizeFrom750(x: 88), height: kSizeFrom750(x: 50)))
        }
        self.discountBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 45))
            mark?.left.equalTo()(self)
            mark?.size.equalTo()(CGSize.init(width: kSizeFrom750(x: 116), height: kSizeFrom750(x: 50)))
        }
        // Initialization code
        //discountButton.titleLabel?.transform = CGAffineTransform.init(rotationAngle: CGFloat.pi / 4 * 3)//CGAffineTransformMakeRotation(M_PI/2.0f);
       // discountLabel.transform = CGAffineTransform.init(rotationAngle: -(CGFloat.pi / 4))
//        almostView
    }
    
    lazy var hotLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.backgroundColor = kRGBColorFromHex(rgbValue: 0xef113a)
        tempLable.text = kHomeHotString
        tempLable.font = kHelveticaFontSize(size: kSizeFrom750(x: 34))
        tempLable.textAlignment = .center
        tempLable.textColor = UIColor.white
        return tempLable
    }()
    
    lazy var almostView: UIView = {
        let tempView = UIView.init()
        tempView.backgroundColor = kRGBColorFromHex(rgbValue: 0xef113a)
        return tempView
    }()
   
    lazy var almostLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.text = kHomeAlmostString
        tempLable.textColor = UIColor.white
        tempLable.textAlignment = .center
        tempLable.font = kHelveticaFontSize(size: kSizeFrom750(x: 23))
        return tempLable
    }()
    lazy var soldLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.backgroundColor = UIColor.white
        tempLable.font = kHelveticaFontSize(size: kSizeFrom750(x: 23))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0xef113a)
        tempLable.text = kHomeSoldOutString
        tempLable.textAlignment = .center
        return tempLable
    }()

    @IBAction func favBtnClick(_ sender: UIButton) {
        /*
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            favBtn.setTitle("\(Int(favBtn.currentTitle!)! + 1)", for: .normal)
        } else {
            favBtn.setTitle("\(Int(favBtn.currentTitle!)! - 1)", for: .normal)
        }
        
        let hud = MBProgressHUD.showAdded(to: self, animated: true)
        hud.mode = .indeterminate
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let param = ["UserID":userID, "ProductID":product.ProductID]
        
        //商品收藏接口
        FPHttpManager().manager.post(ProductCollectionInterface, parameters: param, progress: nil, success: { (task, result) in
            
            hud.hide(animated: true)
        }) { (task, error) in
        }
         */
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configBrowseCollectionCell(productInfo: BrowseActivityInfo) {
        self.nowPrice.text = String(format: "%@%.2f  ",getCurrentPayCharacter(),Double(productInfo.SellingPrice)!)
        
        let oldString = String(format: "%@%.2f",getCurrentPayCharacter(),Double(productInfo.Msrp)!)
        let str2 = NSMutableAttributedString.init(string: oldString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        self.oldPrice.attributedText = str2
        //cell.favBtn.isSelected = (pro.IsColl == "1") ? true:false
       
        self.favBtn.setTitle("\(productInfo.CollCount)", for: .normal)
        
        let discountStr = (1 - (Float(productInfo.SellingPrice)! / Float(productInfo.Msrp)!)) * 100
        if discountStr == 100 {
            self.discountBtn.setTitle(String(format: "  \(kHomeFree)", discountStr), for: .normal)
        } else {
            self.discountBtn.setTitle(String(format: "  -%.0f%%", discountStr), for: .normal)
        }
        self.cartBtn.isHidden = true
        if productInfo.IsColl == "1" {
            self.favBtn.isSelected = true
        }
        self.imgView.setImageWith(NSURL.init(string: productInfo.MainImgLink)! as URL, placeholderImage: kPlaceHolderImage)
        if productInfo.randomNumber == 0 {
            self.almostView.isHidden = false
            self.hotLable.isHidden = true
        }else if productInfo.randomNumber == 1 {
            self.hotLable.isHidden = false
            self.almostView.isHidden = true
        }else{
            self.almostView.isHidden = true
            self.hotLable.isHidden = true
        }
    }
    
    func configMyFavoriteCollectionCell(productInfo: BrowseActivityInfo) {
        self.nowPrice.text = String(format: "%@%.2f  ",getCurrentPayCharacter(),Double(productInfo.SellingPrice)!)
        
        let oldString = String(format: "%@%.2f",getCurrentPayCharacter(),Double(productInfo.Msrp)!)
        let str2 = NSMutableAttributedString.init(string: oldString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        self.oldPrice.attributedText = str2
    
        let discountStr = (1 - (Float(productInfo.SellingPrice)! / Float(productInfo.Msrp)!)) * 100
        if discountStr == 100 {
            self.discountBtn.setTitle(String(format: "  \(kHomeFree)", discountStr), for: .normal)
        } else {
            self.discountBtn.setTitle(String(format: "  -%.0f%%", discountStr), for: .normal)
        }
        self.favBtn.isHidden = true
        self.cartBtn.isHidden = false
    }
    
 
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        if deleteButtonTapped != nil {
            deleteButtonTapped!(self)
        }
    }
}
