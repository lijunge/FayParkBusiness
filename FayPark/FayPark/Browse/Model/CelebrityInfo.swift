//
//  CelebrityInfo.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

//名人名言
class CelebrityInfo {
    var Title: String?
    var ByName: String?
    
    init(jsonData: JSON) {
        self.Title = jsonData["Title"].stringValue
        self.ByName = jsonData["ByName"].stringValue
    }
}
