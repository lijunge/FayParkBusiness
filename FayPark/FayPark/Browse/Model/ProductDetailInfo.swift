//
//  ProductDetailInfo.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProductDetailInfo {
    //产品ID
    var ProductID: String
    //运费
    var Shipping: Double
    //销售价
    var SellingPrice: Double
    //产品标题
    var ProductTitle: String
    //分享价格
    var SharePrice: Double
    //市场价
    var Msrp: Double
    //购物车数量
    var CarCount: Int
    //收藏数量
    var CollCount: Int
    var ProductImgs:String
    //商品图片集
    var ImgLinksList: Array<ProductImg>
    //商品描述
    var ProductDescription: String
    //sku数据集合
    var SkuProductItemList: Array<SkuProductItem>
    //名人名言
    var CelebrityMottoInfoList: Array<CelebrityInfo>
    //是否收藏
    var IsColl: String
    //商品链接
    var ProductShareURL: String
    var CommentCount:Int//评论数量
    var CommentAvg:String//评论均分
    var IsTime:Int/**0.正常/1.活动结束文字提醒/2.显示倒计时/3.活动**/
    var StartTime:String//开始渐渐
    var EndTime:String//结束时间
    var Number:Int//剩余时间
    var ComName: String
    init(jsonData: JSON) {
        ProductID = jsonData["ProductID"].stringValue
        Shipping = jsonData["Shipping"].doubleValue
        SellingPrice = jsonData["SellingPrice"].doubleValue
        ProductTitle = jsonData["ProductTitle"].stringValue
        SharePrice = jsonData["SharePrice"].doubleValue
        Msrp = jsonData["Msrp"].doubleValue
        self.CarCount = jsonData["CarCount"].intValue
        self.CollCount = jsonData["CollCount"].intValue
        self.ProductDescription = jsonData["ProductDescription"].stringValue
        self.IsColl = jsonData["IsColl"].stringValue
        self.ProductShareURL = jsonData["ProductShareURL"].stringValue
        self.CommentCount = jsonData["CommentCount"].intValue
        self.CommentAvg = jsonData["CommentAvg"].stringValue
        self.IsTime = jsonData["IsTime"].intValue
        self.ProductImgs = jsonData["ProductImg"].stringValue
        self.StartTime = jsonData["StartTime"].stringValue
        self.EndTime = jsonData["EndTime"].stringValue
        self.Number = jsonData["Number"].intValue
        self.ComName = jsonData["ComName"].stringValue
        //图片模型数组
        self.ImgLinksList = Array()
        for jsonItem in jsonData["ImgLinksList"].arrayValue {
            self.ImgLinksList.append(ProductImg.init(jsonData: jsonItem))
            
        }
        //sku数据模型数组
        self.SkuProductItemList = Array()
        for jsonItem in jsonData["SkuProductItemList"].arrayValue {
            self.SkuProductItemList.append(SkuProductItem(jsonData: jsonItem))
        }
        //名人数据模型数组
        self.CelebrityMottoInfoList = Array()
        for jsonItem in jsonData["CelebrityMottoInfoList"].arrayValue {
            self.CelebrityMottoInfoList.append(CelebrityInfo(jsonData: jsonItem))
        }
    }
    
}
