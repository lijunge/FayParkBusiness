//
//  SelectProduct.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/2/2.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class SelectProduct {
    //商品的全部信息
    var detailProduct: ProductDetailInfo?
    //选中的sku
    var selectSku: SkuProductItem?
    //购买数量
    var countNum: Int = 1
    //是否分享
    var isShare: Bool = false

}
