//
//  ProductImg.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProductImg {
    //商品图片
    var Img: String?
    
    init(jsonData: JSON) {
        self.Img = jsonData["Img"].stringValue
    }
}
