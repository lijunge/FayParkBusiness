//
//  SelectProProperty.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/2/1.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class SelectProProperty {
    //选择的size
    var selectSize: String = ""
    //选择的color
    var selectColor: String = ""
    //选择的数量
    var selectNum: Int = 1

}
