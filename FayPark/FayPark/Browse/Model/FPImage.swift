//
//  FPImage.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/2.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class FPImage {
    var width : CGFloat = 0.0
    var height : CGFloat = 0.0
    var url : URL?
    
    init(dict : Dictionary<String, Any>) {
        self.width = dict["w"] as! CGFloat
        self.height = dict["h"] as! CGFloat
        self.url = URL.init(string: dict["img"] as! String)
    }
}

