//
//  BrowseActivityModel.swift
//  FayPark
//
//  Created by faypark on 2018/6/7.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON
/*
 * 商品
 */
struct BrowseActivityInfo{
    var SharePrice: String
    var SellingPrice: String
    var Msrp: String
    var ProductID: String
    var IsColl: String
    var Proportion: String
    var CarCount: String
    var CollCount: String
    var MainImgLink: String
    var PageNO: String
    var randomNumber:Int?
    var ID: String
    init(jsonData: JSON) {
        SharePrice = jsonData["SharePrice"].stringValue
        SellingPrice = jsonData["SellingPrice"].stringValue
        Msrp = jsonData["Msrp"].stringValue
        ProductID = jsonData["ProductID"].stringValue
        IsColl = jsonData["IsColl"].stringValue
        Proportion = jsonData["Proportion"].stringValue
        CarCount = jsonData["CarCount"].stringValue
        CollCount = jsonData["CollCount"].stringValue
        MainImgLink = jsonData["MainImgLink"].stringValue
        PageNO = jsonData["PageNO"].stringValue
        ID = jsonData["ID"].stringValue
        self.randomNumber = jsonData["randomNumber"].intValue
    }
    
}
/*
 * 活动街拍
 */
struct BrowseActivityInfoList {
    var ActivityName: String
    var TypeId: String
    var MainImgLink: String
    var BrowseActivityList: Array<BrowseActivityInfo>
    var CategoryList: Array<BrowseHotType>
    init(jsonData: JSON) {
        ActivityName = jsonData["ActivityName"].stringValue
        TypeId = jsonData["TypeId"].stringValue
        MainImgLink = jsonData["MainImgLink"].stringValue
        BrowseActivityList = Array()
        CategoryList = Array()
        
        for jsonItem in jsonData["ActivityStreetPatList"].arrayValue {
            BrowseActivityList.append(BrowseActivityInfo.init(jsonData: jsonItem))
        }
        for jsonItem in jsonData["HotTypeList"].arrayValue {
            CategoryList.append(BrowseHotType.init(jsonData: jsonItem))
        }
    }
}
/*
 * 倒计时
 */
struct BrowseCountDownInfo {
    var StartTime: String
    var TypeId: String
    var EndTime: String
    var Number: Int
    var MainImgLink: String
    var ActivityName: String
    init(jsonData: JSON) {
        StartTime = jsonData["StartTime"].stringValue
        TypeId = jsonData["TypeId"].stringValue
        EndTime = jsonData["EndTime"].stringValue
        Number = jsonData["Number"].intValue
        MainImgLink = jsonData["MainImgLink"].stringValue
        ActivityName = jsonData["ActivityName"].stringValue
    }
}
/*
 * 推荐页面 根据限时商品推荐进入
 */
struct RecommendProductCountDownInfo {
    var StartTime: String
    var EndTime: String
    var Number: String
    var MainImgLink: String
    var ActivityName: String
    var BrowseActivityList: Array<BrowseActivityInfo>
    init(jsonData: JSON) {
        StartTime = jsonData["StartTime"].stringValue
        EndTime = jsonData["EndTime"].stringValue
        Number = jsonData["Number"].stringValue
        MainImgLink = jsonData["MainImgLink"].stringValue
        ActivityName = jsonData["ActivityName"].stringValue
        BrowseActivityList = Array()
        for jsonItem in jsonData["ActivityCountDownList"].arrayValue {
            BrowseActivityList.append(BrowseActivityInfo.init(jsonData: jsonItem))
        }
    }
}
/*
 * 首页分类 list
 */
struct BrowseHotType {
    var TypeId: Int
    var TypeName: String
    init(jsonData: JSON) {
        TypeId = jsonData["TypeId"].intValue
        TypeName = jsonData["TypeName"].stringValue
    }
}
