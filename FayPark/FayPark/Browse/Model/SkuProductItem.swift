//
//  SkuProductItem.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

class SkuProductItem {
    //运费
    var Shipping: Double?
    //销售价
    var SellingPrice: Double?
    //颜色
    var Color: String?
    //skuid
    var ProductSkuId: String?
    //市场价
    var Msrp: Double?
    //分享价
    var SharePrice: Double?
    //产品标题
    var ProductTitle: String?
    //大小
    var Size: String?
    
    init(jsonData: JSON) {
        self.Shipping = jsonData["Shipping"].doubleValue
        self.SellingPrice = jsonData["SellingPrice"].doubleValue
        self.Color = jsonData["Color"].stringValue
        self.ProductSkuId = jsonData["ProductSkuId"].stringValue
        self.Msrp = jsonData["Msrp"].doubleValue
        self.SharePrice = jsonData["SharePrice"].doubleValue
        self.ProductTitle = jsonData["ProductTitle"].stringValue
        self.Size = jsonData["Size"].stringValue
    }
    
    init() {
        self.Shipping = 0.0
        self.SellingPrice = 0.0
        self.Color = ""
        self.ProductSkuId = "0"
        self.Msrp = 0.0
        self.SharePrice = 0.0
        self.ProductTitle = ""
        self.Size = ""
    }
    
}
