//
//  AppDelegate.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/25.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit
import Braintree
import Stripe
import FBSDKCoreKit
import AppsFlyerLib
import PinterestSDK
import SwiftyJSON
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        configRootControllers()
        //监测系统更新
        configUpgradeCurrentVersion()
        //友盟分享
        configUSharePlatforms()
        //支付
        configPaymentPlatforms()
        //监控
        configAppEventTracker()
        //UIButton防止连续点击
        //UIButton.initializeMethod()
        configBuly()
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        } else if #available(iOS 8, *), #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        AppsFlyerTracker.shared().registerUninstall(deviceToken)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        //启动FaceBook监测
        FBSDKAppEvents.activateApp()
        //启动AppFlyer监测
        //AppsFlyerTracker.shared().trackAppLaunch()
        //AppsFlyerTracker.shared().currencyCode = "USD"
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let result = UMSocialManager.default().handleOpen(url, options: options)
        if result {
            //print("处理成功")
        } else {
            //print("处理失败")
        }
        
        if url.scheme?.localizedCaseInsensitiveCompare("com.fayparkzim.FayPark") == ComparisonResult.orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        } else {
            return false
        }
    }
    //bugly
    func configBuly() {
        Bugly.start(withAppId: BuglyID)
    }
    func configRootControllers() {
        //之前本地存储的userID是Int类型，为了保证之前的跟现在的没有差别，在第一次监测更新的时候，强制把给userid赋值
        let isFirstUpdate = UserDefaults.standard.bool(forKey: "isFirstUpdate")
        if !isFirstUpdate {
            //第一次强制更新.保证是游客登陆
            UserDefaults.standard.setValue("", forKey: "kUserID")
            UserDefaults.standard.set(true, forKey: "isFirstUpdate")
            UserDefaults.standard.synchronize()
            self.window?.rootViewController = MainViewController()
        } else {
            self.window?.rootViewController = MainViewController()
        }
        self.window?.makeKeyAndVisible()
    }
    //监测更新
    func configUpgradeCurrentVersion() {
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let param = ["TerminalType":1,
                     "VersionNumber":currentVersion,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay] as [String : Any]
        FPHttpManager().manager.post(VersionListInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON(result as! Data)
            let upgrateString = json["Data"]["IsUpgrade"].stringValue //0 不需要更新 1可选更新 2强制更新
            let titleString = json["Data"]["Title"].stringValue
            let contentString = json["Data"]["Content"].stringValue
            if upgrateString == "1" { //1可选更新
                let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
                let topModel = AlertButtonModel(title: "Update Now", buttonType: .AlertButtonTypeNormal)
                let downModel = AlertButtonModel(title: "Next Time", buttonType: .AlertButtonTypeRed)
                alertView.showWithAlertView(title: titleString, detailString: contentString, buttonArr: [topModel,downModel],cancel: true)
                alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                    if button.tag == 100 {
                        //去AppStore更新
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: fayParkAppStoreString)!, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(URL(string: fayParkAppStoreString)!)
                        }
                    }
                }
            } else if upgrateString == "2"{
                //后台存储的版本高 强制更新
                let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
                alertView.removeBackGesture()
                let topModel = AlertButtonModel(title: "Update Now", buttonType: .AlertButtonTypeNormal)
                alertView.showWithAlertView(title: titleString, detailString: contentString, buttonArr: [topModel],cancel: true)
                alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                    if button.tag == 100 {
                        //去AppStore更新
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: fayParkAppStoreString)!, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(URL(string: fayParkAppStoreString)!)
                        }
                    }
                }
            }
        }) { (task, error) in
            
        }
    }
    //MARK: - UM分享
    func  configUSharePlatforms() {
        //初始化友盟分享包含第三方登录
        UMSocialManager.default().openLog(true)
        UMSocialManager.default().umSocialAppkey = "5a6082a0f43e486f2b00013d"
        UMSocialUIManager.setPreDefinePlatforms([NSNumber(integerLiteral: UMSocialPlatformType.facebook.rawValue), NSNumber(integerLiteral:
            UMSocialPlatformType.googlePlus.rawValue)])
        //,NSNumber(integerLiteral:UMSocialPlatformType.pinterest.rawValue),NSNumber(integerLiteral:UMSocialPlatformType.pinterest.rawValue),NSNumber(integerLiteral:UMSocialPlatformType.instagram.rawValue)
        UMSocialUIManager.addCustomPlatformWithoutFilted(.facebook, withPlatformIcon: UIImage.init(named: "facebook_icon"), withPlatformName: "facebook")
        UMSocialUIManager.addCustomPlatformWithoutFilted(.googlePlus, withPlatformIcon: UIImage.init(named: "google+_icon"), withPlatformName: "google+")
        
        UMSocialShareUIConfig.shareInstance().shareTitleViewConfig.shareTitleViewTitleString = "select your platform"
        UMSocialShareUIConfig.shareInstance().shareCancelControlConfig.shareCancelControlText = "Cancel"
        UMSocialShareUIConfig.shareInstance().shareTitleViewConfig.shareTitleViewBackgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
        UMSocialShareUIConfig.shareInstance().sharePageScrollViewConfig.shareScrollViewBackgroundColor
            = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
        UMSocialShareUIConfig.shareInstance().sharePageScrollViewConfig.shareScrollViewPageBGColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
        UMSocialShareUIConfig.shareInstance().shareContainerConfig.isShareContainerHaveGradient = false
        
        UMSocialShareUIConfig.shareInstance().sharePageControlConfig.sharePageControlBackgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
        UMSocialShareUIConfig.shareInstance().shareCancelControlConfig.shareCancelControlBackgroundColor = UIColor.init(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
        
        
        //web-facebook
        UMSocialManager.default().setPlaform(.facebook, appKey: "145941909350658", appSecret: "ea2edda7896d5b485cd217b9b9ef1608", redirectURL: "http://www.faypark.com/USERAGREEMENT.html")
        //ios-facebook
        //UMSocialManager.default().setPlaform(.facebook, appKey: "2032327413669140", appSecret: "4ffe5fdc357fe9196d5d728990a31178", redirectURL: "http://www.faypark.com/USERAGREEMENT.html")
        
        //pinterest
        //appSecret:ac9f3f96d8ec2fcf503b20382bdba8cb1b0f4aec56206ac9441236b9f3d45450
        UMSocialManager.default().setPlaform(.pinterest, appKey: "4946947070603835673", appSecret: "a4b6606bd9d24be151eae32941f4869079a0a838d0da6d0a91884fd6c2122174", redirectURL: "https://www.faypark.com/")//"a4b6606bd9d24be151eae32941f4869079a0a838d0da6d0a91884fd6c2122174"
        
        //google+
        //UMSocialManager.default().setPlaform(.googlePlus, appKey: "<#T##String!#>", appSecret: "<#T##String!#>", redirectURL: "<#T##String!#>")
        
        //instgram
        UMSocialManager.default().setPlaform(.instagram, appKey: "830ad157bee340a7a14bae38a566a8d5", appSecret: "44349c49e81a4fb89d63e43937db7289", redirectURL: "")
    }
    
    func configPaymentPlatforms() {
        //初始化Paypal
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "AQvzRBkpu3rlqKNgVFbz54niKk8iN_OR0Jb75G3AaWbICj6AbaA2j0sqppOe0MmvZyyakPqrWfEhxuEp", PayPalEnvironmentSandbox: "AYDm_FX4LMhTwOCeQ86HYg-lKHpZzicPwi1WJ4YSJV7Mi-pKvLrNLaZspo8bjitaBPOxUFu1ClYvHW-P"])
//        #if DEBUG
//        //测试环境
//        PayPalMobile.preconnect(withEnvironment: String(PayPalEnvironmentSandbox))
//        #else
//        //正式环境
        PayPalMobile.preconnect(withEnvironment: String(PayPalEnvironmentProduction))
//        #endif

        //Braintree
        BTAppSwitch.setReturnURLScheme("com.fayparkzim.FayPark.payments")
        
        //Stripe
        //正式环境pk_live_44DMZ039hbUdA3Dqso2RmCo7
        //测试环境pk_test_bU4aGDjIuc5Kx0RvIMtu3Ct6
        STPPaymentConfiguration.shared().publishableKey = StripePublishKey
        //
    }
    func configAppEventTracker() {
        AppsFlyerTracker.shared().appleAppID = "1365441622"
        AppsFlyerTracker.shared().appsFlyerDevKey = "zrK9v52jcJs3CRu7EtXgDi"
        AppsFlyerTracker.shared().trackAppLaunch()
    }

}
