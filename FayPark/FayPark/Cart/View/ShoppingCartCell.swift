//
//  ShoppoingCartCell.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/4.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
class ShoppingCartCell: UITableViewCell {

    //左侧选择按钮
    @IBOutlet weak var leftChooseButton: UIButton!
    //商品图片view
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var sizeDetailLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var orignalLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    
    //右侧删除按钮
    typealias DeleteProductBlock = (_ cell : ShoppingCartCell) -> Void
    var deleteProductButtonTapped: DeleteProductBlock?
    
    //增加或者减少商品数量
    typealias PlusOrMinusProductBlock = (_ cell: ShoppingCartCell,_ sender: UIButton) -> Void
    var plusOrMinusProductTapped: PlusOrMinusProductBlock?
    
    //选中取消商品
    typealias ChangeSelectProductBlock = (_ cell: ShoppingCartCell,_ sender: UIButton) -> Void
    var changeSelectProductTapped: ChangeSelectProductBlock?
    
    typealias ProductCountDangerousBlock = (_ cell: ShoppingCartCell,_ sender: UIButton) -> Void
    var productCountDangerousTapped: ProductCountDangerousBlock?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        countView.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        countView.layer.borderWidth = 0.5
        
        plusButton.layer.borderWidth = 0.5
        plusButton.tag = 101
        plusButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        
        minusButton.layer.borderWidth = 0.5
        minusButton.tag = 100
        minusButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        
        countLabel.layer.borderWidth = 0.5
        countLabel.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        
        shippingLabel.layer.borderWidth = 1.0
        shippingLabel.layer.borderColor = kRGBColorFromHex(rgbValue: 0x9bbc65).cgColor
        shippingLabel.adjustsFontSizeToFitWidth = true
    }
    
    //选中取消选中商品
    @IBAction func clickSelected(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if changeSelectProductTapped != nil {
            changeSelectProductTapped!(self,sender)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        if deleteProductButtonTapped != nil {
            deleteProductButtonTapped!(self)
        }
    }
    
    @IBAction func plusOrMinusButtonTapped(_ sender: UIButton) {
        if let countString = countLabel.text {
            if let countNumber = Int(countString) {
                var selectNumber = countNumber
                if sender.tag == 100 {
                    //减
                    if countNumber == 1 {
                        if productCountDangerousTapped != nil {
                            productCountDangerousTapped!(self,sender)
                        }
                        return
                    }
                    selectNumber = countNumber - 1
                    countLabel.text = "\(selectNumber)"
                } else {
                    //加
                    selectNumber = countNumber + 1
                    countLabel.text = "\(selectNumber)"
                }
            }
        }
        if plusOrMinusProductTapped != nil {
            plusOrMinusProductTapped!(self,sender)
        }
    }
    
    func configCartProductCell(cartProduct: CartProduct) {
        productImageView.sd_setImage(with: URL.init(string: cartProduct.MainImgLink), placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        titleLabel.text = cartProduct.ProductTitle
        if (!cartProduct.Size.isEmpty && !cartProduct.Color.isEmpty) {
            sizeDetailLabel.text = "Size:\(cartProduct.Size) Color:\(cartProduct.Color)"
        } else {
            sizeDetailLabel.text = cartProduct.Size.isEmpty ? "Color:\(cartProduct.Color)" : "Size:\(cartProduct.Size)"
        }
        if cartProduct.isShippingFree {
            shippingLabel.text = "Free Shipping"
        } else {
            shippingLabel.text = String(format: "Shipping %@%.2f", getCurrentPayCharacter(),cartProduct.Shipping)
        }

        if cartProduct.IsTime == 0 || cartProduct.IsTime == 1{ //
            //正常商品 随机倒计时商品 显示的价格是销售价
            currentPriceLabel.text = String(format: "%@%.2f", getCurrentPayCharacter(),cartProduct.SellingPrice)
        } else {
            //倒计时商品 和 街拍商品 显示的是分享价
            currentPriceLabel.text = String(format: "%@%.2f", getCurrentPayCharacter(),cartProduct.SharePrice)
        }
        let oldString = String(format: "%@%.2f",getCurrentPayCharacter(),cartProduct.Msrp)
        let str2 = NSMutableAttributedString.init(string: oldString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        orignalLabel.attributedText = str2
        
        countLabel.text = "\(cartProduct.Count)"
        
        leftChooseButton.isSelected = cartProduct.isSelectProduct
    }
    
}
