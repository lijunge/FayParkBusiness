//
//  CheckFooterView.swift
//  FayPark
//
//  Created by faypark on 2018/6/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class CheckFooterView: UIView {

    @IBOutlet var view: UIView!
    @IBOutlet weak var handlePriceLabel: UILabel!
    
    @IBOutlet weak var actualPriceLabel: UILabel!
    
    @IBOutlet weak var ExpressShippingLable: UILabel!
    @IBOutlet weak var DaysLable: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    
    @IBOutlet weak var rapidLabel: UILabel!
    
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var rapidHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var shippingDaysLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        view = Bundle.main.loadNibNamed("CheckFooterView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        shippingDaysLabel.text = "(\(getCurrentShippingDaysString()))"
        addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func setupPaymentPrice(cartPaymentInfo: CartPaymentInfo) {

        let handlePriceString = String(format: "%@%.2f",getCurrentPayCharacter(),cartPaymentInfo.handlePrice)
        let str2 = NSMutableAttributedString.init(string: handlePriceString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        handlePriceLabel.attributedText = str2
        actualPriceLabel.text = String(format: "%@%.2f",getCurrentPayCharacter(),Double(cartPaymentInfo.actualPrice))
        shippingLabel.text = String(format: "%@%.2f",getCurrentPayCharacter(),Double(cartPaymentInfo.estimatedShipping))
        rapidLabel.text = String(format: "+%@%.2f",getCurrentPayCharacter(),Double(cartPaymentInfo.rapidPrice))
        //let totalMoney = Double(kCashServiceString)! + Double(cartPaymentInfo.orderTotalPrice)
        let leftString = String(format: "Order Total: %@", getCurrentPayCharacter())
        let totalString = String(format: "%@%.2f",leftString,Double(cartPaymentInfo.orderTotalPrice))
        totalLabel.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        totalLabel.textAlignment = .right
        totalLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 38))
        totalLabel.attributedText = changeTextChange(regex: leftString, text: totalString, color: kRGBColorFromHex(rgbValue: 0x333333), font: UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 38))!)
        isSelectRapidPayment(isRapid: cartPaymentInfo.isRapidSelected)
        
    }
    func setupCODInfo(selectedPayWay: Int) {
        let selectedLaunage = LanguageHelper.standardLanguager().currentLanguage
        if selectedLaunage == "hi" {
            if selectedPayWay == 3 {//货到付款
                ExpressShippingLable.removeConstraint(rapidHeightConstraint)
                rapidHeightConstraint = NSLayoutConstraint(item: ExpressShippingLable, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(30))
                ExpressShippingLable.addConstraint(rapidHeightConstraint)
                
                ExpressShippingLable.text = localized("kCOCashLogiServiceString")
                DaysLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
                DaysLable.attributedText = changeTextChange(regex: "FREE COD", text: "(2018.8.13-8.31 FREE COD)", color: kRGBColorFromHex(rgbValue: 0xe9668f), font: UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 24))!)
                DaysLable.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 24))
                
                let montyString = getCurrentPayCharacter()
                let rapidString = String(format: "+ %@%@",montyString,kCashServiceString)
                let attriString = changeTextChange(regex: kCashServiceString, text: rapidString, color: kRGBColorFromHex(rgbValue: 0xe9668f), font: UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 28))!)
                attriString.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: rapidString.count))
                rapidLabel.attributedText = attriString
            } else {
                ExpressShippingLable.removeConstraint(rapidHeightConstraint)
                rapidHeightConstraint = NSLayoutConstraint(item: ExpressShippingLable, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(0))
                ExpressShippingLable.addConstraint(rapidHeightConstraint)
            }
        }
    }
    func isSelectRapidPayment(isRapid: Bool) {
        if isRapid {
            ExpressShippingLable.removeConstraint(rapidHeightConstraint)
            rapidHeightConstraint = NSLayoutConstraint(item: ExpressShippingLable, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(30))
           ExpressShippingLable.addConstraint(rapidHeightConstraint)
        } else {
            let selectedLaunage = LanguageHelper.standardLanguager().currentLanguage
            if selectedLaunage != "hi" {
                ExpressShippingLable.removeConstraint(rapidHeightConstraint)
                rapidHeightConstraint = NSLayoutConstraint(item: ExpressShippingLable, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(0))
                ExpressShippingLable.addConstraint(rapidHeightConstraint)
            }
        }
    }
}
