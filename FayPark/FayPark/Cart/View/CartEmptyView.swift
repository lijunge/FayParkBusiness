//
//  CartEmptyView.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/2/9.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class CartEmptyView: UIView {
    var fromWhere = 0
    
    @IBAction func goShoppingClick(_ sender: UIButton) {
        
        if fromWhere == 0 {
            //跳转到首页
            let tabbarVC = UIApplication.shared.keyWindow?.rootViewController as! UITabBarController
            tabbarVC.selectedIndex = 0
        } else {
//            let tabbarVC = UIApplication.shared.keyWindow?.rootViewController as! UITabBarController
//            let navi = tabbarVC.childViewControllers.first as! UINavigationController
//            //print(navi.viewControllers.count)
//            for _ in 0..<navi.viewControllers.count {
//                navi.popViewController(animated: false)
//            }
         self.viewController().navigationController?.popViewController(animated: true)
            
        }
        
    }
    
    @IBOutlet var view : UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        view = Bundle.main.loadNibNamed("CartEmptyView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
