//
//  CartFooterView.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/8.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
class CartFooterView: UIView {
    @IBOutlet var view : UIView!
    
    @IBOutlet weak var topLogisticsView: UIView!
    @IBOutlet weak var shippingLable: UILabel!
    @IBOutlet weak var orderTotalLabel: UILabel!
    @IBOutlet weak var rapidLogisticsLabel: UILabel!
    @IBOutlet weak var pamentLabel: UILabel!
    @IBOutlet weak var handleLabel: UILabel!
   
    @IBOutlet weak var rapidButton: UIButton!
    
    @IBOutlet weak var rapidConstraintHeight: NSLayoutConstraint!
    typealias CartFooterRapidPaymentSelectBlock = (_ sender: UIButton,_ totalPrice: Double) -> Void
    @IBOutlet weak var shippingDayLabel: UILabel!
    var rapidPaymentButtonTapped: CartFooterRapidPaymentSelectBlock?
    
    typealias CartFooterShippingInfoTappedBlock = (_ sender: UIButton) -> Void
    var shippingInfoTapped: CartFooterShippingInfoTappedBlock?
    
    var paymentInfo: CartPaymentInfo
    override init(frame: CGRect) {
        paymentInfo = CartPaymentInfo(params: [:])
        
        super.init(frame: frame)
        
        view = Bundle.main.loadNibNamed("CartFooterView", owner: self, options: nil)?.first as! UIView;
        view.frame = self.bounds;
        addSubview(view);
        
        shippingDayLabel.text = "(\(getCurrentShippingDaysString()))"
        setupRapidHeightConstraint()
    }
    
    func setupRapidHeightConstraint() {
        let currentLanguage = LanguageHelper.standardLanguager().currentLanguage
        if currentLanguage == "hi" {
            rapidButton.removeConstraint(rapidConstraintHeight)
            rapidConstraintHeight = NSLayoutConstraint(item: rapidButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(0))
            rapidButton.addConstraint(rapidConstraintHeight)
        } else {
            rapidButton.removeConstraint(rapidConstraintHeight)
            rapidConstraintHeight = NSLayoutConstraint(item: rapidButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: CGFloat(30))
            rapidButton.addConstraint(rapidConstraintHeight)
        }
    }
    
    @IBAction func rapidLogiTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        var totalValue: Double = 0.0
        if sender.isSelected {
            totalValue = paymentInfo.actualPrice + paymentInfo.estimatedShipping + paymentInfo.rapidPrice
        } else {
            totalValue = paymentInfo.actualPrice + paymentInfo.estimatedShipping
        }
        orderTotalLabel.text = "$\(totalValue)"
        if rapidPaymentButtonTapped != nil {
            rapidPaymentButtonTapped!(sender,totalValue)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        paymentInfo = CartPaymentInfo(params: [:])
        super.init(coder: aDecoder)
    }
    
    func setupPaymentPrice(cartPaymentInfo: CartPaymentInfo) {
        paymentInfo = cartPaymentInfo
        let handlePriceString = String(format: "%@%.2f",getCurrentPayCharacter(),cartPaymentInfo.handlePrice)
        let str2 = NSMutableAttributedString.init(string: handlePriceString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        handleLabel.attributedText = str2
        pamentLabel.text = String(format: "%@%.2f",getCurrentPayCharacter(),Double(cartPaymentInfo.actualPrice))
        shippingLable.text = String(format: "%@%.2f",getCurrentPayCharacter(),Double(cartPaymentInfo.estimatedShipping))
        rapidLogisticsLabel.text = String(format: "+ %@%.2f",getCurrentPayCharacter(),Double(cartPaymentInfo.rapidPrice))
        orderTotalLabel.text = String(format: "%@%.2f",getCurrentPayCharacter(),Double(cartPaymentInfo.orderTotalPrice))
    }
    
    @IBAction func shippingInfoTapped(_ sender: UIButton) {
        configCommonEventMonitor(monitorString: kCartShippingInfo, type: 2)
        if shippingInfoTapped != nil {
            shippingInfoTapped!(sender)
        }
    }
    func getRapidPaymentState() -> Bool {
        return rapidButton.isSelected
    }
}
