//
//  CartFreeGiftHeaderView.swift
//  FayPark
//
//  Created by faypark on 2018/6/19.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class CartFreeGiftHeaderView: UIView {

    var notiseView = UIView()
    var giftView = UIView()
    var giftHeight = 40
    var selectButton = UIButton()
    typealias CartFreeGiftSelectBlock = (_ sender: UIButton) -> Void
    var freeGiftSelectedTapped: CartFreeGiftSelectBlock?
    
    typealias CartFreeGiftDetailBlock = () -> Void
    var freeGiftDetailTapped: CartFreeGiftDetailBlock?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTopHeaderView()
        setupNotiseView()
        setupgiftView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    var freeGiftInfo: FreeGiftInfo? {
        didSet {
            if freeGiftInfo != nil {
                notiseView.isHidden = true
                giftView.isHidden = false
                setupGiftViewDatas(freeGiftInfo: freeGiftInfo!)
                giftHeight = 40 + 162
            } else {
                giftView.isHidden = true
                notiseView.isHidden = false
                giftHeight = 40 + 120
            }
        }
    }
    func setupTopHeaderView() {
        
        let topBackView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 50))
        topBackView.backgroundColor = .white
        addSubview(topBackView)
        
        let notiseLabel = UILabel(frame: CGRect(x: 15, y: 0, width: kScreenWidth - 15, height: 50))
        notiseLabel.text = "FREE GIFT"
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        notiseLabel.font = kGothamBoldFontSize(size: kSizeFrom750(x: 28))
        topBackView.addSubview(notiseLabel)
        
        let moreOptionButton = UIButton(frame: CGRect(x: KScreenWidth - 120 - 15, y: 0, width: 120, height: 50))
        moreOptionButton.setTitle("More Option", for: .normal)
        moreOptionButton.setImage(UIImage(named: "arrowrighticon"), for: .normal)
        moreOptionButton.imageLabelEdgeInset(imageDirection: .Right, imageTitleSpace: 5)
        moreOptionButton.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
        moreOptionButton.titleLabel?.font = kGothamBoldFontSize(size: kSizeFrom750(x: 28))
        moreOptionButton.addTarget(self, action: #selector(moreOptionButtonTapped), for: .touchUpInside)
        topBackView.addSubview(moreOptionButton)
        
        let lineView = UIView(frame: CGRect(x: 0, y: 49, width: kScreenWidth, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        topBackView.addSubview(lineView)
    }
    
    func setupNotiseView() {
        
        let notiseView = UIView(frame: CGRect(x: 0, y: 40, width: kScreenWidth, height: 130))
        
        let notiseLabel = UILabel(frame: CGRect(x: 15, y: 20, width: kScreenWidth - 15, height: 30))
        notiseLabel.text = "Pick your free gift!"
        notiseLabel.font = UIFont(name: kHelveticaRegular, size: 15)
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        notiseView.addSubview(notiseLabel)
        
        let detailLabel = UILabel(frame: CGRect(x: 15, y: notiseLabel.frame.maxY, width: kScreenWidth - 15, height: 20))
        detailLabel.text = "Gifts cannot be purchased separately"
        detailLabel.font = UIFont(name: kGothamRoundedLight, size: 11)
        detailLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        notiseView.addSubview(detailLabel)
        
//        let rightImg = UIImageView(frame: CGRect(x: kScreenWidth - 15 - 12, y: notiseView.frame.size.height / 2.0 - 12 / 2.0, width: 12, height: 12))
//        rightImg.image = UIImage(named: "arrowrighticon")
//        notiseView.addSubview(rightImg)
        
        let bottomSpacingView = UIView(frame: CGRect(x: 0, y: detailLabel.frame.maxY + 10, width: kScreenWidth, height: 50))
        bottomSpacingView.backgroundColor = kRGBColorFromHex(rgbValue: 0xFEF4F2)
        notiseView.addSubview(bottomSpacingView)
    
        let productLabel = UILabel(frame: CGRect(x: 15, y: 0, width: KScreenWidth - 30, height: 50))
        let productString = "My choices (pick 5+ items to get FREE shipping)"
        productLabel.text = productString
        productLabel.textColor = kRGBColorFromHex(rgbValue: 0xE9668F)
        productLabel.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 26))
        productLabel.attributedText = changeTextChange(regex: "FREE shipping", text: productString, color: kRGBColorFromHex(rgbValue: 0xE9668F), font: kGothamBoldFontSize(size: kSizeFrom750(x: 28)))
        bottomSpacingView.addSubview(productLabel)
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(notiseFreeGiftViewTapped))
        notiseView.addGestureRecognizer(tapGesture)
        
        notiseView.isHidden = true
        addSubview(notiseView)
    }
    
    func setupgiftView() {
        
        giftView = UIView(frame: CGRect(x: 0, y: 40, width: kScreenWidth, height: 162))

        selectButton = UIButton(frame: CGRect(x: 7, y: 112 / 2.0 - 45 / 2.0, width: 45, height: 45))
        selectButton.setImage(UIImage(named: "cart_circle_normal_icon"), for: .normal)
        selectButton.setImage(UIImage(named: "cart_circle_ok_icon"), for: .selected)
        selectButton.addTarget(self, action: #selector(freeGiftSelectButtonTapped(sender:)), for: .touchUpInside)
        selectButton.isSelected = true
        giftView.addSubview(selectButton)

        let gifImageView = UIImageView(frame: CGRect(x: selectButton.frame.maxX + 3, y: 17, width: 90, height: 90))
        gifImageView.tag = 100
        giftView.addSubview(gifImageView)

        let gifImageX = gifImageView.frame.maxX + 6
        let titleLabel = UILabel(frame: CGRect(x: gifImageX, y: 17, width: kScreenWidth - gifImageX - 10, height: 15))
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont(name: kHelveticaRegular, size: 13)
        titleLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        titleLabel.tag = 101
        giftView.addSubview(titleLabel)
        
        let sizeLabel = UILabel(frame: CGRect(x: gifImageX, y: titleLabel.frame.maxY, width: kScreenWidth - gifImageX - 10, height: 30))
        sizeLabel.font = UIFont(name: kHelveticaRegular, size: 12)
        sizeLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        sizeLabel.tag = 102
        giftView.addSubview(sizeLabel)
        
        let shippingLabel = UILabel(frame: CGRect(x: gifImageX, y: sizeLabel.frame.maxY + 20, width: 100, height: 25))
        shippingLabel.font = UIFont(name: kHelveticaRegular, size: 11)
        shippingLabel.textColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        shippingLabel.layer.borderColor = kRGBColorFromHex(rgbValue: 0x9bbc65).cgColor
        shippingLabel.layer.borderWidth = 1.0
        shippingLabel.textAlignment = .center
        shippingLabel.adjustsFontSizeToFitWidth = true
        shippingLabel.tag = 103
        giftView.addSubview(shippingLabel)
        
        let countLabel = UILabel(frame: CGRect(x: kScreenWidth - 50, y: sizeLabel.frame.maxY + 20, width: 50, height: 25))
        countLabel.text = "X1"
        countLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        countLabel.font = UIFont(name: kGothamRoundedBold, size: 11)
        countLabel.textAlignment = .left
        giftView.addSubview(countLabel)
        
//        let rightImg = UIImageView(frame: CGRect(x: kScreenWidth - 15 - 12, y: giftView.frame.size.height / 2.0 - 12 / 2.0, width: 12, height: 12))
//        rightImg.image = UIImage(named: "arrowrighticon")
//        giftView.addSubview(rightImg)
        
        let bottomSpacingView = UIView(frame: CGRect(x: 0, y: countLabel.frame.maxY + 10, width: kScreenWidth, height: 50))
        bottomSpacingView.backgroundColor = kRGBColorFromHex(rgbValue: 0xFEF4F2)
        giftView.addSubview(bottomSpacingView)
        
        let productLabel = UILabel(frame: CGRect(x: 15, y: 0, width: KScreenWidth - 30, height: 50))
        let productString = "My choices (pick 5+ items to get FREE shipping)"
        productLabel.text = productString
        productLabel.textColor = kRGBColorFromHex(rgbValue: 0xE9668F)
        productLabel.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 26))
        productLabel.attributedText = changeTextChange(regex: "FREE shipping", text: productString, color: kRGBColorFromHex(rgbValue: 0xE9668F), font: kGothamBoldFontSize(size: kSizeFrom750(x: 28)))
        productLabel.adjustsFontSizeToFitWidth = true
        productLabel.textAlignment = .left
        productLabel.tag = 104
        bottomSpacingView.addSubview(productLabel)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(changeFreeGiftViewTapped))
        giftView.addGestureRecognizer(tapGesture)
        
        giftView.isHidden = true
        addSubview(giftView)
    }
    
    func setupGiftViewDatas(freeGiftInfo: FreeGiftInfo) {
        let imgView = giftView.viewWithTag(100) as! UIImageView
        imgView.sd_setImage(with: URL.init(string: freeGiftInfo.MainImgLink), placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        let titleLabel = giftView.viewWithTag(101) as! UILabel
        if freeGiftInfo.FreeGiftTitle.isEmpty {
            titleLabel.text = freeGiftInfo.FreeGiftName
        } else {
            titleLabel.text = freeGiftInfo.FreeGiftTitle
        }
        
        let sizeLabel = giftView.viewWithTag(102) as! UILabel
        if (!freeGiftInfo.Size.isEmpty && !freeGiftInfo.Color.isEmpty) {
            sizeLabel.text = "Size:\(freeGiftInfo.Size) Color:\(freeGiftInfo.Color)"
        } else {
            sizeLabel.text = freeGiftInfo.Size.isEmpty ? "Color:\(freeGiftInfo.Color)" : "Size:\(freeGiftInfo.Size)"
        }
        
        let shippingLabel = giftView.viewWithTag(103) as! UILabel
        shippingLabel.text = String(format: "Shipping %@%.2f", getCurrentPayCharacter(),freeGiftInfo.Shipping)
    }
    @objc func moreOptionButtonTapped() {
        configCommonEventMonitor(monitorString: kFreeGiftCartMore, type: 2)
        if freeGiftDetailTapped != nil {
            freeGiftDetailTapped!()
        }
    }
    func changeFreeFiftSelectState(isSelected: Bool) {
        selectButton.isSelected = isSelected
        selectButton.setImage(UIImage(named: "cart_circle_normal_icon"), for: .normal)
        selectButton.setImage(UIImage(named: "cart_circle_ok_icon"), for: .selected)
    }
    
    func changeShippingPriceFree(isFree: Bool) {
        let shippingLabel = giftView.viewWithTag(103) as! UILabel
        if isFree {
            shippingLabel.text = "Free Shipping"
        } else {
            if let tmpGift = freeGiftInfo {
                shippingLabel.text = String(format: "Shipping %@%.2f", getCurrentPayCharacter(),tmpGift.Shipping)
            }
        }
    }
    func changeFreeShipping(_ count: Int) {
        let shippingLabel = giftView.viewWithTag(104) as! UILabel
        let shiipingText = "My choices (pick \(count)+ items to get FREE shipping)"
        shippingLabel.text = shiipingText
        shippingLabel.attributedText = changeTextChange(regex: "FREE shipping", text: shiipingText, color: kRGBColorFromHex(rgbValue: 0xE9668F), font: kGothamBoldFontSize(size: kSizeFrom750(x: 28)))
    }
    
    func getFreeGiftSelectedState() -> Bool {
        if !giftView.isHidden {
            return selectButton.isSelected
        }
        return false
    }
    
    @objc func freeGiftSelectButtonTapped(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if freeGiftSelectedTapped != nil {
            freeGiftSelectedTapped!(sender)
        }
    }
    @objc func notiseFreeGiftViewTapped() {
        if freeGiftDetailTapped != nil {
            freeGiftDetailTapped!()
        }
    }
    @objc func changeFreeGiftViewTapped() {
        configCommonEventMonitor(monitorString: kFreeGiftCartImg, type: 2)
        if freeGiftDetailTapped != nil {
            freeGiftDetailTapped!()
        }
    }
}
