//
//  FreeGiftCollectionViewCell.swift
//  FayPark
//
//  Created by faypark on 2018/6/21.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class FreeGiftCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var freeImgView: UIImageView!
    @IBOutlet weak var sizeLabel: UILabel!
    
    @IBOutlet weak var colorLabel: UILabel!
    
    @IBOutlet weak var shippingLabel: UILabel!
  
    
    @IBOutlet weak var freeGiftSelectButton: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        shippingLabel.layer.borderColor = kRGBColorFromHex(rgbValue: 0x9bbc65).cgColor
        shippingLabel.layer.borderWidth = 0.5
        self.sizeLabel.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 24))
        self.sizeLabel.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        
        self.colorLabel.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 24))
        self.colorLabel.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        freeGiftSelectButton.isHidden = true
    }

    func configFreeCollectionCell(freeGiftItem: FreeGiftInfo) {
        let placeImage = creatImageWithColor(color: kSpacingColor)
        freeImgView.sd_setImage(with: URL(string: freeGiftItem.MainImgLink), placeholderImage: placeImage, options: .init(rawValue: 0), completed: nil)
        sizeLabel.text = "Size: \(freeGiftItem.Size)"
        colorLabel.text = "Color: \(freeGiftItem.Color)"
        if freeGiftItem.IsSelected {
            freeGiftSelectButton.isHidden = false
        } else {
            freeGiftSelectButton.isHidden = true
        }
        shippingLabel.text = String(format: "Shipping: %@%.2f",getCurrentPayCharacter(),Double(freeGiftItem.Shipping))
    }
    
}
