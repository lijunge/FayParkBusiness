//
//  NetworkView.swift
//  FayPark
//
//  Created by faypark on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NetworkView: UIView {

    
    @IBOutlet var View: UIView!
    
    @IBOutlet weak var refreshButton: UIButton!
    
    @IBAction func refreshButtonTapped(_ sender: UIButton) {
    
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        View = Bundle.main.loadNibNamed("NetworkView", owner: self, options: nil)?.first as! UIView
        View.frame = self.bounds
        addSubview(View)
        
        refreshButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
        refreshButton.layer.borderWidth = 0.5
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
