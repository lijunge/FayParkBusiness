//
//  CartProduct.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/2/2.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON
/*
 * 商品
 */
class CartProduct {
    var Shipping: Double        //运价
    var SellingPrice: Double    //销售价
    var SharePrice: Double      //优惠价
    var Msrp: Double            //市场价
    var ProductSKUID: String
    var Color: String
    var ProductID: String
    var UserCarID: Int
    var Count: Int
    var Size: String
    var MainImgLink: String
    var DiscountPrice: Double
    var ProductTitle: String
    var Number: Int //剩余时间秒数
    var StartTime: String
    var EndTime: String
    //是否被选中
    var isSelectProduct : Bool = true
    var IsTime: Int // 0 正常商品 1随机倒计时商品 2倒计时商品 3活动商品
    var ComID: Int // 供应商ID
    var isShippingFree: Bool = false
    init(jsonData: JSON) {
        self.Shipping = jsonData["Shipping"].doubleValue
        self.SellingPrice = jsonData["SellingPrice"].doubleValue
        self.ProductSKUID = jsonData["ProductSKUID"].stringValue
        self.Color = jsonData["Color"].stringValue
        self.ProductID = jsonData["ProductID"].stringValue
        self.UserCarID = jsonData["UserCarID"].intValue
        self.Count = jsonData["Count"].intValue
        self.Msrp = jsonData["Msrp"].doubleValue
        self.Size = jsonData["Size"].stringValue
        self.MainImgLink = jsonData["MainImgLink"].stringValue
        self.DiscountPrice = jsonData["DiscountPrice"].doubleValue
        self.ProductTitle = jsonData["ProductTitle"].stringValue
        IsTime = jsonData["IsTime"].intValue
        SharePrice = jsonData["SharePrice"].doubleValue
        ComID = jsonData["ComID"].intValue
        Number = jsonData["Number"].intValue
        StartTime = jsonData["StartTime"].stringValue
        EndTime = jsonData["EndTime"].stringValue
    }
}
/*
 * 购物车所有信息
 */
struct CartInfo {
    //快速运费的系数
    var ExpressShipping: Double
    //达到一定数量免运费
    var PayCount: Int
    var FreeGiftList: Array<FreeGiftInfo>
    var ProductList: Array<CartProduct>
    init(jsonData: JSON) {
        ExpressShipping = jsonData["ExpressShipping"].doubleValue
        PayCount = jsonData["PayCount"].intValue
        FreeGiftList = Array()
        ProductList = Array()
        for jsonItem in jsonData["UserFreeGitInfoList"].arrayValue {
            FreeGiftList.append(FreeGiftInfo.init(jsonData: jsonItem))
        }
        for jsonItem in jsonData["UserCartInfoList"].arrayValue {
            ProductList.append(CartProduct.init(jsonData: jsonItem))
        }
    }
}
/**
 * 购物车底部结算信息
 */
struct CartPaymentInfo {
    var handlePrice: Double
    var actualPrice: Double
    var estimatedShipping: Double
    var rapidPrice: Double
    var orderTotalPrice: Double
    var isRapidSelected = false
    init(params:JSON) {
        handlePrice = params["handlePrice"].doubleValue
        actualPrice = params["actualPrice"].doubleValue
        estimatedShipping = params["estimatedShipping"].doubleValue
        rapidPrice = params["rapidPrice"].doubleValue
        orderTotalPrice = params["orderTotalPrice"].doubleValue
        isRapidSelected = params["isRapidSelected"].boolValue
    }
}
/*
 * freegift
 */
struct FreeGiftInfo {
    var FreeGiftTitle: String
    var Shipping: Double
    var Color: String
    var FreeGiftID: Int
    var MainImgLink: String
    var Size: String
    var Count: Int
    var FreeGiftName: String
    var IsSelected = false
    init(jsonData: JSON) {
        FreeGiftTitle = jsonData["FreeGiftTitle"].stringValue
        Shipping = jsonData["Shipping"].doubleValue
        Color = jsonData["Color"].stringValue
        FreeGiftID = jsonData["FreeGiftID"].intValue
        MainImgLink = jsonData["MainImgLink"].stringValue
        Size = jsonData["Size"].stringValue
        Count = jsonData["Count"].intValue
        FreeGiftName = jsonData["FreeGiftName"].stringValue
    }
}
/**
 * freeGift 列表
 */
struct FreeGiftTypeItem {
    var FreeGiftList: [FreeGiftInfo]
    var TypeName: String
    init(jsonData: JSON) {
        FreeGiftList = [FreeGiftInfo]()
        TypeName = jsonData["TypeName"].stringValue
        for jsonItem in jsonData["FreeGiftList"].arrayValue {
            FreeGiftList.append(FreeGiftInfo.init(jsonData: jsonItem))
        }
    }
}
/**
 * 购买时创建临时的订单 StrList
 */
struct PaymentProductInfo {
    var UserCarID: Int
    var ProductID: String
    var ProductSKUID: String
    var ProductSKUCount: Int
    var Price: Double
    var MsrpPrice: Double
    var Shipping: Double
    var Discount: Double
    var IsTime: Int
    var OrderType: Int
    
    init() {
        UserCarID = 0
        ProductID = ""
        ProductSKUID = ""
        ProductSKUCount = 0
        Price = 0.0
        MsrpPrice = 0.0
        Shipping = 0.0
        Discount = 0.0
        IsTime = 0
        OrderType = 0
    }
}

/**
 * 购买创建临时订单
 */
struct CreateTmpPayOrderInfo {
    var UserID: String
    var ProdutTotal: Double
    var ShippingTotal: Double
    var OrdinaryShippingTotal: Double
    var FastShippingTotal: Double
    var OrderPayTotal: Double
    var DiscountTotal: Double
    var IsFreeShipping: Int
    var ClientType = 1
    var StrList: String
    init(jsonData: JSON) {
        UserID = jsonData["UserID"].stringValue
        ProdutTotal = jsonData["ProdutTotal"].doubleValue
        ShippingTotal = jsonData["ShippingTotal"].doubleValue
        OrderPayTotal = jsonData["OrderPayTotal"].doubleValue
        DiscountTotal = jsonData["DiscountTotal"].doubleValue
        IsFreeShipping = jsonData["IsFreeShipping"].intValue
        StrList = jsonData["StrList"].stringValue
        OrdinaryShippingTotal = jsonData["OrdinaryShippingTotal"].doubleValue
        FastShippingTotal = jsonData["FastShippingTotal"].doubleValue
    }
}
/**
 * 创建订单返回
 *
 */
struct TmpPayOrderSuccessInfo {
    var PayCode: String
    var PaymentType: Int //0 没有支付方式 1 Paypal 2 stripe 3 货到付款
    var AddressInfoList: [MyAddressInfo]
    var PaymentItemList: [MyWalletInfo]
    init(jsonData: JSON) {
        PayCode = jsonData["PayCode"].stringValue
        PaymentType = jsonData["PaymentType"].intValue
        AddressInfoList = [MyAddressInfo]()
        PaymentItemList = [MyWalletInfo]()
        for item in jsonData["AddressInfoList"].arrayValue {
            AddressInfoList.append(MyAddressInfo.init(jsonData: item))
        }
        for item in jsonData["PaymentItemList"].arrayValue {
            PaymentItemList.append(MyWalletInfo.init(jsonData: item))
        }
    }
}
