//
//  CheckOutModel.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/2/7.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

class AddressModel {
    var Contry: String
    var States: String
    var City: String
    var ZipCode: String
    var Apt: String
    var Address: String
    var AddrID: Int
    var Name: String
    var Phone: String
    
    init(jsonData: JSON) {
        Contry = jsonData["Contry"].stringValue
        States = jsonData["States"].stringValue
        City = jsonData["City"].stringValue
        ZipCode = jsonData["ZipCode"].stringValue
        Apt = jsonData["Apt"].stringValue
        Address = jsonData["Address"].stringValue
        AddrID = jsonData["AddrID"].intValue
        Name = jsonData["Name"].stringValue
        Phone = jsonData["Phone"].stringValue
    }
}


class CheckOutModel {
    var ItemToal: Double
    var ShippingToal: Double
    var DiscountToal: Double
    var OrderToal: Double
    var AddressList: [AddressModel]

    init(jsonData: JSON) {
        self.ItemToal = jsonData["ItemToal"].doubleValue
        self.ShippingToal = jsonData["ShippingToal"].doubleValue
        self.DiscountToal = jsonData["DiscountToal"].doubleValue
        self.OrderToal = jsonData["OrderToal"].doubleValue
        
        self.AddressList = Array()
        for jsonItem in jsonData["AddressItemList"].arrayValue {
            self.AddressList.append(AddressModel.init(jsonData: jsonItem))
        }
    }
}

