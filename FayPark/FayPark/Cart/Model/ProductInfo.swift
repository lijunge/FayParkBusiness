//
//  ProductInfo.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/4.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class ProductInfo {
    var cn_price : Float
    var count : Int
    var image : String
    var title : String
    var price: Float
    
    var productIsChoosed : Bool = true
    
    var productIsEditing : Bool = false
    
    init(dict : Dictionary<String, Any>) {
        cn_price = dict["cn_price"] as! Float
        count = dict["count"] as! Int
        image = dict["image"] as! String
        title = dict["title"] as! String
        price = dict["price"] as! Float
    }
    
}
