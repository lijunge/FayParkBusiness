//
//  OrderCompleteViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class OrderCompleteViewController: BaseViewController {

    @IBOutlet weak var homePageButton: UIButton!
    
    @IBOutlet weak var myAccountButton: UIButton!
    
    @IBAction func homePageButtonTapped(_ sender: UIButton) {
        configCommonEventMonitor(monitorString: kCheckOutHome, type: 2)
        let mainVC = MainViewController()
        let window = UIApplication.shared.keyWindow
        window!.rootViewController = mainVC
        mainVC.selectedIndex = 0
    }
    
    @IBAction func MyAccountButtonTapped(_ sender: UIButton) {
        configCommonEventMonitor(monitorString: kCheckOutAccount, type: 2)
        let mainVC = MainViewController()
        let window = UIApplication.shared.keyWindow
        window!.rootViewController = mainVC
        mainVC.selectedIndex = 3
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Order Completed"
        homePageButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
        homePageButton.layer.borderWidth = 0.5
        
        myAccountButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0xe9668f).cgColor
        myAccountButton.layer.borderWidth = 0.5
        
        configCommonEventMonitor(monitorString: kCheckOutSuccess, type: 1)
    }
    override func leftBackButtonTapped() {
        let mainVC = MainViewController()
        let window = UIApplication.shared.keyWindow
        window!.rootViewController = mainVC
        mainVC.selectedIndex = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
