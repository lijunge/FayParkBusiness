//
//  CheckOutViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import Braintree
import FBSDKCoreKit
import AppsFlyerLib
import AFNetworking
class CheckOutViewController: BaseViewController {

    var scrollView = UIScrollView()
    var addressView = UIView()
    var paymentView = UIView()
    var orderView = CheckFooterView()
    var securityCodeView = UIView()
    var secuCodeField = UITextField()
    //支付相关
    var payPalConfig = PayPalConfiguration()
    //支付框架
    var braintreeClient: BTAPIClient?
    var payPalDriver: BTPayPalDriver?
    
    @IBOutlet weak var checkOutBtn: UIButton!

    /** 地址信息
     */
    var addressInfo: MyAddressInfo? {
        didSet {
            if let tmpAddressInfo = addressInfo {
                //有地址
                setupAddressInfoView(isShowAddress: true)
                setupAddressViewDatas(addressInfo: tmpAddressInfo)
                self.commitAddressInfo = tmpAddressInfo
            } else {
                setupAddressInfoView(isShowAddress: false)
            }
        }
    }
    /** 支付类型
     *  0 没有支付方式
     *  1 PayPal支付
     *  2 信用卡支付
     *  3 货到付款
     *  4 Mercado 支付
     */
    var paymentType: Int? {
        didSet {
            if let tmpPaymentType = paymentType {
                setupPaymentInfoView(paymentType: tmpPaymentType)
                self.commitPayType = tmpPaymentType
            }
        }
    }
    /** 订单信息
     */
    var paymentInfo: CartPaymentInfo? {
        didSet {
            if let tmpPaymentInfo = paymentInfo {
                setupOrderInfoView(paymentInfo: tmpPaymentInfo)
            }
        }
    }
    /** 信用卡信息
     */
    var cardInfo: MyWalletInfo? {
        didSet {
            if let tmpCardInfo = cardInfo {
                setupPaymentViewDatas(paymentInfo: tmpCardInfo, selectedPayWay: 2)
                self.commitCardInfo = tmpCardInfo
            }
        }
    }
    //buynow创建订单的参数
    var payOrderInfo: CreateTmpPayOrderInfo?
    //创建订单成功的返回信息
    var tmpOrderSuccessInfo: TmpPayOrderSuccessInfo?
    
    var commitAddressInfo: MyAddressInfo?
    var commitCardInfo: MyWalletInfo?
    var commitPayType: Int?
    
    var itemParamsArr = [[String: Any]]() //监测使用到的item
    var itemProductIDString = ""          //监测使用到的productID组合
    var itemProductCount = 0              //监测使用到的支付个数
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = kCheckOutTitleString
        self.view.backgroundColor = UIColor.white
        self.checkOutBtn.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 40))
        self.checkOutBtn.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        
        configCommonEventMonitor(monitorString: kCheckOutPage, type: 1)
    }
    //是否有地址
    func setupAddressInfoView(isShowAddress: Bool) {
    
        scrollView = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight - kSizeFrom750(x: 220) - statusRect)
        scrollView.contentSize = CGSize(width: KScreenWidth, height: kScreenHeight)
        scrollView.isScrollEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        view.addSubview(scrollView)
        
        addressView = UIView(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 130))
        addressView.backgroundColor = UIColor.white
        
        let notiseLabel = UILabel(frame: CGRect(x: 15, y: 10, width: KScreenWidth - 30, height: 35))
        notiseLabel.text = kCOShippingAddressString
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        notiseLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 32))
        addressView.addSubview(notiseLabel)
    
        let addButton = UIButton(frame: CGRect(x: 15, y: notiseLabel.frame.maxY, width: 30, height: 30))
        addButton.addTarget(self, action: #selector(addNewAddressInfoTapped), for: .touchUpInside)
        addButton.setImage(UIImage(named: "addnew_icon"), for: .normal)
        addButton.isHidden = isShowAddress
        addButton.tag = 103
        addressView.addSubview(addButton)

        let addNotiseLabel = UILabel(frame: CGRect(x: addButton.frame.maxX + 10, y: notiseLabel.frame.maxY, width: 100, height: 30))
        addNotiseLabel.text = kCOAddToString
        addNotiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        addNotiseLabel.font = UIFont(name: kHelveticaRegular, size: 18)
        addNotiseLabel.isHidden = isShowAddress
        addNotiseLabel.tag = 104
        addressView.addSubview(addNotiseLabel)

        let nameLabel = UILabel(frame: CGRect(x: 15, y: notiseLabel.frame.maxY, width: KScreenWidth - 30, height: 30))
        nameLabel.text = "Visen chen   1366203432"
        nameLabel.font = UIFont(name: kHelveticaRegular, size: 16)
        nameLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        nameLabel.tag = 100
        nameLabel.isHidden = !isShowAddress
        addressView.addSubview(nameLabel)
        
        let addressLabel = UILabel(frame: CGRect(x: 15, y: nameLabel.frame.maxY, width: KScreenWidth - 30, height: 40))
        addressLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        addressLabel.font = UIFont(name: kMSReference, size: 13)
        let addressString = "No. 32,Guiping Road,No .680,Xuhui District Road,Shangehai,China 704"
        let addressH = addressString.getHeightWithConstrainedWidth(width: KScreenWidth - 30, font: UIFont(name: kMSReference, size: 13)!)
        addressLabel.text = addressString
        addressLabel.numberOfLines = 0
        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.frame = CGRect(x: 15, y: nameLabel.frame.maxY, width: KScreenWidth - 30, height: addressH)
        addressLabel.tag = 101
        addressLabel.isHidden = !isShowAddress
        addressView.addSubview(addressLabel)
        
        var lineViewY = notiseLabel.frame.maxY + 15
        if isShowAddress {
            lineViewY = addressLabel.frame.maxY + 15
        } else {
            lineViewY = addNotiseLabel.frame.maxY + 15
        }
        let lineView = UIView(frame: CGRect(x: 15, y: lineViewY, width: KScreenWidth - 30, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        lineView.tag = 102
        addressView.addSubview(lineView)
        
        let arrowImgView = UIImageView(frame: CGRect(x: KScreenWidth - 12 - 12, y: addressView.frame.height / 2.0 - 15 / 2.0, width: 12, height: 12))
        arrowImgView.image = UIImage(named: "arrowrighticon")
        addressView.addSubview(arrowImgView)
        
    
        addressView.frame = CGRect(x: 0, y: 10, width: KScreenWidth, height: lineViewY + 5)
        scrollView.addSubview(addressView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(addressViewTapped))
        addressView.addGestureRecognizer(tapGesture)
        
    }
    
    func setupPaymentInfoView(paymentType: Int) {
        var payHeight:CGFloat = 100
        if paymentType == 4 {
            payHeight = 140
        }
        paymentView = UIView(frame: CGRect(x: 0, y: addressView.frame.maxY, width: KScreenWidth, height: payHeight))
        paymentView.backgroundColor = UIColor.white
        
        let payNotiseLabel = UILabel(frame: CGRect(x: 15, y: 10, width: KScreenWidth - 30, height: 35))
        payNotiseLabel.text = kCOPaymentOptionString
        payNotiseLabel.font = UIFont(name: kMSReference, size: 16)
        payNotiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        paymentView.addSubview(payNotiseLabel)
        
        let leftImgView = UIImageView(image: UIImage(named: "creditcard_icon_normal"))
        leftImgView.tag = 201
        leftImgView.frame = CGRect(x: 15, y: payNotiseLabel.frame.maxY + 10, width: 35, height: 30)
        leftImgView.contentMode = .scaleAspectFit
        if paymentType == 0 {
            //没有支付方式
            leftImgView.image = UIImage(named: "creditcard_icon_normal")
            leftImgView.frame = CGRect(x: 15, y: payNotiseLabel.frame.maxY + 10, width: 35, height: 30)
            checkOutBtn.setTitle(localized("kCOCheckOutButtonString"), for: .normal)
        } else if paymentType == 1 {
            //paypal
            leftImgView.image = UIImage(named: "address_paypal")
            leftImgView.frame = CGRect(x: 15, y: payNotiseLabel.frame.maxY + 10, width: 100, height: 30)
            checkOutBtn.setTitle(localized("kCOCheckOutButtonString"), for: .normal)
        } else if paymentType == 3 {
            //货到付款
            leftImgView.image = UIImage(named: "address_cashondelivery")
            leftImgView.frame = CGRect(x: 15, y: payNotiseLabel.frame.maxY + 10, width: 35, height: 30)
            checkOutBtn.setTitle(localized("kCOCashPlaceOrderButtonString"), for: .normal)
        } else {
            //stripe
            leftImgView.image = UIImage(named: "creditcard_icon_normal")
            leftImgView.frame = CGRect(x: 15, y: payNotiseLabel.frame.maxY + 10, width: 35, height: 30)
            checkOutBtn.setTitle(localized("kCOCheckOutButtonString"), for: .normal)
        }
        paymentView.addSubview(leftImgView)
        
        let moneyLabel = UILabel(frame: CGRect(x: leftImgView.frame.maxX + 10, y: payNotiseLabel.frame.maxY + 10, width: 200, height: 30))
        moneyLabel.tag = 200
        if paymentType == 3 {
            moneyLabel.text = localized("kCashDeliveryString")
            moneyLabel.font = UIFont(name: kGothamRoundedBold, size: kSizeFrom750(x: 30))
            moneyLabel.textColor = kRGBColorFromHex(rgbValue: 0x28558f)
        } else {
            moneyLabel.text = ""
        }
        paymentView.addSubview(moneyLabel)
        
//        if paymentType == 4 {
//            
//            securityCodeView = UIView(frame: CGRect(x: 0, y: leftImgView.frame.maxY + 10, width: KScreenWidth, height: 40))
//            paymentView.addSubview(securityCodeView)
//            
//            let secuLabel = UILabel(frame: CGRect(x: 15, y: 0, width: 250, height: 40))
//            secuLabel.text = "Please enter security code:"
//            secuLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
//            secuLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
//            securityCodeView.addSubview(secuLabel)
//            
//            secuCodeField = UITextField(frame: CGRect(x: KScreenWidth - 15 - 130, y: 5, width: 130, height: 30))
//            secuCodeField.placeholder = "Securety Code"
//            secuCodeField.keyboardType = .numberPad
//            securityCodeView.addSubview(secuCodeField)
//            
//            let monthLineView2 = UIView(frame: CGRect(x: KScreenWidth - 15 - 130, y: secuCodeField.frame.maxY, width: 130, height: 1))
//            monthLineView2.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
//            securityCodeView.addSubview(monthLineView2)
//            
//        }
        
        let arrowImgView = UIImageView(frame: CGRect(x: KScreenWidth - 12 - 12, y: payHeight / 2.0 - 15 / 2.0, width: 12, height: 12))
        arrowImgView.image = UIImage(named: "arrowrighticon")
        paymentView.addSubview(arrowImgView)
        
        scrollView.addSubview(paymentView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(paymentViewTapped))
        paymentView.addGestureRecognizer(tapGesture)
        
    }
    
    func setupOrderInfoView(paymentInfo: CartPaymentInfo) {
    
        orderView = CheckFooterView.init(frame: CGRect(x: 0, y: paymentView.frame.maxY, width: KScreenWidth, height: 270))
        scrollView.addSubview(orderView)
        orderView.setupPaymentPrice(cartPaymentInfo: paymentInfo)
        if let type = self.paymentType {
            orderView.setupCODInfo(selectedPayWay: type)
        }
        scrollView.contentSize = CGSize(width: KScreenWidth, height: orderView.frame.maxY + 20)
    }
    
    override func leftBackButtonTapped() {
        configCommonEventMonitor(monitorString: kCheckOutBackEvent, type: 2)
        let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        let configModel = AlertButtonModel(title: kCOContinuPaymentString, buttonType: .AlertButtonTypeNormal)
        let cancelModel = AlertButtonModel(title: kCONotNowString, buttonType: .AlertButtonTypeRed)
        alertView.showWithAlertView(title: kCOSureToLeaveString, detailString: kCOItemStillUnpaidString, buttonArr: [configModel,cancelModel],cancel: false)
        alertView.bottomButtonTapped = { (button: UIButton) -> Void in
            if button.tag == 101 {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Actions
    
    @objc func addressViewTapped() {
        let addressVC = UserAddressViewController()
        addressVC.isPaymentSelect = true
        if let tmpAddressInfo = commitAddressInfo {
            addressVC.paymentSelectAddressID = tmpAddressInfo.AddrID
        }
        addressVC.isSelectPaymentAddressInfoBlock = { (addressInfo: MyAddressInfo?) -> Void in
            if let addressInfo = addressInfo {
                self.setupAddressViewDatas(addressInfo: addressInfo)
                self.commitAddressInfo = addressInfo
            } else {
                let addButton = self.addressView.viewWithTag(103) as! UIButton
                addButton.isHidden = false
                
                let notiseLabel = self.addressView.viewWithTag(104) as! UILabel
                notiseLabel.isHidden = false
                
                let nameLabel = self.addressView.viewWithTag(100) as! UILabel
                nameLabel.isHidden = true
                
                let addressLabel = self.addressView.viewWithTag(101) as! UILabel
                addressLabel.isHidden = true
                
                let lineView = self.addressView.viewWithTag(102)
                lineView!.frame = CGRect(x: 15, y: notiseLabel.frame.maxY + 15, width: KScreenWidth - 30, height: 1)
                
                self.addressView.frame = CGRect(x: 0, y: 10, width: KScreenWidth, height: lineView!.frame.maxY + 1)
                self.paymentView.frame = CGRect(x: 0, y: self.addressView.frame.maxY, width: KScreenWidth, height: 100)
                
                self.orderView.frame = CGRect(x: 0, y: self.paymentView.frame.maxY, width: KScreenWidth, height: 270)
                self.commitAddressInfo = nil
            }
            
        }
        navigationController?.pushViewController(addressVC, animated: true)
    }

    @objc func paymentViewTapped() {
        let paymentVC = MyWalletViewController()
        paymentVC.isCheckoutPush = true
        if self.commitPayType == 0 { //没有支付方式
            paymentVC.selectedWalletID = 0
        } else if self.commitPayType == 1 { //payPal支付
            paymentVC.selectedWalletID = 1
        } else if self.commitPayType == 2 { //卡支付
            if let tmpCardInfo = self.commitCardInfo {
                paymentVC.selectedWalletID = tmpCardInfo.CardInfoID
            }
        } else if self.commitPayType == 3 { //货到付款
            paymentVC.selectedWalletID = 3
        }
        paymentVC.selectPaymentWayBlock = { (walletInfo: MyWalletInfo,selectedPayWay: Int) -> Void in
            self.commitPayType = selectedPayWay
            if selectedPayWay != 0 {
                self.commitCardInfo = walletInfo
                self.commitPayType = selectedPayWay
            }
            self.setupPaymentViewDatas(paymentInfo: walletInfo, selectedPayWay: selectedPayWay)
        }
        navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    @objc func addNewAddressInfoTapped() {
        let addressVC = UserAddressViewController()
        addressVC.isPaymentSelect = true
        addressVC.isSelectPaymentAddressInfoBlock = { (addressInfo: MyAddressInfo?) -> Void in
            if let tmpAddress = addressInfo {
                self.setupAddressViewDatas(addressInfo: tmpAddress)
                self.commitAddressInfo = tmpAddress
            }else {
                let addButton = self.addressView.viewWithTag(103) as! UIButton
                addButton.isHidden = false
                
                let notiseLabel = self.addressView.viewWithTag(104) as! UILabel
                notiseLabel.isHidden = false
                
                let nameLabel = self.addressView.viewWithTag(100) as! UILabel
                nameLabel.isHidden = true
                
                let addressLabel = self.addressView.viewWithTag(101) as! UILabel
                addressLabel.isHidden = true
                
                let lineView = self.addressView.viewWithTag(102)
                lineView!.frame = CGRect(x: 15, y: notiseLabel.frame.maxY + 15, width: KScreenWidth - 30, height: 1)
                
                self.addressView.frame = CGRect(x: 0, y: 10, width: KScreenWidth, height: lineView!.frame.maxY + 1)
                self.paymentView.frame = CGRect(x: 0, y: self.addressView.frame.maxY, width: KScreenWidth, height: 100)
                self.orderView.frame = CGRect(x: 0, y: self.paymentView.frame.maxY, width: KScreenWidth, height: 270)
                self.commitAddressInfo = nil
            }
            
        }
        navigationController?.pushViewController(addressVC, animated: true)
    }
    
    func setupAddressViewDatas(addressInfo: MyAddressInfo) {
        
        let addButton = addressView.viewWithTag(103) as! UIButton
        addButton.isHidden = true
        
        let notiseLabel = addressView.viewWithTag(104) as! UILabel
        notiseLabel.isHidden = true
        
        let nameLabel = addressView.viewWithTag(100) as! UILabel
        nameLabel.isHidden = false
        nameLabel.text = "\(addressInfo.Name)     \(addressInfo.Phone)"
        
        let addressString = "\(addressInfo.Apt) \(addressInfo.Address)  \(addressInfo.City) \(addressInfo.States) \(addressInfo.Contry) \(addressInfo.ZipCode)"
        let addressLabel = addressView.viewWithTag(101) as! UILabel
        addressLabel.text = addressString
        let addressH = addressString.getHeightWithConstrainedWidth(width: KScreenWidth - 30, font: UIFont(name: kMSReference, size: 13)!)
        addressLabel.frame = CGRect(x: 15, y: nameLabel.frame.maxY, width: KScreenWidth - 30, height: addressH)
        addressLabel.isHidden = false
        let lineView = addressView.viewWithTag(102)
        lineView!.frame = CGRect(x: 15, y: addressLabel.frame.maxY + 15, width: KScreenWidth - 30, height: 1)
        
        addressView.frame = CGRect(x: 0, y: 10, width: KScreenWidth, height: lineView!.frame.maxY + 1)
        paymentView.frame = CGRect(x: 0, y: addressView.frame.maxY, width: KScreenWidth, height: 100)
        orderView.frame = CGRect(x: 0, y: paymentView.frame.maxY, width: view.frame.width, height: 270)
    }
    
    func setupPaymentViewDatas(paymentInfo: MyWalletInfo?,selectedPayWay: Int) {
        orderView.setupCODInfo(selectedPayWay: selectedPayWay)
        let maxY = 55
        let leftImgView = paymentView.viewWithTag(201) as! UIImageView
        let notiseLabel = paymentView.viewWithTag(200) as! UILabel
        if selectedPayWay == 1 {
            //paypal
            leftImgView.frame = CGRect(x: 15, y: maxY, width: 100, height: 30)
            leftImgView.image = UIImage(named: "address_paypal")
            notiseLabel.text = ""
            checkOutBtn.setTitle(localized("kCOCheckOutButtonString"), for: .normal)
        } else if selectedPayWay == 3 {
            //货到付款
            leftImgView.frame = CGRect(x: 15, y: maxY, width: 35, height: 30)
            notiseLabel.frame = CGRect(x: Int(leftImgView.frame.maxX + 10), y: maxY, width: 200, height: 30)
            notiseLabel.text = localized("kCashDeliveryString")
            notiseLabel.font = UIFont(name: kGothamRoundedBold, size: kSizeFrom750(x: 30))
            notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x28558f)
            leftImgView.image = UIImage(named: "address_cashondelivery")
            checkOutBtn.setTitle(localized("kCOCashPlaceOrderButtonString"), for: .normal)
            
        } else {
            //stripe
            checkOutBtn.setTitle(localized("kCOCheckOutButtonString"), for: .normal)
            if let tmpPaymentInfo = paymentInfo {
                leftImgView.frame = CGRect(x: 15, y: maxY, width: 35, height: 30)
                notiseLabel.frame = CGRect(x: Int(leftImgView.frame.maxX + 10), y: maxY, width: 200, height: 30)
                notiseLabel.text = "*\(tmpPaymentInfo.CardNum)"
                if tmpPaymentInfo.CardType == "Visa" {
                    leftImgView.image = UIImage(named: "card_visa_icon")
                } else if tmpPaymentInfo.CardType == "MasterCard" {
                    leftImgView.image = UIImage(named: "card_mastercard_icon")
                } else if tmpPaymentInfo.CardType == "American Express" {
                    leftImgView.image = UIImage(named: "card_americanexpress_icon")
                } else if tmpPaymentInfo.CardType == "JCB" {
                    leftImgView.image = UIImage(named: "card_jcb_icon")
                } else if tmpPaymentInfo.CardType == "Discover" {
                    leftImgView.image = UIImage(named: "card_discover_icon")
                } else if tmpPaymentInfo.CardType == "Diners Club" {
                    leftImgView.image = UIImage(named: "card_dinersclub_icon")
                }
            }
        }
    }
    
    //支付
    @IBAction func checkOutTapped(_ sender: UIButton) {
        setupCheckOutCurrentOrder()
    }
    
    func setupCheckOutCurrentOrder() {
        //地址
        if self.commitAddressInfo == nil {
            showToast(kCOSelectAAddressString)
            return
        }
        if self.commitPayType == 0 {
            showToast(kCOSelectAPaymentString)
            return
        } else if self.commitPayType == 1 {
            //PayPal支付
            self.configAppEventMoniter(monitorString: kSelectPayPalCheckOutEvent, staticsType: "2")
            configPayPalCheckOut()
        } else if self.commitPayType == 2 {
            //信用卡支付
            self.configAppEventMoniter(monitorString: kSelectStripeCheckOutEvent, staticsType: "2")
            configStripeCardCheckOut()
        } else if self.commitPayType == 3 {
            //货到付款
            self.configAppEventMoniter(monitorString: kSelectCashCheckOutEvent, staticsType: "2")
            configCashDeliveryCheckOut()
        }
    }
    
    func configStripeCardCheckOut() {
    
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        if let payorderInfo = self.payOrderInfo {
            if let orderSuccessInfo = self.tmpOrderSuccessInfo {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                let countryCode = getCurrentCountryCodeString()
                let countryPay = getCurrentPayCharacter()
                let param = ["UserID":userID,
                             "OrderPayTotal": payorderInfo.OrderPayTotal,
                             "CardID": self.commitCardInfo!.CardInfoID,
                             "AddrID": self.commitAddressInfo!.AddrID,
                             "PayCode": orderSuccessInfo.PayCode,
                             "ClientType": 1,
                             "CountrySName":countryCode,
                             "CountryCurrency":countryPay]
                FPHttpManager().manager.post(StripeCardCheckOut, parameters: param, progress: nil, success: { (task, result) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    let json = JSON.init(result as Any)
                    print("configStripeCardCheckOut\(json)")
                    if json["Code"].intValue == 200 {
                        self.configStripeCheckOutSuccessEvent(monitor: kStripeCheckOutSuccess, totalPrice: payorderInfo.OrderPayTotal, productID: self.itemProductIDString, numberItem: self.itemProductCount, eventContent: self.itemParamsArr)
                        self.checkOutsuccess()
                    } else if json["Code"].intValue == 400 { //已经被支付过了
                        let string = json["Msg"].stringValue
                        showToast(string)
                        self.navigationController?.popToRootViewController(animated: true)
                    } else {
                        self.configCheckOutFailEvent(monitor: kStripeCheckOutFail)
                        self.checkOutFailed()
                    }
                }) { (task, error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
    }
    
    func configPayPalCheckOut() {
        //获取paypal客户端token
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        if let payorderInfo = self.payOrderInfo {
            if let orderSuccessInfo = self.tmpOrderSuccessInfo {
                MBProgressHUD.showAdded(to: self.view, animated: true)
                FPHttpManager().manager.post(GetTokenInterface, parameters: param, progress: nil, success: { (task, result) in
                    
                    let json = JSON(result as Any)
                    let token = json["Data"]["ClientToken"].stringValue
                    
                    self.braintreeClient = BTAPIClient.init(authorization: token)
                    self.payPalDriver = BTPayPalDriver.init(apiClient: self.braintreeClient!)
                    self.payPalDriver?.viewControllerPresentingDelegate = self
                    self.payPalDriver?.appSwitchDelegate = self
                    
                    let request = BTPayPalRequest.init(amount: String(2.32))
                    request.displayName = "faypark"
                    request.offerCredit = true
                    request.currencyCode = getCurrentPaymentCodeString()
                    
                    self.payPalDriver?.requestOneTimePayment(request, completion: { (payNonce, error) in
                        if let tmpPayNonce = payNonce {
                            let countryCode = getCurrentCountryCodeString()
                            let countryPay = getCurrentPayCharacter()
                            let payParam = ["UserID":userID,
                                            "payment_method_nonce":tmpPayNonce.nonce,
                                            "OrderPayTotal":payorderInfo.OrderPayTotal,
                                            "AddrID":self.commitAddressInfo!.AddrID,
                                            "PayCode":orderSuccessInfo.PayCode,
                                            "ClientType":1,
                                            "CountrySName":countryCode,
                                            "CountryCurrency":countryPay]
                            FPHttpManager().manager.post(PaypleCheckOut, parameters: payParam, progress: nil, success: { (task, result) in
                                let json = JSON(result as Any)
                                print("configPayPalCheckOut\(json)")
                                MBProgressHUD.hide(for: self.view, animated: true)
                                //付款成功,弹出提示
                                if json["Code"].intValue == 200 {
                                    self.configPayPalCheckOutSuccessEvent(monitor: kPayPalCheckOutSuccess, totalPrice: payorderInfo.OrderPayTotal, productID: self.itemProductIDString, numberItem: self.itemProductCount, eventContent: self.itemParamsArr)
                                    //显示支付成功界面
                                    self.checkOutsuccess()
                                } else {
                                    self.configCheckOutFailEvent(monitor: kPayPalCheckOutFail)
                                    self.checkOutFailed()
                                }
                                
                            }, failure: { (task, error) in
                                MBProgressHUD.hide(for: self.view, animated: true)
                            })
                        } else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    })
                }) { (task, error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
    }
    func configCashDeliveryCheckOut() {
        let userID = UserManager.shareUserManager.getUserID()
        if let payorderInfo = self.payOrderInfo,let orderSuccessInfo = self.tmpOrderSuccessInfo,let addressInfo = self.addressInfo {
            let param = ["UserID":userID,
                         "AddrID": addressInfo.AddrID,
                         "PayCode": orderSuccessInfo.PayCode,
                         "ClientType": 1] as [String : Any]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            FPHttpManager().manager.post(CashPaymentInterface, parameters: param, progress: nil, success: { (task, result) in
                MBProgressHUD.hide(for: self.view, animated: true)
                let json = JSON.init(result as Any)
                print("cashCheckOutButtonTapped\(json)")
                if json["Code"].stringValue == "200" {
                    self.checkOutsuccess()
                    self.configCashCheckOutSuccessEvent(monitor: kCashCheckOutSuccess, totalPrice: payorderInfo.OrderPayTotal, productID: self.itemProductIDString, numberItem: self.itemProductCount, eventContent: self.itemParamsArr)
                } else {
                    self.checkOutFailed()
                }
            }) { (task, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.checkOutFailed()
            }
        }
    }
    
    func configMercadoCheckOut() {
        if let payorderInfo = self.payOrderInfo,let orderSuccessInfo = self.tmpOrderSuccessInfo  {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let userID = UserManager.shareUserManager.getUserID()
            let param = ["UserID": userID]
            //获取PublishKey
            FPHttpManager().manager.post(MercadoPublishKeyInterface, parameters: param, progress: nil, success: { (task, result) in
                MBProgressHUD.hide(for: self.view, animated: true)
                let json = JSON.init(result as Any)
                print("configMercadoCheckOut\(json)")
                if json["Code"].stringValue == "200" {
                    let publishKey = json["Data"]["MPPublishableKey"].stringValue
                    let MercadoTokenInterface = "https://api.mercadopago.com/v1/card_tokens?public_key=\(publishKey)"
                    let tokenParams = ["card_number":"5474925432670366",
                                       "security_code":"366",
                                       "expiration_month":11,
                                       "expiration_year":2020,
                                       "cardholder":["name":"test"]] as [String : Any]
                    //获取token
                    let manage = FPHttpManager().manager
                    manage.requestSerializer = AFJSONRequestSerializer()
                    manage.responseSerializer.acceptableContentTypes = NSSet(array: ["text/html","application/json","text/json","text/plain","text/javascript"]) as? Set<String>
                    manage.post(MercadoTokenInterface, parameters: tokenParams, progress: nil, success: { (task, result) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let json = JSON.init(result as Any)
                        print("MercadoTokenInterface\(json)")
                        let mpToken = json["id"].stringValue
                        //获取customID
                        let customerIDParams = ["UserID": userID,
                                                "UserEmail":"test_payer_99543265@testuser.com"]
                        
                        FPHttpManager().manager.post(MercadoCustomIDInterface, parameters: customerIDParams, progress: nil, success: { (task, result) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let json = JSON.init(result as Any)
                            print("MercadoCustomIDInterface\(json)")
                            if json["Code"].stringValue == "200" {
                                let customerID = json["Data"]["MPCustomerId"].stringValue
                                let createCardParams = ["UserID": userID,
                                                        "MPCustomerId":customerID,
                                                        "MPToken": mpToken,
                                                        "ClientType": "1"]
                                print("createCard\(createCardParams)")
                                FPHttpManager().manager.post(MercadoCreateCardInterface, parameters: createCardParams, progress: nil, success: { (task, result) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    let json = JSON.init(result as Any)
                                    print("MercadoCreateCardInterface\(json)")
                                    if json["Code"].stringValue == "200" {
                                        let cardID = json["Data"]["MPCardId"].stringValue
                                        let checkOutParams = ["UserID": userID,
                                                              "OrderPayTotal": payorderInfo.OrderPayTotal,
                                                              "CardID":cardID,
                                                              "AddrID":self.commitAddressInfo!.AddrID,
                                                              "PayCode":orderSuccessInfo.PayCode,
                                                              "ClientType":"1"] as [String : Any]
                                        FPHttpManager().manager.post(MercadoCheckOutInterface, parameters: checkOutParams, progress: nil, success: { (task, result) in
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                            let json = JSON.init(result as Any)
                                            print("MercadoCheckOutInterface\(json)")
                                            if json["Code"].stringValue == "200" {
                                                //self.configPayPalCheckOutSuccessEvent(monitor: kPayPalCheckOutSuccess, totalPrice: payorderInfo.OrderPayTotal, productID: self.itemProductIDString, numberItem: self.itemProductCount, eventContent: self.itemParamsArr)
                                                //显示支付成功界面
                                                self.checkOutsuccess()
                                            } else {
                                                self.configCheckOutFailEvent(monitor: kPayPalCheckOutFail)
                                                self.checkOutFailed()
                                            }
                                        }) { (task, error) in
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                        }
                                    } else {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                }) { (task, error) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                }
                            } else {
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        }) { (task, error) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    }) { (task, error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }) { (task, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
 
    func configCashCheckOutSuccessEvent(monitor: String,totalPrice:Double,productID: String,numberItem: Int,eventContent:[[String: Any]]) {
        let userID = UserManager.shareUserManager.getUserID()
        let totalPriceString = decimalNumberWithDouble(convertValue: totalPrice)
        let totalDoble = Double(totalPriceString)!
        let eventContentJsonString = convertArrayToString(arr: eventContent as [AnyObject])
        let currencyString = getCurrentPaymentCodeString()
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: monitor, productID: productID)
        AppsFlyerTracker.shared().trackEvent(monitor, withValues: [AFEventParamPrice:totalDoble, AFEventParamCurrency: currencyString, AFEventParamContentType: "product_group", AFEventParamContentId:productID, AFEventContent: eventContentJsonString,"num_items":numberItem,"content_name":monitor])
    }
    //Stripe支付成功监测
    func configStripeCheckOutSuccessEvent(monitor: String,totalPrice:Double,productID: String,numberItem: Int,eventContent:[[String: Any]]) {
        let userID = UserManager.shareUserManager.getUserID()
        let totalPriceString = decimalNumberWithDouble(convertValue: totalPrice)
        let totalDoble = Double(totalPriceString)!
        let eventContentJsonString = convertArrayToString(arr: eventContent as [AnyObject])
        let currencyString = getCurrentPaymentCodeString()
        //FayPark
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: monitor, productID: productID)
        //FBS
        let params = [FBSDKAppEventParameterNameContentID : productID, FBSDKAppEventParameterNameContentType : "product_group", FBSDKAppEventParameterNameNumItems : numberItem, FBSDKAppEventParameterNamePaymentInfoAvailable : 0,
                      FBSDKAppEventParameterNameCurrency : currencyString,
                      FBSDKAppEventNameViewedContent:eventContentJsonString] as [String : Any]
        FBSDKAppEvents.logPurchase(totalDoble, currency: currencyString, parameters: params)
        //AppsFlyer
        AppsFlyerTracker.shared().trackEvent(monitor, withValues:[AFEventParamPrice:totalPriceString, AFEventParamCurrency: currencyString, AFEventParamContentType: "product_group", AFEventParamContentId:productID, AFEventContent: eventContentJsonString,"num_items":numberItem,"content_name":monitor])
        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId:productID,AFEventParamContentType: "product_group",AFEventParamRevenue:totalPriceString,AFEventParamCurrency: currencyString])
    }
    func configCheckOutFailEvent(monitor: String) {
        let userID = UserManager.shareUserManager.getUserID()
        FBSDKAppEvents.logEvent(monitor)
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: monitor, productID: "0")
        AppsFlyerTracker.shared().trackEvent(monitor, withValues: nil)
    }

    func configPayPalCheckOutSuccessEvent(monitor: String,totalPrice:Double,productID: String,numberItem: Int,eventContent:[[String: Any]]) {
        let userID = UserManager.shareUserManager.getUserID()
        let totalPriceString = decimalNumberWithDouble(convertValue: totalPrice)
        let totalDoble = Double(totalPriceString)!
        let eventContentJsonString = convertArrayToString(arr: eventContent as [AnyObject])
        let currencyString = getCurrentPaymentCodeString()
        //Faypark
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: monitor, productID: productID)
        //FBS
        let params = [FBSDKAppEventParameterNameContentID : productID, FBSDKAppEventParameterNameContentType : "product_group", FBSDKAppEventParameterNameNumItems : numberItem, FBSDKAppEventParameterNamePaymentInfoAvailable : 0,
                      FBSDKAppEventParameterNameCurrency : currencyString,
                      FBSDKAppEventNameViewedContent:eventContentJsonString] as [String : Any]
        FBSDKAppEvents.logPurchase(totalDoble, currency: currencyString, parameters: params)
        //AppsFlyer
        AppsFlyerTracker.shared().trackEvent(monitor, withValues:[AFEventParamPrice:totalPriceString, AFEventParamCurrency: currencyString, AFEventParamContentType: "product_group", AFEventParamContentId:productID,"num_items":numberItem,"content_name":monitor])
        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId:productID,AFEventParamContentType: "product_group",AFEventParamRevenue:totalPriceString,AFEventParamCurrency: currencyString])
    }
    
    func checkOutsuccess() {
        //成功
        let orderCompletVC = OrderCompleteViewController()
        self.navigationController?.pushViewController(orderCompletVC, animated: true)
    }
    func checkOutFailed() {
        configCommonEventMonitor(monitorString: kCheckOutFail, type: 1)
        let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        let alertModel = AlertButtonModel(title: kPMTryAgainString, buttonType: .AlertButtonTypeNormal)
        alertView.showWithAlertView(title: kPMSomethingWrongString, detailString: kPMRecheckPaymentString, buttonArr: [alertModel],cancel: false)
        alertView.bottomButtonTapped = { (button: UIButton) -> Void in
            self.setupCheckOutCurrentOrder()
        }
    }
    func configAppEventMoniter(monitorString: String,staticsType: String) {
        let userID = UserManager.shareUserManager.getUserID()
        FBSDKAppEvents.logEvent(monitorString)
        AppsFlyerTracker.shared().trackEvent(monitorString, withValues: nil)
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: staticsType, name: monitorString, productID: "0")
    }
}

//PayPal 支付
extension CheckOutViewController: PayPalPaymentDelegate {
   
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        MBProgressHUD.hide(for: self.view, animated: true)
        checkOutFailed()
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        
    }
}

extension CheckOutViewController: BTAppSwitchDelegate,BTViewControllerPresentingDelegate {
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        
    }
    
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
    }
}

