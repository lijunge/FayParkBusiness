//
//  CartViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/26.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit
import JTSImageViewController
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import AppsFlyerLib

class CartViewController: UIViewController {
    
    
    @IBOutlet weak var downTrainImgView: UIImageView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var allSelectButton: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var freeNotiseLabel: UILabel!
    
    var giftHeaderView = CartFreeGiftHeaderView()
    //购物车为空时显示的View
    var goShoppingView: CartEmptyView!
    //tableView的FootView
    var footerView : CartFooterView!
    
    //购物车所有数据
    var cartAllInfo: CartInfo?
    var cartProductArr = [CartProduct]()
    //freegift
    var freeGiftInfo: FreeGiftInfo?
    //支付信息
    var allPaymentInfo: CartPaymentInfo?
    //创建订单成功信息
    var tmpOrderSuccessInfo: TmpPayOrderSuccessInfo?
    
    //倒计时商品倒计时identier
    let countDownIdentifier = "countDownIdentifier"
    //随机倒计时商品
    let randomCountDownIdentifier = "randomCountDownIdentifier"
    //活动街拍商品
    let activityCountDownIdentifier = "activityCountDownIdentifier"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserManager.shareUserManager.getUserName().isEmpty {
            let loginVC = NewLoginViewController()
            self.present(loginVC, animated: true, completion: nil)
        } else {
            //请求购物车数据
            requestCartDataFormServer()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = kCartTitleString
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        //self.freeNotiseLabel.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 26))
        self.totalPriceLabel.adjustsFontSizeToFitWidth = true
        configCheckOutEventMonitor(monitor: kCartPage, type: 1)
        setupSubViews()
    }
    
    func setupSubViews() {
        setupTableView()
        setupEmptyView()
    }
    
    func setupTableView() {
        //注册商品cell和结算cell
        tableView.register(UINib.init(nibName: "ShoppingCartCell", bundle: nil), forCellReuseIdentifier: "ShoppingCartCell")
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        tableView.rowHeight = 190
        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.showsVerticalScrollIndicator = false
        setupGiftHeaderView()
        setupTableFooterView()
    }
    
    func setupGiftHeaderView() {
        giftHeaderView = CartFreeGiftHeaderView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 120))
        //进入选择freeGift
        giftHeaderView.freeGiftDetailTapped = { () -> Void in
            let freeGiftVC = FreeGiftViewController()
            freeGiftVC.selectedFreeGiftInfo = self.freeGiftInfo
            freeGiftVC.selectFreeGiftInfoBlock = { (freeGiftInfo: FreeGiftInfo) -> Void in
                self.cartAllInfo?.FreeGiftList.append(freeGiftInfo)
                self.freeGiftInfo = freeGiftInfo
                self.giftHeaderView.freeGiftInfo = freeGiftInfo
                let tmpCartArr = self.cartProductArr
                var selectArr = [CartProduct]()
                for i in 0 ..< tmpCartArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        selectArr.append(tmpProdcutInfo)
                    }
                }
                if selectArr.count >= 1 {
                    self.giftHeaderView.changeShippingPriceFree(isFree: true)
                } else {
                    self.giftHeaderView.changeShippingPriceFree(isFree: false)
                }
                let freeHeight = CGFloat(self.giftHeaderView.giftHeight)
                self.giftHeaderView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: freeHeight)
                self.setupNavigationTitleAndTabBarBadge(cartProductCount: tmpCartArr.count)
            }
            self.navigationController?.pushViewController(freeGiftVC, animated: true)
        }
        //是否选择购买freegift
        giftHeaderView.freeGiftSelectedTapped = { (sender: UIButton) -> Void in
            let tmpCartArr = self.cartProductArr
            var selectArr = [CartProduct]()
            for i in 0 ..< tmpCartArr.count {
                let tmpProdcutInfo = self.cartProductArr[i]
                if tmpProdcutInfo.isSelectProduct {
                    selectArr.append(tmpProdcutInfo)
                }
            }
            self.configSelectCartPaymentPrice(selectArr: selectArr)
        }
        tableView.tableHeaderView = giftHeaderView
    }
    func setupTableFooterView() {
        footerView = CartFooterView.init(frame: CGRect(x: 0, y: 10, width: view.frame.width, height: 240))
        footerView.rapidPaymentButtonTapped = { (sender: UIButton,totalPrice: Double) -> Void in
            self.totalPriceLabel.text = String(format: "%@%.2f", getCurrentPayCharacter(),totalPrice)
        }
        footerView.shippingInfoTapped = { (sender: UIButton) -> Void in
            let shippingVC = NewProductInfoViewController()
            shippingVC.type = 2
            self.navigationController?.pushViewController(shippingVC, animated: true)
        }
        footerView.isHidden = true
        tableView.tableFooterView = footerView
    }
    func setupEmptyView() {
        goShoppingView = CartEmptyView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight))
        goShoppingView.fromWhere = 0
        goShoppingView.isHidden = true
        view.addSubview(goShoppingView)
    }
    
    func setupNavigationTitleAndTabBarBadge(cartProductCount: Int) {
        let tabbarVC = UIApplication.shared.keyWindow?.rootViewController
        if !self.giftHeaderView.giftView.isHidden {
            //有freegift
            if cartProductCount == 0 {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "1"
            } else {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "\(cartProductCount + 1)"
            }
            self.navigationItem.title = String(format: "%@(%d)",kCartTitleString,cartProductCount + 1)
        } else {
            //没有freegift
            if cartProductCount == 0 {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = nil
                self.navigationItem.title = kCartTitleString
            } else {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "\(cartProductCount)"
                self.navigationItem.title = String(format: "%@(%d)",kCartTitleString,cartProductCount)
                
            }
        }
    }
    
    // MARK: - 获取购物车数据
    func requestCartDataFormServer() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(CartInfoInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as Any)
            print("购物车数据\(json)")
            self.cartAllInfo = CartInfo(jsonData: json["Data"])
            if let tmpCartInfo = self.cartAllInfo {
                if tmpCartInfo.ProductList.count == 0 && tmpCartInfo.FreeGiftList.count == 0 {
                    self.goShoppingView.isHidden = false
                } else {
                    self.goShoppingView.isHidden = true
                    if let freeInfoDic =  UserDefaults.standard.object(forKey: "SelectFreeInfo") as? [String: Any] {
                        let freeJson = JSON(freeInfoDic)
                        self.freeGiftInfo = FreeGiftInfo(jsonData: freeJson)
                    }
                    if self.freeGiftInfo == nil{
                        self.freeGiftInfo = tmpCartInfo.FreeGiftList.first
                        self.giftHeaderView.freeGiftInfo = self.freeGiftInfo
                    } else {
                        self.giftHeaderView.freeGiftInfo = self.freeGiftInfo
                    }
                    self.giftHeaderView.changeFreeShipping(tmpCartInfo.PayCount)
                    self.cartProductArr = tmpCartInfo.ProductList
                    let freeHeight = CGFloat(self.giftHeaderView.giftHeight)
                    self.giftHeaderView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: freeHeight)
                    
                    //默认情况下 全部选中的状态
                    self.freeNotiseLabel.isHidden = true
                    self.downTrainImgView.isHidden = true
                    /*
                    if self.cartProductArr.count >= tmpCartInfo.PayCount {
                        self.freeNotiseLabel.isHidden = true
                        self.downTrainImgView.isHidden = true
                        self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
                        self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 51)
                        self.bottomView.addConstraint(self.bottomViewHeightConstraint)
                    } else {
                        self.downTrainImgView.isHidden = false
                        self.freeNotiseLabel.text = String(format: "Free Shipping(%d different items,free gift not included)", tmpCartInfo.PayCount)
                        self.freeNotiseLabel.isHidden = false
                        self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
                        self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 91)
                        self.bottomView.addConstraint(self.bottomViewHeightConstraint)
                    }
                    */
                    self.allSelectButton.isSelected = true
                    self.setupNavigationTitleAndTabBarBadge(cartProductCount: self.cartProductArr.count)
                    self.footerView.isHidden = false
                    self.configDefultCartPaymentPrice(productArr: tmpCartInfo.ProductList)
                    self.setupCountDownProduct() //倒计时商品
                    self.setupActivityCountDownProduct() //活动街拍商品
                }
                self.tableView.reloadData()
            }
        }) { (task, error) in
            
        }
    }
    //MARK: - 倒计时 IsTime = 2
    func setupCountDownProduct() {
        var timerProductArr = [CartProduct]()
        for i in 0 ..< self.cartProductArr.count {
            let productInfo = self.cartProductArr[i]
            if productInfo.IsTime == 2 { //倒计时商品
                timerProductArr.append(productInfo)
            }
        }
        if timerProductArr.count != 0 {
            let timerProductInfo = timerProductArr.first!
            //有倒计时商品
            let countDownInter = TimeInterval(timerProductInfo.Number)
            TimeCountDownManager.manager.scheduledCountDownWith(countDownIdentifier, timeInteval: countDownInter, countingDown: { (timeInterval) in
                
            }) { (timeInterval) in
                //倒计时时间到的话，需要更新倒计时商品的价格
                let tmpProductArr = self.cartProductArr
                for i in 0 ..< tmpProductArr.count {
                    let productInfo = self.cartProductArr[i]
                    if productInfo.IsTime == 2 {//倒计时商品
                        productInfo.IsTime = 0 // 正常商品
                    }
                }
                self.tableView.reloadData()
                var selectArr = [CartProduct]()
                for i in 0 ..< self.cartProductArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        selectArr.append(tmpProdcutInfo)
                    }
                }
                self.configSelectCartPaymentPrice(selectArr: selectArr)
            }
        }
    }
    //MARK: - 随机倒计时商品
    func setupRandomCountDownProduct(atIndex: Int) {
        for i in 0 ..< self.cartProductArr.count {
            let productInfo = self.cartProductArr[i]
            if productInfo.IsTime == 1 { //随机倒计时商品
                //有倒计时商品
                let tmpRandomCountDownIdentifier = "\(randomCountDownIdentifier) + \(productInfo.UserCarID)"
                let randominterval = TimeInterval(productInfo.Number)
                TimeCountDownManager.manager.scheduledCountDownWith(tmpRandomCountDownIdentifier, timeInteval: randominterval, countingDown: { (timeInterval) in
                    
                    if let headerView = self.tableView.viewWithTag(productInfo.UserCarID) {
                        let time = Int(timeInterval)
                        let titleLabel = headerView.viewWithTag(100) as! UILabel
                       
                        if time / 60 < 10 {
                            titleLabel.text = String(format: "0%ld", time / 60)
                        } else {
                            titleLabel.text = String(format: "%ld", time / 60)
                        }

                        let secondLabel = headerView.viewWithTag(101) as! UILabel
                        if time % 60 < 10 {
                            secondLabel.text = String(format: "0%ld", time % 60)
                        } else {
                            secondLabel.text = String(format: "%ld", time % 60)
                        }
                    }
                }) { (timeInterval) in
                    //倒计时时间到的话，需要更新倒计时商品的价格
                    if i < self.cartProductArr.count {
                        let tmpProductInfo = self.cartProductArr[i]
                        if tmpProductInfo.ProductID == productInfo.ProductID {
                            tmpProductInfo.IsTime = 0
                            self.tableView.reloadData()
                        }
                    }
                    var selectArr = [CartProduct]()
                    for i in 0 ..< self.cartProductArr.count {
                        let tmpProdcutInfo = self.cartProductArr[i]
                        if tmpProdcutInfo.isSelectProduct {
                            selectArr.append(tmpProdcutInfo)
                        }
                    }
                    self.configSelectCartPaymentPrice(selectArr: selectArr)
                }
            }
        }
    }
    //MARK： 活动商品倒计时
    func setupActivityCountDownProduct() {
        var timerProductArr = [CartProduct]()
        for i in 0 ..< self.cartProductArr.count {
            let productInfo = self.cartProductArr[i]
            if productInfo.IsTime == 3 { //倒计时商品
                timerProductArr.append(productInfo)
            }
        }
        if timerProductArr.count != 0 {
            let timerProductInfo = timerProductArr.first!
            //有倒计时商品
            let countDownInter = TimeInterval(timerProductInfo.Number)
            TimeCountDownManager.manager.scheduledCountDownWith(activityCountDownIdentifier, timeInteval: countDownInter, countingDown: { (timeInterval) in
            }) { (timeInterval) in
                //倒计时时间到的话，需要更新倒计时商品的价格
                let tmpProductArr = self.cartProductArr
                for i in 0 ..< tmpProductArr.count {
                    let productInfo = self.cartProductArr[i]
                    if productInfo.IsTime == 3 {//倒计时商品
                        productInfo.IsTime = 0 // 正常商品
                    }
                }
                self.tableView.reloadData()
                var selectArr = [CartProduct]()
                for i in 0 ..< self.cartProductArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        selectArr.append(tmpProdcutInfo)
                    }
                }
                self.configSelectCartPaymentPrice(selectArr: selectArr)
            }
        }
    }
    //根据所选商品显示
    func configSelectCartPaymentPrice(selectArr: [CartProduct]) {
        let isSelectedFreeGift: Bool = giftHeaderView.getFreeGiftSelectedState()
        let isSelectedRapidPayment: Bool = self.footerView.getRapidPaymentState()
        DispatchQueue.global().async {
            var handleTotalPrice: Double = 0.0
            var actualPrice: Double = 0.0 //实际支付商品总金额
            var shippingPrice: Double = 0.0 //总运费
            var productShippingPrice = 0.0 //商品运费
            var freeGiftShippingPrice = 0.0 //FreeGift运费
            var comIDSet = Set<Int>() //供应商ID 不重复
            let freeCount = self.cartAllInfo!.PayCount
            for item in 0 ..< selectArr.count {
                let productInfo = selectArr[item]
                var productShipping = 0.0
                var countPrice = 0.0
                if productInfo.IsTime == 0 {
                    countPrice = Double(productInfo.Count) * productInfo.SellingPrice
                } else {
                    countPrice = Double(productInfo.Count) * productInfo.SharePrice
                }
                handleTotalPrice += productInfo.Msrp
                actualPrice += countPrice
                if selectArr.count >= freeCount {
                    productShipping = 0.0
                } else {
                    productShipping = productInfo.Shipping
                }
                productShippingPrice += Double(productInfo.Count) * productShipping
                comIDSet.insert(productInfo.ComID)
            }
            if selectArr.count == 0 {  //0  1-3 3
                if isSelectedFreeGift {
                    if let tmpGift = self.freeGiftInfo {
                        freeGiftShippingPrice = Double(tmpGift.Shipping)
                    }
                } else {
                    freeGiftShippingPrice = 0.0
                }
            } else if selectArr.count >= freeCount {
                productShippingPrice = 0.0
                freeGiftShippingPrice = 0.0
            } else {
                freeGiftShippingPrice = 0.0
            }
            
            if isSelectedFreeGift {
                if selectArr.count == 0 {
                    comIDSet.insert(0)
                }
            } else {
                if comIDSet.contains(0) {
                    comIDSet.remove(0)
                }
            }
            //运费=普通运费+freegift运费
            shippingPrice = productShippingPrice + freeGiftShippingPrice
            let rapidPrice: Double = self.cartAllInfo!.ExpressShipping * Double(comIDSet.count)
            var totalPrice: Double = 0.0
            if isSelectedRapidPayment {
                totalPrice = actualPrice + shippingPrice + rapidPrice
            } else {
                totalPrice = actualPrice + shippingPrice
            }
            let params = ["handlePrice": handleTotalPrice,
                          "actualPrice": actualPrice,
                          "estimatedShipping": shippingPrice,
                          "rapidPrice": rapidPrice,
                          "orderTotalPrice": totalPrice,
                          "isRapidSelected": isSelectedRapidPayment] as [String : Any]
            let json = JSON(params)
            let cartPayInfo = CartPaymentInfo(params: json)
            DispatchQueue.main.async(execute: {
                self.allPaymentInfo = cartPayInfo
                self.footerView.setupPaymentPrice(cartPaymentInfo: cartPayInfo)
                self.totalPriceLabel.text = String(format: "%@%.2f", getCurrentPayCharacter(),totalPrice)
            })
        }
    }
    /**
     * 首次进入购物车时显示的结算清单
     */
    func configDefultCartPaymentPrice(productArr: [CartProduct]) {
        if let cartAllInfo = self.cartAllInfo {
            if productArr.count >= cartAllInfo.PayCount {
                let tmpCartArr = productArr
                for i in 0 ..< tmpCartArr.count {
                    let tmpCartInfo = productArr[i]
                    tmpCartInfo.isShippingFree = true
                }
            }
            if self.cartProductArr.count == 0 {
                giftHeaderView.changeShippingPriceFree(isFree: false)
            } else {
                giftHeaderView.changeShippingPriceFree(isFree: true)
            }
            configSelectCartPaymentPrice(selectArr: productArr)
        }
    }
    
    //MARK:- Actions
    @IBAction func allSelectButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected { //全部选中
            print("全部选中")
            //self.giftHeaderView.changeFreeFiftSelectState(isSelected: true)
            if self.cartProductArr.count >= 1 {
                self.giftHeaderView.changeShippingPriceFree(isFree: true)
            } else {
                self.giftHeaderView.changeShippingPriceFree(isFree: false)
            }
            var isFree = false
            if self.cartProductArr.count >= (self.cartAllInfo?.PayCount)! {
                isFree = true
            }
            let tmpCartArr = self.cartProductArr
            for i in 0 ..< tmpCartArr.count {
                let cartInfo = self.cartProductArr[i]
                cartInfo.isSelectProduct = true
                cartInfo.isShippingFree = isFree
            }
            /* 底部提示框暂时不要
            if let cartAllInfo = self.cartAllInfo {
                if tmpCartArr.count >= cartAllInfo.PayCount {
                    self.freeNotiseLabel.isHidden = true
                    self.downTrainImgView.isHidden = true
                    self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
                    self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 51)
                    self.bottomView.addConstraint(self.bottomViewHeightConstraint)
                } else {
                    self.freeNotiseLabel.isHidden = false
                    self.downTrainImgView.isHidden = false
                    self.freeNotiseLabel.text = String(format: "Free Shipping(%d different items,free gift not included)", cartAllInfo.PayCount)
                    self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
                    self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 91)
                    self.bottomView.addConstraint(self.bottomViewHeightConstraint)
                }
 
            }
            */
            self.tableView.reloadData()
            self.configSelectCartPaymentPrice(selectArr: tmpCartArr)
        } else {
            //全部取消
            print("全部取消")
            //self.giftHeaderView.changeFreeFiftSelectState(isSelected: false)
            self.giftHeaderView.changeShippingPriceFree(isFree: false)
            let giftState = self.giftHeaderView.getFreeGiftSelectedState()
            let rapidState = self.footerView.getRapidPaymentState()
            var shippingValue = 1.8
            if let tmpCartInfo = self.cartAllInfo {
                shippingValue = tmpCartInfo.ExpressShipping
            }
            var giftShippingValue = 0.0
            if let tmpFreeInfo = self.freeGiftInfo {
                if giftState {
                    giftShippingValue = Double(tmpFreeInfo.Shipping)
                }
            }
            var orderTotal = giftShippingValue
            if rapidState {
                orderTotal = giftShippingValue + shippingValue
            }
            let params = ["handlePrice": 0.0,
                          "actualPrice": 0.0,
                          "estimatedShipping": giftShippingValue,
                          "rapidPrice": shippingValue,
                          "orderTotalPrice": orderTotal] as [String : Double]
            let json = JSON(params)
            let cartPayInfo = CartPaymentInfo(params: json)
            let tmpCartArr = self.cartProductArr
            for i in 0 ..< tmpCartArr.count {
                let cartInfo = self.cartProductArr[i]
                cartInfo.isSelectProduct = false
                cartInfo.isShippingFree = false
            }
            self.allPaymentInfo = cartPayInfo
            self.footerView.setupPaymentPrice(cartPaymentInfo: cartPayInfo)
            self.totalPriceLabel.text = String(format: "%@%.2f", getCurrentPayCharacter(),orderTotal)
//            self.freeNotiseLabel.isHidden = false
//            self.downTrainImgView.isHidden = false
//            if let cartAllInfo = self.cartAllInfo {
//                self.freeNotiseLabel.text = String(format: "Free Shipping(%d different items,free gift not included)", cartAllInfo.PayCount)
//                self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
//                self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 91)
//                self.bottomView.addConstraint(self.bottomViewHeightConstraint)
//            }
            self.tableView.reloadData()
        }
    }
    /**
     * 购物车结算按钮
     */
    /**
     * @params
     * productTotal  -> 商品总金额
     * ShippingTotal -> 运费总金额
     * OrderPayTotal -> 支付金额
     * DiscountTotal -> 优惠金额
     * IsFreeShipping -> 0 正常运费，按照每个商品的运费计算（即种类小于指定数量） 1  快运费，根据供应商数量+每个商品数量的运费
     * strList        -> 支付信息集合
     */
    /** 支付信息集合
     *  OrderType  1 购买的是产品  2 购买的是Freegift
     *  FreeGift商品 ProductID传FreeGiftID ，其它都传 0
     *  只有FreeGift商品付款时，ProductID传FreeGiftID ，运费需要赋值，数量ProductSKUCount传 1，其它都传 0
     *
     */
    @IBAction func buyNowButtonTapped(_ sender: UIButton) {
        //结算  freegift + 商品
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        configCheckOutEventMonitor(monitor: kCheckOutEvent, type: 2)
        let isSelectedFreeGift: Bool = giftHeaderView.getFreeGiftSelectedState()
        let isSelectedRapidState: Bool = footerView.getRapidPaymentState()
        DispatchQueue.global().async {
            var selectArr = [CartProduct]()
            for i in 0 ..< self.cartProductArr.count {
                let product = self.cartProductArr[i]
                if product.isSelectProduct {
                    selectArr.append(product)
                }
            }
            if !isSelectedFreeGift && selectArr.count == 0 {
                let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
                let topModel = AlertButtonModel(title: kOkString, buttonType: .AlertButtonTypeNormal)
                alertView.showWithAlertView(title: kCartMessageString, detailString: kCartSelectOneItemString, buttonArr: [topModel],cancel: false)
                MBProgressHUD.hide(for: self.view, animated: true)
                return
            }
            var handleTotalPrice: Double = 0.0
            var actualPrice: Double = 0.0   //实际支付商品总金额
            var shippingPrice: Double = 0.0 //总运费金额 = 普通运费金额+快速运费
            var normalShippingPrice: Double = 0.0 //普通运费金额 = 商品金额+Freegift
            var freeGiftShippingPrice = 0.0 //FreeGift运费
            var comIDSet = Set<Int>() //供应商ID 不重复
            var paymentArr = [[String: Any]]()
            var productIDString = "" //产品ID集合 ，监测事件时使用
            var itemParamArr = [[String: Any]]() //产品item集合 id：产品ID quantity：产品数量 item_price: 产品SKU成交单价 + shipping价格
            let freeCount = self.cartAllInfo!.PayCount
            for item in 0 ..< selectArr.count {
                let productInfo = selectArr[item]
                var countPrice = 0.0
                var payPrice = 0.0
                var itemPrice = 0.0
                var productShipping = 0.0
                if productInfo.IsTime == 0 {
                    countPrice = Double(productInfo.Count) * productInfo.SellingPrice
                    payPrice = productInfo.SellingPrice
                } else {
                    countPrice = Double(productInfo.Count) * productInfo.SharePrice
                    payPrice = productInfo.SharePrice
                }
                if selectArr.count >= freeCount {
                    productShipping = 0.0
                } else {
                    productShipping = productInfo.Shipping
                }
                itemPrice = payPrice + productShipping
                productIDString += "\(productInfo.ProductID),"
                handleTotalPrice += productInfo.Msrp
                actualPrice += countPrice
                normalShippingPrice += Double(productInfo.Count) * productShipping
                comIDSet.insert(productInfo.ComID)
                let dic = ["UserCarID": productInfo.UserCarID,
                           "ProductID": productInfo.ProductID,
                           "ProductSKUID": productInfo.ProductSKUID,
                           "ProductSKUCount": productInfo.Count,
                           "Price": payPrice,
                           "MsrpPrice": productInfo.Msrp,
                           "Shipping": productShipping,
                           "Discount": productInfo.DiscountPrice,
                           "IsTime": productInfo.IsTime,
                           "OrderType": 1] as [String : Any]
                paymentArr.append(dic)
                let itemParam = ["id":productInfo.ProductID,
                                 "quantity":productInfo.Count,
                                 "item_price":itemPrice] as [String : Any]
                itemParamArr.append(itemParam)
            }
            //购物车商品payment
            if isSelectedFreeGift {
                //选择了Freegift
                if let freeGiftInfo = self.freeGiftInfo {
                    var tmpShippingPrice = 0.0
                    if selectArr.count == 0 {
                        tmpShippingPrice = Double(freeGiftInfo.Shipping)
                    } else {
                        tmpShippingPrice = 0
                    }
                    let dic = ["UserCarID": 0,
                               "ProductID": freeGiftInfo.FreeGiftID,
                               "ProductSKUID": 0,
                               "ProductSKUCount": 1,
                               "Price": 0,
                               "MsrpPrice": 0,
                               "Shipping": tmpShippingPrice,
                               "Discount": 0,
                               "IsTime": 0,
                               "OrderType": 2] as [String : Any]
                    paymentArr.append(dic)
                }
            }
            
            if selectArr.count == 0 {  //0  1-3 3
                if isSelectedFreeGift {
                    if let tmpGift = self.freeGiftInfo {
                        freeGiftShippingPrice = Double(tmpGift.Shipping)
                    }
                } else {
                    freeGiftShippingPrice = 0
                }
            } else if selectArr.count >= freeCount {
                freeGiftShippingPrice = 0
                normalShippingPrice = 0
            } else {
                freeGiftShippingPrice = 0
            }
            shippingPrice = normalShippingPrice + freeGiftShippingPrice //普通运费+freegift运费
            if isSelectedFreeGift {
                if selectArr.count == 0 {
                    comIDSet.insert(0)
                }
            } else {
                if comIDSet.contains(0) {
                    comIDSet.remove(0)
                }
            }
            var rapidPrice: Double = 0.0
            var totalPrice: Double = 0.0
            var isRapidShipping = 0
            if isSelectedRapidState {//是否选择了快运费
                rapidPrice = self.cartAllInfo!.ExpressShipping * Double(comIDSet.count)
                totalPrice = actualPrice + shippingPrice + rapidPrice
                isRapidShipping = 1
            } else {
                totalPrice = actualPrice + shippingPrice
                isRapidShipping = 0
                rapidPrice = 0.0
            }
            let strInfoDic = ["StrListInfoList": paymentArr]
            let strInfoString = convertDictionaryToString(dict: strInfoDic as [String : AnyObject])
           
            let shippingTotal = rapidPrice + shippingPrice //总运费金额 = 普通运费金额+快速运费+FreeGift运费
            let discountPrice = handleTotalPrice - actualPrice
            /**
             * productTotal 商品总金额 ShippingTotal 运费总金额 OrdinaryShippingTotal 普通运费金额
             * FastShippingTotal 快速运费总金额 OrderPayTotal 支付金额  DiscountTotal 优惠金额
             * IsFreeShipping 0 表示正常运费  1 表示快运费
             */
            let countryCode = getCurrentCountryCodeString()
            let countryPay = getCurrentPayCharacter()
            let param = ["UserID":userID,
                         "ProdutTotal": actualPrice,
                         "ShippingTotal": shippingTotal,
                         "OrdinaryShippingTotal":shippingPrice,
                         "FastShippingTotal":rapidPrice,
                         "OrderPayTotal": totalPrice,
                         "DiscountTotal": discountPrice,
                         "IsFreeShipping": isRapidShipping,
                         "ClientType": 1,
                         "StrList": strInfoString,
                         "CountrySName":countryCode,
                         "CountryCurrency":countryPay,
                         "ComIdCount":comIDSet.count]
            FPHttpManager().manager.post(CreatePaymentOrderInterface, parameters: param, progress: nil, success: { (task, result) in
                let json = JSON.init(result as Any)
                print("创建临时订单成功\(json)")
                let payJsonParams = JSON(param)
                let payOrderInfo = CreateTmpPayOrderInfo(jsonData: payJsonParams)
                let payParams = ["handlePrice": handleTotalPrice,
                                 "actualPrice": actualPrice,
                                 "estimatedShipping": shippingPrice,
                                 "rapidPrice": rapidPrice,
                                 "orderTotalPrice": totalPrice,
                                 "isRapidSelected": isSelectedRapidState] as [String : Any]
                let payJson = JSON(payParams)
                let cartPayInfo = CartPaymentInfo(params: payJson)
                self.tmpOrderSuccessInfo = TmpPayOrderSuccessInfo(jsonData: json["Data"])
                let currentLanguage = LanguageHelper.standardLanguager().currentLanguage
                if let tmpAddressArr = self.tmpOrderSuccessInfo?.AddressInfoList {
                    if let paymentItemArr = self.tmpOrderSuccessInfo?.PaymentItemList {
                        if tmpAddressArr.count == 0 && paymentItemArr.count == 0 && self.tmpOrderSuccessInfo?.PaymentType == 0 {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            //既没有卡 也没有地址，则为第一次购买，进入到选择地址和卡信息页面
                            let editAddressVC = AddressEditViewController()
                            editAddressVC.isFirstEditAddress = true
                            editAddressVC.payPramsInfo = payOrderInfo //创建订单的参数
                            editAddressVC.paymentInfo = cartPayInfo   //支付信息
                            editAddressVC.tmpOrderSuccessInfo = self.tmpOrderSuccessInfo
                            editAddressVC.itemProductIDString = productIDString
                            editAddressVC.itemProductCount = selectArr.count
                            editAddressVC.itemParamsArr = itemParamArr
                            self.navigationController?.pushViewController(editAddressVC, animated: true)
                        } else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            //直接checkOut页面
                            let checkOutVC = CheckOutViewController()
                            checkOutVC.addressInfo = tmpAddressArr.first
                            if currentLanguage == "hi" {
                                if self.tmpOrderSuccessInfo?.PaymentType == 0 { //印度没有支付方式就按照货到付款
                                    checkOutVC.paymentType = 3
                                } else {
                                    checkOutVC.paymentType = self.tmpOrderSuccessInfo?.PaymentType
                                }
                            } else {
                                checkOutVC.paymentType = self.tmpOrderSuccessInfo?.PaymentType
                            }
                            checkOutVC.paymentInfo = cartPayInfo
                            checkOutVC.cardInfo = paymentItemArr.first
                            checkOutVC.payOrderInfo = payOrderInfo
                            checkOutVC.tmpOrderSuccessInfo = self.tmpOrderSuccessInfo
                            checkOutVC.itemParamsArr = itemParamArr
                            checkOutVC.itemProductCount = selectArr.count
                            checkOutVC.itemProductIDString = productIDString
                            self.navigationController?.pushViewController(checkOutVC, animated: true)
                        }
                    }else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        //直接checkOut页面
                        let checkOutVC = CheckOutViewController()
                        checkOutVC.addressInfo = tmpAddressArr.first
                        if currentLanguage == "hi" {
                            if self.tmpOrderSuccessInfo?.PaymentType == 0 { //印度没有支付方式就按照货到付款
                                checkOutVC.paymentType = 3
                            } else {
                                checkOutVC.paymentType = self.tmpOrderSuccessInfo?.PaymentType
                            }
                        } else {
                            checkOutVC.paymentType = self.tmpOrderSuccessInfo?.PaymentType
                        }
                        checkOutVC.paymentInfo = cartPayInfo
                        checkOutVC.cardInfo = nil
                        checkOutVC.payOrderInfo = payOrderInfo
                        checkOutVC.tmpOrderSuccessInfo = self.tmpOrderSuccessInfo
                        checkOutVC.itemParamsArr = itemParamArr
                        checkOutVC.itemProductCount = selectArr.count
                        checkOutVC.itemProductIDString = productIDString
                        self.navigationController?.pushViewController(checkOutVC, animated: true)
                    }
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    let checkOutVC = CheckOutViewController()
                    checkOutVC.addressInfo = nil
                    if currentLanguage == "hi" {
                        if self.tmpOrderSuccessInfo?.PaymentType == 0 { //印度没有支付方式就按照货到付款
                            checkOutVC.paymentType = 3
                        } else {
                            checkOutVC.paymentType = self.tmpOrderSuccessInfo?.PaymentType
                        }
                    } else {
                        checkOutVC.paymentType = self.tmpOrderSuccessInfo?.PaymentType
                    }
                    checkOutVC.paymentInfo = cartPayInfo
                    checkOutVC.cardInfo = nil
                    checkOutVC.payOrderInfo = payOrderInfo
                    checkOutVC.tmpOrderSuccessInfo = self.tmpOrderSuccessInfo
                    checkOutVC.itemParamsArr = itemParamArr
                    checkOutVC.itemProductCount = selectArr.count
                    checkOutVC.itemProductIDString = productIDString
                    self.navigationController?.pushViewController(checkOutVC, animated: true)
                }
 
                MBProgressHUD.hide(for: self.view, animated: true)
                //点击购物车checkout按钮回掉成功添加这个事件
                self.configCheckOutOrderMonitor(monitor: kCartCheckOutDetect, totalPrice: totalPrice, productID: productIDString, numberItem: selectArr.count, eventContent: itemParamArr)
            }) { (task, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: 购物车数量变化，删除 Private Action Method
    func didChangeSelectedProduct(selectedIndex: IndexPath,sender: UIButton) {
        let tmpCartArr = self.cartProductArr
        var selectArr = [CartProduct]()
        for i in 0 ..< tmpCartArr.count {
            let tmpProdcutInfo = self.cartProductArr[i]
            if i == selectedIndex.section {
                tmpProdcutInfo.isSelectProduct = sender.isSelected
            }
            if tmpProdcutInfo.isSelectProduct {
                selectArr.append(tmpProdcutInfo)
            }
        }
        if selectArr.count != self.cartProductArr.count {
            self.allSelectButton.isSelected = false
        } else {
            self.allSelectButton.isSelected = true
        }
        if let cartAllInfo = self.cartAllInfo {
            // 所选商品种类大于等于提供的数量，所有选中的商品免运费 ，freegift也免费
            if selectArr.count >= 1 {
                giftHeaderView.changeShippingPriceFree(isFree: true)
            } else {
                giftHeaderView.changeShippingPriceFree(isFree: false)
            }

            if selectArr.count >= cartAllInfo.PayCount { //free
                let tmpCartArr = self.cartProductArr
                for i in 0 ..< tmpCartArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        tmpProdcutInfo.isShippingFree = true
                    } else {
                        tmpProdcutInfo.isShippingFree = false
                    }
                }
                let tmpSelectArr = selectArr
                for i in 0 ..< tmpSelectArr.count {
                    let tmpProdcutInfo = selectArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        tmpProdcutInfo.isShippingFree = true
                    } else {
                        tmpProdcutInfo.isShippingFree = false
                    }
                }
//                self.freeNotiseLabel.isHidden = true
//                self.downTrainImgView.isHidden = true
//                self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
//                self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 51)
//                self.bottomView.addConstraint(self.bottomViewHeightConstraint)
            } else {
                let tmpCartArr = self.cartProductArr
                for i in 0 ..< tmpCartArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    tmpProdcutInfo.isShippingFree = false
                }
                let tmpSelectArr = selectArr
                for i in 0 ..< tmpSelectArr.count {
                    let tmpProdcutInfo = selectArr[i]
                    tmpProdcutInfo.isShippingFree = false
                }
//                self.freeNotiseLabel.isHidden = false
//                self.downTrainImgView.isHidden = false
//                self.freeNotiseLabel.text = String(format: "Free Shipping(%d different items,free gift not included)", cartAllInfo.PayCount)
//                self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
//                self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 91)
//                self.bottomView.addConstraint(self.bottomViewHeightConstraint)
            }
        }
        self.configSelectCartPaymentPrice(selectArr: selectArr)
        self.tableView.reloadData()
    }
    
    // 0 减少商品 1 增加商品
    func didChangeSelectProductCountValue(selectedIndex: IndexPath,changeType: Int) {
        let selectedProduct = self.cartProductArr[selectedIndex.section]
        if changeType == 0 {
            if selectedProduct.Count == 0 {
                selectedProduct.Count = 0
            } else {
                selectedProduct.Count -= 1
            }
        } else {
            selectedProduct.Count += 1
        }
        let tmpCartArr = self.cartProductArr
        var selectArr = [CartProduct]()
        for i in 0 ..< tmpCartArr.count {
            let tmpProdcutInfo = tmpCartArr[i]
            if tmpProdcutInfo.isSelectProduct {
                selectArr.append(tmpProdcutInfo)
            }
        }
        configSelectCartPaymentPrice(selectArr: selectArr)
        changeCartProductCountToServer(userCartID: selectedProduct.UserCarID, type: changeType, deleteType: 0)
    }
    //当商品数量减到0 时提示用户是否是删除用户
    func didMinusProductCountDangerous(selectedIndex: Int) {
        let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        let topModel = AlertButtonModel(title: kYesString, buttonType: .AlertButtonTypeNormal)
        let downModel = AlertButtonModel(title: kNoString, buttonType: .AlertButtonTypeRed)
        alertView.showWithAlertView(title: kAreYouSureString, detailString: KCartDeleteItemString, buttonArr: [topModel,downModel],cancel: false)
        alertView.bottomButtonTapped = { (button: UIButton) -> Void in
            if button.tag == 100 {
                self.didDeleteSelectProduct(selectedIndex: selectedIndex)
            }
        }
    }
    //删除
    func didDeleteSelectProduct(selectedIndex: Int) {
        let selectedProduct = self.cartProductArr[selectedIndex]
        self.cartProductArr.remove(at: selectedIndex)
        let tmpCartArr = self.cartProductArr
        var selectArr = [CartProduct]()
        var normalCountDownArr = [CartProduct]()
        var activityCountDownArr = [CartProduct]()
        for i in 0 ..< tmpCartArr.count {
            let tmpProdcutInfo = tmpCartArr[i]
            if tmpProdcutInfo.isSelectProduct {
                selectArr.append(tmpProdcutInfo)
            }
            if tmpProdcutInfo.IsTime == 2 { //倒计时商品
                normalCountDownArr.append(tmpProdcutInfo)
            } else if tmpProdcutInfo.IsTime == 3 { //活动商品
                activityCountDownArr.append(tmpProdcutInfo)
            }
        }
        
        if let cartAllInfo = self.cartAllInfo {
            // 所选商品种类大于等于提供的数量，所有选中的商品免运费 ，freegift也免费
            if selectArr.count >= 1 {
                giftHeaderView.changeShippingPriceFree(isFree: true)
            } else {
                giftHeaderView.changeShippingPriceFree(isFree: false)
            }
            if selectArr.count >= cartAllInfo.PayCount {
                let tmpCartArr = self.cartProductArr
                for i in 0 ..< tmpCartArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        tmpProdcutInfo.isShippingFree = true
                    } else {
                        tmpProdcutInfo.isShippingFree = false
                    }
                }
                let tmpSelectArr = selectArr
                for i in 0 ..< tmpSelectArr.count {
                    let tmpProdcutInfo = selectArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        tmpProdcutInfo.isShippingFree = true
                    } else {
                        tmpProdcutInfo.isShippingFree = false
                    }
                }
//                self.freeNotiseLabel.isHidden = true
//                self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
//                self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 51)
//                self.bottomView.addConstraint(self.bottomViewHeightConstraint)
            } else {
                let tmpCartArr = self.cartProductArr
                for i in 0 ..< tmpCartArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    tmpProdcutInfo.isShippingFree = false
                }
                let tmpSelectArr = selectArr
                for i in 0 ..< tmpSelectArr.count {
                    let tmpProdcutInfo = selectArr[i]
                    tmpProdcutInfo.isShippingFree = false
                }
//                self.freeNotiseLabel.isHidden = false
//                self.freeNotiseLabel.text = String(format: "Free Shipping(%d different items,free gift not included)", cartAllInfo.PayCount)
//                self.bottomView.removeConstraint(self.bottomViewHeightConstraint)
//                self.bottomViewHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 91)
//                self.bottomView.addConstraint(self.bottomViewHeightConstraint)
            }
        }
        self.tableView.reloadData()
        configSelectCartPaymentPrice(selectArr: selectArr)
        
        var selectedCountDownIdentifier = ""
        if selectedProduct.IsTime == 1 { //随机倒计时
            selectedCountDownIdentifier = "\(randomCountDownIdentifier) + \(selectedIndex)"
        } else if selectedProduct.IsTime == 2 { //普通倒计时
            if normalCountDownArr.count == 0 {
                selectedCountDownIdentifier = countDownIdentifier
            }
        } else if selectedProduct.IsTime == 3 { //活动商品
            if activityCountDownArr.count == 0 {
                selectedCountDownIdentifier = activityCountDownIdentifier
            }
        }
        TimeCountDownManager.manager.cancelTask(selectedCountDownIdentifier)
        changeCartProductCountToServer(userCartID: selectedProduct.UserCarID, type: 0, deleteType: 1)
    }
    //增加或者删除服务器中的商品数量 type 0 减少 1增加 deleteType 0 单个删除 1 全部删除
    func changeCartProductCountToServer(userCartID: Int,type: Int,deleteType: Int) {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "UserCarID":userCartID,
                     "Type": type,
                     "DeleteType": deleteType,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(UpdateCartInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON.init(result as Any)
            let cartProductCount = json["Data"]["CarProductCount"].intValue
            self.setupNavigationTitleAndTabBarBadge(cartProductCount: cartProductCount)
            self.tableView.reloadData()
        }) { (task, error) in
        }
    }
}

//MARK: - tableView代理方法
extension CartViewController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.cartProductArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingCartCell", for: indexPath) as! ShoppingCartCell
        let timeProduct = self.cartProductArr[indexPath.section]
        cell.configCartProductCell(cartProduct: timeProduct)
        cell.changeSelectProductTapped = { (cell: ShoppingCartCell,sender: UIButton) -> Void in
            self.didChangeSelectedProduct(selectedIndex: indexPath, sender: sender)
        }
        cell.plusOrMinusProductTapped = { (cell: ShoppingCartCell,sender: UIButton) -> Void in
            let changeType = sender.tag == 100 ? 0 : 1
            self.didChangeSelectProductCountValue(selectedIndex: indexPath, changeType: changeType)
        }
        cell.deleteProductButtonTapped = { (cell: ShoppingCartCell) -> Void in
            
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            let topModel = AlertButtonModel(title: kYesString, buttonType: .AlertButtonTypeNormal)
            let downModel = AlertButtonModel(title: kNoString, buttonType: .AlertButtonTypeRed)
            alertView.showWithAlertView(title: kAreYouSureString, detailString: KCartDeleteItemString, buttonArr: [topModel,downModel],cancel: false)
            alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                if button.tag == 100 {
                    self.didDeleteSelectProduct(selectedIndex: indexPath.section)
                }
            }
        }
        cell.productCountDangerousTapped = { (cell: ShoppingCartCell,sender: UIButton) -> Void in
            self.didMinusProductCountDangerous(selectedIndex: indexPath.section)
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let productInfo = self.cartProductArr[indexPath.section]
        let eventString = String(format: "%@%@", kCartProductSelectedEvent,productInfo.ProductID)
        configCommonEventMonitor(monitorString: eventString, type: 2)
        //跳转商品详情
        let detailVC = NewProductDetailViewController()
        detailVC.requestProID = productInfo.ProductID
        navigationController?.pushViewController(detailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let productInfo = self.cartProductArr[section]
        
        let randomView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 38))
        randomView.backgroundColor = kRGBColorFromHex(rgbValue:0xe9668f)
        randomView.tag = productInfo.UserCarID
    
        let labelW: CGFloat = 32
        let labelH: CGFloat = 26
        let labelY: CGFloat = 38 / 2.0 - labelH / 2.0
        let hourTimeLabel = UILabel(frame: CGRect(x: 58, y: labelY, width: labelW, height: labelH))
        hourTimeLabel.backgroundColor = UIColor.white
        hourTimeLabel.font = UIFont(name: kHelveticaRegular, size: 13)
        hourTimeLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        hourTimeLabel.tag = 100
        hourTimeLabel.textAlignment = .center
        randomView.addSubview(hourTimeLabel)
        
        let separatorLabel = UILabel(frame: CGRect(x: hourTimeLabel.frame.maxX, y: labelY, width: 8, height: labelH))
        separatorLabel.textColor = UIColor.white
        separatorLabel.backgroundColor = kRGBColorFromHex(rgbValue:0xe9668f)
        separatorLabel.text = ":"
        separatorLabel.textAlignment = .center
        randomView.addSubview(separatorLabel)
        
        let minuteLabel = UILabel(frame: CGRect(x: separatorLabel.frame.maxX, y: labelY, width: labelW, height: labelH))
        minuteLabel.backgroundColor = UIColor.white
        minuteLabel.font = UIFont(name: kHelveticaRegular, size: 13)
        minuteLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        minuteLabel.tag = 101
        minuteLabel.textAlignment = .center
        randomView.addSubview(minuteLabel)
        
        let notiseLabel = UILabel(frame: CGRect(x: minuteLabel.frame.maxX + 10, y: 0, width: 200, height: 38))
        notiseLabel.text = kCartCheckOutNowString
        notiseLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 27))
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0xffffff)
        let fitSize = notiseLabel.sizeThatFits(CGSize.zero)
        notiseLabel.frame = CGRect(x: minuteLabel.frame.maxX + 10, y: 0, width: fitSize.width, height: 38)
        randomView.addSubview(notiseLabel)
        
        let moneyX = notiseLabel.frame.maxX
        let moneyW = KScreenWidth - notiseLabel.frame.maxX
        let moneyLabel = UILabel(frame: CGRect(x: moneyX, y: 0, width: moneyW, height: 38))
        moneyLabel.textColor = kRGBColorFromHex(rgbValue: 0xffffff)
        moneyLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 45))
        moneyLabel.tag = 102
        moneyLabel.text = String(format: "%@%.2f", getCurrentPayCharacter(),productInfo.SharePrice)
        moneyLabel.adjustsFontSizeToFitWidth = true
        randomView.addSubview(moneyLabel)
       
        let topTriImgView = UIImageView(frame: CGRect(x: moneyX + 8, y: 38 - 7, width: 13, height: 7))
        topTriImgView.image = UIImage(named: "cart_triangle_img")
        randomView.addSubview(topTriImgView)
        if productInfo.IsTime == 1 { //随机倒计时商品
            //有倒计时商品
            let tmpRandomCountDownIdentifier = "\(randomCountDownIdentifier) + \(productInfo.UserCarID)"
            let randominterval = TimeInterval(productInfo.Number)
            TimeCountDownManager.manager.scheduledCountDownWith(tmpRandomCountDownIdentifier, timeInteval: randominterval, countingDown: { (timeInterval) in
                let time = Int(timeInterval)
                if time / 60 < 10 {
                    hourTimeLabel.text = String(format: "0%ld", time / 60)
                } else {
                    hourTimeLabel.text = String(format: "%ld", time / 60)
                }
                if time % 60 < 10 {
                    minuteLabel.text = String(format: "0%ld", time % 60)
                } else {
                    minuteLabel.text = String(format: "%ld", time % 60)
                }
            }) { (timeInterval) in
                //倒计时时间到的话，需要更新倒计时商品的价格
                for i in 0 ..< self.cartProductArr.count {
                    let tmpProductInfo = self.cartProductArr[i]
                    if tmpProductInfo.ProductID == productInfo.ProductID {
                        tmpProductInfo.IsTime = 0
                        self.tableView.reloadData()
                    }
                }
                var selectArr = [CartProduct]()
                for i in 0 ..< self.cartProductArr.count {
                    let tmpProdcutInfo = self.cartProductArr[i]
                    if tmpProdcutInfo.isSelectProduct {
                        selectArr.append(tmpProdcutInfo)
                    }
                }
                self.configSelectCartPaymentPrice(selectArr: selectArr)
            }
        }
        return randomView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let productInfo = self.cartProductArr[section]
        if productInfo.IsTime == 1 {
            return 38
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let tableView = scrollView as! UITableView
        let sectionHeaderHeight: CGFloat = 38
        let sectionFooterHeight: CGFloat = 10
        let offsetY = tableView.contentOffset.y
        if offsetY <= sectionHeaderHeight && offsetY >= 0 {
            tableView.contentInset = UIEdgeInsetsMake(-offsetY, 0, -sectionFooterHeight, 0)
        } else if (offsetY >= sectionHeaderHeight && offsetY <= tableView.contentSize.height - tableView.frame.size.height - sectionFooterHeight) {
            tableView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, -sectionFooterHeight, 0)
        } else if (offsetY >= tableView.contentSize.height - tableView.frame.size.height - sectionFooterHeight && offsetY <= tableView.contentSize.height - tableView.frame.size.height) {
            tableView.contentInset = UIEdgeInsetsMake(-offsetY, 0, -(tableView.contentSize.height - tableView.frame.size.height - sectionFooterHeight), 0)
        }
    }
}

extension CartViewController {
    
    func configCheckOutEventMonitor(monitor: String,type: Int) {
        let userID = UserManager.shareUserManager.getUserID()
        AppsFlyerTracker.shared().trackEvent(monitor, withValues: nil)
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: String(type), name: monitor, productID: "0")
    }
    
    //checkout 支付监控 FayPark Appsflyer
    func configCheckOutOrderMonitor(monitor: String,totalPrice: Double,productID: String,numberItem: Int,eventContent:[[String: Any]]) {
        let totalPriceString = decimalNumberWithDouble(convertValue: totalPrice)
        let totalDoble = Double(totalPriceString)!
        let eventContentJsonString = convertArrayToString(arr: eventContent as [AnyObject])
        let userID = UserManager.shareUserManager.getUserID()
        let currency = getCurrentPaymentCodeString()
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: kCartCheckOutDetect, productID: "0")
        AppsFlyerTracker.shared().trackEvent(AFEventInitiatedCheckout, withValues: [AFEventParamPrice:totalPriceString, AFEventParamCurrency: currency, AFEventParamContentType: "product_group", AFEventParamContentId:productID, AFEventContent: eventContentJsonString,AFEventParamPaymentInfoAvailable: 0,"NUM_ITEMS":numberItem])
     
        let params = [FBSDKAppEventParameterNameContentID : productID, FBSDKAppEventParameterNameContentType : "product_group", FBSDKAppEventParameterNameNumItems : numberItem, FBSDKAppEventParameterNamePaymentInfoAvailable : 0,
            FBSDKAppEventParameterNameCurrency : currency,
            FBSDKAppEventNameViewedContent:eventContentJsonString] as [String : Any]
        FBSDKAppEvents.logEvent(FBSDKAppEventNameInitiatedCheckout, valueToSum: totalDoble, parameters: params)
    }
}
