//
//  FreeGiftViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/21.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD

class FreeGiftViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var infoLable: UILabel!
    var freeGiftList = [FreeGiftInfo]()
    var freeGiftTypeList = [FreeGiftTypeItem]()
    var selectedFreeGiftInfo: FreeGiftInfo?
    let collectionIdentifier = "FreeGiftCollectionViewCell"
    let headerIdentifier = "FreeGiftHeaderIdentifier"
    let footerIdentifier = "FreeGiftFooterIdentifier"
    let defultIdentifier = "DefultFreeGiftCell"
    var selectIndexPath = IndexPath(row: 0, section: 0) //设置一个默认值
    var isHomePush = false //是否是从首页FreeGift页面进来的
    typealias SelectFreeGiftInfoBlock = (_ freeGiftInfo: FreeGiftInfo) -> Void
    var selectFreeGiftInfoBlock: SelectFreeGiftInfoBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kFreeGiftTitleString
        setupRightNavigationItem()
        setupCollectionView()
        requestFreeGiftListFromServer()
        configCommonEventMonitor(monitorString: kFreeGiftPage, type: 1)
        self.infoLable.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        self.infoLable.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 23))
    }

    func setupCollectionView() {
       
        let itemSpacing: CGFloat = 5
        let imageW = (kScreenWidth - 40) / 3
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = itemSpacing
        layout.minimumLineSpacing = itemSpacing
        layout.itemSize = CGSize(width: imageW, height: 280)
        layout.headerReferenceSize = CGSize(width: kScreenWidth, height: 50)
    
    
        collectionView.collectionViewLayout = layout
        collectionView.register(UINib(nibName: collectionIdentifier, bundle: nil), forCellWithReuseIdentifier: collectionIdentifier)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: defultIdentifier)
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerIdentifier)
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerIdentifier)
        collectionView.backgroundColor = UIColor.white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator =  false
        collectionView.showsHorizontalScrollIndicator = false
       
    }
    
    func setupRightNavigationItem() {
        let rightItemButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        rightItemButton.setTitle("Done", for: .normal)
        rightItemButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xe9668f), for: .normal)
        rightItemButton.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 32))
        rightItemButton.addTarget(self, action: #selector(rightDoneButtonItemTapped), for: .touchUpInside)
        
        let rightNaviItem = UIBarButtonItem(customView: rightItemButton)
        navigationItem.rightBarButtonItem = rightNaviItem
    }
    
    override func leftBackButtonTapped() {
        configCommonEventMonitor(monitorString: kFreeGiftBack, type: 2)
        if isHomePush {
            //进入购入车页面
            let mainVC = MainViewController()
            let window = UIApplication.shared.keyWindow
            window!.rootViewController = mainVC
            mainVC.selectedIndex = 0
        }
        navigationController?.popViewController(animated: true)
    }
    
    func requestFreeGiftListFromServer() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let prams = ["CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(FreeGiftListInterface, parameters: prams, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("requestFreeGiftListFromServer\(json)")
            let proArray = json["Data"]["FreeGiftTypeList"]
            for i in 0 ..< proArray.count {
                var tempProduct = FreeGiftTypeItem(jsonData: proArray[i])
                var freeList = tempProduct.FreeGiftList
                for j in 0 ..< tempProduct.FreeGiftList.count {
                    var freeInfo = tempProduct.FreeGiftList[j]
                    if let tmpFreeInfo = self.selectedFreeGiftInfo {
                        let feeID = tmpFreeInfo.FreeGiftID
                        if feeID == freeInfo.FreeGiftID {
                            freeInfo.IsSelected = true
                            freeList.remove(at: j)
                            freeList.insert(freeInfo, at: j)
                        }
                    }
                }
                tempProduct.FreeGiftList = freeList
                self.freeGiftTypeList.append(tempProduct)
            }
            self.collectionView.reloadData()
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    @objc func rightDoneButtonItemTapped() {
        guard self.freeGiftTypeList.count > 0 else {
            return
        }
        configCommonEventMonitor(monitorString: kFreeGiftDone, type: 2)
        let freeItem: FreeGiftTypeItem = self.freeGiftTypeList[selectIndexPath.section]
        let freeInfo = freeItem.FreeGiftList[selectIndexPath.row]
        let freeDic = ["FreeGiftTitle": freeInfo.FreeGiftTitle,
                       "Shipping": freeInfo.Shipping,
                       "Color": freeInfo.Color,
                       "FreeGiftID": freeInfo.FreeGiftID,
                       "MainImgLink": freeInfo.MainImgLink,
                       "Size": freeInfo.Size,
                       "Count": freeInfo.Count,
                       "FreeGiftName": freeInfo.FreeGiftName,
                       "IsSelected": freeInfo.IsSelected] as [String : Any]
        UserDefaults.standard.set(freeDic, forKey: "SelectFreeInfo")
        UserDefaults.standard.synchronize()
        if isHomePush {
        
            let mainVC = MainViewController()
            let window = UIApplication.shared.keyWindow
            window!.rootViewController = mainVC
            mainVC.selectedIndex = 2
            
        } else {
            if selectFreeGiftInfoBlock != nil {
                selectFreeGiftInfoBlock!(freeInfo)
            }
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension FreeGiftViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.freeGiftTypeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let freeItem: FreeGiftTypeItem = self.freeGiftTypeList[section]
        return freeItem.FreeGiftList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionIdentifier, for: indexPath) as! FreeGiftCollectionViewCell
        var freeItem: FreeGiftTypeItem = self.freeGiftTypeList[indexPath.section]
        let freeInfo = freeItem.FreeGiftList[indexPath.row]
        cell.configFreeCollectionCell(freeGiftItem: freeInfo)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let freeGiftListArr = self.freeGiftTypeList
        for i in 0 ..< freeGiftListArr.count {
            var freeItem = self.freeGiftTypeList[i]
            let tmpFreeList = freeItem.FreeGiftList
            for j in 0 ..< tmpFreeList.count {
                var freeInfo = freeItem.FreeGiftList[j]
                if i == indexPath.section {
                    if j == indexPath.row {
                        freeInfo.IsSelected = true
                    } else {
                        freeInfo.IsSelected = false
                    }
                } else {
                    freeInfo.IsSelected = false
                }
                freeItem.FreeGiftList.remove(at: j)
                freeItem.FreeGiftList.insert(freeInfo, at: j)
            }
            self.freeGiftTypeList.remove(at: i)
            self.freeGiftTypeList.insert(freeItem, at: i)
        }
        selectIndexPath = indexPath
        self.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableView: UICollectionReusableView!
        if kind == UICollectionElementKindSectionHeader {
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerIdentifier, for: indexPath)
            for view in reusableView.subviews {
                view.removeFromSuperview()
            }
            if indexPath.section != self.freeGiftTypeList.count {
                let freeItem: FreeGiftTypeItem = self.freeGiftTypeList[indexPath.section]
                let titleLabel = UILabel(frame: CGRect(x: 10, y: 0, width: kScreenWidth, height: 50))
                titleLabel.text = freeItem.TypeName
                titleLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
                titleLabel.font = UIFont(name: kGothamRoundedBold, size: 15)
                reusableView.addSubview(titleLabel)
            }
            
        } else if kind == UICollectionElementKindSectionFooter {
            
            reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerIdentifier, for: indexPath)
            
            for view in reusableView.subviews {
                view.removeFromSuperview()
            }
            let titleLabel = UILabel(frame: CGRect(x: 10, y: 0, width: kScreenWidth - 20, height: 80))
            titleLabel.text = kFGFooterString
            titleLabel.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
            titleLabel.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 28))
            titleLabel.numberOfLines = 0
            titleLabel.lineBreakMode = .byWordWrapping
            reusableView.addSubview(titleLabel)
            
        }
        return reusableView
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let size = CGSize(width: KScreenWidth - 20, height: 80)
        if section == self.freeGiftTypeList.count - 1 {
            return size
        }
        return CGSize(width: 0, height: 0)
    }

}
