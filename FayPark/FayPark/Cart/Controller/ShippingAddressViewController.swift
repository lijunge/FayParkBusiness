//
//  ShippingAddressViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/5.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import Braintree
import MBProgressHUD
import Stripe
import FBSDKCoreKit
import AppsFlyerLib
import AFNetworking
class ShippingAddressViewController: BaseViewController, UIScrollViewDelegate {

    var purchaseAmount : Double = 0.0
    //支付相关
    var payPalConfig = PayPalConfiguration()
    //支付框架
    var braintreeClient: BTAPIClient?
    var payPalDriver: BTPayPalDriver?
    //用户地址
    var addressInfo: MyAddressInfo?
    //支付的订单信息
    var payOrderInfo: CreateTmpPayOrderInfo?
    //支付的底部支付信息
    var paymentInfo: CartPaymentInfo?
    //创建订单成功信息
    var tmpOrderSuccessInfo: TmpPayOrderSuccessInfo?
    
    var itemParamsArr = [[String: Any]]() //监测使用到的item
    var itemProductIDString = "" //监测使用到的productID组合
    var itemProductCount = 0     //监测使用到的支付个数
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        title = kPaymentTitleString
        configCommonEventMonitor(monitorString: kPaymentInfoPage, type: 1)
        setupHeaderView()
        setupSubViews()
    }
    lazy var backScrollView: UIScrollView = {
        let temScrollView = UIScrollView.init()
        temScrollView.isScrollEnabled = true
        temScrollView.showsVerticalScrollIndicator = false
        temScrollView.contentSize = CGSize(width: KScreenWidth, height: kScreenHeight+20)
        return temScrollView
    }()
    lazy var stripePayView: UIView = {
        let tmpStripeView = UIView.init()
        tmpStripeView.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight - 100)
        tmpStripeView.backgroundColor = UIColor.white
        
        let topNotiseLabel = UILabel(frame: CGRect(x: 17, y: 0, width: KScreenWidth - 34, height: 50))
        topNotiseLabel.text = localized("kCIHeaderEnterNotise")
        topNotiseLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        topNotiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        topNotiseLabel.numberOfLines = 0
        tmpStripeView.addSubview(topNotiseLabel)
        
        self.cardInputField.frame = CGRect(x: 17, y: topNotiseLabel.frame.maxY + 10, width: kScreenWidth - 20, height: 30)
        self.cardInputField.delegate = self
        tmpStripeView.addSubview(self.cardInputField)
        
        let cardLineView = UIView(frame: CGRect(x: 17, y: cardInputField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        cardLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpStripeView.addSubview(cardLineView)
        
        let cvWidth = (kScreenWidth - 34) / 2
        self.cvcInputField.frame = CGRect(x: 17, y: cardLineView.frame.maxY + 10, width: cvWidth, height: 30)
        self.cvcInputField.delegate = self
        tmpStripeView.addSubview(self.cvcInputField)
        
        let mmW = cvWidth / 2 - 10
        self.mmInputField.frame = CGRect(x: self.cvcInputField.frame.maxX, y: cardLineView.frame.maxY + 10, width: mmW, height: 30)
        self.mmInputField.delegate = self
        tmpStripeView.addSubview(self.mmInputField)
        
        let tmpLabel = UILabel(frame: CGRect(x: self.mmInputField.frame.maxX, y: cardLineView.frame.maxY + 10, width: 10, height: 30))
        tmpLabel.text = "/"
        tmpLabel.textAlignment = .center
        tmpLabel.textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        tmpStripeView.addSubview(tmpLabel)
        
        self.yyInputField.frame = CGRect(x: tmpLabel.frame.maxX, y: cardLineView.frame.maxY + 10, width: mmW, height: 30)
        self.yyInputField.delegate = self
        tmpStripeView.addSubview(self.yyInputField)
        
        let monthLineView = UIView(frame: CGRect(x: 17, y: self.cvcInputField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        monthLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpStripeView.addSubview(monthLineView)
        
        self.zipInputField.frame = CGRect(x: 17, y: monthLineView.frame.maxY + 10, width: kScreenWidth - 20, height: 30)
        self.zipInputField.delegate = self
        tmpStripeView.addSubview(self.zipInputField)
        
        let zipLineView = UIView(frame: CGRect(x: 17, y: self.zipInputField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        zipLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpStripeView.addSubview(zipLineView)
        
        let zipNotiseLabel = UILabel(frame: CGRect(x: 17, y: zipLineView.frame.maxY + 4, width: KScreenWidth, height: 40))
        zipNotiseLabel.text = localized("kZipNotiseLabel")
        zipNotiseLabel.numberOfLines = 0
        zipNotiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        zipNotiseLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        tmpStripeView.addSubview(zipNotiseLabel)
        
        let payLabel = UILabel(frame: CGRect(x: 17, y: zipNotiseLabel.frame.maxY, width: kScreenWidth - 34, height: 25))
        payLabel.text = kCISecurePaymentString
        payLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        payLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        tmpStripeView.addSubview(payLabel)
        
        let trustLabel = UILabel(frame: CGRect(x: 17, y: payLabel.frame.maxY + 3, width: kScreenWidth - 34, height: 70))
        trustLabel.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        trustLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        trustLabel.numberOfLines = 0
        trustLabel.text = kCITrustNotiseString
        tmpStripeView.addSubview(trustLabel)
        
        let leftLabel = UILabel(frame: CGRect(x: 17, y: trustLabel.frame.maxY + 3, width: 120, height: 20))
        leftLabel.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        leftLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        leftLabel.numberOfLines = 0
        leftLabel.text = localized("kAcceptCard")
        tmpStripeView.addSubview(leftLabel)
        
        let rightLabel1 = UILabel(frame: CGRect(x: leftLabel.frame.maxX + 5, y: trustLabel.frame.maxY + 3, width: 100, height: 20))
        rightLabel1.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        rightLabel1.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        rightLabel1.numberOfLines = 0
        rightLabel1.text = localized("kRightVisa")
        tmpStripeView.addSubview(rightLabel1)
        
        let rightLabel2 = UILabel(frame: CGRect(x: leftLabel.frame.maxX + 5, y: rightLabel1.frame.maxY, width: 100, height: 20))
        rightLabel2.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        rightLabel2.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        rightLabel2.numberOfLines = 0
        rightLabel2.text = localized("kRightMasterCard")
        tmpStripeView.addSubview(rightLabel2)
        
        let rightLabel3 = UILabel(frame: CGRect(x: leftLabel.frame.maxX + 5, y: rightLabel2.frame.maxY , width: 130, height: 20))
        rightLabel3.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        rightLabel3.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        rightLabel3.numberOfLines = 0
        rightLabel3.text = localized("kRightExpress")
        tmpStripeView.addSubview(rightLabel3)

        var titleString = localized("kPMCheckOutButtonString")
        if let tmpInfo = self.paymentInfo {
            let payCharacterString = getCurrentPayCharacter()
            titleString = String(format: "%@ %@%.2f", titleString,payCharacterString,tmpInfo.orderTotalPrice)
        }
        let doneButton = UIButton(frame: CGRect(x: 14, y: rightLabel3.frame.maxY + 30, width: kScreenWidth - 28, height: 49))
        doneButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        doneButton.setTitle(titleString, for: .normal)
        doneButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        doneButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        doneButton.addTarget(self, action: #selector(stripeCheckOutButtonTapped), for: .touchUpInside)
        tmpStripeView.addSubview(doneButton)
    
        return tmpStripeView
    }()
    lazy var paypalView: UIView = {
        let tempView = UIView.init()
        tempView.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight - 100)
        tempView.backgroundColor = UIColor.white
        
        let tipLabel = UILabel(frame: CGRect(x: 17, y: 0, width: KScreenWidth - 34, height: 30))
        tipLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tipLabel.text = localized("kPayPalLabel")
        tipLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        tempView.addSubview(tipLabel)
        
        let secureLabel = UILabel(frame: CGRect(x: 17, y: tipLabel.frame.maxY, width: KScreenWidth - 34, height: 50))
        secureLabel.text = localized("kPalPalNotiseString")
        secureLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        secureLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        secureLabel.numberOfLines = 0
        tempView.addSubview(secureLabel)
        
        var titleString = localized("kPMCheckOutButtonString")
        if let tmpInfo = self.paymentInfo {
            let payCharacterString = getCurrentPayCharacter()
            titleString = String(format: "%@ %@%.2f", titleString,payCharacterString,tmpInfo.orderTotalPrice)
        }
        let doneButton = UIButton(frame: CGRect(x: 14, y: secureLabel.frame.maxY + 30, width: kScreenWidth - 28, height: 49))
        doneButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        doneButton.setTitle(titleString, for: .normal)
        doneButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        doneButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        doneButton.addTarget(self, action: #selector(paypalCheckOutButtonTapped), for: .touchUpInside)
        tempView.addSubview(doneButton)
        
        return tempView
    }()
    lazy var cashDeliveryView: UIView = {
        let tempCashView = UIView.init()
        tempCashView.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight - 100)
        tempCashView.backgroundColor = UIColor.white
        
        let tipLabel = UILabel.init()
        tipLabel.frame = CGRect(x: 17, y: 0, width: KScreenWidth - 34, height: 25)
        tipLabel.text = localized("kCashDeliveryString")
        tipLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tipLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        tempCashView.addSubview(tipLabel)
        
        let cashLabel = UILabel.init()
        cashLabel.frame = CGRect(x: 17, y: tipLabel.frame.maxY, width: KScreenWidth - 34, height: 70)
        cashLabel.text = localized("kCDNotiseString")
        cashLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        cashLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        cashLabel.numberOfLines = 0
        tempCashView.addSubview(cashLabel)
        
        let logiLabel = UILabel.init()
        logiLabel.frame = CGRect(x: 17, y: cashLabel.frame.maxY, width: 280, height: 30)
        logiLabel.text = localized("kCDLogiString")
        logiLabel.font =  UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        logiLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        logiLabel.adjustsFontSizeToFitWidth = true
        tempCashView.addSubview(logiLabel)
        
        self.montyLabel.frame = CGRect(x: KScreenWidth - 80 - 17, y: cashLabel.frame.maxY, width: 80, height: 30)
        let payCharacterString = getCurrentPayCharacter()
        let leftString = "+ \(payCharacterString)\(kCashServiceString)"
        var attriString = changeTextChange(regex: kCashServiceString, text: leftString, color: kRGBColorFromHex(rgbValue: 0xe9668f), font: UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 28))!)
        attriString.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: leftString.count))
        self.montyLabel.attributedText = attriString
        tempCashView.addSubview(self.montyLabel)
        
        let estimatedLabel = UILabel(frame: CGRect(x: 17, y: self.montyLabel.frame.maxY, width: 250, height: 30))
        estimatedLabel.text = localized("kPMEstimatedString")
        estimatedLabel.font =  UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        estimatedLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        estimatedLabel.textAlignment = .left
        tempCashView.addSubview(estimatedLabel)
        
        let rightEstimateLabel = UILabel(frame: CGRect(x: KScreenWidth - 17 - 150, y: self.montyLabel.frame.maxY, width: 150, height: 30))
        rightEstimateLabel.font =  UIFont(name: kMSReference, size: kSizeFrom750(x: 22))
        rightEstimateLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        rightEstimateLabel.textAlignment = .right
        rightEstimateLabel.attributedText = changeTextChange(regex: "FREE COD", text: "2018.8.13-8.31 FREE COD", color: kRGBColorFromHex(rgbValue: 0xe9668f), font: UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 20))!)
        tempCashView.addSubview(rightEstimateLabel)
        
        let spaceView = UIView(frame: CGRect(x: 0, y: estimatedLabel.frame.maxY, width: KScreenWidth, height: 10))
        spaceView.backgroundColor = kSpacingColor
        tempCashView.addSubview(spaceView)
        
        let totalLabel = UILabel(frame: CGRect(x: 0, y: spaceView.frame.maxY, width: KScreenWidth - 17, height: 40))
        totalLabel.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        if let tmpInfo = self.paymentInfo {
            //let totalMoney = Double(kCashServiceString)! + Double(tmpInfo.orderTotalPrice)
            let totalMoney = Double(tmpInfo.orderTotalPrice)
            let totalString  = "Only Total: \(payCharacterString) \(totalMoney)"
            totalLabel.attributedText = changeTextChange(regex: "Only Total: \(payCharacterString)", text: totalString, color: kRGBColorFromHex(rgbValue: 0x333333), font: UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 30))!)
        }
        totalLabel.textAlignment = .right
        tempCashView.addSubview(totalLabel)
        
        let spaceView2 = UIView(frame: CGRect(x: 0, y: totalLabel.frame.maxY, width: KScreenWidth, height: 10))
        spaceView2.backgroundColor = kSpacingColor
        tempCashView.addSubview(spaceView2)
        
        let deliverLabel = UILabel(frame: CGRect(x: 17, y: spaceView2.frame.maxY, width: KScreenWidth, height: 30))
        deliverLabel.text = localized("kPMDeliverString")
        deliverLabel.font =  UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        deliverLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempCashView.addSubview(deliverLabel)
        
        let nameLabel = UILabel(frame: CGRect(x: 20, y: deliverLabel.frame.maxY, width: KScreenWidth - 20, height: 30))
        if let addressInfo = self.addressInfo {
            nameLabel.text = "\(addressInfo.Name)  \(addressInfo.Phone)"
        }
        nameLabel.font =  UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        nameLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempCashView.addSubview(nameLabel)
        
        let addressLabel = UILabel(frame: CGRect(x: 20, y: nameLabel.frame.maxY, width: KScreenWidth - 40, height: 50))
        if let addressInfo = self.addressInfo {
            addressLabel.text = "\(addressInfo.Apt), \(addressInfo.Address), \(addressInfo.City), \(addressInfo.States),\(addressInfo.Contry), \(addressInfo.ZipCode) "
        }
        addressLabel.numberOfLines = 0
        addressLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        addressLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        tempCashView.addSubview(addressLabel)
        
        var titleString = localized("kPMPlaceOrderButtonString")
//        if let tmpInfo = self.paymentInfo {
//            let totalMoney = Double(kCashServiceString)! + Double(tmpInfo.orderTotalPrice)
//            titleString = String(format: "%@ %@%.2f", titleString,payCharacterString,totalMoney)
//        }
        let doneButton = UIButton(frame: CGRect(x: 14, y: addressLabel.frame.maxY + 30, width: kScreenWidth - 28, height: 49))
        doneButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        doneButton.setTitle(titleString, for: .normal)
        doneButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        doneButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        doneButton.addTarget(self, action: #selector(cashCheckOutButtonTapped), for: .touchUpInside)
        tempCashView.addSubview(doneButton)
        
        return tempCashView
    }()
    lazy var mercadoCheckView: UIView = {
        let tmpCheckView = UIView.init()
        tmpCheckView.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: kScreenHeight - 100)
        tmpCheckView.backgroundColor = UIColor.white
        
        let topNotiseLabel = UILabel(frame: CGRect(x: 17, y: 0, width: KScreenWidth - 34, height: 50))
        topNotiseLabel.text = "Please enter correct information below"
        topNotiseLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        topNotiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        topNotiseLabel.numberOfLines = 0
        tmpCheckView.addSubview(topNotiseLabel)
        
        self.emailField.frame = CGRect(x: 17, y: topNotiseLabel.frame.maxY + 10, width: kScreenWidth - 20, height: 30)
        self.emailField.delegate = self
        tmpCheckView.addSubview(self.emailField)
        let emailLineView = UIView(frame: CGRect(x: 17, y: emailField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        emailLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpCheckView.addSubview(emailLineView)
        
        self.fullNameField.frame = CGRect(x: 17, y: emailLineView.frame.maxY + 10, width: kScreenWidth - 20, height: 30)
        self.fullNameField.delegate = self
        tmpCheckView.addSubview(self.fullNameField)
        let fullLineView = UIView(frame: CGRect(x: 17, y: fullNameField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        fullLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpCheckView.addSubview(fullLineView)
        
        self.cardInputField.frame = CGRect(x: 17, y: fullLineView.frame.maxY + 10, width: kScreenWidth - 20, height: 30)
        self.cardInputField.delegate = self
        tmpCheckView.addSubview(self.cardInputField)
        
        let cardLineView = UIView(frame: CGRect(x: 17, y: cardInputField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        cardLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpCheckView.addSubview(cardLineView)
        
        let cvWidth = (kScreenWidth - 44) / 2
        let mmW = cvWidth / 2 - 10
        self.mmInputField.frame = CGRect(x: 17, y: cardLineView.frame.maxY + 10, width: mmW, height: 30)
        self.mmInputField.delegate = self
        self.mmInputField.textAlignment = .left
        tmpCheckView.addSubview(self.mmInputField)
        
        let tmpLabel = UILabel(frame: CGRect(x: self.mmInputField.frame.maxX, y: cardLineView.frame.maxY + 10, width: 10, height: 30))
        tmpLabel.text = "/"
        tmpLabel.textAlignment = .center
        tmpLabel.textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        tmpCheckView.addSubview(tmpLabel)
        
        self.yyInputField.frame = CGRect(x: tmpLabel.frame.maxX, y: cardLineView.frame.maxY + 10, width: mmW, height: 30)
        self.yyInputField.delegate = self
        tmpCheckView.addSubview(self.yyInputField)
        
        let monthLineView = UIView(frame: CGRect(x: 17, y: mmInputField.frame.maxY + 10, width: cvWidth, height: 1))
        monthLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpCheckView.addSubview(monthLineView)
        
        self.securyCodeField.frame = CGRect(x: cvWidth + 27, y: cardLineView.frame.maxY + 10, width: cvWidth, height: 30)
        self.securyCodeField.delegate = self
        tmpCheckView.addSubview(self.securyCodeField)
        let secuLineView = UIView(frame: CGRect(x: cvWidth + 27, y: securyCodeField.frame.maxY + 10, width: cvWidth, height: 1))
        secuLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tmpCheckView.addSubview(secuLineView)
        
        let acceptLabel = UILabel(frame: CGRect(x: 17, y: secuLineView.frame.maxY + 4, width: KScreenWidth, height: 25))
        acceptLabel.text = "Acceptable Credit Cards:"
        acceptLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        acceptLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        tmpCheckView.addSubview(acceptLabel)
        
        let accDetailLabel = UILabel(frame: CGRect(x: 17, y: acceptLabel.frame.maxY, width: kScreenWidth - 34, height: 25))
        accDetailLabel.text = "VISA/MasterCard/American Express"
        accDetailLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        accDetailLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 30))
        tmpCheckView.addSubview(accDetailLabel)
        
        let debitLabel = UILabel(frame: CGRect(x: 17, y: accDetailLabel.frame.maxY + 4, width: kScreenWidth - 34, height: 25))
        debitLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        debitLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        debitLabel.numberOfLines = 0
        debitLabel.text = "Acceptable Debit Cards:"
        tmpCheckView.addSubview(debitLabel)
        
        let leftLabel = UILabel(frame: CGRect(x: 17, y: debitLabel.frame.maxY, width: kScreenWidth - 34, height: 60))
        leftLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        leftLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 30))
        leftLabel.numberOfLines = 0
        leftLabel.lineBreakMode = .byWordWrapping
        leftLabel.text = "VISA/MasterCard/MP/IXE/HSBC/Santander/Banorte/Banamex/Bancomer"
        tmpCheckView.addSubview(leftLabel)
        
        var titleString = localized("kPMCheckOutButtonString")
        if let tmpInfo = self.paymentInfo {
            let payCharacterString = getCurrentPayCharacter()
            titleString = String(format: "%@ %@%.2f", titleString,payCharacterString,tmpInfo.orderTotalPrice)
        }
        let doneButton = UIButton(frame: CGRect(x: 14, y: leftLabel.frame.maxY + 30, width: kScreenWidth - 28, height: 49))
        doneButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        doneButton.setTitle(titleString, for: .normal)
        doneButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        doneButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        doneButton.addTarget(self, action: #selector(mercadoCheckoutButtonTapped), for: .touchUpInside)
        tmpCheckView.addSubview(doneButton)
        
        return tmpCheckView
    }()
    lazy var cardInputField: UITextField = {
        let tmpField = UITextField.init()
        tmpField.placeholder = localized("kCardNumberPlaceholder")
        tmpField.keyboardType = .numberPad
        return tmpField
    }()
    lazy var cvcInputField: UITextField = {
        let temField = UITextField.init()
        temField.placeholder = localized("kCVCPlaceholder")
        temField.keyboardType = .numberPad
        temField.delegate = self
        return temField
    }()
    lazy var mmInputField: UITextField = {
        let temField = UITextField.init()
        let textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.alignment = NSTextAlignment.center
        
        let attri = NSAttributedString(string: localized("kMMPlaceholder"), attributes: [NSAttributedStringKey.foregroundColor: textColor,NSAttributedStringKey.paragraphStyle: paraStyle])
        temField.attributedPlaceholder = attri
        temField.keyboardType = .numberPad
        let selectedCountryCode = getCurrentCountryCodeString()
        if selectedCountryCode == "MX" || selectedCountryCode == "AR" {
            temField.textAlignment = .left
        } else {
            temField.textAlignment = .center
        }
        temField.delegate = self
        return temField
    }()
    lazy var yyInputField: UITextField = {
        let temField = UITextField.init()
        let textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.alignment = NSTextAlignment.center
        let attri2 = NSAttributedString(string: localized("kYYPlaceholder"), attributes: [NSAttributedStringKey.foregroundColor: textColor,NSAttributedStringKey.paragraphStyle: paraStyle])
        temField.attributedPlaceholder = attri2
        temField.keyboardType = .numberPad
        temField.textAlignment = .center
        temField.delegate = self
        return temField
    }()
    lazy var zipInputField: UITextField = {
        let temField = UITextField.init()
        temField.placeholder = localized("kZipPlaceholder")
        temField.delegate = self
        temField.keyboardType = .numberPad
        return temField
    }()
    lazy var montyLabel: UILabel = {
        let tmpLabel = UILabel.init()
        tmpLabel.textAlignment = .right
        tmpLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 28))
        tmpLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        return tmpLabel
    }()
    lazy var emailField: UITextField = {
        let tmpField = UITextField.init()
        tmpField.placeholder = "Email"
        tmpField.text = UserManager.shareUserManager.getEmail()
        return tmpField
    }()
    lazy var fullNameField: UITextField = {
        let tmpField = UITextField.init()
        tmpField.placeholder = "Full Name"
        return tmpField
    }()
    lazy var securyCodeField: UITextField = {
        let tmpField = UITextField.init()
        tmpField.placeholder = "Securety Code"
        tmpField.delegate = self
        tmpField.keyboardType = .numberPad
        return tmpField
    }()
    
    func setupHeaderView() {
        let tipsView = UIView(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 40))
        tipsView.backgroundColor = kRGBColorFromHex(rgbValue: 0xfff4f2)
        
        let tipsLabel = UILabel(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 40))
        tipsLabel.text = localized("kPMNotiseString")
        tipsLabel.textColor = kRGBColorFromHex(rgbValue: 0xE9668F)
        tipsLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 30))
        tipsLabel.textAlignment = .center
        tipsView.addSubview(tipsLabel)
        view.addSubview(tipsView)
        
        let selectedCountryCode = getCurrentCountryCodeString()
        var totalButtonCount = 2
        if selectedCountryCode == "MY" || selectedCountryCode == "AR" {
            totalButtonCount = 1
        }
        let buttonWidth: CGFloat = KScreenWidth / CGFloat(totalButtonCount)
        for i in 0 ..< totalButtonCount {
            let itemButton = CheckOutButton(frame: CGRect(x: CGFloat(i) * buttonWidth, y: 40, width: buttonWidth, height: 60))
            if selectedCountryCode == "IN" {
                //印度 没有paypal 有货到付款
                if i == 0 {
                    itemButton.setImage(UIImage(named: "visa_discver_card_icon"), for: .normal)
                    itemButton.selectView.isHidden = true
                } else if i == 1 {
                    itemButton.setImage(UIImage(named: "checkout_cash_icon"), for: .normal)
                    itemButton.selectView.isHidden = false
                }
            }
//            else if selectedCountryCode == "MX"  {
//                //|| selectedCountryCode == "AR"
//                if i == 0 {
//                    itemButton.setImage(UIImage(named: "visa_discver_card_icon"), for: .normal)
//                    itemButton.selectView.isHidden = false
//                } else if i == 1 {
//                    itemButton.setImage(UIImage(named: "paypal_card_icon"), for: .normal)
//                    itemButton.selectView.isHidden = true
//                }
//            }
            else {
                if i == 0 {
                    itemButton.setImage(UIImage(named: "visa_discver_card_icon"), for: .normal)
                    itemButton.selectView.isHidden = false
                } else if i == 1 {
                    itemButton.setImage(UIImage(named: "paypal_card_icon"), for: .normal)
                    itemButton.selectView.isHidden = true
                }
            }
            itemButton.addTarget(self, action: #selector(paymentBtnClick(sender:)), for: .touchUpInside)
            itemButton.tag = 100 + i
            view.addSubview(itemButton)
        }
    }
    func setupSubViews() {
        view.addSubview(self.backScrollView)
        self.backScrollView.frame = CGRect(x: 0, y: 100, width: KScreenWidth, height: kScreenHeight - 100)
        self.backScrollView.addSubview(self.stripePayView)
        let selectedCountryCode = getCurrentCountryCodeString()
        if selectedCountryCode == "IN" {
            //印度 stripe 货到付款
            self.stripePayView.isHidden = true
            self.cashDeliveryView.isHidden = false
            self.backScrollView.addSubview(self.cashDeliveryView)
        }
//        else if selectedCountryCode == "MX"  { //mo
//            //|| selectedCountryCode == "AR"
//            //self.backScrollView.addSubview(self.mercadoCheckView)
//            self.stripePayView.isHidden = false
//            self.mercadoCheckView.isHidden = false
//            self.paypalView.isHidden = true
//            self.backScrollView.addSubview(self.paypalView)
//        }
        else if selectedCountryCode == "MY" || selectedCountryCode == "AR" { // 马来西亚 阿根廷 只有stripe支付
            self.stripePayView.isHidden = false
            self.mercadoCheckView.isHidden = true
            self.paypalView.isHidden = true
        } else {
            //非印度 stripe PayPal
            self.paypalView.isHidden = true
            self.backScrollView.addSubview(self.paypalView)
        }
    }
    @objc func paymentBtnClick(sender: UIButton) {
        view.endEditing(true)
        let selectedButton = sender as! CheckOutButton
        selectedButton.selectView.isHidden = false
        selectedButton.isSelected = true
        let selectedCountryCode = getCurrentCountryCodeString()
        if sender.tag == 100 {
            //Stripe
            if selectedCountryCode != "MY" && selectedCountryCode != "AR" {
                let normalButton = view.viewWithTag(101) as! CheckOutButton
                normalButton.isSelected = false
                normalButton.selectView.isHidden = true
                self.stripePayView.isHidden = false
                if selectedCountryCode == "IN" {
                    self.cashDeliveryView.isHidden = true
                } else {
                    self.paypalView.isHidden = true
                }
                configCommonEventMonitor(monitorString: kPaymentSelectStripeEvent, type: 2)
            }
            
        } else if sender.tag == 101 {
            //Paypal 或者是 货到付款
            let normalButton = view.viewWithTag(100) as! CheckOutButton
            normalButton.selectView.isHidden = true
            normalButton.isSelected = false
            self.stripePayView.isHidden = true
            if selectedCountryCode == "IN" {
                //货到付款
                self.cashDeliveryView.isHidden = false
                configCommonEventMonitor(monitorString: kPaymentSelectCashEvent, type: 2)
            } else {
                //PayPal
                self.paypalView.isHidden = false
                configCommonEventMonitor(monitorString: kPaymentSelectPayPalEvent, type: 2)
            }
        }
    }
    override func leftBackButtonTapped() {
        configCommonEventMonitor(monitorString: kFirstPaymentBackEvent, type: 2)
        navigationController?.popViewController(animated: true)
    }
    @objc func stripeCheckOutButtonTapped() {
        //信用卡支付
        configCommonEventMonitor(monitorString: kPaymentStripeCheckOutEvent, type: 2)
        paymentWithStripeCard()
    }
    @objc func paypalCheckOutButtonTapped() {
        configCommonEventMonitor(monitorString: kPaymentPaypalCheckOutEvent, type: 2)
        paymentWithPaypal()
        //Paypal支付
        //            let checkOutVC = CheckOutViewController()
        //            checkOutVC.addressInfo = self.addressInfo
        //            checkOutVC.paymentType = 1
        //            checkOutVC.paymentInfo = self.paymentInfo
        //            checkOutVC.payOrderInfo = self.payOrderInfo
        //            checkOutVC.tmpOrderSuccessInfo = self.tmpOrderSuccessInfo
        //            checkOutVC.itemParamsArr = self.itemParamsArr
        //            checkOutVC.itemProductCount = self.itemProductCount
        //            checkOutVC.itemProductIDString = self.itemProductIDString
        //            self.navigationController?.pushViewController(checkOutVC, animated: true)
    }
    @objc func cashCheckOutButtonTapped() {
        configCommonEventMonitor(monitorString: kPaymentCashCheckOutEvent, type: 2)
        let userID = UserManager.shareUserManager.getUserID()
        if let payorderInfo = self.payOrderInfo,let orderSuccessInfo = self.tmpOrderSuccessInfo,let addressInfo = self.addressInfo {
            let param = ["UserID":userID,
                         "AddrID": addressInfo.AddrID,
                         "PayCode": orderSuccessInfo.PayCode,
                         "ClientType": 1] as [String : Any]
            MBProgressHUD.showAdded(to: self.view, animated: true)
            FPHttpManager().manager.post(CashPaymentInterface, parameters: param, progress: nil, success: { (task, result) in
                MBProgressHUD.hide(for: self.view, animated: true)
                let json = JSON.init(result as Any)
                print("cashCheckOutButtonTapped\(json)")
                if json["Code"].stringValue == "200" {
                     self.checkOutsuccess()
                     self.configCashCheckOutSuccessEvent(monitor: kPaymentCashCheckOutSuccess, totalPrice: payorderInfo.OrderPayTotal, productID: self.itemProductIDString, numberItem: self.itemProductCount, eventContent: self.itemParamsArr)
                }
            }) { (task, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
                self.checkOutFailed()
            }
        }
    }
    //Mercado 支付
    @objc func mercadoCheckoutButtonTapped() {
        configCheckoutWithMercado()
    }
    func paymentWithStripeCard() {
        if cardInputField.text == "" || cvcInputField.text == "" || mmInputField.text == "" || yyInputField.text == ""  {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = .text
            hud.detailsLabel.text = kPMEnterCorrectCardString//"请输入信用卡信息"
            hud.hide(animated: true, afterDelay: 1.0)
            return
        }
        var mmString = ""
        if let mmFieldString = mmInputField.text {
            if mmFieldString.count == 1 && Int(mmFieldString)! < 10 {
                mmString = "0\(mmFieldString)"
            } else {
                mmString = mmFieldString
            }
        }
        let mbhud = MBProgressHUD.showAdded(to: self.view, animated: true)
        let yyString = yyInputField.text!
        let cardParams = STPCardParams()
        cardParams.number = cardInputField.text!
        cardParams.expMonth = UInt.init(mmString)!
        cardParams.expYear = UInt.init(yyString)!
        cardParams.cvc = cvcInputField.text!
        cardParams.address.postalCode = zipInputField.text!
        cardParams.currency = getCurrentPaymentCodeString()
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            if error != nil {
                mbhud.mode = MBProgressHUDMode.text
                mbhud.detailsLabel.text = kPMCardInvalidString
                mbhud.hide(animated: true, afterDelay: 2.0)
                return
            }
            self.logAddedPaymentInfoEvent(success: true)
            AppsFlyerTracker.shared().trackEvent(AFEventAddPaymentInfo, withValues: [AFEventParamSuccess : true])
            
            //创建信用卡
            guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
                return
            }
            let param1 = ["UserID":userID]
            FPHttpManager.init().manager.post(StripeCreatCustomIDInterface, parameters: param1, progress: nil, success: { (task, result) in
                let json = JSON.init(result as! Data)
                print("创建信用卡\(json)")
                if json["Code"].stringValue == "400" {
                    mbhud.mode = MBProgressHUDMode.text
                    mbhud.label.text = json["Msg"].string
                    mbhud.hide(animated: true, afterDelay: 2.0)
                    return
                }
                let customerID = json["Data"]["CustomerID"].stringValue
                let stripeToken: STPToken = token!
                let countryCode = getCurrentCountryCodeString()
                let countryPay = getCurrentPayCharacter()
                let param = ["UserID":userID,
                             "CustomerID":customerID,
                             "Token": stripeToken.tokenId,
                             "ClientType": "1",
                             "CountrySName":countryCode,
                             "CountryCurrency":countryPay]
                FPHttpManager().manager.post(MyWalletAddInterface, parameters: param, progress: nil, success: { (task, result) in
                    
                    let json = JSON.init(result as Any)
                    print("doneButtonTapped\(json)")
                    if json["Code"].stringValue != "200"{
                        mbhud.hide(animated: true)
                        showToast(json["Msg"].string!)
                        return
                    }
                    let createWallet = MyWalletInfo(jsonData: json["Data"])
                    if let payorderInfo = self.payOrderInfo,let orderSuccessInfo = self.tmpOrderSuccessInfo,let addressInfo = self.addressInfo {
                        let countryCode = getCurrentCountryCodeString()
                        let countryPay = getCurrentPayCharacter()
                        let param = ["UserID":userID,
                                     "OrderPayTotal": payorderInfo.OrderPayTotal,
                                     "CardID": createWallet.CardInfoID,
                                     "AddrID": addressInfo.AddrID,
                                     "PayCode": orderSuccessInfo.PayCode,
                                     "ClientType": 1,
                                     "CountrySName":countryCode,
                                     "CountryCurrency":countryPay]
                        FPHttpManager().manager.post(StripeCardCheckOut, parameters: param, progress: nil, success: { (task, result) in
                            mbhud.hide(animated: true)
                            let json = JSON.init(result as Any)
                            print("configStripeCardCheckOut\(json)")
                            if json["Code"].intValue == 200 {
                                self.configStripeCheckOutSuccessEvent(monitor: kPaymentStripeCheckOutSuccess, totalPrice: payorderInfo.OrderPayTotal, productID: self.itemProductIDString, numberItem: self.itemProductCount, eventContent: self.itemParamsArr)
                                self.checkOutsuccess()
                            } else if json["Code"].intValue == 400 { //已经被支付过了
                                let string = json["Msg"].stringValue
                                showToast(string)
                                self.navigationController?.popToRootViewController(animated: true)
                            } else {
                                self.configCheckOutFailEvent(monitor: kStripeCheckOutFail)
                                self.checkOutFailed()
                            }
                        }) { (task, error) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    }
                    
                    /*
                    let cardBrand: STPCardBrand = (stripeToken.card?.brand)!
                    createWallet.TopOne = 0
                    createWallet.CardNum = (stripeToken.card?.last4)!
                    if cardBrand == STPCardBrand.visa {
                        createWallet.CardType = "Visa"
                    } else if cardBrand == STPCardBrand.masterCard {
                        createWallet.CardType = "MasterCard"
                    } else if cardBrand == STPCardBrand.amex {
                        createWallet.CardType = "American Express"
                    } else if cardBrand == STPCardBrand.JCB {
                        createWallet.CardType = "JCB"
                    } else if cardBrand == STPCardBrand.discover {
                        createWallet.CardType = "Discover"
                    } else if cardBrand == STPCardBrand.dinersClub {
                        createWallet.CardType = "Diners Club"
                    }
                    let checkOutVC = CheckOutViewController()
                    checkOutVC.addressInfo = self.addressInfo
                    checkOutVC.paymentType = 2
                    checkOutVC.cardInfo = createWallet
                    checkOutVC.paymentInfo = self.paymentInfo
                    checkOutVC.payOrderInfo = self.payOrderInfo
                    checkOutVC.tmpOrderSuccessInfo = self.tmpOrderSuccessInfo
                    checkOutVC.itemParamsArr = self.itemParamsArr
                    checkOutVC.itemProductCount = self.itemProductCount
                    checkOutVC.itemProductIDString = self.itemProductIDString
                    configCommonEventMonitor(monitorString: kSaveCardEvent, type: 2)
                    self.navigationController?.pushViewController(checkOutVC, animated: true)
                     */
                }) { (task, error) in
                    mbhud.hide(animated: true)
                }
            }, failure: { (task, error) in
                mbhud.mode = .text
                mbhud.label.text = kPMSaveFailureString
                mbhud.hide(animated: true, afterDelay: 1.0)
            })
        }
    }
    //Paypal 支付
    func paymentWithPaypal() {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,"CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        if let payorderInfo = self.payOrderInfo,let orderSuccessInfo = self.tmpOrderSuccessInfo,let addressInfo = self.addressInfo {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            FPHttpManager().manager.post(GetTokenInterface, parameters: param, progress: nil, success: { (task, result) in
                let json = JSON(result as Any)
                let token = json["Data"]["ClientToken"].stringValue
                
                self.braintreeClient = BTAPIClient.init(authorization: token)
                self.payPalDriver = BTPayPalDriver.init(apiClient: self.braintreeClient!)
                self.payPalDriver?.viewControllerPresentingDelegate = self
                self.payPalDriver?.appSwitchDelegate = self
                
                let request = BTPayPalRequest.init(amount: String(2.32))
                request.displayName = "faypark"
                request.offerCredit = true
                request.currencyCode = getCurrentPaymentCodeString()
                
                self.payPalDriver?.requestOneTimePayment(request, completion: { (payNonce, error) in
                    if let tmpPayNonce = payNonce {
                        let countryCode = getCurrentCountryCodeString()
                        let countryPay = getCurrentPayCharacter()
                        let payParam = ["UserID":userID,
                                        "payment_method_nonce":tmpPayNonce.nonce,
                                        "OrderPayTotal":payorderInfo.OrderPayTotal,
                                        "AddrID":addressInfo.AddrID,
                                        "PayCode":orderSuccessInfo.PayCode,
                                        "ClientType":1,
                                        "CountrySName":countryCode,
                                        "CountryCurrency":countryPay]
                        FPHttpManager().manager.post(PaypleCheckOut, parameters: payParam, progress: nil, success: { (task, result) in
                            let json = JSON(result as Any)
                            print("configPayPalCheckOut\(json)")
                            MBProgressHUD.hide(for: self.view, animated: true)
                            //付款成功,弹出提示
                            if json["Code"].intValue == 200 {
                                self.configPayPalCheckOutSuccessEvent(monitor: kPaymentPayPalCheckOutSuccess, totalPrice: payorderInfo.OrderPayTotal, productID: self.itemProductIDString, numberItem: self.itemProductCount, eventContent: self.itemParamsArr)
                                //显示支付成功界面
                                self.checkOutsuccess()
                            } else {
                                self.configCheckOutFailEvent(monitor: kPayPalCheckOutFail)
                                self.checkOutFailed()
                            }
                            
                        }, failure: { (task, error) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                        })
                    } else {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                })
            }) { (task, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func configCheckoutWithMercado() {
        if let payorderInfo = self.payOrderInfo,let orderSuccessInfo = self.tmpOrderSuccessInfo,let addressInfo = self.addressInfo  {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            if emailField.text == "" || cardInputField.text == "" || fullNameField.text == "" || mmInputField.text == "" || yyInputField.text == "" || securyCodeField.text == "" {
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.mode = .text
                hud.detailsLabel.text = kPMEnterCorrectCardString//"请输入信用卡信息"
                hud.hide(animated: true, afterDelay: 1.0)
                return
            }
            var mmString = ""
            if let mmFieldString = mmInputField.text {
                if mmFieldString.count == 1 && Int(mmFieldString)! < 10 {
                    mmString = "0\(mmFieldString)"
                } else {
                    mmString = mmFieldString
                }
            }
            let yyString = self.yyInputField.text
           
            let userID = UserManager.shareUserManager.getUserID()
            let param = ["UserID": userID]
            //获取PublishKey
            
            FPHttpManager().manager.post(MercadoPublishKeyInterface, parameters: param, progress: nil, success: { (task, result) in
                MBProgressHUD.hide(for: self.view, animated: true)
                let json = JSON.init(result as Any)
                print("configMercadoCheckOut\(json)")
                if json["Code"].stringValue == "200" {
                    let publishKey = json["Data"]["MPPublishableKey"].stringValue
                    let MercadoTokenInterface = "https://api.mercadopago.com/v1/card_tokens?public_key=\(publishKey)"
                    let tokenParams = ["card_number":"5474925432670366",
                                       "security_code":"366",
                                       "expiration_month":11,
                                       "expiration_year":2020,
                                       "cardholder":["name":"test"]] as [String : Any]
                    print("toke para\(tokenParams)")
                    //获取token
                    let manage = FPHttpManager().manager
                    manage.requestSerializer = AFJSONRequestSerializer()
                    manage.responseSerializer.acceptableContentTypes = NSSet(array: ["text/html","application/json","text/json","text/plain","text/javascript"]) as? Set<String>
                    manage.post(MercadoTokenInterface, parameters: tokenParams, progress: nil, success: { (task, result) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let json = JSON.init(result as Any)
                        print("MercadoTokenInterface\(json)")
                        let mpToken = json["id"].stringValue
                        //获取customID
                        let customerIDParams = ["UserID": "eq4256",
                                                "UserEmail":"test_user_76209764@testuser.com"]
                        FPHttpManager().manager.post(MercadoCustomIDInterface, parameters: customerIDParams, progress: nil, success: { (task, result) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                            let json = JSON.init(result as Any)
                            print("MercadoCustomIDInterface\(json)")
                            if json["Code"].stringValue == "200" {
                                let customerID = json["Data"]["MPCustomerId"].stringValue
                                let createCardParams = ["UserID": "eq4256",
                                                        "MPCustomerId":customerID,
                                                        "MPToken": mpToken,
                                                        "ClientType": "1"]
                                FPHttpManager().manager.post(MercadoCreateCardInterface, parameters: createCardParams, progress: nil, success: { (task, result) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    let json = JSON.init(result as Any)
                                    print("MercadoCreateCardInterface\(json)")
                                    if json["Code"].stringValue == "200" {
                                        let cardID = json["Data"]["MPCardId"].stringValue
                                        let checkOutParams = ["UserID": "eq4256",
                                                              "OrderPayTotal": payorderInfo.OrderPayTotal,
                                                              "CardID":cardID,
                                                              "AddrID":addressInfo.AddrID,
                                                              "PayCode":orderSuccessInfo.PayCode,
                                                              "ClientType":"1"] as [String : Any]
                                        FPHttpManager().manager.post(MercadoCheckOutInterface, parameters: checkOutParams, progress: nil, success: { (task, result) in
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                            let json = JSON.init(result as Any)
                                            print("MercadoCheckOutInterface\(json)")
                                            if json["Code"].stringValue == "200" {
                                                //显示支付成功界面
                                                self.checkOutsuccess()
                                            } else {
                                                self.configCheckOutFailEvent(monitor: kPayPalCheckOutFail)
                                                self.checkOutFailed()
                                            }
                                        }) { (task, error) in
                                            MBProgressHUD.hide(for: self.view, animated: true)
                                        }
                                    } else {
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                }) { (task, error) in
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                }
                            } else {
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        }) { (task, error) in
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    }) { (task, error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }) { (task, error) in
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    //Stripe支付成功监测
    func configStripeCheckOutSuccessEvent(monitor: String,totalPrice:Double,productID: String,numberItem: Int,eventContent:[[String: Any]]) {
        let userID = UserManager.shareUserManager.getUserID()
        let totalPriceString = decimalNumberWithDouble(convertValue: totalPrice)
        let totalDoble = Double(totalPriceString)!
        let eventContentJsonString = convertArrayToString(arr: eventContent as [AnyObject])
        let currencyString = getCurrentPaymentCodeString()
        //FayPark
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: kStripeCheckOutSuccess, productID: productID)
        //FBS
        let params = [FBSDKAppEventParameterNameContentID : productID, FBSDKAppEventParameterNameContentType : "product_group", FBSDKAppEventParameterNameNumItems : numberItem, FBSDKAppEventParameterNamePaymentInfoAvailable : 0,
                      FBSDKAppEventParameterNameCurrency : currencyString,
                      FBSDKAppEventNameViewedContent:eventContentJsonString] as [String : Any]
        FBSDKAppEvents.logPurchase(totalDoble, currency: currencyString, parameters: params)
        //AppsFlyer
        AppsFlyerTracker.shared().trackEvent(monitor, withValues:[AFEventParamPrice:totalDoble, AFEventParamCurrency: currencyString, AFEventParamContentType: "product_group", AFEventParamContentId:productID, AFEventContent: eventContentJsonString,"num_items":numberItem,"content_name":monitor])
        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId:productID,AFEventParamContentType: "product_group",AFEventParamRevenue:totalPriceString,AFEventParamCurrency: currencyString])
    }
    //paypal
    func configPayPalCheckOutSuccessEvent(monitor: String,totalPrice:Double,productID: String,numberItem: Int,eventContent:[[String: Any]]) {
        let userID = UserManager.shareUserManager.getUserID()
        let totalPriceString = decimalNumberWithDouble(convertValue: totalPrice)
        let totalDoble = Double(totalPriceString)!
        let eventContentJsonString = convertArrayToString(arr: eventContent as [AnyObject])
        let currencyString = getCurrentPaymentCodeString()
        //Faypark
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: monitor, productID: productID)
        //FBS
        let params = [FBSDKAppEventParameterNameContentID : productID, FBSDKAppEventParameterNameContentType : "product_group", FBSDKAppEventParameterNameNumItems : numberItem, FBSDKAppEventParameterNamePaymentInfoAvailable : 0,
                      FBSDKAppEventParameterNameCurrency : currencyString,
                      FBSDKAppEventNameViewedContent:eventContentJsonString] as [String : Any]
        FBSDKAppEvents.logPurchase(totalDoble, currency: currencyString, parameters: params)
        //AppsFlyer
        AppsFlyerTracker.shared().trackEvent(monitor, withValues:[AFEventParamPrice:totalDoble, AFEventParamCurrency: currencyString, AFEventParamContentType: "product_group", AFEventParamContentId:productID,"num_items":numberItem,"content_name":monitor])
        AppsFlyerTracker.shared().trackEvent(AFEventPurchase, withValues: [AFEventParamContentId:productID,AFEventParamContentType: "product_group",AFEventParamRevenue:totalPriceString,AFEventParamCurrency: currencyString])
    }
    func configCashCheckOutSuccessEvent(monitor: String,totalPrice:Double,productID: String,numberItem: Int,eventContent:[[String: Any]]) {
        let userID = UserManager.shareUserManager.getUserID()
        let totalPriceString = decimalNumberWithDouble(convertValue: totalPrice)
        let totalDoble = Double(totalPriceString)!
        let eventContentJsonString = convertArrayToString(arr: eventContent as [AnyObject])
        let currencyString = getCurrentPaymentCodeString()
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: monitor, productID: productID)
        AppsFlyerTracker.shared().trackEvent(kCashCheckOutSuccess, withValues: [AFEventParamPrice:totalDoble, AFEventParamCurrency: currencyString, AFEventParamContentType: "product_group", AFEventParamContentId:productID, AFEventContent: eventContentJsonString,"num_items":numberItem,"content_name":monitor])
    }
    func configCheckOutFailEvent(monitor: String) {
        let userID = UserManager.shareUserManager.getUserID()
        FBSDKAppEvents.logEvent(monitor)
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: "3", name: monitor, productID: "0")
        AppsFlyerTracker.shared().trackEvent(monitor, withValues: nil)
    }
    func checkOutsuccess() {
        //成功
        let orderCompletVC = OrderCompleteViewController()
        self.navigationController?.pushViewController(orderCompletVC, animated: true)
    }
    func checkOutFailed() {
        configCommonEventMonitor(monitorString: kCheckOutFail, type: 1)
        let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        let alertModel = AlertButtonModel(title: kPMTryAgainString, buttonType: .AlertButtonTypeNormal)
        alertView.showWithAlertView(title: kPMSomethingWrongString, detailString: kPMRecheckPaymentString, buttonArr: [alertModel],cancel: false)
        alertView.bottomButtonTapped = { (button: UIButton) -> Void in
            self.paymentWithStripeCard()
        }
    }
    func configAppEventMoniter(monitorString: String,staticsType: String) {
        let userID = UserManager.shareUserManager.getUserID()
        FBSDKAppEvents.logEvent(monitorString)
        AppsFlyerTracker.shared().trackEvent(monitorString, withValues: nil)
        FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: staticsType, name: monitorString, productID: "0")
    }
    func logAddedPaymentInfoEvent(success : Bool) {
        let tipNo = success ? 1 : 0
        let params = [FBSDKAppEventParameterNameSuccess : tipNo]
        FBSDKAppEvents.logEvent(FBSDKAppEventNameAddedPaymentInfo, parameters: params)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ShippingAddressViewController: UITextFieldDelegate {
    // MARK: - TextField代理
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == cardInputField {
            if range.location >= 16 {
                cvcInputField.becomeFirstResponder()
                return false
            }
        }
        if textField == cvcInputField {
            if range.location >= 3 {
                mmInputField.becomeFirstResponder()
                return false
            }
        }
        if textField == mmInputField {
            if range.location >= 2 {
                yyInputField.becomeFirstResponder()
                return false
            }
        }
        if textField == yyInputField {
            if range.location >= 4 {
                zipInputField.becomeFirstResponder()
                return false
            }
        }

        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

extension ShippingAddressViewController: BTAppSwitchDelegate, BTViewControllerPresentingDelegate {
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {

    }

    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {

    }

    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {

    }

    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {

    }

    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {

    }
}
