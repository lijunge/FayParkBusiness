//
//  MainViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/26.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController,UITabBarControllerDelegate {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        //设置tabbar
        setupTabBar()
        setupTabBarControllers()
    }
    func setupTabBar() {
        view.backgroundColor = UIColor.white
        tabBar.isTranslucent = false
        //设置字体偏移
        UITabBarItem.appearance().titlePositionAdjustment = UIOffsetMake(0.0, -3.0)
        tabBar.tintColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        tabBar.barTintColor = .white
    }
    func setupTabBarControllers() {
        addChildViewController(childVC: BrowseViewController(), title: kTabBarHomeString, normalImg: "browse_normal", selectedImg: "browse_pressed")
        addChildViewController(childVC: CategoriesViewController(), title: kTabBarCategoriesString, normalImg: "categories_normal", selectedImg: "categories_pressed")
        addChildViewController(childVC: CartViewController(), title: kTabBarCartString, normalImg: "cart_normal", selectedImg: "cart_pressed")
        addChildViewController(childVC: AccountViewController(), title: kTabBarAccountString, normalImg: "account_normal", selectedImg: "account_pressed")
        self.selectedIndex = 0
    }

    func addChildViewController(childVC: UIViewController,title: String,normalImg: String,selectedImg: String) {
        self.tabBarItem.title = title as String

        childVC.title = title as String

        var norImg = UIImage(named:normalImg as String)
        norImg = norImg?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        var selImg = UIImage(named:selectedImg as String)
        selImg = selImg?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        let tabItem = UITabBarItem(title: title as String, image: norImg,selectedImage: selImg)
       
        tabItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: kRGBColorFromHex(rgbValue: 0x333333),NSAttributedStringKey.font: UIFont(name: kGothamRoundedLight, size: 10) ?? UIFont.systemFont(ofSize: 10)], for: .normal)
        tabItem.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: kRGBColorFromHex(rgbValue: 0x9bbc65),NSAttributedStringKey.font: UIFont(name: kGothamRoundedLight, size: 10) ?? UIFont.systemFont(ofSize: 10)], for: .selected)
        if #available(iOS 10.0, *) {
            tabItem.badgeColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        }
        let naVc = BaseNavigationController()
        naVc.tabBarItem = tabItem
        naVc.addChildViewController(childVC)

        self.addChildViewController(naVc)
    }
    //MARK: - 拦截tabbarcontroller跳转
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        var itemTitleArr = ["Bottom-Home","Bottom-Categories","Bottom-Cart","Bottom-Account"]
        var currentIndex = 0
        if viewController == tabBarController.viewControllers![0] {
            currentIndex = 0
        } else if viewController == tabBarController.viewControllers![1] {
            currentIndex = 1
        } else if viewController == tabBarController.viewControllers![2] {
            currentIndex = 2
        } else {
            currentIndex = 3
        }
        let itemString = itemTitleArr[currentIndex]
        configCommonEventMonitor(monitorString: itemString, type: 2)
        if viewController != tabBarController.viewControllers![0] {
            if UserManager.shareUserManager.getUserName().isEmpty {
                if  viewController == tabBarController.viewControllers?[2] ||   viewController == tabBarController.viewControllers?[3] {
                    let loginVC = NewLoginViewController()
                    self.present(loginVC, animated: true, completion: nil)
                    return false
                }
            }
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
