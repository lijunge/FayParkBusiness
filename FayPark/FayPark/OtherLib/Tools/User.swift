//
//  User.swift
//  FayPark
//  用户数据类
//  Created by 陈小奔 on 2018/1/23.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct User {
    var UserID: String?
    var UserName: String?
    var FirstName: String?
    var LastName: String?
    var Phone: String?
    var Email: String?
    var Birthday: String?
    var Gender: String?
    var UserImg: String?
    
    var CarProductCount: Int?

    init(jsonData: JSON) {
        UserID    = jsonData["UserID"].stringValue
        UserName = jsonData["UserName"].stringValue
        FirstName  = jsonData["FirstName"].stringValue
        LastName = jsonData["LastName"].stringValue
        Phone     = jsonData["Phone"].stringValue
        Email = jsonData["Email"].stringValue
        Birthday = jsonData["Birthday"].stringValue
        Gender = jsonData["Gender"].stringValue
        UserImg = jsonData["UserImg"].stringValue
        CarProductCount = jsonData["CarProductCount"].intValue
    }
}
