//
//  FPHttpManager.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import AFNetworking
import SwiftyJSON

class FPHttpManager {
    
    let manager = AFHTTPSessionManager()
    
    init() {
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.responseSerializer.acceptableContentTypes = NSSet(array: ["text/html","application/json","text/json","text/plain"]) as? Set<String>
        //manager.requestSerializer.setValue("application/json,charset=utf-8", forHTTPHeaderField: "Content-Type")
         manager.securityPolicy = AFSecurityPolicy.default()
         manager.securityPolicy.allowInvalidCertificates = true
         manager.securityPolicy.validatesDomainName = false
        
    }
    
    var idfv = UIDevice.current.identifierForVendor?.uuidString
    func fayParkMonitor(userID: String, staicsType: String, name: String, productID: String) -> () {
        
        let param = ["UserID":userID, "ProductID":productID, "StatisticsType":staicsType, "Name": name, "UniqueValue":idfv ?? 0, "ClientType":"1"] as [String : Any]
        
        manager.post(FayParkMonitorInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON(result as Any)
            print("fayParkMonitor\(json)")
        }) { (task,error) in
            print("fayParkMonitor\(name)\(error)")
        }
    }
}
