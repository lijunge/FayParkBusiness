//
//  LocationManager.swift
//  FayPark
//
//  Created by faypark on 2018/7/31.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject {
    let locationManager: CLLocationManager
    static let shareInstance: LocationManager = {
        let instance = LocationManager()
        return instance
    }()
    var locationSuccessBlock: ((_ address: String) -> Void)?
    var locationFailureBlock: ((_ error: String) -> Void)?
    override init() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestWhenInUseAuthorization()
        locationManager.pausesLocationUpdatesAutomatically = true
        super.init()
        locationManager.delegate = self
        startUpdatingLocation()
    }
    func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        } else {
            //获取位置权限被拒
            if self.locationFailureBlock != nil {
                self.locationFailureBlock!("location failure")
            }
        }
    }
    func showTurnOnLocationServericeAlert() {
        
    }
}

func getBrowseSelectedContry() -> String {
    if let countryString = UserDefaults.standard.object(forKey: "kSelectedCountry") {
        return String.init(format: "%@", countryString as! CVarArg)
    }
    return ""
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let newLocation = locations.last {
            CLGeocoder().reverseGeocodeLocation(newLocation) { (pms, error) in
                if let countryString = pms?.last?.isoCountryCode {
                    manager.stopUpdatingLocation()
                    if self.locationSuccessBlock != nil {
                        self.locationSuccessBlock!(countryString)
                    }
                } else {
                    if self.locationFailureBlock != nil {
                        self.locationFailureBlock!("location failure")
                    }
                }
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
        if (error as NSError).domain == kCLErrorDomain && (error as NSError).code == CLError.Code.denied.rawValue {
            showTurnOnLocationServericeAlert()
            if self.locationFailureBlock != nil {
                self.locationFailureBlock!("location failure")
            }
        }
    }
}
