//
//  AppApi.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/23.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

/**
 * 基础API接口文件
 */

/**
 * *************************** 测试环境 && 正式环境 ***********************
 */
#if DEBUG //测试环境https://test.faypark.com/FayParkWebApi FayparkWebNewApi
let MainInterface: String = "http://test.faypark.com/FayParkWebApi"
let H5Interface: String = "https://test.faypark.com"
let StripePublishKey: String = "pk_test_bU4aGDjIuc5Kx0RvIMtu3Ct6"
#else
let MainInterface: String = "https://webapi.faypark.com/V3.1"
let H5Interface: String = "https://www.faypark.com"
let StripePublishKey: String = "pk_live_44DMZ039hbUdA3Dqso2RmCo7"
#endif
let BuglyKey: String = "d2f1e5c4-934b-4b23-a445-52b2d66e3e84"
let BuglyID: String = "79b65c0ac7"

let fayParkAppStoreString = "itms-apps://itunes.apple.com/cn/app/faypark/id1365441622?mt=8"

let ProductTrackingURLString = "https://m.17track.net/en/track#nums="
let SupportURLString = "\(H5Interface)/homeSupport.html?type=0"
let AboutUSUrlString = "\(H5Interface)/about_faypark.html?type=0"
let ShippingUrlString = "\(H5Interface)/homeSupport_question1.html?type=0"
let ReturnUrlString = "\(H5Interface)/homeSupport_question2.html?type=0"
let supportFayParkEmailString = "Support@Faypark.com"
let orderFayParkEmailString = "Orders@Faypark.com"
let businessFayParkEmailString = "BD@Faypark.com"
let marketFayParkEmailString = " Marketing@Faypark.com"
let suggestionFayParkEmailString = "Suggestion@Faypark.com"

/*********登录/退出接口************/
//登录接口
let LoginInterface = "\(MainInterface)/api/Login/Login"
//注册接口
let RegisterInterface = "\(MainInterface)/api/SignUp/OwnRegister"
//Facebook注册接口
let FacebookRegInterface = "\(MainInterface)/api/SignUp/FaceBookRegister"
//退出登录接口
let LogoutInterface = "\(MainInterface)/api/Login/Logout"
//版本更新
let VersionListInterface = "\(MainInterface)/api/Version/GetVersionNewList"
//设置国家
let SettingUserCountryInterface = "\(MainInterface)/api/UserCenter/SettingUserCountry"
/*********首页接口************/
//1.2 获取首页活动接口
let BrowseActivityListInterface = "\(MainInterface)/api/Index/GetActivityList"
//产品列表接口
let ProductListInterface = "\(MainInterface)/api/Index/GetIndexProductList"
//产品类型筛选接口
let ProductFilterInterface = "\(MainInterface)/api/Index/GetProductCategoryList"
//获取活动倒计时列表
let BrowseRecommendProductListInterface = "\(MainInterface)/api/Index/GetActivityCountDownList"

/*********用户中心************/
//消息中心接口
let NotiCenterInterface = "\(MainInterface)/api/UserCenter/Notifications"
//用户中心提示数目接口
let AccountTipNumInterface = "\(MainInterface)/api/UserCenter/UserCenterInt"

/*********我的收藏************/
//收藏列表
let FavoriteListInterface = "\(MainInterface)/api/UserCenter/MyFavorites"
//收藏删除
let FavoriteDeleteInterface = "\(MainInterface)/api/UserCenter/MyFavoritesDelete"

/*********我的历史订单************/
let MyHistoryOrderListInterface = "\(MainInterface)/api/Order/GetOrderList"
//订单详情
let OrderDetailInterface = "\(MainInterface)/api/Order/OrderDetail"
//订单收货
let ConfirmReceiveProductInterface = "\(MainInterface)/api/Order/GetUserConfirmCollect"
// 获取评论列表
let ProductCommentListInterface = "\(MainInterface)/api/ProductComment/GetProductCommentList"
//新增评论
let AddProductCommentInfoInterface = "\(MainInterface)/api/ProductComment/GetAddProductCommentInfo"

/*********我的信用卡************/
//获取用户卡列表接口
let MyWalletListInterface = "\(MainInterface)/api/UserCenter/GetUserCardInfo"
let MyWalletAddInterface = "\(MainInterface)/api/UserCenter/GetAddUserCardInfo"
let MyWalletSetDefultPayInterface = "\(MainInterface)/api/UserCenter/SetUserDefaultCard"

/*********我的地址************/
//我的地址列表
let AccountMyAddressInterface = "\(MainInterface)/api/UserCenter/GetUserAddressInfo"
//新增地址接口
let AccountEditAddressInterface = "\(MainInterface)/api/UserCenter/GetUpdateAddressInfo"
//删除地址接口
let AccountDeleteAddressInterface = "\(MainInterface)/api/UserCenter/DeleteUserAddress"
//设置默认地址
let AccountDefultAddressInterface = "\(MainInterface)/api/UserCenter/SetUserDefaultAddress"

/*********客服界面************/
let SupportInterface = "\(MainInterface)/api/UserCenter/SupportList"

/*********商品详情************/
//商品详情接口
let ProductDetailInterface = "\(MainInterface)/api/ProductDetails/GetProductDetails"
//商品收藏接口
let ProductCollectionInterface = "\(MainInterface)/api/ProductDetails/GetProductCollection"
//商品分享接口
let ProductShareInterface = "\(MainInterface)/api/ProductDetails/GetProductShare"
//商品瀑布流
let ProductWaterfallInterface = "\(MainInterface)/api/ProductDetails/GetDetailsProductList"


/*********找回密码***********/
let FindYourPasswordInterface = "\(MainInterface)/api/UserCenter/GetForgetPwd"
//更改密码
let ChangePasswordInterface = "\(MainInterface)/api/UserCenter/GetModifyPwd"
//修改个人资料
let UpdateProfileInterface = "\(MainInterface)/api/UserCenter/GetUpdateUserData"
//上传头像
let UploadUserHeadImgInterface = "\(MainInterface)/api/UserCenter/PostFormData"

/**********购物车************/
let CartInfoInterface = "\(MainInterface)/api/ProductDetails/GetUserCartInfo"
//添加进购物车接口
let AddCartInterface = "\(MainInterface)/api/ProductDetails/GetAddShoppingCart"
//分享并添加至购物车
let ShareAndCartInterface = "\(MainInterface)/api/ProductDetails/GetProductShareTimeCart"
//更新购物车数据接口
let UpdateCartInterface = "\(MainInterface)/api/ProductDetails/GetUpdateUserCart"
//购物车显示数量接口
let CartNumberInterface = "\(MainInterface)/api/UserCenter/GetUserBuyCarCount"
//获取freegift列表
let FreeGiftListInterface = "\(MainInterface)/api/ProductDetails/GetFreeGiftList"
//创建支付的临时订单buynow
let CreatePaymentOrderInterface = "\(MainInterface)/api/ProductDetails/GetPaymentAddressDefault"

/***********支付************/
//获取客户端token
let GetTokenInterface = "\(MainInterface)/api/Paypal/GetClientToken"
//退款
let RefundInterface = "\(MainInterface)/api/Paypal/OrderRefundNew"
let StripeCreatCustomIDInterface = "\(MainInterface)/api/Stripe/GetStripeNewCustomerId"
//信用卡支付 Stripe
let StripeCardCheckOut = "\(MainInterface)/api/Stripe/StripeNewCheckout"
//PayPal 支付
let PaypleCheckOut = "\(MainInterface)/api/Paypal/PayPalCheckout"
//货到付款支付
let CashPaymentInterface = "\(MainInterface)/api/CashDelivery/CashPaymentCheckout"
//Mercado 支付 PublishKey
let MercadoPublishKeyInterface = "\(MainInterface)/api/MP/GetMPPublishableKey"
//获取CustomID
let MercadoCustomIDInterface = "\(MainInterface)/api/MP/GetMPCustomerId"
//创建卡
let MercadoCreateCardInterface = "\(MainInterface)/api/MP/GetMPCustomerCard"
//mercado 扣款
let MercadoCheckOutInterface = "\(MainInterface)/api/MP/GetMPCheckout"


/***********监测*************/
let FayParkMonitorInterface = "\(MainInterface)/api/Monitor/WriteMonitor"

/******************获取类型1列表*******************/
let FayParkGetTypeAggregateListFace  = "\(MainInterface)/api/Index/GetTypeAggregateList"
/******************获取类型2列表*******************/
let FayParkGetNavigationTypeList = "\(MainInterface)/api/Index/GetNavigationTypeList"
/*******************根据类型筛选列表****************/
let FayParkGetConditionList = "\(MainInterface)/api/Index/GetConditionList"
/******************获取类型列表*******************/
let FayParkGetActivityNameList = "\(MainInterface)/api/Index/GetActivityNameList"
/******************游客登陆*******************/
let FayParkGetUserTourist = "\(MainInterface)/api/Login/GetUserTourist"


