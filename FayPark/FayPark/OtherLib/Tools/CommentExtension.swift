//
//  CommentExtension.swift
//  FayPark
//
//  Created by faypark on 2018/6/17.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

//设置按钮的默认点击间隔时间
fileprivate let defaultDuration = 0.5

//MARK: String extension
extension String {
    /**
     * 获取字符串的高度。指定宽度和字体大小
     */
    func getHeightWithConstrainedWidth(width: CGFloat,font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat(MAXFLOAT))
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return boundingBox.height
    }
    /**
     * 获取字符串的size。指定最大宽度和高度和字体大小
     */
    func getSizeOfStringWithConstrainedFontAndMaxSize(font: UIFont,maxSize: CGSize) -> CGSize {
        let boundingBox = self.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        let boundSize = CGSize(width: boundingBox.width, height: boundingBox.height)
        return boundSize
    }
    
    static func getAttributeString(str:String, lineSpace:CGFloat, font:UIFont) -> NSAttributedString {
        
        let paragh: NSMutableParagraphStyle = NSMutableParagraphStyle.init()
        paragh.lineSpacing = lineSpace
        let attText: NSAttributedString = NSAttributedString.init(string: str, attributes: [NSAttributedStringKey.paragraphStyle : paragh, NSAttributedStringKey.font:font])
        return attText
    }
    
    static func getHeighForAttributeString(str:NSAttributedString, textWidth:CGFloat) -> CGFloat {
        
        return str.boundingRect(with: CGSize.init(width: textWidth, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, context: nil).size.height
    }
    
    static func getWidthForAttributeString(str:NSAttributedString, textHeight:CGFloat) -> CGFloat {
        
        return str.boundingRect(with: CGSize.init(width: CGFloat(MAXFLOAT), height: textHeight), options: .usesLineFragmentOrigin, context: nil).size.width
    }
}
//MARK UIButton Extension
extension UIButton {
    //图片的方向
    enum ButtonImageDirection {
        case Top    //图片在上 文字在下
        case Right  //图片在右 文字在左
        case Left
        case Down
    }
    func imageLabelEdgeInset(imageDirection: ButtonImageDirection,imageTitleSpace: CGFloat) {
        //得到imageView和titleLabel的宽高
        let imageWidth = self.imageView?.frame.size.width
        let imageHeight = self.imageView?.frame.size.height
        
        var labelWidth: CGFloat! = 0.0
        var labelHeight: CGFloat! = 0.0
        
        labelWidth = self.titleLabel?.intrinsicContentSize.width
        labelHeight = self.titleLabel?.intrinsicContentSize.height
        
        //初始化imageEdgeInsets和labelEdgeInsets
        var imageEdgeInsets = UIEdgeInsets.zero
        var labelEdgeInsets = UIEdgeInsets.zero
        
        //重点： 修改imageEdgeInsets和labelEdgeInsets的相对位置的计算 上左下右
        if imageDirection == ButtonImageDirection.Top {
            //图片在上边
            imageEdgeInsets = UIEdgeInsetsMake(-labelHeight-imageTitleSpace/2, 0, 0, -labelWidth)
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWidth!, -imageHeight!-imageTitleSpace/2, 0)
        } else if (imageDirection == ButtonImageDirection.Right) {
            //图片在右边
            imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth, 0, -labelWidth - imageTitleSpace)
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWidth!, 0, imageWidth!)
        }
        self.titleEdgeInsets = labelEdgeInsets
        self.imageEdgeInsets = imageEdgeInsets
    }
    private struct AssociatedKeys {
        static var clickDurationTime = "my_clickDurationTime"
        static var isIgnoreEvent = "my_isIgnoreEvent"
    }
    //点击间隔时间
    var clickDurationTime: TimeInterval {
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.clickDurationTime, newValue as TimeInterval, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            if let clickDurationTime = objc_getAssociatedObject(self, &AssociatedKeys.clickDurationTime) as? TimeInterval {
                return clickDurationTime
            }
            return defaultDuration
        }
    }
    //是否忽略点击事件
    var isIgnoreEvent: Bool {
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.isIgnoreEvent, newValue as Bool, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        get {
            if let isIgnoreEvent = objc_getAssociatedObject(self, &AssociatedKeys.isIgnoreEvent) as? Bool {
                return isIgnoreEvent
            }
            return false
        }
    }
    public class func initializeMethod() {
        if self == UIButton.self {
            let systemSel = #selector(UIButton.sendAction(_:to:for:))
            let sSel = #selector(UIButton.mySendAction(_:to:for:))
            let systemMethod = class_getInstanceMethod(self, systemSel)
            let sMethod = class_getInstanceMethod(self, sSel)
            
            let isTrue = class_addMethod(self, systemSel, method_getImplementation(sMethod!), method_getTypeEncoding(sMethod!))
            if isTrue {
                class_replaceMethod(self, sSel, method_getImplementation(systemMethod!), method_getTypeEncoding(systemMethod!))
            } else {
                method_exchangeImplementations(systemMethod!, sMethod!)
            }
        }
    }
    @objc private dynamic func mySendAction(_ action: Selector, to target: Any?,for event: UIEvent?) {
        if !isIgnoreEvent {
            isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + clickDurationTime) {
                self.isUserInteractionEnabled = true
            }
        }
        mySendAction(action, to: target, for: event)
    }
}

//MARK： - 字典
extension Dictionary where Key: ExpressibleByStringLiteral, Value:AnyObject {
    var jsonString:String {
        do {
            let stringData = try JSONSerialization.data(withJSONObject: self as NSDictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let string = String(data: stringData, encoding: String.Encoding.utf8){
                return string
            }
        } catch _ {
            
        }
        return ""
    }
}

//UIImage
extension UIImage {
    /**
     * 重设图片大小
     */
    func resizeImage(reSize: CGSize) -> UIImage {
        //UIGraphicsBeginImageContext(reSize);
        UIGraphicsBeginImageContextWithOptions(reSize,false,0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: reSize.width, height: reSize.height))
        let reSizeImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return reSizeImage
    }
    /**
     * 等比率缩放
     */
    func scaleImage(scaleSize: CGFloat) -> UIImage {
        let reSize = CGSize(width: self.size.width * scaleSize, height: self.size.height * scaleSize)
        print(reSize)
        return resizeImage(reSize: reSize)
    }
    
}
        
        
class DrawPointImage: UIImage {
    
    class func getBlackPointImage() -> UIImage {
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: kSizeFrom750(x: 16), height: kSizeFrom750(x: 16)))
        view.backgroundColor = UIColor.black
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let currentImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let img = currentImg?.resizableImage(withCapInsets: UIEdgeInsetsMake(currentImg!.size.height * 0.49, currentImg!.size.width * 0.49, currentImg!.size.height * 0.49, currentImg!.size.width * 0.49), resizingMode: .stretch)
        return img!
    }
}

extension Date {
    static func dateToString(date : Date, formatterString : String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = formatterString
        let dateStr = formatter.string(from: date)
        return dateStr
    }
    
    static func stringToDate(dateString : String, formatterString : String) -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = formatterString
        let date = formatter.date(from: dateString)
        return date!
    }
}

extension UIView {
    func viewController () -> (UIViewController){
        var next:UIResponder?
        next = self.next!
        repeat {
            //2.判断响应者对象是否是视图控制器类型
            if ((next as?UIViewController) != nil) {
                return (next as! UIViewController)
                
            }else {
                next = next?.next
            }
            
        } while next != nil
        
        return UIViewController()
    }
}
