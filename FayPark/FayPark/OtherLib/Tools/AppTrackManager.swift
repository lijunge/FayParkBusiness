//
//  AppTrackManager.swift
//  FayPark
//
//  Created by faypark on 2018/7/4.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

//*************************** 首页页面 ******************************
let kFayParkHomePage = "Faypark-Home"
let kCategoryPage = "Categories"
let kHomeBannerTop = "Home-Banner-Top"
let kHomeLimitBanner = "Home-Banner-LimitedTimeOffer"
let kHomeEarrings = "Home-Earrings"
let kHomeNecklaces = "Home-Necklaces"
let kHomeRings = "Home-Rings"
let kHomeBracelets = "Home-Bracelets"
let kHomeHair = "Home-HairAccessories"
//*************************** 商品详情页面 ***************************
//1.页面监测
//2.点击事件
let kDetailPage = "Detail-Product"
let kDetailCollectionEvent = "Detail-Collection" //收藏
let kDetailShareEvent = "Detail-Share" //分享
let kDetailAddToCartEvent = "Detail-AddToCart" //购买
let kDetailConfirmAddToCartEvent = "Detail-Confirm-AddToCar" //sku确认事件

let kFaceBookShareEvent = "Detail-FacebookShare" //faceBookShare
let kPinterestShareEvent = "Detail-PinterestkShare" // PinterestkShare
let kGooglePlusShareEvent = "Detail-Google+Share" //
let kInstagramShareEvent = "Detail-InstagramShare"
let kTwitterShareEvent = "Detail-Twitter"

let kDetailTopToCartEvent = "Detail-Top-ToCar" //顶部菜单栏购物车
let kDetailShareToCartEvent = "Detail-ShareForm-ToCar" //分享界面-去购物车
let kDetailMoreReviewEvent = "Detail-ViewMoreReviews" //查看更多评论
let kDetailMoreLikeEvent = "Detail-PeopleAlsoViewd"   //查看更多推荐商品

let kShippingInfoEvent = "Detail-ShippingInfo" //Shipping INfo
let kSecurityPamentEvent = "Detail-SecurityPayment" //securyty payment
let kRefundPolicyEvent = "Detail-RefundPolicy"      //Refund

//3.事件监测
//3.1 FayPark 监测
let kAddToWishlistSuccess = "AddToWishlist-Success" //添加收藏成功用这个事件
let kAddToCartSuccess = "AddToCart-Success" //添加购物车成功用这个事件

//3.2 FSBook 监测
/**
 * 1.FBSDKAppEventNameViewedContent 产品详情页面监测
 * 2.FBSDKAppEventNameAddedToCart   添加购物车成功监测
 * 3.FBSDKAppEventNameAddedToWishlist 添加收藏成功监测
 */

//3.3 AppsFlyer监测
/**
 * 1.AddToWishlist-Success 添加收藏成功监测
 * 2.AddToCart-Success     添加购物车成功监测
 */

//*************************** 购物车 ***************************
//1.页面监测
let kCartPage = "Cart"
let kFreeGiftPage = "FreeGift"
let kFreeGiftMore = "Freegift-More"
let kFreeGiftDone = "Freegift-Done"
let kFreeGiftBack = "Freegift-Back"
let kFreeGiftCartImg = "Cart-Freegift-image"
let kFreeGiftCartMore = "Cart-Freegift-more"
//2.按钮事件监测
let kCheckOutEvent = "Cart-CheckOut" //checkout
let kCartShippingInfo = "Cart-ShippingInfo"
let kCartProductSelectedEvent = "Cart-Detail-Product"
//3.事件监测

//3.1 FayPark 监测
let kCartCheckOutDetect = "Cart-CheckOut-Start" //购物车去支付用这个事件
//3.2 FSBook 监测
/**
 * 1.FBSDKAppEventNameInitiatedCheckout 点击购物车checkout按钮回掉成功添加这个事件
 */
//3.3 AppsFlyer监测
/**
 * Cart-CheckOut-Start 购物车去支付用这个事件
 */

//*************************** 支付页面 ***************************
//1.页面监测
let kCheckOutPage = "CheckOut"
let kCheckOutSuccess = "CheckOut Success"
let kCheckOutFail = "CheckOut Fail"
//2.按钮事件监测
let kSelectPayPalEvent = "CheckOut-SelectPaypal" //选择
let kSelectStripeEvent = "CheckOut-SelectStripe"
let kSelectCashEvent = "Select-SelectCashOnDelivery"

let kSaveCardEvent = "CheckOut-SaveCard"
let kAccountSaveCardEvent = "Account-SaveCard"
let kSelectPayPalCheckOutEvent = "CheckOut-Paypal" //提交paypal支付
let kSelectStripeCheckOutEvent = "CheckOut-Stripe" //提交stripe支付
let kSelectCashCheckOutEvent = "CheckOut-CashOnDelivery" //提交cash支付

//第一次支付的页面
let kPaymentSelectPayPalEvent = "Select-SelectPaypal-PaymentInfo"
let kPaymentSelectStripeEvent = "Select-SelectStripe-PaymentInfo"
let kPaymentSelectCashEvent = "Select-SelectCashOnDelivery-PaymentInfo"

let kPaymentPaypalCheckOutEvent = "CheckOut-Paypal-PaymentInfo"
let kPaymentStripeCheckOutEvent = "CheckOut-Stripe-PaymentInfo"
let kPaymentCashCheckOutEvent = "CheckOut-CashOnDelivery-PaymentInfo"
//3.事件监测
//3.1 FayPark 监测
//正常支付成功
let kPayPalCheckOutSuccess = "CheckOut-Paypal-Success" //Paypal支付成功用这个事件
let kStripeCheckOutSuccess = "CheckOut-Stripe-Success" //Stripe支付成功用这个事件
let kCashCheckOutSuccess = "CheckOut-CashOnDelivery-Success" //活到付款成功
//第一次支付成功
let kPaymentPayPalCheckOutSuccess = "CheckOut-Paypal-PaymentInfo-Success" //Paypal支付成功用这个事件
let kPaymentStripeCheckOutSuccess = "CheckOut-Stripe-PaymentInfo-Success" //Stripe支付成功用这个事件
let kPaymentCashCheckOutSuccess = "CheckOut-CashOnDelivery-PaymentInfo-Success" //货到付款成功

let kStripeCheckOutFail = "CheckOut-Stripe-Fail" //stripe支付失败
let kPayPalCheckOutFail = "CheckOut-Paypal-Fail" //paypal支付失败

//3.2 FSB监测
/**
 * logPurchase  支付成功添加这个事件
 */
//3.3 AppsFlyer监测
/**
 *  CheckOut-Paypal-Success paypal支付成功添加这个事件
 *  CheckOut-Stripe-Success stripe支付成功添加这个事件
 **/
//3.3
//*************************** Shipping地址页面 ***************************
//1.页面监测
let kShippingAddressPage = "Shipping Address"
let kAddAddressEvent = "CheckOut-SaveShippingAddress" //Shipping Address //fayPark fsb appsflyer都监测
//*************************** 登陆 注册 页面 ***************************
//1.page
let kLoginPage = "Login In"
let kRegisterPage = "Create Account"

//2.事件
let kLoginEvent = "LoginIn-Faypark"
let kRegisterEvent = "CreateAccount-Done"
let kFacebookLoginEvent = "LoginIn-SignIn-Facebook"
let kFacebookRegisterEvent = "Pop-SignIn-Facebook"
//3.事件
let kLoginFayParkSuccess = "LoginIn-Faypark-Success" //faypark登录成功添加这个事件
let kRegisterFayParkSuccess = "CreateAccount-Done-Success" //faypark注册成功添加这个事件
let kLoginFacebookSuccess = "LoginIn-Facebook-Success"     //Facebook登录成功添加这个事件
let kRegisterFacebookSuccess = "SignIn-Facebook-Success"   //Facebook注册成功添加这个事件
// FBS监测
// 1.FBSDKAppEventNameCompletedRegistration Faypark注册成功添加这个事件 REGISTRATION_METHOD：CreateAccount-Done-Success
// 2.FBSDKAppEventNameCompletedRegistration Facebook注册成功添加这个事件 REGISTRATION_METHOD：SignIn-Facebook-Success
//Appsflyer
//1 CreateAccount-Done-Success .Facebook注册成功添加这个事件 REGISTRATION_METHOD：SignIn-Facebook-Success
//2 SignIn-Facebook-Success（Facebook注册成功添加这个事件） REGISTRATION_METHOD：CreateAccount-Done-Success
//*************************** 个人中心 页面 ***************************
//1.page
let kAccountPage = "Account"
let kNotificationPage = "Notifications"
let kOrderHistoryPage = "Order History"
let kOrderDetailPage = "Order-"
let kCollectPage = "My Like"
let kRefundPage = "Apply Refund"
let kSupportPage = "Support"
let kSettingPage = "Settings"
let kChangeProfilePage = "Change Profile"
let kChangePasswordPage = "Change Password"
let kForgetPasswordPage = "Forgot Password"
let kAddressPage = "Shipping Address" //地址页面
let kSelectAddressPage = "Select Shipping Address"
let kCardPage = "Payment Option"
let kSelectPaymentInfoPage = "Select Payment Operation"
let kPaymentInfoPage = "Select Payment Info"
let kLogoutPage = "Sign Out"
let kAddressMangerPage = "Shipping Address Manager"
let kPayOptionManagerPage = "Payment Option Manager"
//2.事件
let kOrderRefundEvent = "Order-ApplyRefund"
let kCheckOutSelectShippingAddressEvent = "CheckOut-SelectShippingAddress" //支付页面进入的选择地址
let kCheckOutSaveShippingAddressEvent = "CheckOut-SaveShippingAddress" //支付页面保存地址
let kAccountSaveShippingAddressEvent = "Account-SaveShippingAddress" //我的页面保存地址
let kFirstCheckOutSaveShippingAddressEvent = "CheckOut-SaveShippingAddress-PaymentInfo" //首次进入保存的地址
//shipping地址页面首次返回 左侧返回按钮
let kFirstShippingBackEvent = "CheckOut-ShippingAddress-Back"
//付款方式 首次返回
let kFirstPaymentBackEvent = "CheckOut-PaymentInfo-Back"
//checkout 页面返回
let kCheckOutBackEvent = "CheckOut-Back"

//3.
let kOrderRefundSuccess = "Order-ApplyRefund-Success" //退款成功添加这个事件

let kCheckOutHome = "CheckOutSuccess-Home"
let kCheckOutAccount = "CheckOutSuccess-MyAccount"
