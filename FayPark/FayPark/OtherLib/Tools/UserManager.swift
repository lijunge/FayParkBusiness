//
//  UserManager.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/23.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class UserManager {
    
    static let shareUserManager: UserManager = UserManager()
    //缓存用户数据到本地
    func setUser(user: User) -> Void {
        UserDefaults.standard.set(user.UserID!, forKey: "kUserID")
        UserDefaults.standard.set(user.UserName!, forKey: "kUserName")
        UserDefaults.standard.set(user.FirstName!, forKey: "kFirstName")
        UserDefaults.standard.set(user.LastName!, forKey: "kLastName")
        UserDefaults.standard.set(user.Phone!, forKey: "kPhone")
        UserDefaults.standard.set(user.Email!, forKey: "kEmail")
        UserDefaults.standard.set(user.Birthday!, forKey: "kBirthday")
        UserDefaults.standard.set(user.Gender!, forKey: "kGender")
        UserDefaults.standard.set(user.UserImg!, forKey: "kUserImg")
        UserDefaults.standard.set(user.CarProductCount!, forKey: "kCarProductCount")
        UserDefaults.standard.synchronize()
    }
    
    //用户登出
    func userLogOut() -> Void {
        UserDefaults.standard.set(nil, forKey: "kUserID")
        UserDefaults.standard.set(nil, forKey: "kUserName")
        UserDefaults.standard.set(nil, forKey: "kFirstName")
        UserDefaults.standard.set(nil, forKey: "kLastName")
        UserDefaults.standard.set(nil, forKey: "kPhone")
        UserDefaults.standard.set(nil, forKey: "kEmail")
        UserDefaults.standard.set(nil, forKey: "kBirthday")
        UserDefaults.standard.set(nil, forKey: "kGender")
        UserDefaults.standard.set(nil, forKey: "kUserImg")
        UserDefaults.standard.set(nil, forKey: "kCarProductCount")
        UserDefaults.standard.synchronize()
        //deleteImgFromFile()
    }

    func setUserImg(userImage: String) {
        UserDefaults.standard.set(userImage, forKey: "kUserImg")
        UserDefaults.standard.synchronize()
    }
    func getUserName() -> String {
        if let username = UserDefaults.standard.object(forKey: "kUserName") {
            return String.init(format: "%@", username as! CVarArg)
        }
        return ""
    }
    func getFirstName() -> String {
        if let firstName = UserDefaults.standard.object(forKey: "kFirstName") {
            return String.init(format: "%@", firstName as! CVarArg)
        }
        return ""
    }
    func getLastName() -> String {
        if let lastName = UserDefaults.standard.object(forKey: "kLastName") {
            return String.init(format: "%@", lastName as! CVarArg)
        }
        return ""
    }
    func getUserImg() -> String {
        if let tmpString = UserDefaults.standard.object(forKey: "kUserImg") {
            return String.init(format: "%@", tmpString as! CVarArg)
        }
        return ""
    }
    func getBirthday() -> String {
        if let tmpString = UserDefaults.standard.object(forKey: "kBirthday") {
            return String.init(format: "%@", tmpString as! CVarArg)
        }
        return ""
    }
    func getEmail() -> String {
        if let tmpString = UserDefaults.standard.object(forKey: "kEmail") {
            return String.init(format: "%@", tmpString as! CVarArg)
        }
        return ""
    }
    func getGender() -> String {
        if let tmpString = UserDefaults.standard.object(forKey: "kGender") {
            return String.init(format: "%@", tmpString as! CVarArg)
        }
        return ""
    }
    func getUserID() -> String {
        if let userID = UserDefaults.standard.object(forKey: "kUserID") {
            return String.init(format: "%@", userID as! CVarArg)
        }
        return ""
    }
    
    func getCartNumber() -> String {
        if let userID = UserDefaults.standard.object(forKey: "kCarProductCount") {
            return String.init(format: "%@", userID as! CVarArg)
        }
        return ""
    }
    var carProductCount: String? {
        get {
            if let userID = UserDefaults.standard.object(forKey: "kCarProductCount") {
                return String.init(format: "%@", userID as! CVarArg)
            }
            return ""
        }
        set {
            if let count = newValue {
                UserDefaults.standard.set(count, forKey: "kCarProductCount")
                UserDefaults.standard.synchronize()
            }
        }
    }
}
