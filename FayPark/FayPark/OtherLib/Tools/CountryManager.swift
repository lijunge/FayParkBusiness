//
//  CountryManager.swift
//  FayPark
//
//  Created by faypark on 2018/8/2.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
let kCashServiceString = "65.00"
let kSelectedCountry = "selectedCountry"
let kCurrentMoneyString = getCurrentPayCharacter()
let allCountryArr = ["United States","Afghanistan","Aland lslands","Albania","Algeria",
                     "American Samoa","Andorra","Angola","Anguilla","Antarctica",
                     "Antigua and Barbuda","Argentina","Armenia","Aruba","Ascension Island",
                     "Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh",
                     "Barbados","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia",
                     "Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil",
                     "British Indian Ocean Territory","Brunei","Bulgaria","Burkina Faso",
                     "Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands",
                     "Central African Republic","Chad","Chile","China","Christmas Island",
                     "Cocos(Keeling)Islands","Columbia","Comoros","Congo","Congo(DRC)",
                     "Cook Islands","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Cyprus",
                     "Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic",
                     "Ecuador","Egypt","El Salvador","Eritrea","Estonia","Ethiopia",
                     "Falkland Islands","Faroe Islands","Fiji Islands","Finland","France",
                     "Frech Polynesia","French Guiana","French Southern and Antarctic Lands",
                     "Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece",
                     "Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea",
                     "Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands",
                     "Honduras","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland",
                     "Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan",
                     "Kazakhstan","Kenya","Kiribati","Korea","Kuwait","Kyrgyzstan","Laos",
                     "Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania",
                     "Luxembourg","Macedonia,Former Yugoslav Republic of","Madagascar","Malawi",
                     "Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique",
                     "Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco",
                     "Mongolia","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru",
                     "Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand",
                     "Nicaragua","Niger","Nigeria","Niue","Norfolk Island","North Korea",
                     "Northern Mariana Islands","Norway","Oman","Pakistan","Palau",
                     "Palestinian Authority","Panama","Papua New Guinea","Paraguay","Peru",
                     "Philippines","Pitcairn Islands","Poland","Portugal","Puerto Rico",
                     "Qatar","Reunion","Romania","Russia","Rwanda","Samoa","San Marino",
                     "Sao Tome and Principe","Saudi Arabia","Senegal","Serbia,Montenegro",
                     "Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia",
                     "Solomon Islands","Somalia","South Africa",
                     "South Georgia and South Sandwich Islands","Spain","Sri Lanka","St.Helena",
                     "St.Kitts and Nevis","St.Lucia","St.Pierre and Miquelon",
                     "St.Vincent and the Grenadines","Sudan","Suriname","Svalbard and Jan Mayen",
                     "Swaziland","Sweden","Switzerland","Syria","Tajikistan","Tanzania",
                     "Thailand","Timor-Leste","Togo","Tokelau","Tonga","Trinidad and Tobago",
                     "Tristan da Cunha","Tunisia","Turkey","Turkmenistan",
                     "Turks and Caicos Islands","Tuvalu","Uganda","Ukraine",
                     "United Arab Emirates","United Kingdom",
                     "United States Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu",
                     "Vatican City","Venezuela","Vietnam","Virgin Islands",
                     "Virgin Islands,British","Wallis and Futuna","White Russia","Yemen",
                     "Zambia","Zimbabwe"]

let kAPPCountryInfoArr = [["CountryName":"United States",
                        "CountryCode": "US",
                        "CountryLanguage":"en",
                        "CountryPayment":"USD",
                        "CountryImg": "list_unitedstates_country",
                        "CountryPayCode":"USD"],
                       ["CountryName":"India",
                        "CountryCode": "IN",
                        "CountryLanguage":"hi",
                        "CountryPayment":"Rs.",
                        "CountryImg": "list_india_country",
                        "CountryPayCode":"INR"],
                       ["CountryName":"Australia",
                        "CountryCode": "AU",
                        "CountryLanguage":"en-AU",
                        "CountryPayment":"AUD",
                        "CountryImg": "list_australia_country",
                        "CountryPayCode":"AUD"],
                       ["CountryName":"New Zealand",
                        "CountryCode": "NZ",
                        "CountryLanguage":"en-NZ",
                        "CountryPayment":"NZD",
                        "CountryImg": "list_newzealand_country",
                        "CountryPayCode":"NZD"],
                       ["CountryName":"United Kingdom",
                        "CountryCode": "UK",
                        "CountryLanguage":"en-GB",
                        "CountryPayment":"GBP",
                        "CountryImg": "list_unitedkingdom_country",
                        "CountryPayCode":"GBP"],
                       ["CountryName":"China",
                        "CountryCode": "CN",
                        "CountryLanguage":"zh-Hans",
                        "CountryPayment":"CNY",
                        "CountryImg": "list_china_country",
                        "CountryPayCode":"CNY"],
                       ["CountryName":"Brazil",
                        "CountryCode": "BR",
                        "CountryLanguage":"pt-br",
                        "CountryPayment":"BRL",
                        "CountryImg": "list_brazil_country",
                        "CountryPayCode":"BRL"],
                       ["CountryName":"Columbia",
                        "CountryCode": "CO",
                        "CountryLanguage":"en-co",
                        "CountryPayment":"COP",
                        "CountryImg": "list_columbia_country",
                        "CountryPayCode":"COP"],
                       ["CountryName":"Chile",
                        "CountryCode": "CL",
                        "CountryLanguage":"en-cl",
                        "CountryPayment":"CLP",
                        "CountryImg": "list_chile_country",
                        "CountryPayCode":"CLP"],
                       ["CountryName":"Argentina",
                        "CountryCode": "AR",
                        "CountryLanguage":"en-ar",
                        "CountryPayment":"ARS",
                        "CountryImg": "list_argentina_country",
                        "CountryPayCode":"ARS"],
                       ["CountryName":"Finland",
                        "CountryCode": "FI",
                        "CountryLanguage":"fi",
                        "CountryPayment":"EUR",
                        "CountryImg": "list_finland_country",
                        "CountryPayCode":"EUR"],
                       ["CountryName":"France",
                        "CountryCode": "FR",
                        "CountryLanguage":"fr",
                        "CountryPayment":"EUR",
                        "CountryImg": "list_france_country",
                        "CountryPayCode":"EUR"],
                       ["CountryName":"Poland",
                        "CountryCode": "PL",
                        "CountryLanguage":"pl",
                        "CountryPayment":"EUR",
                        "CountryImg": "list_poland_country",
                        "CountryPayCode":"EUR"],
                       ["CountryName":"Denmark",
                        "CountryCode": "DK",
                        "CountryLanguage":"da",
                        "CountryPayment":"DKR",
                        "CountryImg": "list_denmark_country",
                        "CountryPayCode":"DKR"],
                       ["CountryName":"Germany",
                        "CountryCode": "DE",
                        "CountryLanguage":"de",
                        "CountryPayment":"EUR",
                        "CountryImg": "list_germany_country",
                        "CountryPayCode":"EUR"],
                       ["CountryName":"Russia",
                        "CountryCode": "RU",
                        "CountryLanguage":"ru",
                        "CountryPayment":"RUB",
                        "CountryImg": "list_russia_country",
                        "CountryPayCode":"RUB"],
                       ["CountryName":"Korea",
                        "CountryCode": "KR",
                        "CountryLanguage":"ko",
                        "CountryPayment":"KRW",
                        "CountryImg": "list_korea_country",
                        "CountryPayCode":"KRW"],
                       ["CountryName":"Japan",
                        "CountryCode": "JP",
                        "CountryLanguage":"ja",
                        "CountryPayment":"JPY",
                        "CountryImg": "list_japan_country",
                        "CountryPayCode":"JPY"],
                       ["CountryName":"Italy",
                        "CountryCode": "IT",
                        "CountryLanguage":"it",
                        "CountryPayment":"EUR",
                        "CountryImg": "list_italy_country",
                        "CountryPayCode":"EUR"],
                       ["CountryName":"Mexico",
                        "CountryCode": "MX",
                        "CountryLanguage":"es-mx",
                        "CountryPayment":"MXN",
                        "CountryImg": "list_mexico_country",
                        "CountryPayCode":"MXN"],
                       ["CountryName":"Thailand",
                        "CountryCode": "TH",
                        "CountryLanguage":"th",
                        "CountryPayment":"THB",
                        "CountryImg": "list_thailand_country",
                        "CountryPayCode":"THB"],
                       ["CountryName":"Malaysia",
                        "CountryCode": "MY",
                        "CountryLanguage":"malay",
                        "CountryPayment":"MYR",
                        "CountryImg": "list_malaysia_country",
                        "CountryPayCode":"MYR"],
                       ["CountryName":"Spain",
                        "CountryCode": "ES",
                        "CountryLanguage":"es",
                        "CountryPayment":"EUR",
                        "CountryImg": "list_spain_country",
                        "CountryPayCode":"EUR"]]
func getCurrentPayCharacter() -> String {
    if let countryDic = UserDefaults.standard.value(forKey: kSelectedCountry) as? [String: String]{
        if let countryString = countryDic["CountryPayment"] {
            if !countryString.isEmpty {
                return countryString
            } else {
                return "USD"
            }
        }
    }
    return "USD"
}
func getCurrentLaguage() -> String {
    if let countryDic = UserDefaults.standard.value(forKey: kSelectedCountry) as? [String: String]{
        if let countryString = countryDic["CountryLanguage"] {
            if !countryString.isEmpty {
                return countryString
            } else {
                return "en"
            }
        }
    }
    return "en"
}
func getCurrentCountryNameString() -> String {
    if let countryDic = UserDefaults.standard.value(forKey: kSelectedCountry) as? [String: String]{
        if let countryString = countryDic["CountryName"] {
            if !countryString.isEmpty {
                return countryString
            } else {
                return "United States"
            }
        }
    }
    return "United States"
}
func getCurrentCountryCodeString() -> String {
    if let countryDic = UserDefaults.standard.value(forKey: kSelectedCountry) as? [String: String]{
        if let countryString = countryDic["CountryCode"] {
            if !countryString.isEmpty {
                return countryString
            } else {
                return "US"
            }
        }
    }
    return "US"
}
func getCurrentPaymentCodeString() -> String {
    if let countryDic = UserDefaults.standard.value(forKey: kSelectedCountry) as? [String: String]{
        if let countryString = countryDic["CountryPayCode"] {
            if !countryString.isEmpty {
                return countryString
            } else {
                return "USD"
            }
        }
    }
    return "USD"
}
func getCurrentShippingDaysString() -> String {
    let currentLanguage = LanguageHelper.standardLanguager().currentLanguage
    if currentLanguage == "hi" {
        return localized("kCODDayString")
    }
    return localized("kShippingDayString")
}
