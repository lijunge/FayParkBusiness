//
//  UITextViewLanguage.swift
//  FayPark
//
//  Created by faypark on 2018/7/27.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
/**
 *  解决IB中uitextview不lcoalization的bug
 */
@IBDesignable
extension UITextView {    
    @IBInspectable
    var local:Bool {
        get{
            return true
        }
        set(newlocal) {
            self.text = localized(self.text)
        }
    }
}
