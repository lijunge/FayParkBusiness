//
//  UIImageLanguage.swift
//  FayPark
//
//  Created by faypark on 2018/7/27.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
/**
 *  解决IB中image不lcoalization的bug
 */
@IBDesignable
extension UIImageView {
    
    @IBInspectable
    var local:String {
        get {
            return ""
        }
        set(newlocal) {
            self.image = localizedImage(newlocal)
        }
    }
}

