//
//  CommentUtlis.swift
//  FayPark
//
//  Created by faypark on 2018/6/11.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import MBProgressHUD
import FBSDKCoreKit
import AppsFlyerLib
//MARK: -颜色方法
//十六进制颜色
func kRGBColorFromHex(rgbValue: Int) -> (UIColor) {
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
                   green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
                   blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,
                   alpha: 1.0)
}
//RGB
//有透明度
func KRGBA(_ r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat) -> UIColor{
    return UIColor (red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}
//没有透明度
func KRGBColor (_ r:CGFloat,g:CGFloat,b:CGFloat)-> UIColor{
    return UIColor (red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1)
}

//字体名字
let kGothamRoundedLight = "Gotham Rounded"
let kGothamRoundedBold = "Gotham-Bold"
let kHelveticaRegular = "Helvetica"
let kMSReference = "MS Reference Sans Serif"

let kScreenTopHeight = UIApplication.shared.statusBarFrame.size.height + 44.0
let kSpacingColor = kRGBColorFromHex(rgbValue: 0xf5f5f5)
let kPlaceHolderImage = creatImageWithColor(color: kSpacingColor)
//屏幕宽高、比例计算尺寸
let KScreenWidth = UIScreen.main.bounds.size.width
let KScreeenHeight = UIScreen.main.bounds.height
let kScreenRatio = (KScreenWidth / 320)

func kSizeFrom750(x:CGFloat) -> CGFloat {
    if UI_USER_INTERFACE_IDIOM() == .pad {
        return (x * (KScreenWidth / 768) * 768 / 1024)
    }
    return (x * kScreenRatio * 320 / 750)
}
func kGothamRoundedFontSize(size : CGFloat) -> UIFont {
    return UIFont.init(name: kGothamRoundedLight, size: size)!
}
func kGothamBoldFontSize(size : CGFloat) -> UIFont {
    return UIFont.init(name: kGothamRoundedBold, size: size)!
}
func kHelveticaFontSize(size : CGFloat) -> UIFont {
    return UIFont.init(name: kHelveticaRegular, size: size)!
}
func kMSReferenceFontSize(size : CGFloat) -> UIFont {
    return UIFont.init(name: kMSReference, size: size)!
}
//提示框
func showToast(_ message:String) {
    var hud = MBProgressHUD.init()
    let app = UIApplication.shared.delegate
    let window = app?.window
    hud = MBProgressHUD.showAdded(to: (window as? UIView)!, animated: true)
    hud.mode = MBProgressHUDMode.text
    hud.label.text = message
    hud.hide(animated: true, afterDelay: 1)
}

//MARK：-  String  Dictionary 互转
func convertStringToDictionary(text: String) -> [String:AnyObject]? {
    if let data = text.data(using: String.Encoding.utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: [JSONSerialization.ReadingOptions.init(rawValue: 0)]) as? [String:AnyObject]
        } catch let error as NSError {
            print(error)
        }
    }
    return nil
}

func convertDictionaryToString(dict:[String:AnyObject]) -> String {
    var result:String = ""
    do {
        //如果设置options为JSONSerialization.WritingOptions.prettyPrinted，则打印格式更好阅读
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.init(rawValue: 0))
        
        if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
            result = JSONString.replacingOccurrences(of: "\\", with: "")
        }
    } catch {
        result = ""
    }
    return result
}

func convertArrayToString(arr:[AnyObject]) -> String {
    var result:String = ""
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: arr, options: JSONSerialization.WritingOptions.init(rawValue: 0))
        if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
            result = JSONString
        }
    } catch {
        result = ""
    }
    return result

}
// image
func kImage(iconName:String) -> UIImage {
    return UIImage.init(named: iconName)!

}
// MARK: - 获取联系人姓名首字母(传入汉字字符串, 返回大写拼音首字母)
func getFirstLetterFromString(aString: String) -> (String) {
    if aString.isEmpty || aString == "" {
        return ""
    }
    // 注意,这里一定要转换成可变字符串
    let mutableString = NSMutableString.init(string: aString)
    // 将中文转换成带声调的拼音
    CFStringTransform(mutableString as CFMutableString, nil, kCFStringTransformToLatin, false)
    // 去掉声调(用此方法大大提高遍历的速度)
    let pinyinString = mutableString.folding(options: String.CompareOptions.diacriticInsensitive, locale: NSLocale.current)
    // 将拼音首字母装换成大写
    let strPinYin = polyphoneStringHandle(nameString: aString, pinyinString: pinyinString).uppercased()
    // 截取大写首字母
    let firstString = String(strPinYin.first!)
    //strPinYin.substring(to: strPinYin.index(strPinYin.startIndex, offsetBy:1))
    // 判断姓名首位是否为大写字母
    let regexA = "^[A-Z]$"
    let predA = NSPredicate.init(format: "SELF MATCHES %@", regexA)
    return predA.evaluate(with: firstString) ? firstString : "#"
}
/// 多音字处理
func polyphoneStringHandle(nameString:String, pinyinString:String) -> String {
    if nameString.hasPrefix("长") {return "chang"}
    if nameString.hasPrefix("沈") {return "shen"}
    if nameString.hasPrefix("厦") {return "xia"}
    if nameString.hasPrefix("地") {return "di"}
    if nameString.hasPrefix("重") {return "chong"}
    return pinyinString;
}
//MARK: - 标准事件监测
func configCommonEventMonitor(monitorString: String,type: Int) {
    let userID = UserManager.shareUserManager.getUserID()
    //FBS监测（标准事件监测）
    FBSDKAppEvents.logEvent(monitorString)
    //AppFlayer监测
    AppsFlyerTracker.shared().trackEvent(monitorString, withValues: nil)
    //FayPark监测
    FPHttpManager.init().fayParkMonitor(userID: userID, staicsType: String(type), name: monitorString, productID: "0")
}
//MARK: -
/**
 * 通过传入的色值生成图片
 **/
func creatImageWithColor(color: UIColor) -> UIImage{
    let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    context!.setFillColor(color.cgColor)
    context!.fill(rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image!
}
//MARK: - double数据精度丢失
func decimalNumberWithDouble(convertValue: Double) -> String {
    let doubleString = String(format: "%.2f", convertValue)
    let decNumber = NSDecimalNumber(string: doubleString)
    return decNumber.stringValue
}

//根据正则表达式改变文字颜色
//eg:cell.titleLable.attributedText = changeTextChange(regex: "(100%)", text: "(100%)SECURITY PAYMENT", color: kRGBColorFromHex(rgbValue: 0x000000), font: kGothamRoundedFontSize(size: kSizeFrom750(x: 32)))
func changeTextChange(regex: String, text: String, color: UIColor,font: UIFont) -> NSMutableAttributedString {
    let attributeString = NSMutableAttributedString(string: text)
    do {
        let regexExpression = try NSRegularExpression(pattern: regex, options: NSRegularExpression.Options())
        let result = regexExpression.matches(in: text, options: NSRegularExpression.MatchingOptions(), range: NSMakeRange(0, text.count))
        for item in result {
            attributeString.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: item.range)
            attributeString.addAttribute(NSAttributedStringKey.font, value: font, range: item.range)
        }
    } catch {
        print("Failed with error: \(error)")
    }
    return attributeString
}
