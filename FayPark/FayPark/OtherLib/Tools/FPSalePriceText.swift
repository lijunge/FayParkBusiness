//
//  FPSalePriceText.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/24.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class FPSalePriceText {
    //价格与原价样式
     static func fayparkPriceAttribute(price: String, oldPrice: String) -> NSAttributedString {
        let mutableAttributeString = NSMutableAttributedString.init()
        
        let str1 = NSMutableAttributedString.init(string: price)
        str1.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.darkGray], range: NSRange(location: 0, length: str1.length))
        
        let str2 = NSMutableAttributedString.init(string: oldPrice)
        str2.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.lightGray, NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray, NSAttributedStringKey.font : UIFont.systemFont(ofSize: kSizeFrom750(x: 28))], range: NSRange(location: 0, length: str2.length))
        
        mutableAttributeString.append(str1)
        mutableAttributeString.append(str2)
        return mutableAttributeString
    }
    //增加参数颜色和字体大小
    static func fayparkPriceAttribute(price: String, oldPrice: String, priceColor: UIColor, oldPriceColor: UIColor) -> NSAttributedString {
        let mutableAttributeString = NSMutableAttributedString.init()
        
        let str1 = NSMutableAttributedString.init(string: price)
        str1.addAttributes([NSAttributedStringKey.foregroundColor : priceColor], range: NSRange(location: 0, length: str1.length))
        
        let str2 = NSMutableAttributedString.init(string: oldPrice)
        str2.addAttributes([NSAttributedStringKey.foregroundColor : oldPriceColor, NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray, NSAttributedStringKey.font : UIFont.systemFont(ofSize: kSizeFrom750(x: 28))], range: NSRange(location: 0, length: str2.length))
        
        mutableAttributeString.append(str1)
        mutableAttributeString.append(str2)
        return mutableAttributeString
    }
    
    //详情页购物车或分享弹窗字体
    static func fayparkAddCartAttribute(firstText: String, secondText: String, firstColor: UIColor, secondColor: UIColor) -> NSAttributedString {
        let mutableAttributeString = NSMutableAttributedString.init()
        
        let str1 = NSMutableAttributedString.init(string: firstText)
        str1.addAttributes([NSAttributedStringKey.foregroundColor : firstColor], range: NSRange(location: 0, length: str1.length))
        
        let str2 = NSMutableAttributedString.init(string: secondText)
        str2.addAttributes([NSAttributedStringKey.foregroundColor : secondColor], range: NSRange(location: 0, length: str2.length))
        
        mutableAttributeString.append(str1)
        mutableAttributeString.append(str2)
        return mutableAttributeString
    }
    
    static func fayparkFontColorAttribute(price: String, oldPrice: String, priceColor: UIColor, oldPriceColor: UIColor, priceFont: CGFloat, oldPriceFont: CGFloat) -> NSAttributedString {
        let mutableAttributeString = NSMutableAttributedString.init()
        
        let str1 = NSMutableAttributedString.init(string: price)
        str1.addAttributes([NSAttributedStringKey.foregroundColor : priceColor,NSAttributedStringKey.font : UIFont(name: "Avenir-Roman", size: priceFont) ?? UIFont.systemFont(ofSize: priceFont)], range: NSRange(location: 0, length: str1.length))
        
        let str2 = NSMutableAttributedString.init(string: oldPrice)
        str2.addAttributes([NSAttributedStringKey.foregroundColor : oldPriceColor, NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray, NSAttributedStringKey.font : UIFont(name: "Avenir-Light", size: oldPriceFont) ?? UIFont.systemFont(ofSize: oldPriceFont)], range: NSRange(location: 0, length: str2.length))
        
        mutableAttributeString.append(str1)
        mutableAttributeString.append(str2)
        return mutableAttributeString
    }
}
