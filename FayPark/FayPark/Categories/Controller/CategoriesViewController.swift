//
//  CategoriesViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/26.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import AppsFlyerLib
import SwiftyJSON
import MBProgressHUD

class CategoriesViewController: UIViewController {
    
    var liftTitleArray = [kCGAllString,kCGNeckString,kCGBraceletesString,
                          kCGEarringsString,kCGHairString,kCGWatchString,
                          kCGAnkletString,kCGOtherString]
    
    var oneCateTitle = Array<JSON>.init()
    var twoCateTitle = Array<Any>.init()
    var newArray = Array<Any>.init()
    
    var liftView:CategoriesLiftView!
    var rightView:CategoriesRightView!
    
    var liftIndx:Int = 0
    var rightIndx:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = kCGTitleString
        self.obtainLaodDeta()
        configEventMonitor()
        setupSubViews()
    }
    func setupSubViews() {
        self.liftView = CategoriesLiftView.init(frame: CGRect.init(x: 0, y: 0, width: kSizeFrom750(x: 250), height: kScreenHeight), style: .plain)
        self.liftView.select = { (indx) in
            self.rightView.liftTitleArray = self.twoCateTitle[indx] as! [JSON]
            self.liftIndx = indx
            self.rightView.reloadData()
        }
        
        self.rightView = CategoriesRightView.init(frame: CGRect.init(x: kSizeFrom750(x: 250), y: 0, width: KScreenWidth-kSizeFrom750(x: 250), height: kScreenHeight), style: .grouped)
        self.view.addSubview(self.liftView)
        self.view.addSubview(self.rightView)
        self.liftView.backgroundColor = kRGBColorFromHex(rgbValue: 0xfcfcfc)
        self.rightView.backgroundColor = UIColor.white
        
        self.rightView.selectBlock = { (indx) in
            let cate = NewCateDetailViewController()
            cate.cateType = .CateDetailProduct
            cate.cateHederTitle = self.twoCateTitle[self.liftIndx] as! [JSON]
            cate.tuochNumber = indx
            cate.OneTypeID = self.oneCateTitle[self.liftIndx]["OneTypeId"].intValue
            let firstMenuString = self.oneCateTitle[self.liftIndx]["OneTypeName"].stringValue
            let tmpArr = self.twoCateTitle[self.liftIndx] as! [JSON]
            let secondMenuString = tmpArr[indx]["SecondTypeName"].stringValue
            cate.firstMenuSelectString = firstMenuString
            cate.secondMenuSelectString = secondMenuString
            cate.title = firstMenuString
            var NewArr = Array<JSON>.init()
            NewArr = self.twoCateTitle[self.liftIndx] as! [JSON]
            //取出2级ID
            for dic in 0..<NewArr.count {
                if dic == indx {
                    cate.SecondTypeID = NewArr[dic]["SecondTypeId"].intValue
                }
            }
            self.navigationController?.pushViewController(cate, animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:-------获取一级 二级 列表
    func obtainLaodDeta() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let params = ["CountrySName":countryCode,
                      "CountryCurrency":countryPay]
        FPHttpManager().manager.post(FayParkGetTypeAggregateListFace, parameters: params, progress: nil, success: { (task,data) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(data as! Data)
            let proArray = json["Data"]["TypeAggrepateList"].array
            self.oneCateTitle = proArray!
            if json["Code"].intValue == 200 {
                for dic in proArray! {
                    let arr = dic["SecondTypeList"].array
                    self.newArray.append(arr as Any)
                }
                self.twoCateTitle = self.newArray
                self.liftView.liftTitleArray = self.oneCateTitle
                self.liftView.reloadData()
                self.liftView.setSelectedFirstCell()
                
                self.rightView.liftTitleArray = self.twoCateTitle[0] as! [JSON]
                self.rightView.reloadData()

            }else{
                showToast(json["Msg"].string!)
            }
            
        }) { (task, error) in
            
        }
    }
}

extension CategoriesViewController {
    func setupNavigationBar() {
        //设置navigationbar左item
        let leftBtn = UIButton(type: .custom)
        leftBtn.setImage(UIImage.init(named: "list_icon"), for: .normal)
        leftBtn.addTarget(self, action: #selector(leftItemClick), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBtn)
    }
    
    @objc func leftItemClick() {

    }
    
    func configEventMonitor() {
        configCommonEventMonitor(monitorString: kCategoryPage, type: 1)
    }
}
