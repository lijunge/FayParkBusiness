//
//  OneTypeNameModel.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/21.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
class OneTypeNameModel {
    
    var OneTypeName:String//一级分类名称
    var OneTypeID:Int//一级分类ID
    
    init(jsonDate:JSON) {
        
        self.OneTypeName = jsonDate["OneTypeName"].stringValue
        self.OneTypeID = jsonDate["OneTypeID"].intValue
    }

}

class SecondTypeName {
    var SecondTypeName:String
    var SecondTypeID:Int
    
    init(jsonDate:JSON) {
        self.SecondTypeID = jsonDate["SecondTypeID"].intValue
        self.SecondTypeName = jsonDate["SecondTypeName"].stringValue
    }
}
