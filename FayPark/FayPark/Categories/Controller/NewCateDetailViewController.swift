//
//  NewCateDetailViewController.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/21.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SDCycleScrollView
import JTSImageViewController
import AFNetworking
import SwiftyJSON
import SDWebImage
import MBProgressHUD
import MJRefresh
import FBSDKCoreKit
import AppsFlyerLib


enum CateDetailProductType {
    case CateDetailHomeSaleProduct
    case CateDetailProduct
}

class NewCateDetailViewController: BaseViewController {

    var collectionView : UICollectionView!
    var cateType: CateDetailProductType!
    //商品列表
    var productList = Array<BrowseActivityInfo>()
    var groupEnter: DispatchGroup = DispatchGroup.init()
    var cateHederTitle = Array<JSON>.init()
    var tuochNumber:Int = 0
    var circleLabel:UILabel!
    var OneTypeID:Int!
    var SecondTypeID:Int = 0
    var pageIndex:Int = 1
    var firstMenuSelectString: String?
    var secondMenuSelectString: String?
    var noView:NoContentView!
    var selectColumn:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.setNavigationBarItem()
        switch cateType {
        case .CateDetailHomeSaleProduct:
            self.setAddSecondDate()
            break
        case .CateDetailProduct:
            self.setCateUI()
            break
        default:
            print("default")
        }
    }
    
    //MARK: - 设置导航栏右上角
    func setNavigationBarItem() {
        let rightView = UIView()
        rightView.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
        
        let rigghtButton = UIButton(type: .custom)
        rigghtButton.frame = CGRect(x: 30, y: 10, width: 30, height: 30)
        rigghtButton.setImage(UIImage.init(named: "cart_icon"), for: .normal)
        rigghtButton.addTarget(self, action: #selector(rightCartClick), for: .touchUpInside)
        rightView.addSubview(rigghtButton)
        
        circleLabel = UILabel(frame: CGRect(x: 60-16, y: 10, width: 16, height: 16))
        circleLabel.font = UIFont.systemFont(ofSize: 10)
        circleLabel.textAlignment = .center
        circleLabel.textColor = UIColor.white
        circleLabel.layer.cornerRadius = 8
        circleLabel.backgroundColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        
        circleLabel.layer.masksToBounds = true
        rightView.addSubview(circleLabel)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightView)
    }
    @objc func rightCartClick(){
        
        let email =  UserDefaults.standard.object(forKey: "kUserName") as! String
        if email.count == 0{
            self.present(NewLoginViewController(), animated: true, completion: nil)
            return
        }
        //跳转到购物车
        let tabbarVC = parent?.parent as! UITabBarController
        tabbarVC.selectedIndex = 2
        self.navigationController?.popViewController(animated: false)
    }
    func setCateUI() {
        
        let columnView = SelectedColumnView.init(frame:CGRect.init(x: 0, y: 0, width: KScreenWidth, height: kSizeFrom750(x: 100)) ,title:self.cateHederTitle,touchNumber:self.tuochNumber)
        columnView.selectBlock = {(indx,titleString) in
            
            if let newNoView = self.noView {
                newNoView.removeFromSuperview()
            }
            self.productList = Array<BrowseActivityInfo>()
            self.SecondTypeID = indx as! Int
            self.secondMenuSelectString = titleString
            self.selectColumn = true
            self.addLodaDate(indx: 1)
            if self.firstMenuSelectString != nil && self.secondMenuSelectString != nil {
                let menuString = "\(self.firstMenuSelectString!)-\(self.secondMenuSelectString!)"
                self.configEventMonitor(menuString: menuString)
            }
        }
        self.view.addSubview(columnView)

        self.addLoadView()
        self.addLodaDate(indx: self.pageIndex)
        self.setFootRefreshControl()
        self.setHederRefreshControl()
        if firstMenuSelectString != nil && secondMenuSelectString != nil {
            let menuString = "\(firstMenuSelectString!)-\(secondMenuSelectString!)"
            configEventMonitor(menuString: menuString)
        }
        self.updateCartButtonNum()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    // MARK: - 更新购物车按钮数目
    func updateCartButtonNum() {
        //显示购物车数量
        let tabbarVC = UIApplication.shared.keyWindow?.rootViewController
        if let carCount = UserManager.shareUserManager.carProductCount {
            if let number = Int(carCount) {
                if number != 0 {
                    circleLabel.text = String(number+1)
                    tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "\(number+1)"
                } else {
                    circleLabel.text = "1"
                    tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "1"
                }
            }
        }
    }
    //MARK:___________下拉刷新______________
    func setHederRefreshControl() {
        self.collectionView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.productList = Array<BrowseActivityInfo>()
            self.addLodaDate(indx: 1)
            self.collectionView.mj_header.endRefreshing()
        })
    }
    //MARK:___________上拉加载______________
    func setFootRefreshControl() {
        self.collectionView.mj_footer = MJRefreshAutoFooter.init(refreshingBlock:{
            self.pageIndex += 1
            self.addLodaDate(indx: self.pageIndex)
            self.collectionView.mj_footer.endRefreshing()
        })
    }
    //MARK:获取二级列表---------
    func setAddSecondDate() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        print(self.OneTypeID)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let params = ["OneTypeID":self.OneTypeID,
                      "CountrySName":countryCode,
                      "CountryCurrency":countryPay] as [String : Any]
        FPHttpManager().manager.post(FayParkGetNavigationTypeList, parameters: params, progress: nil, success: { (task, data) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let json = JSON(data as! Data)
            if json["Code"].stringValue == "200"{
                self.cateHederTitle = json["Data"]["NavigationList"].array!
                self.setCateUI()
            }else{
                showToast(json["Msg"].stringValue)
            }
        }) { (tsak, error) in
            
        }
    }
    
    func addLodaDate(indx:Int){
        self.groupEnter.enter()
        let userID = UserManager.shareUserManager.getUserID()
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "Index":indx,
                     "Count":10,
                     "OneTypeID":self.OneTypeID,
                     "SecondTypeID":self.SecondTypeID,
                     "Gender":0,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay] as [String : Any]
        FPHttpManager().manager.post(FayParkGetConditionList, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("addLodaDateaddLodaDate\(json)")
            if json["Code"].stringValue == "200"{
                let proArray = json["Data"]["ProductConditiontList"]
                for i in 0..<proArray.count {
                    let x = arc4random() % 10
                    var tempProduct = BrowseActivityInfo(jsonData: proArray[i])
                    tempProduct.randomNumber = Int(x)
                    self.productList.append(tempProduct)
                }
                
                self.groupEnter.leave()
                self.collectionView.reloadData()
                if self.selectColumn == true  && proArray.count != 0 {
                    self.collectionView.scrollToItem(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
                    self.selectColumn = !self.selectColumn
                }
                if indx == 1 && proArray.count == 0 {
                    self.noView = NoContentView.init(frame: self.collectionView.frame, type: 3)
                    self.view.addSubview(self.noView)
                }
            }else{
                showToast(json["Msg"].stringValue)
            }
        }) { (task, error) in
            self.groupEnter.leave()
        }
    }
    
    func addLoadView(){
        
        let layout = ZJFlexibleLayout(delegate: self)
        let collectionFrame = CGRect(x: 0, y: kSizeFrom750(x: 100), width: KScreenWidth, height: kScreenHeight)
        collectionView = UICollectionView(frame: collectionFrame, collectionViewLayout: layout)
        collectionView.showsVerticalScrollIndicator = false
        view.addSubview(collectionView)
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "FayParkCollectionCell", bundle: nil), forCellWithReuseIdentifier: "fayparkcell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    lazy var cateView:UIImageView = {
        let tempcateView = UIImageView.init(frame:CGRect.init(x: KScreenWidth-kSizeFrom750(x: 100), y: 0, width: kSizeFrom750(x: 100), height: kSizeFrom750(x: 100)))
        tempcateView.backgroundColor = UIColor.white
        return tempcateView
    }()
    
    lazy var cateViewLine: UIView = {
        let tempcateViewLine = UIView.init(frame:CGRect.init(x: 0, y: kSizeFrom750(x: 20), width: kSizeFrom750(x: 2), height: kSizeFrom750(x: 60)))
        tempcateViewLine.backgroundColor = kSpacingColor
        return tempcateViewLine
    }()
}
//MARK: - CollectionView 数据源
extension NewCateDetailViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fayparkcell", for: indexPath) as! FayParkCollectionCell
        if self.productList.count != 0 {
            let pro = productList[indexPath.item]
            cell.imgUrl = URL.init(string: pro.MainImgLink)
            cell.configBrowseCollectionCell(productInfo: pro)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productVC = NewProductDetailViewController()
        productVC.requestProID = productList[indexPath.item].ProductID
        navigationController?.pushViewController(productVC, animated: true)
    }
}

//MARK: - 瀑布流代理
extension NewCateDetailViewController: ZJFlexibleDataSource {
    
    func sizeOfItemAtIndexPath(at indexPath: IndexPath) -> CGSize {
        let pro = productList[indexPath.item]
        let itemWidth = (UIScreen.main.bounds.width - 24) / 2
        //let itemHeight = (imageSize!.height/imageSize!.width * itemWidth)
        let proString = pro.Proportion
        var proFloat: CGFloat = 0
        if let doubleValue = Double(proString){
            proFloat = CGFloat(doubleValue)
        }
        let itemHeight = itemWidth / proFloat
        return CGSize(width: itemWidth, height: itemHeight + 80 + 62)
    }
    
    func heightOfAdditionalContent(at indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func numberOfCols(at section: Int) -> Int {
        return 2
    }
    
    func spaceOfCells(at section: Int) -> CGFloat{
        return 13
    }
    
    func sectionInsets(at section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 13, left: 13, bottom: 80, right: 13)
    }
}
extension NewCateDetailViewController {
    func configEventMonitor(menuString: String) {
        configCommonEventMonitor(monitorString: menuString, type: 1)
    }
}

