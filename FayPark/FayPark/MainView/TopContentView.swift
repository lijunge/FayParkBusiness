//
//  TopView.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/27.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit

protocol TopContentViewDelegate {
    func hideTopContentView()
}


class TopContentView: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var cateBtn: FPRightButton!
    
    @IBOutlet weak var genderBtn: UIButton!
    
    @IBOutlet weak var salesBtn: UIButton!
    
    @IBOutlet var cateArray: [UIButton]!
    @IBOutlet var genderArray: [UIButton]!
    @IBOutlet var saleArray: [UIButton]!
    
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    
    var delegate : TopContentViewDelegate?
    
    //三个不同的分类
    var cateTitles : [String] = ["All","Earrings","Necklaces","Rings","Bracelets","Hair Accessories","Others"]
    var genderTitles : [String] = ["All","Male","Female"]
    var salesTitles : [String] = ["All","Hot","On Sale"]
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        view = Bundle.main.loadNibNamed("TopContentView", owner: self, options: nil)?.first as! UIView;
        view.frame = self.bounds;
        addSubview(view);
        
        //增加边框
        cateBtn.layer.borderColor = UIColor.black.cgColor
        cateBtn.tag = 10
        cateBtn.layer.borderWidth = 1.0
        cateBtn.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        cateBtn.setImage(UIImage.init(named: "arrow_up"), for: .normal)
        
        genderBtn.layer.borderColor = UIColor.black.cgColor
        genderBtn.tag = 11
        genderBtn.layer.borderWidth = 1.0
        genderBtn.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        genderBtn.setImage(UIImage.init(named: "arrow_up"), for: .normal)
        
        salesBtn.layer.borderColor = UIColor.black.cgColor
        salesBtn.layer.borderWidth = 1.0
        salesBtn.tag = 12
        salesBtn.addTarget(self, action: #selector(clickBtn(sender:)), for: .touchUpInside)
        salesBtn.setImage(UIImage.init(named: "arrow_up"), for: .normal)
        //点击弹出pickerView
        
        cateArray.first?.isSelected = true
        genderArray.first?.isSelected = true
        saleArray.first?.isSelected = true
        
        for item in cateArray {
            item.addTarget(self, action: #selector(cateArrayClick(sender:)), for: .touchUpInside)
            if item.isSelected == true {
                item.setTitleColor(.white, for: .normal)
                item.backgroundColor = UIColor(red: 155/255, green: 188/255, blue: 101/255, alpha: 1.0)
            }
        }
        
        for item in genderArray {
            item.addTarget(self, action: #selector(genderArrayClick(sender:)), for: .touchUpInside)
            if item.isSelected == true {
                item.setTitleColor(.white, for: .normal)
                item.backgroundColor = UIColor(red: 155/255, green: 188/255, blue: 101/255, alpha: 1.0)
            }
        }
        
        for item in saleArray {
            item.addTarget(self, action: #selector(saleArrayClick(sender:)), for: .touchUpInside)
            if item.isSelected == true {
                item.setTitleColor(.white, for: .normal)
                item.backgroundColor = UIColor(red: 155/255, green: 188/255, blue: 101/255, alpha: 1.0)
            }
        }

    }
    
    @objc func cateArrayClick(sender: UIButton) {
        sender.isSelected = !sender.isSelected

        changeButtonState(currentBtn: sender, buttonArray: cateArray)
    }
    
    @objc func genderArrayClick(sender: UIButton) {
        sender.isSelected = !sender.isSelected

        changeButtonState(currentBtn: sender, buttonArray: genderArray)
    }
    
    @objc func saleArrayClick(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        changeButtonState(currentBtn: sender, buttonArray: saleArray)
    }
    
    func changeButtonState(currentBtn: UIButton, buttonArray: [UIButton]) {
        for item in buttonArray {
            if item != currentBtn {
                item.isSelected = false
                item.setTitleColor(.darkGray, for: .normal)
                item.backgroundColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
            } else {
                item.setTitleColor(.white, for: .normal)
                item.backgroundColor = UIColor(red: 155/255, green: 188/255, blue: 101/255, alpha: 1.0)
            }
        }
    }
    
    
    @IBAction func resetBtnClick(_ sender: UIButton) {
        cateArrayClick(sender: cateArray.first!)
        genderArrayClick(sender: genderArray.first!)
        saleArrayClick(sender: saleArray.first!)
    }
    
    @IBAction func searchBtnClick(_ sender: UIButton) {
        delegate?.hideTopContentView()
        var index = (0,0,0,0)
        for (btnIndex,item) in cateArray.enumerated() {
            if item.isSelected == true {
                //print(btnIndex)

                switch btnIndex {
                case 0:
                    index = (0,1,0,0)
                case 1:
                    index = (4,1,0,0)
                case 2:
                    index = (6,3,0,0)
                case 3:
                    index = (3,5,0,0)
                case 4:
                    index = (5,2,0,0)
                case 5:
                    index = (7,4,0,0)
                case 6:
                    index = (8,6,0,0)
                case 7:
                    //print("watches")
                    index = (9,0,0,0)
                case 8:
                    //print("anklets")
                    index = (10,0,0,0)
                default:
                    break
                }
                
                //通知首页更新
                NotificationCenter.default.post(name: Notification.Name(rawValue: "changeCate"), object: nil, userInfo: ["index":index])
            }
        }
    }
    
    @objc func clickBtn(sender:UIButton) {
        //根据点击不同的btn传递不同的titles
        switch sender.tag {
        case 10:
            UsefulPickerView.showSingleColPicker("Categories", data: cateTitles, defaultSelectedIndex: 0) {[unowned self] (selectedIndex, selectedValue) in
                self.cateBtn.setTitle(selectedValue, for: .normal)
            }
            //delegate?.showPickerView(titles: cateTitles,topBtn: sender)
        case 11:
            UsefulPickerView.showSingleColPicker("Gender", data: genderTitles, defaultSelectedIndex: 0) {[unowned self] (selectedIndex, selectedValue) in
                self.genderBtn.setTitle(selectedValue, for: .normal)
            }
            //delegate?.showPickerView(titles: genderTitles,topBtn: sender)
        case 12:
            UsefulPickerView.showSingleColPicker("Sales", data: salesTitles, defaultSelectedIndex: 0) {[unowned self] (selectedIndex, selectedValue) in
                self.salesBtn.setTitle(selectedValue, for: .normal)
            }
            //delegate?.showPickerView(titles: salesTitles,topBtn: sender)
        default:
            return
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    @IBAction func clickSearch(_ sender: UIButton) {
        delegate?.hideTopContentView()
        
        var index = (0,0,0,0)
        
        //"All","Earrings","Necklaces","Rings","Bracelets","Hair Accessories","Others"
        
        switch cateBtn.currentTitle! {
        case "All":
            index = (0,1,0,0)
        case "Earrings":
            index = (4,1,0,0)
        case "Necklaces":
            index = (5,2,0,0)
        case "Rings":
            index = (6,3,0,0)
        case "Bracelets":
            index = (7,4,0,0)
        case "Hair Accessories":
            index = (3,5,0,0)
        case "Others":
            index = (8,6,0,0)
        default:
            break
        }
        
        //通知首页更新
        NotificationCenter.default.post(name: Notification.Name(rawValue: "changeCate"), object: nil, userInfo: ["index":index])
    }
    
}
