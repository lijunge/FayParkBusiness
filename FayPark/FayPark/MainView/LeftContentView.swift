//
//  LeftContentView.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/27.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit

class LeftContentView: UIView {
    @IBOutlet var view: UIView!
   // @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var iconArray: [[String]]!
    var titlesArray: [[String]]!
    var userNameLabel:UILabel!
    var headBtn:UIButton!
    var loginInfoLable:UILabel!
    var loginBtn:UIButton!
    var rightDetailImgView:UIImageView!
    //闭包
    var hideOrJump : ((IndexPath) -> Void)?

    //MARK:初始化方法
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        view = Bundle.main.loadNibNamed("LeftContentView", owner: self, options: nil)?.first as! UIView;
        view.frame = self.bounds;
        addSubview(view);
        
        let sectionTitle1 = ["Orders","Shipping Info","Refund Policy","Wishlist"]
        let sectionTitle2 = ["About Us","Rate App","Contact Us","Settings"]

        let sectionIcon1 = ["account_orders_icon","help_Shipping_icon","help_return_icon","account_wishlist_icon"]
        let sectionIcon2 = ["account_about_icon","menu_rateus_icon","menu_contact_icon","account_settings_icon"]

        titlesArray = [sectionTitle1,sectionTitle2]
        iconArray = [sectionIcon1,sectionIcon2]
        //检测字体是否已经添加
        /*
         for name in UIFont.familyNames {
         print(name)
         }
         */
        //print(UIFont.fontNames(forFamilyName: "Avenir"))

        //加载button
       // let fontName = UIFont(name: "Avenir-Light", size: 20.0)
//        //关闭
//        closeBtn.setImage(UIImage.init(named: "close"), for: .normal)
//        closeBtn.titleLabel?.font = fontName
//        closeBtn.tag = 1000
//        closeBtn.addTarget(self, action: #selector(clickButton(btn:)), for: .touchUpInside)
    
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib.init(nibName: "LeftContentCell", bundle: nil), forCellReuseIdentifier: "reuse_left_cell");
        tableView.isScrollEnabled = false//设置table不滑动
        setupSubViews()
        updateHeadViewInfo()
    }
    //MARK: - UI
    func setupSubViews() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: kSizeFrom750(x: 260)))
        headerView.backgroundColor = UIColor.white
        
        headBtn = UIButton.init(frame: CGRect(x: kSizeFrom750(x: 30), y: kSizeFrom750(x: 100), width: kSizeFrom750(x: 120), height: kSizeFrom750(x: 120)))
        headBtn.backgroundColor = kRGBColorFromHex(rgbValue: 0xcdcdcd)
        headBtn.layer.cornerRadius = kSizeFrom750(x: 60)
        headBtn.layer.masksToBounds = true
        headerView.addSubview(headBtn)
        headBtn.titleLabel?.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 60))
        
        self.loginInfoLable = UILabel.init(frame: CGRect.init(x: 0, y: kSizeFrom750(x: 105), width: headerView.frame.size.width, height: kSizeFrom750(x: 45)))
        self.loginInfoLable.setContentHuggingPriority(.required, for: .horizontal)
        self.loginInfoLable.text = "Welcome to Faypark"
        self.loginInfoLable.textAlignment = .center
        self.loginInfoLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        self.loginInfoLable.font = kHelveticaFontSize(size: kSizeFrom750(x: 40))
        headerView.addSubview(self.loginInfoLable)
        
        
        self.loginBtn = UIButton.init(frame: CGRect.init(x:(headerView.mj_w-kSizeFrom750(x: 320))/2, y: self.loginInfoLable.frame.maxY + kSizeFrom750(x: 25), width: kSizeFrom750(x: 320), height: kSizeFrom750(x: 60)))
        self.loginBtn.layer.borderWidth = kSizeFrom750(x: 1)
        self.loginBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        self.loginBtn.setTitle("Sign Up/Log In", for: .normal)
        self.loginBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
        self.loginBtn.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 26))
        headerView.addSubview(self.loginBtn)
        
        userNameLabel = UILabel(frame: CGRect(x: headBtn.frame.maxX + kSizeFrom750(x: 20), y: headBtn.center.y-kSizeFrom750(x: 30), width: kSizeFrom750(x: 370), height: kSizeFrom750(x: 60)))
        userNameLabel.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 35))
        userNameLabel.textAlignment = .left
        userNameLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        headerView.addSubview(userNameLabel)
        
        let joinTimeLabel = UILabel(frame: CGRect(x: headBtn.frame.maxX + kSizeFrom750(x: 20), y: userNameLabel.frame.maxY, width: kSizeFrom750(x: 370), height: kSizeFrom750(x: 60)))
        joinTimeLabel.textAlignment = .left
        joinTimeLabel.text = "Joined May,2018"
        joinTimeLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        joinTimeLabel.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
/***暂时不显示***/        //headerView.addSubview(joinTimeLabel)
        
        rightDetailImgView = UIImageView(frame: CGRect(x: headerView.frame.width - kSizeFrom750(x: 58), y: headBtn.center.y-kSizeFrom750(x: 15), width: kSizeFrom750(x: 30), height: kSizeFrom750(x: 30)))
        rightDetailImgView.image = UIImage.init(named: "arrowrighticon")
        rightDetailImgView.contentMode = .scaleToFill
        headerView.addSubview(rightDetailImgView)
        tableView.tableHeaderView = headerView
        
//        let tap = UITapGestureRecognizer.init(target: self, action: #selector(topImage))
//        headerView.addGestureRecognizer(tap)
    }
    //MARK: - Private Method
//    @objc func clickButton(btn:Any) -> Void {
//        let button = btn as! UIButton
//        //把tag放入闭包
//        //执行闭包
//        hideOrJump!(button.tag)
//    }
//
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    func updateHeadViewInfo() {
        if UserManager.shareUserManager.getUserImg().isEmpty {
            //本地没有
            let fileManager = FileManager.default
            let rootPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                               .userDomainMask, true)[0] as String
            let filePath = "\(rootPath)/pickedimage.png"

            if fileManager.fileExists(atPath: filePath) {
                let data = fileManager.contents(atPath: filePath)
                if let tmpData = data {
                    let image = UIImage(data: tmpData)
                    headBtn.setImage(image, for: .normal)
                } else {
                    let userName = UserManager.shareUserManager.getFirstName()
                    let string = getFirstLetterFromString(aString: userName)
                    setHeadImgFromFistNameCharacter(character: string)
                }
            } else {
                let userName = UserManager.shareUserManager.getFirstName()
                let string = getFirstLetterFromString(aString: userName)
                setHeadImgFromFistNameCharacter(character: string)
            }
        } else {
            //本地存储的有后台连接
            let string = UserManager.shareUserManager.getUserImg()
            headBtn.sd_setImage(with: URL(string: string), for: .normal, placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
            setHeadImgWithImage()
        }
    }
    func setHeadImgFromFistNameCharacter(character: String) {
        headBtn.layer.borderWidth = 0.5
        headBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0xababab).cgColor
        headBtn.backgroundColor = kRGBColorFromHex(rgbValue: 0xcdcdcd)
        var titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
        if character == "A" || character == "B" || character == "C" || character == "D" || character == "E" {
            titleColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        } else if character == "F" || character == "G" || character == "H" || character == "I" || character == "J" {
            titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
        } else if character == "K" || character == "L" || character == "M" || character == "N" || character == "O" {
            titleColor = kRGBColorFromHex(rgbValue: 0x00acf0)
        } else if character == "P" || character == "Q" || character == "R" || character == "S" || character == "T" {
            titleColor = kRGBColorFromHex(rgbValue: 0xc45cac)
        } else {
            titleColor = kRGBColorFromHex(rgbValue: 0xf97f46)
        }
        headBtn.setTitleColor(titleColor, for: .normal)
        headBtn.setTitle(character, for: .normal)
        headBtn.titleLabel?.font = UIFont(name: kHelveticaRegular, size: 25)
    }
    func setHeadImgWithImage() {
        headBtn.layer.borderWidth = 0.0
        headBtn.backgroundColor = UIColor.white
        headBtn.setTitleColor(UIColor.white, for: .normal)
    }
}

//MARK: -tableView代理
extension LeftContentView: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView:  UITableView) -> Int {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arr = titlesArray[section]
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuse_left_cell") as! LeftContentCell
        let sectionTitleArr = titlesArray[indexPath.section]
        let sectionIconArr = iconArray[indexPath.section]
        cell.iconView.image = UIImage.init(named: sectionIconArr[indexPath.row])
        cell.titleNameLabel.text = sectionTitleArr[indexPath.row]
        cell.titleNameLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        cell.titleNameLabel.textColor = kRGBColorFromHex(rgbValue: 0xffffff)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kSizeFrom750(x: 110)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //跳转选择器
        hideOrJump!(indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: kSizeFrom750(x: 12)))
        headerView.backgroundColor = kRGBColorFromHex(rgbValue: 0xf5f5f5)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
}
