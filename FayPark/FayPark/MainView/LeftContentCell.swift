//
//  LeftContentCell.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/29.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit
import Masonry

class LeftContentCell: UITableViewCell {
    @IBOutlet var iconView: UIImageView!

    @IBOutlet var titleNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        let bg = UIView(frame: bounds)
        bg.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        selectedBackgroundView = bg
        
        self.titleNameLabel.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        self.titleNameLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.iconView.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self.mas_centerY)
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 40))
            mark?.width.height().equalTo()(kSizeFrom750(x: 45))
        }
        self.titleNameLabel.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self.mas_centerY)
            mark?.left.equalTo()(self.iconView.mas_right)?.setOffset(kSizeFrom750(x: 30))
        }
        self.titleNameLabel.setContentHuggingPriority(.required, for: .horizontal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        if  highlighted {
            titleNameLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
            let tempImage = iconView.image?.withRenderingMode(.alwaysTemplate)
            iconView.tintColor = UIColor(red: 155/255, green: 188/255, blue: 101/255, alpha: 1.0)
            iconView.image = tempImage
        } else {
            titleNameLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
            let tempImage = iconView.image?.withRenderingMode(.alwaysTemplate)//alwaysOriginal
            iconView.tintColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0)
            iconView.image = tempImage
        }
    }
}
