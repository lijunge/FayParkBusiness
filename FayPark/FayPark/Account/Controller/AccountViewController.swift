//
//  AccountViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/26.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit
import AFNetworking
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import AppsFlyerLib
import MessageUI

class AccountViewController: UIViewController{
    
    @IBOutlet weak var accountTableView: UITableView!
    
    var headAccountView: AccountHeadView!
   
    var accountTitlesArr = [[kAccountOrdersString,
                             kAccountListShippingInfoString,
                             kAccountListRefundPolicyString,
                             kAccountListWishlistString,
                             kAccountListAboutUsString],
                            [kAccountListShippingAddressString,
                             kAccountListPaymentOptionString,
                             kAccountListSettingsString,
                             kAccountListSupportString]]
    var accountImgArr = [["account_orders_icon","help_Shipping_icon","help_return_icon","account_wishlist_icon","account_about_icon"],["icon_shipping_account","account_walletpayment_icon","account_settings_icon","account_help_icon"]]
    
    var centerTipNum: CenterTipCount? = nil
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateHeadViewInfo()
        //请求tipcount
        requestAccountTipCount()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Account"
        self.automaticallyAdjustsScrollViewInsets = false
        view.backgroundColor = kSpacingColor
        configCommonEventMonitor(monitorString: kAccountPage, type: 1)
        setupTableView()    
    }
    func setupTableView() {
        accountTableView.delegate = self
        accountTableView.dataSource = self
        accountTableView.register(UINib.init(nibName: "AccountTableViewCell", bundle: nil), forCellReuseIdentifier: "reuse")
        accountTableView.rowHeight = 55
        accountTableView.separatorStyle = .none
        accountTableView.separatorColor = .clear
        accountTableView.showsVerticalScrollIndicator = false
        view.addSubview(accountTableView)
        
        setupTableHeaderView()
        setupTableFooterView()
    }
    func setupTableHeaderView() {
        headAccountView = AccountHeadView.init(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 104))
        if let name = UserDefaults.standard.object(forKey: "kUserName") {
            headAccountView.nameLabel.text = (name as! String)
        }
        let tagGes = UITapGestureRecognizer.init(target: self, action: #selector(clickHead))
        headAccountView.addGestureRecognizer(tagGes)
        accountTableView.tableHeaderView = headAccountView
    }
    func setupTableFooterView() {
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(footerViewSingleTapped))
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 125))
        footerView.backgroundColor = UIColor.white
        footerView.addGestureRecognizer(tapgesture)
        
        let frontString = "If you have any questions,\nplease contact us via email "
       
        let frontAttriString = NSMutableAttributedString.init(string: frontString)
        frontAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0x999999), NSAttributedStringKey.font : UIFont(name: kGothamRoundedLight, size: 15) ?? UIFont.systemFont(ofSize: 15)], range: NSRange(location: 0, length: frontAttriString.length))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 11
//        paragraphStyle.firstLineHeadIndent = 60
//        paragraphStyle.headIndent = 60
//        paragraphStyle.tailIndent = 10
//        paragraphStyle.baseWritingDirection = .rightToLeft
        paragraphStyle.alignment = .center

        frontAttriString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: frontAttriString.length))
        let lastAttriString = NSMutableAttributedString.init(string: supportFayParkEmailString)
        lastAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0x9bbc65),kCTUnderlineStyleAttributeName as NSAttributedStringKey: NSUnderlineStyle.styleSingle.rawValue , NSAttributedStringKey.font : UIFont(name: kGothamRoundedLight, size: 15) ?? UIFont.systemFont(ofSize: 15)], range: NSRange(location: 0, length: lastAttriString.length))
        lastAttriString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: lastAttriString.length))
        
        let mutableAttributeString = NSMutableAttributedString.init()
        mutableAttributeString.append(frontAttriString)
        mutableAttributeString.append(lastAttriString)
        
        let notiseLabel = UILabel(frame: CGRect(x: 60, y: 0, width: kScreenWidth - 120, height: 125))
        notiseLabel.textAlignment = .center
        notiseLabel.numberOfLines = 0
        notiseLabel.lineBreakMode = .byWordWrapping
        notiseLabel.attributedText = mutableAttributeString
        footerView.addSubview(notiseLabel)
        accountTableView.tableFooterView = footerView
    }
    func updateHeadViewInfo() {
        let userName = UserManager.shareUserManager.getUserName()
        headAccountView.nameLabel.text = userName
        if UserManager.shareUserManager.getUserImg().isEmpty {
            let userName = UserManager.shareUserManager.getFirstName()
            let string = getFirstLetterFromString(aString: userName)
            headAccountView.setHeadImgFromFistNameCharacter(character: string)
        } else {
            //本地存储的有后台连接
            let string = UserManager.shareUserManager.getUserImg()
            headAccountView.headButton.sd_setImage(with: URL(string: string), for: .normal, placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
            headAccountView.setHeadImgWithImage()
        }
        
    }
    // MARK: - 获取用户中心的未查看数据
    func requestAccountTipCount() -> Void {
    
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        
        FPHttpManager().manager.post(AccountTipNumInterface, parameters: param, progress: nil, success: { (task, result) in
            //MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            self.centerTipNum = CenterTipCount.init(jsonData: json["Data"])
            //刷新tableView
            //根据是否有数字显示对应的tip
            if var centerTipNumer = self.centerTipNum {
                centerTipNumer.CarProductCount = json["Data"]["CarProductCount"].intValue
                centerTipNumer.NotificationsCount = json["Data"]["NotificationsCount"].intValue
                centerTipNumer.MyFavoritesCount = json["Data"]["MyFavoritesCount"].intValue
                centerTipNumer.OrderCount = json["Data"]["OrderCount"].intValue
            }
            
            self.accountTableView.reloadData()
            
            let tabbarVC = UIApplication.shared.keyWindow?.rootViewController
            if json["Data"]["CarProductCount"] == 0 {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "1"
            } else {
                tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "\(json["Data"]["CarProductCount"].intValue+1)"
            }
        }, failure: { (task, error) in
            
        })
    }
    
    @objc func clickHead() -> Void {
        let profileVC = ProfileViewController()
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func footerViewSingleTapped() {
        guard MFMailComposeViewController.canSendMail() else {
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            let detailString = "Your device hasn't login \"Mail\". \n Please verify that you have enter account in \"Mail\"."
            let topModel = AlertButtonModel(title: "Yes", buttonType: .AlertButtonTypeNormal)
            alertView.showWithAlertView(title: "Cannot Get Mail", detailString: detailString, buttonArr: [topModel],cancel: false)
            return
        }
        
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setSubject("") //主题
        mailVC.setToRecipients([supportFayParkEmailString]) //收件人
        mailVC.setCcRecipients([""]) //抄送
        mailVC.setBccRecipients([""]) //密松
        mailVC.setMessageBody("", isHTML: false) //内容，允许使用html内容
        if let image = UIImage(named: "qq") {
            if let data = UIImagePNGRepresentation(image) {
                //添加文件
                mailVC.addAttachmentData(data, mimeType: "image/png", fileName: "qq")
            }
        }
        self.present(mailVC, animated: true) {
        }
    }
    
}
extension AccountViewController: UITableViewDelegate,UITableViewDataSource {
    //MARK: - tableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arr = accountTitlesArr[section]
        return arr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return accountTitlesArr.count
    }
    //MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuse") as! AccountTableViewCell
        cell.titleLabel.text = accountTitlesArr[indexPath.section][indexPath.row]
        cell.titleLabel.font = UIFont.init(name: kGothamRoundedLight, size: 15.0)
        cell.titleLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        cell.cellIco.image = UIImage.init(named: accountImgArr[indexPath.section][indexPath.row])
        cell.badgeButton.isHidden = true
        cell.selectionStyle = .none
        if indexPath.section == 0 {
            //My Orders
            if centerTipNum?.OrderCount != 0 && indexPath.row == 0 {
                cell.badgeButton.isHidden = false
                let notiseString = "\(centerTipNum?.OrderCount ?? 0)"
                cell.setBadgeString(badgeString: notiseString)
            }
            //My Favorites
            if centerTipNum?.MyFavoritesCount != 0 && indexPath.row == 3 {
                cell.badgeButton.isHidden = false
                let notiseString = "\(centerTipNum?.MyFavoritesCount ?? 0)"
                cell.setBadgeString(badgeString: notiseString)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                //历史订单orderhistory
                let orderhisVC = OrderHistoryViewController()
                navigationController?.pushViewController(orderhisVC, animated: true)
            case 1:
                let webVC = SupportWebController()
                webVC.urlString = ShippingUrlString
                webVC.titleString = kAccountListShippingInfoString
                self.navigationController?.pushViewController(webVC, animated: true)
            case 2:
                
                let webVC = SupportWebController()
                webVC.urlString = ReturnUrlString
                webVC.titleString = kAccountListRefundPolicyString
                self.navigationController?.pushViewController(webVC, animated: true)
            case 3:
                
                //收藏mylikes
                let mylikeVC = MyFavoritesViewController()
                navigationController?.pushViewController(mylikeVC, animated: true)
            case 4:
                //help
                let webVC = SupportWebController()
                webVC.urlString = AboutUSUrlString
                webVC.titleString = kAccountListAboutUsString
                self.navigationController?.pushViewController(webVC, animated: true)
            default:
                break
            }
        } else {
            switch indexPath.row {
            case 0:
                //地址address
                let addVC = UserAddressViewController()
                navigationController?.pushViewController(addVC, animated: true)
                
            case 1:
                //信用卡
                let addVC = MyWalletViewController()
                navigationController?.pushViewController(addVC, animated: true)
            case 2:
                
                let settingVC = SettingViewController()
                navigationController?.pushViewController(settingVC, animated: true)
            case 3:
                //help
                let webVC = SupportWebController()
                webVC.urlString = SupportURLString
                configCommonEventMonitor(monitorString: kSupportPage, type: 1)
                webVC.titleString = kAccountListSupportString
                self.navigationController?.pushViewController(webVC, animated: true)
            default:
                break
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        headerView.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        return headerView
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        headerView.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {return 0.0}
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let tableView = scrollView as! UITableView
        let sectionHeaderHeight: CGFloat = 10
        let sectionFooterHeight: CGFloat = 10
        let offsetY = tableView.contentOffset.y
        if offsetY <= sectionHeaderHeight && offsetY >= 0 {
            tableView.contentInset = UIEdgeInsetsMake(-offsetY, 0, -sectionFooterHeight, 0)
        } else if (offsetY >= sectionHeaderHeight && offsetY <= tableView.contentSize.height - tableView.frame.size.height - sectionFooterHeight) {
            tableView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, -sectionFooterHeight, 0)
        } else if (offsetY >= tableView.contentSize.height - tableView.frame.size.height - sectionFooterHeight && offsetY <= tableView.contentSize.height - tableView.frame.size.height) {
            tableView.contentInset = UIEdgeInsetsMake(-offsetY, 0, -(tableView.contentSize.height - tableView.frame.size.height - sectionFooterHeight), 0)
        }
    }
}

extension AccountViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        guard error == nil else {
            print("error")
            return
        }
        //发送状态
        switch result {
        case .cancelled:
            print("send result: canceld")
        case .failed:
            print("send result: failed")
        case .saved:
            print("send result: saved")
        case .sent:
            print("send result: sent")
        }
    }
}

