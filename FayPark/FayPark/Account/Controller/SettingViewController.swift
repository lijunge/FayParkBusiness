//
//  SettingViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/8.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import AppsFlyerLib
import MBProgressHUD
import AFNetworking
import SwiftyJSON

class SettingViewController: UITableViewController {
    
    var settingTitles = [kSTTitleString1,kSTTitleString2,kSTTitleString3]
    let cellIdentifier = "SettingTableViewCell"
    var updateFlag = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        title = kSettingTitleString
        
        configCommonEventMonitor(monitorString: kSettingPage, type: 1)

        tableView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.rowHeight = 58
        setupNavigationBar()
        checkAppVersion()
    }
    func setupNavigationBar() {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.setImage(UIImage(named: "arrow_left_icon"), for: .normal)
        backButton.titleLabel?.isHidden = true
        backButton.contentHorizontalAlignment = .center
        backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        let backW: CGFloat = kScreenWidth > 375.0 ? 50 : 44
        backButton.frame = CGRect(x: 0, y: 0, width: backW, height: 40)
        backButton.addTarget(self, action: #selector(leftBackButtonTapped), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backButton)
    }
    @objc func leftBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    //退出登录
    func logoutAction() {
        configCommonEventMonitor(monitorString: kLogoutPage, type: 1)
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        let manger = AFHTTPSessionManager()
        manger.responseSerializer.acceptableContentTypes = nil
        manger.securityPolicy = AFSecurityPolicy.default()
        manger.securityPolicy.allowInvalidCertificates = true
        manger.securityPolicy.validatesDomainName = false
        manger.responseSerializer = AFHTTPResponseSerializer()
        
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        
        let param = ["UserID":userID]
        manger.post(LogoutInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            if json["Code"].intValue == 200 {
                print("logout success\(json)")
                //删除本地缓存的用户数据
                UserManager.shareUserManager.userLogOut()
                
                let user = User.init(jsonData: json["Data"])
                UserManager.shareUserManager.setUser(user: user)//缓存用户登录信息
                self.loginTourist()
            }
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func loginTourist() {//MARK:游客登陆
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let params = ["Type":"1",
                      "CountrySName":countryCode,
                      "CountryCurrency":countryPay]
        FPHttpManager().manager.post(FayParkGetUserTourist, parameters: params, progress: nil, success: { (task, data) in
            let json = JSON(data as! Data)
            if json["Code"].stringValue == "200" {
                print("user login Torist\(json)")
                let user = User.init(jsonData: json["Data"])
                UserManager.shareUserManager.setUser(user: user)//缓存用户登录信息
                //您已退出!!
                //发送通知,让首页Browse刷新
                NotificationCenter.default.post(name: Notification.Name(rawValue: "alreadyLogin"), object: nil, userInfo: nil)
                self.tabBarController?.selectedIndex = 0
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
        }) { (task, error) in
            
        }
    }
    
    func checkAppVersion() {
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let param = ["TerminalType":1,
                     "VersionNumber":currentVersion,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay] as [String : Any]
        FPHttpManager().manager.post(VersionListInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON(result as! Data)
            let upgrateString = json["Data"]["IsUpgrade"].stringValue //0 不需要更新 1可选更新 2强制更新
            if upgrateString == "1" || upgrateString == "2"{
                self.updateFlag = 1
            } else if upgrateString == "0"{
                self.updateFlag = 0
            }
            self.tableView.reloadData()
        }) { (task, error) in
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingTitles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SettingTableViewCell
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.titleLabel.text = settingTitles[indexPath.row]
        if indexPath.row == 1 {
            if self.updateFlag == 0 {
                cell.notiseLabel.isHidden = true
            } else {
                cell.notiseLabel.isHidden = false
            }
        }
        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = ChangePasswordController()
            navigationController?.pushViewController(vc, animated: true)
        //版本更新
        case 1:
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            if self.updateFlag == 0 {
                //最新版本
                let alertModel = AlertButtonModel(title: kSTConfirmString, buttonType: .AlertButtonTypeNormal)
                alertView.showWithAlertView(title: kSTNotificationString, detailString: kSTLastestVersionString, buttonArr: [alertModel],cancel: true)
                alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                }
            } else {
                let topModel = AlertButtonModel(title: kUpdateNowString, buttonType: .AlertButtonTypeNormal)
                let downModel = AlertButtonModel(title: kUpdateNextTimeString, buttonType: .AlertButtonTypeRed)
                alertView.showWithAlertView(title: kSTNotificationString, detailString: kSTUpdateNowDetailString, buttonArr: [topModel,downModel],cancel: false)
                alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                    if button.tag == 100 {
                        //去AppStore更新
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: fayParkAppStoreString)!, options: [:], completionHandler: nil)
                        } else {
                        }
                    }
                }
            }
        case 2:
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            let alertModel = AlertButtonModel(title: kSTLogoutString, buttonType: .AlertButtonTypeNormal)
            alertView.showWithAlertView(title: kSTLogoutNotiseString, detailString: "", buttonArr: [alertModel],cancel: false)
            alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                self.logoutAction()
            }
        default:
            break
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "alreadyLogin"), object: nil)/// 移除通知
    }
}

