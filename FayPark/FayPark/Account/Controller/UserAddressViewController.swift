//
//  UserAddressViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/10.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import Presentr
import AFNetworking
import SwiftyJSON
import MBProgressHUD

class UserAddressViewController: UITableViewController {

    var headerView: UIView!
    var pageIndex = 1
    var addressArr = [MyAddressInfo]()
    var selectButtonArr = [Int]()
    var isPaymentSelect = false //是否是从支付页面进来的选择地址，如果是支付页面进来的，选择框是指选择成为此次支付地址，如果是从我的地址页面进来，则是选择成为默认地址
    var paymentSelectAddressID: Int?
    
    typealias SelectPaymentAddressBlock = (_ addressInfo: MyAddressInfo?) -> Void
    var isSelectPaymentAddressInfoBlock: SelectPaymentAddressBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        title = kUserAddressTitleString

        tableView.register(UINib.init(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "addresscell")
        tableView.backgroundColor = .white
        tableView.tableFooterView = UIView()
        setupNavigationBar()
        setupHeaderView()
        requestMyAddressListFromServer()
        if isPaymentSelect { //支付页面过来的
            configCommonEventMonitor(monitorString: kSelectAddressPage, type: 1)
        } else { // 不是支付页面
            configCommonEventMonitor(monitorString: kAddressMangerPage, type: 1)
        }
    }
    func setupHeaderView() {
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 76))
        headerView.backgroundColor = UIColor.white
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(headerViewTapped))
        headerView.addGestureRecognizer(tapGesture)
                
        let headImg = UIImageView(frame: CGRect(x: 20, y: headerView.frame.size.height / 2.0 - 22 / 2.0, width: 22, height: 22))
        headImg.image = UIImage(named: "addnew_icon")
        headerView.addSubview(headImg)
        
        let notiseLabel = UILabel(frame: CGRect(x: headImg.frame.maxX + 20, y: 0, width: kScreenWidth - 70, height: headerView.frame.size.height - 1))
        notiseLabel.text = kUANewAddressString
        notiseLabel.textAlignment = .left
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        notiseLabel.font = UIFont(name: kHelveticaRegular, size: 18)
        headerView.addSubview(notiseLabel)
        
        let downLineView = UIView(frame: CGRect(x: 0, y: headerView.frame.size.height - 1, width: kScreenWidth, height: 1))
        downLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        headerView.addSubview(downLineView)
        
        tableView.tableHeaderView = headerView
    }
    
    func requestMyAddressListFromServer() {
        MBProgressHUD.showAdded(to: view, animated: true)
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "Index":self.pageIndex,
                     "Count":10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(AccountMyAddressInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("requestMyAddressListFromServer\(json)")
            for item in json["Data"]["UserAddressInfoList"].arrayValue {
                var addressItem = MyAddressInfo.init(jsonData: item)
                if self.isPaymentSelect {
                    if let tmpAddID = self.paymentSelectAddressID {
                        if tmpAddID == addressItem.AddrID {
                            addressItem.isPaySelect = true
                        }
                    }
                }
                self.addressArr.append(addressItem)
                if addressItem.TopOne == 0 {
                    self.selectButtonArr.append(0)
                } else {
                    self.selectButtonArr.append(1)
                }
            }
            self.tableView.reloadData()
        }, failure: { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        })
    }
    func removeSelectedAddressInfoToServer(indexPath: Int) {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let addressInfo = self.addressArr[indexPath]
        self.addressArr.remove(at: indexPath)
        self.selectButtonArr.remove(at: indexPath)
        //self.tableView.deleteSections([indexPath], with: UITableViewRowAnimation.middle)
        //更新数据,否则数组越界
        self.tableView.reloadData()
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "AddrId":addressInfo.AddrID,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: view, animated: true)
        FPHttpManager().manager.post(AccountDeleteAddressInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("removeSelectedAddressInfoToServer\(json)")
            if json["Code"].stringValue == "400" {
                let msgString = json["Msg"].stringValue
                showToast(msgString)
                return
            }
        }, failure: { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        })
    }
    func setSelectedAddressInfoToDefultAddress(button: UIButton,selectIndex: Int) {
        //设置默认地址
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        if button.isSelected {
            var addressInfo = self.addressArr[selectIndex]
            if isPaymentSelect {
                let tmpArr = Array.init(repeating: 0, count: self.addressArr.count)
                for num in 0 ..< tmpArr.count {
                    if num == selectIndex {
                        addressInfo.isPaySelect = true
                        self.addressArr.remove(at: selectIndex)
                        self.addressArr.insert(addressInfo, at: selectIndex)
                        self.selectButtonArr.remove(at: num)
                        self.selectButtonArr.insert(1, at: num)
                    } else {
                        var addressInfo = self.addressArr[num]
                        addressInfo.isPaySelect = false
                        self.addressArr.remove(at: num)
                        self.addressArr.insert(addressInfo, at: num)
                        self.selectButtonArr.remove(at: num)
                        self.selectButtonArr.insert(0, at: num)
                    }
                }
                self.tableView.reloadData()
                if isSelectPaymentAddressInfoBlock != nil{
                    isSelectPaymentAddressInfoBlock!(addressInfo)
                    configCommonEventMonitor(monitorString: kCheckOutSelectShippingAddressEvent, type: 2)
                    navigationController?.popViewController(animated: true)
                    return
                }
            }
            let tmpArr = Array.init(repeating: 0, count: self.addressArr.count)
            for num in 0 ..< tmpArr.count {
                if num == selectIndex {
                    addressInfo.TopOne = 1
                    self.addressArr.remove(at: selectIndex)
                    self.addressArr.insert(addressInfo, at: selectIndex)
                    self.selectButtonArr.remove(at: num)
                    self.selectButtonArr.insert(1, at: num)
                } else {
                    var addressInfo = self.addressArr[num]
                    addressInfo.TopOne = 0
                    self.addressArr.remove(at: num)
                    self.addressArr.insert(addressInfo, at: num)
                    self.selectButtonArr.remove(at: num)
                    self.selectButtonArr.insert(0, at: num)
                }
            }
            self.tableView.reloadData()
            //1.请求设置默认地址
            let countryCode = getCurrentCountryCodeString()
            let countryPay = getCurrentPayCharacter()
            let param = ["UserID":userID,
                         "AddrId":addressInfo.AddrID,
                         "CountrySName":countryCode,
                         "CountryCurrency":countryPay]
            FPHttpManager().manager.post(AccountDefultAddressInterface, parameters: param, progress: nil, success: { (task, result) in
                let json = JSON(result as! Data)
                if json["Code"].stringValue == "400" {
                    let msgString = json["Msg"].stringValue
                    showToast(msgString)
                    return
                }
            }, failure: { (task, error) in
               
            })
        }
    }
    @objc func headerViewTapped() {
        
        let addVC = AddressEditViewController()
        addVC.isPayEditAddress = isPaymentSelect
        addVC.addAddressInfoBlock = { (addressInfo: MyAddressInfo,isUpdate: Bool) in
            self.addressArr.append(addressInfo)
            self.selectButtonArr.append(0)
            self.tableView.reloadData()
        }
        navigationController?.pushViewController(addVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc func backButtonTapped() {
        if self.isPaymentSelect {
            var selectedArr = [MyAddressInfo]()
            for item in 0 ..< self.addressArr.count {
                let addressInfo = self.addressArr[item]
                if addressInfo.isPaySelect {
                    selectedArr.append(addressInfo)
                }
            }
            if selectedArr.count > 0 {
                if isSelectPaymentAddressInfoBlock != nil{
                    if let firstObj = selectedArr.first {
                        isSelectPaymentAddressInfoBlock!(firstObj)
                        navigationController?.popViewController(animated: true)
                        return
                    }
                }
            } else {
                if isSelectPaymentAddressInfoBlock != nil{
                    isSelectPaymentAddressInfoBlock!(nil)
                    navigationController?.popViewController(animated: true)
                    return
                }
            }
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    func setupNavigationBar() {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.setImage(UIImage(named: "arrow_left_icon"), for: .normal)
        backButton.titleLabel?.isHidden = true
        backButton.contentHorizontalAlignment = .center
        backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        let backW: CGFloat = kScreenWidth > 375.0 ? 50 : 44
        backButton.frame = CGRect(x: 0, y: 0, width: backW, height: 40)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backButton)
    }
}

extension UserAddressViewController {
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return addressArr.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addresscell", for: indexPath) as! AddressTableViewCell
        let addressInfo = addressArr[indexPath.section]
        cell.configCell(addressInfo: addressInfo,isPayment: isPaymentSelect)
        //选中
        cell.selectedButtonTapped = { (selectedButton: UIButton) -> Void in
            self.setSelectedAddressInfoToDefultAddress(button: selectedButton, selectIndex: indexPath.section)
        }
        //删除地址闭包
        cell.deleteAddress = { (cell) -> Void in
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            let topModel = AlertButtonModel(title: kYesString, buttonType: .AlertButtonTypeNormal)
            let downModel = AlertButtonModel(title: kNoString, buttonType: .AlertButtonTypeRed)
            alertView.showWithAlertView(title: kAreYouSureString, detailString: kUADeleteAddressString, buttonArr: [topModel,downModel],cancel: false)
            alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                if button.tag == 100 {
                    self.removeSelectedAddressInfoToServer(indexPath: indexPath.section)
                }
            }
        }
        //编辑地址闭包
        cell.editAddress = { (editCell) -> Void in
            let selectedIndex = tableView.indexPath(for: editCell)
            let editAddressVC = AddressEditViewController()
            editAddressVC.addressInfo = self.addressArr[(selectedIndex?.section)!]
            editAddressVC.addAddressInfoBlock = { (addressInfo: MyAddressInfo,isUpdate: Bool) -> Void in
                if isUpdate {
                    //更新
                    self.addressArr.remove(at: indexPath.section)
                    self.addressArr.insert(addressInfo, at: indexPath.section)
                    self.tableView.reloadData()
                } else {
                    self.addressArr.append(addressInfo)
                    self.selectButtonArr.append(0)
                    self.tableView.reloadData()
                }
            }
            self.navigationController?.pushViewController(editAddressVC, animated: true)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let editAddressVC = AddressEditViewController()
        editAddressVC.addressInfo = self.addressArr[indexPath.section]
        editAddressVC.addAddressInfoBlock = { (addressInfo: MyAddressInfo,isUpdate: Bool) -> Void in
            if isUpdate {
                //更新
                self.addressArr.remove(at: indexPath.section)
                self.addressArr.insert(addressInfo, at: indexPath.section)
                self.tableView.reloadData()
            } else {
                self.addressArr.append(addressInfo)
                self.selectButtonArr.append(0)
                self.tableView.reloadData()
            }
        }
        self.navigationController?.pushViewController(editAddressVC, animated: true)
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 10))
        view.backgroundColor = kRGBColorFromHex(rgbValue: 0xf5f5f5)
        return view
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.addressArr.count == 0 {
            return 0.0
        }
        return kSizeFrom750(x: 20)
    }
}
