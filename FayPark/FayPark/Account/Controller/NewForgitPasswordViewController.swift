//
//  NewForgitPasswordViewController.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/28.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import FBSDKCoreKit
import AppsFlyerLib

class NewForgitPasswordViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = kForgetPasswordTitleString
        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(self.lineView)
        self.view.addSubview(self.resetPassword)
        self.view.addSubview(self.emailLable)
        self.view.addSubview(self.emailTField)
        self.view.addSubview(self.submitBtn)
        self.view.addSubview(self.headerView)
        self.headerView.addSubview(self.headerLiftView)
        self.headerView.addSubview(self.headerInfoLable)
    }
    @objc func backBtn(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func submit() {
        
        FBSDKAppEvents.logEvent(kForgetPasswordPage)
        AppsFlyerTracker.shared().trackEvent(kForgetPasswordPage, withValues: nil)
        
        if self.emailTField.text == "" {
            showToast(kFPEnterCorrectEmailString)
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["Email":self.emailTField.text!,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay] as [String : Any]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FPHttpManager().manager.post(FindYourPasswordInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            if json["Code"].stringValue == "200"{
                let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
                let alertModel = AlertButtonModel(title: kFPLoginString, buttonType: .AlertButtonTypeNormal)
                alertView.showWithAlertView(title: "", detailString: json["Msg"].stringValue, buttonArr: [alertModel],cancel: false)
                alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                showToast(json["Msg"].stringValue)
            }
            
        }) { (task, error) in

        }
        
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.headerView.mas_makeConstraints { (mark) in
            mark?.top.left().right().equalTo()(self.view)
            mark?.height.equalTo()(kSizeFrom750(x: 155))
        }
        self.headerLiftView.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.headerView)?.offset()(kSizeFrom750(x: 70))
            mark?.left.equalTo()(self.headerView)?.offset()(kSizeFrom750(x: 40))
            mark?.width.height().equalTo()(kSizeFrom750(x: 55))
        }
        self.headerInfoLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.headerLiftView)?.offset()(kSizeFrom750(x: 8))
            mark?.centerX.equalTo()(self.headerView.mas_centerX)
        }
        self.headerInfoLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.lineView.mas_makeConstraints { (mark) in
            mark?.left.right().equalTo()(self.view)
            mark?.top.equalTo()(self.headerView.mas_bottom)
            mark?.height.equalTo()(kSizeFrom750(x: 2))
        }
        
        self.resetPassword.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.lineView.mas_bottom)?.offset()(kSizeFrom750(x: 28))
            mark?.left.equalTo()(self.view)?.offset()(kSizeFrom750(x: 48))
        }
        self.resetPassword.setContentHuggingPriority(.required, for: .horizontal)
        self.emailLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.resetPassword.mas_bottom)?.offset()(kSizeFrom750(x: 85))
            mark?.left.equalTo()(self.resetPassword)
        }
        self.emailLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.emailTField.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.emailLable.mas_bottom)
            mark?.left.equalTo()(self.view)?.offset()(kSizeFrom750(x: 48))
            mark?.right.equalTo()(self.view)?.offset()(-kSizeFrom750(x: 48))
            mark?.height.equalTo()(kSizeFrom750(x: 75))
            
        }
        
        self.submitBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.emailTField.mas_bottom)?.offset()(kSizeFrom750(x: 136))
            mark?.left.right().equalTo()(self.emailTField)
            mark?.height.equalTo()(kSizeFrom750(x: 90))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    lazy var headerView: UIView = {
        let tempView = UIView.init()
        return tempView
    }()
    
    lazy var headerLiftView: UIButton = {
        let tempImg = UIButton.init()
        tempImg.setImage(kImage(iconName: "arrow_left_icon"), for: .normal)
        tempImg.addTarget(self, action: #selector(backBtn), for: .touchUpInside)
        tempImg.contentMode = .scaleAspectFit
        return tempImg
    }()
    lazy var headerInfoLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = kHelveticaFontSize(size: kSizeFrom750(x: 36))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempLable.text = kFPForgetPasswordString
        return tempLable
    }()
    
    lazy var resetPassword: UILabel = {
        let tempLable = UILabel.init()
        tempLable.text = kFPEnterRegistrationEmailString
        tempLable.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 36))
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        return tempLable
    }()
    lazy var emailLable: UILabel = {
        let tempEmailLable = UILabel.init()
        return lableStyle(lable: tempEmailLable, title: "\(kFPEmailString):")
    }()
    lazy var emailTField: TextFieldStly = {
        let tempEmailTField = TextFieldStly.init()
        return textFieldStyle(textF: tempEmailTField)
    }()
    //MARK:TextField 样式---------------------------------------
    func textFieldStyle(textF:TextFieldStly)->TextFieldStly{
        textF.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        textF.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        textF.tintColor = kRGBColorFromHex(rgbValue: 0x333333)
        return textF
    }
    //LableMARK: 样式------------------------------------------
    func lableStyle(lable:UILabel,title:String) -> UILabel {
        lable.textColor = kRGBColorFromHex(rgbValue: 0xb3b3b3)
        lable.font = UIFont.init(name: kMSReference, size: kSizeFrom750(x: 30))
        lable.textAlignment = .left
        lable.text = title
        return lable
    }
    lazy var submitBtn: UIButton = {
        let tempSignBtn = UIButton.init()
        return btnStyle(btn: tempSignBtn, title: kFPSendString)
    }()
    //MARK:登陆/注册 BTN 样式
    func btnStyle(btn:UIButton,title:String)->UIButton {
        btn.backgroundColor = kRGBColorFromHex(rgbValue: 0x9bbc69)
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.titleLabel?.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        btn.addTarget(self, action: #selector(submit), for: .touchUpInside)
        return btn
    }
    lazy var lineView: UIView = {
        let tempLine = UIView.init()
        tempLine.backgroundColor = kSpacingColor
        return tempLine
    }()

}
