//
//  CardInputViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/14.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import Stripe
import AppsFlyerLib
import FBSDKCoreKit
import AFNetworking
class CardInputViewController: BaseViewController {

    var cardField = UITextField()
    var cvcField = UITextField()
    var mmField = UITextField()
    var yyField = UITextField()
    var zipField = UITextField()
    var stripeToken: String = ""
    var customerID: String = ""
    var yearButton = UIButton()
    var emailField = UITextField()
    var fullNameField = UITextField()
    var secureField = UITextField()
    
    typealias CreateNewWalletBlock = (_ walletInfo: MyWalletInfo) -> Void
    var createNewWalletBlock: CreateNewWalletBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kCardInputTitleString
        view.backgroundColor = UIColor.white
        let selectedCountryCode = getCurrentCountryCodeString()
        if selectedCountryCode == "MX" || selectedCountryCode == "AR" {
            setupMercadoView()
        } else {
            setupSubViews()
        }
    }

    func setupSubViews() {
                
        let notiseLabel = UILabel(frame: CGRect(x: 17, y: 1, width: kScreenWidth - 34, height: 66))
        notiseLabel.text = localized("kCIHeaderEnterNotise")
        notiseLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        notiseLabel.numberOfLines = 0
        notiseLabel.lineBreakMode = .byWordWrapping
        view.addSubview(notiseLabel)
        
        cardField = UITextField(frame: CGRect(x: 17, y: notiseLabel.frame.maxY + 10, width: kScreenWidth - 34, height: 30))
        cardField.placeholder = localized("kCardNumberPlaceholder")
        cardField.delegate = self
        cardField.keyboardType = .numberPad
        view.addSubview(cardField)
        
        let cardLineView = UIView(frame: CGRect(x: 17, y: cardField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        cardLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(cardLineView)
        
        let cvWidth = (kScreenWidth - 34) / 2
        cvcField = UITextField(frame: CGRect(x: 17, y: cardLineView.frame.maxY + 10, width: cvWidth, height: 30))
        cvcField.placeholder = localized("kCVCPlaceholder")
        cvcField.delegate = self
        cvcField.keyboardType = .numberPad
        view.addSubview(cvcField)
        
        let mmW = cvWidth / 2 - 10
        let textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.alignment = NSTextAlignment.center
        
        let attri = NSAttributedString(string: localized("kMMPlaceholder"), attributes: [NSAttributedStringKey.foregroundColor: textColor,NSAttributedStringKey.paragraphStyle: paraStyle])
        
        mmField = UITextField(frame: CGRect(x: cvcField.frame.maxX, y: cardLineView.frame.maxY + 10, width: mmW, height: 30))
        mmField.attributedPlaceholder = attri
        mmField.delegate = self
        mmField.keyboardType = .numberPad
        mmField.textAlignment = .center
        view.addSubview(mmField)
        
        let tmpLabel = UILabel(frame: CGRect(x: mmField.frame.maxX, y: cardLineView.frame.maxY + 10, width: 10, height: 30))
        tmpLabel.text = "/"
        tmpLabel.textAlignment = .center
        tmpLabel.textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        view.addSubview(tmpLabel)
        
        let attri2 = NSAttributedString(string: localized("kYYPlaceholder"), attributes: [NSAttributedStringKey.foregroundColor: textColor,NSAttributedStringKey.paragraphStyle: paraStyle])
        yyField = UITextField(frame: CGRect(x: tmpLabel.frame.maxX, y: cardLineView.frame.maxY + 10, width: mmW, height: 30))
        yyField.attributedPlaceholder = attri2
        yyField.delegate = self
        yyField.keyboardType = .numberPad
        yyField.textAlignment = .center
        view.addSubview(yyField)
        
    
        let monthLineView = UIView(frame: CGRect(x: 17, y: cvcField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        monthLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(monthLineView)
        
        zipField = UITextField(frame: CGRect(x: 17, y: monthLineView.frame.maxY + 10, width: kScreenWidth - 34, height: 30))
        zipField.placeholder = localized("kZipPlaceholder")
        zipField.delegate = self
        zipField.keyboardType = .numberPad
        view.addSubview(zipField)
        
        let zipLineView = UIView(frame: CGRect(x: 17, y: zipField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        zipLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(zipLineView)
        
        let payLabel = UILabel(frame: CGRect(x: 17, y: zipLineView.frame.maxY + 10, width: kScreenWidth - 34, height: 30))
        payLabel.text = kCISecurePaymentString
        payLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        payLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        view.addSubview(payLabel)
        
        let trustLabel = UILabel(frame: CGRect(x: 17, y: payLabel.frame.maxY + 3, width: kScreenWidth - 34, height: 70))
        trustLabel.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        trustLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        trustLabel.numberOfLines = 0
        trustLabel.text = kCITrustNotiseString
        
        view.addSubview(trustLabel)
        
        let leftLabel = UILabel(frame: CGRect(x: 17, y: trustLabel.frame.maxY + 3, width: 120, height: 20))
        leftLabel.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        leftLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        leftLabel.numberOfLines = 0
        leftLabel.text = localized("kAcceptCard")
        view.addSubview(leftLabel)
        let rightLabel1 = UILabel(frame: CGRect(x: leftLabel.frame.maxX + 5, y: trustLabel.frame.maxY + 3, width: 100, height: 20))
        rightLabel1.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        rightLabel1.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        rightLabel1.numberOfLines = 0
        rightLabel1.text = localized("kRightVisa")
        view.addSubview(rightLabel1)
        
        let rightLabel2 = UILabel(frame: CGRect(x: leftLabel.frame.maxX + 5, y: rightLabel1.frame.maxY, width: 100, height: 20))
        rightLabel2.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        rightLabel2.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        rightLabel2.numberOfLines = 0
        rightLabel2.text = localized("kRightMasterCard")
        view.addSubview(rightLabel2)
        
        let rightLabel3 = UILabel(frame: CGRect(x: leftLabel.frame.maxX + 5, y: rightLabel2.frame.maxY , width: 130, height: 20))
        rightLabel3.textColor = kRGBColorFromHex(rgbValue: 0x7c7c7c)
        rightLabel3.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 25))
        rightLabel3.numberOfLines = 0
        rightLabel3.text = localized("kRightExpress")
        view.addSubview(rightLabel3)
        
        let doneButton = UIButton(frame: CGRect(x: 14, y: rightLabel3.frame.maxY + 30, width: kScreenWidth - 28, height: 49))
        doneButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        doneButton.setTitle(kDoneString, for: .normal)
        doneButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        doneButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        doneButton.addTarget(self, action: #selector(doneButtonTapped), for: .touchUpInside)
        view.addSubview(doneButton)
    }
    
    func setupMercadoView() {
        
        let notiseLabel = UILabel(frame: CGRect(x: 17, y: 1, width: kScreenWidth - 34, height: 50))
        notiseLabel.text = "Please enter correct information below"
        notiseLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        notiseLabel.numberOfLines = 0
        notiseLabel.lineBreakMode = .byWordWrapping
        view.addSubview(notiseLabel)
        
        emailField = UITextField(frame: CGRect(x: 17, y: notiseLabel.frame.maxY + 10, width: kScreenWidth - 34, height: 30))
        emailField.placeholder = "Email"
        emailField.delegate = self
        view.addSubview(emailField)
        let emailLineView = UIView(frame: CGRect(x: 17, y: emailField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        emailLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(emailLineView)
        
        fullNameField = UITextField(frame: CGRect(x: 17, y: emailLineView.frame.maxY + 10, width: kScreenWidth - 34, height: 30))
        fullNameField.placeholder = "Full Name"
        fullNameField.delegate = self
        view.addSubview(fullNameField)
        let nameLineView = UIView(frame: CGRect(x: 17, y: fullNameField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        nameLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(nameLineView)
        
        cardField = UITextField(frame: CGRect(x: 17, y: nameLineView.frame.maxY + 10, width: kScreenWidth - 34, height: 30))
        cardField.placeholder = localized("kCardNumberPlaceholder")
        cardField.delegate = self
        cardField.keyboardType = .numberPad
        view.addSubview(cardField)
        
        let cardLineView = UIView(frame: CGRect(x: 17, y: cardField.frame.maxY + 10, width: kScreenWidth - 34, height: 1))
        cardLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(cardLineView)
        
        let cvWidth = (kScreenWidth - 44) / 2
        let mmW = cvWidth / 2 - 10
        let textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.alignment = NSTextAlignment.center
        
        let attri = NSAttributedString(string: localized("kMMPlaceholder"), attributes: [NSAttributedStringKey.foregroundColor: textColor,NSAttributedStringKey.paragraphStyle: paraStyle])
        mmField = UITextField(frame: CGRect(x: 17, y: cardLineView.frame.maxY + 10, width: mmW, height: 30))
        mmField.attributedPlaceholder = attri
        mmField.delegate = self
        mmField.keyboardType = .numberPad
        mmField.textAlignment = .left
        view.addSubview(mmField)
        
        let tmpLabel = UILabel(frame: CGRect(x: mmField.frame.maxX, y: cardLineView.frame.maxY + 10, width: 10, height: 30))
        tmpLabel.text = "/"
        tmpLabel.textAlignment = .center
        tmpLabel.textColor = kRGBColorFromHex(rgbValue: 0xcccccc)
        view.addSubview(tmpLabel)
        
        let attri2 = NSAttributedString(string: localized("kYYPlaceholder"), attributes: [NSAttributedStringKey.foregroundColor: textColor,NSAttributedStringKey.paragraphStyle: paraStyle])
        yyField = UITextField(frame: CGRect(x: tmpLabel.frame.maxX, y: cardLineView.frame.maxY + 10, width: mmW, height: 30))
        yyField.attributedPlaceholder = attri2
        yyField.delegate = self
        yyField.keyboardType = .numberPad
        yyField.textAlignment = .center
        view.addSubview(yyField)
        let monthLineView = UIView(frame: CGRect(x: 17, y: yyField.frame.maxY + 10, width: cvWidth, height: 1))
        monthLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(monthLineView)
        
        secureField = UITextField(frame: CGRect(x: cvWidth + 27, y: cardLineView.frame.maxY + 10, width: cvWidth, height: 30))
        secureField.placeholder = "Securety Code"
        secureField.delegate = self
        secureField.keyboardType = .numberPad
        view.addSubview(secureField)
        let monthLineView2 = UIView(frame: CGRect(x: cvWidth + 27, y: cvcField.frame.maxY + 10, width: cvWidth, height: 1))
        monthLineView2.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        view.addSubview(monthLineView2)
        
        let acceptLabel = UILabel(frame: CGRect(x: 17, y: monthLineView2.frame.maxY + 4, width: KScreenWidth, height: 25))
        acceptLabel.text = "Acceptable Credit Cards:"
        acceptLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        acceptLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        view.addSubview(acceptLabel)
        
        let accDetailLabel = UILabel(frame: CGRect(x: 17, y: acceptLabel.frame.maxY, width: kScreenWidth - 34, height: 25))
        accDetailLabel.text = "VISA/MasterCard/American Express"
        accDetailLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        accDetailLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 30))
        view.addSubview(accDetailLabel)
        
        let debitLabel = UILabel(frame: CGRect(x: 17, y: accDetailLabel.frame.maxY + 4, width: kScreenWidth - 34, height: 25))
        debitLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        debitLabel.font = UIFont(name: kMSReference, size: kSizeFrom750(x: 30))
        debitLabel.numberOfLines = 0
        debitLabel.text = "Acceptable Debit Cards:"
        view.addSubview(debitLabel)
        
        let leftLabel = UILabel(frame: CGRect(x: 17, y: debitLabel.frame.maxY, width: kScreenWidth - 34, height: 60))
        leftLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        leftLabel.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 30))
        leftLabel.numberOfLines = 0
        leftLabel.lineBreakMode = .byWordWrapping
        leftLabel.text = "VISA/MasterCard/MP/IXE/HSBC/Santander/Banorte/Banamex/Bancomer"
        view.addSubview(leftLabel)

        
        let doneButton = UIButton(frame: CGRect(x: 14, y: leftLabel.frame.maxY + 30, width: kScreenWidth - 28, height: 49))
        doneButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        doneButton.setTitle(kDoneString, for: .normal)
        doneButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        doneButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        doneButton.addTarget(self, action: #selector(addCardForMercado), for: .touchUpInside)
        view.addSubview(doneButton)
    }
    
    @objc func doneButtonTapped() {
        //let yearString = (self.yearButton.titleLabel?.text)!
        if cardField.text == "" || cvcField.text == "" || mmField.text == "" || yyField.text == "" {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = .text
            hud.detailsLabel.text = kCIEnterCorrectCardString//"请输入信用卡信息"
            hud.hide(animated: true, afterDelay: 1.0)
            return
        }
        var mmString = ""
        if let mmFieldString = mmField.text {
            if mmFieldString.count == 1 && Int(mmFieldString)! < 10 {
                mmString = "0\(mmFieldString)"
            } else {
                mmString = mmFieldString
            }
        }
        let yyString = yyField.text!
        let cardParams = STPCardParams()
        cardParams.number = cardField.text!
        cardParams.expMonth = UInt.init(mmString)!
        cardParams.expYear = UInt.init(yyString)!
        cardParams.cvc = cvcField.text!
        cardParams.address.postalCode = zipField.text!
        cardParams.currency = getCurrentPaymentCodeString()
        let mbhud = MBProgressHUD.showAdded(to: self.view, animated: true)
        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            if error != nil {
                mbhud.mode = MBProgressHUDMode.text
                mbhud.detailsLabel.text = kCICardInvalidString
                mbhud.hide(animated: true, afterDelay: 2.0)
                return
            }
            
            self.logAddedPaymentInfoEvent(success: true)
            AppsFlyerTracker.shared().trackEvent(AFEventAddPaymentInfo, withValues: [AFEventParamSuccess : true])
      
            //创建信用卡
            guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
                return
            }
            
            let param1 = ["UserID":userID]
            FPHttpManager.init().manager.post(StripeCreatCustomIDInterface, parameters: param1, progress: nil, success: { (task, result) in
                let json = JSON.init(result as! Data)
                print("创建信用卡\(json)")
                if json["Code"].stringValue == "400" {
                    mbhud.mode = MBProgressHUDMode.text
                    mbhud.label.text = json["Msg"].string
                    mbhud.hide(animated: true, afterDelay: 2.0)
                    return
                }
                let stripeToken: STPToken = token!
                let countryCode = getCurrentCountryCodeString()
                let countryPay = getCurrentPayCharacter()
                let param = ["UserID":userID,
                             "CustomerID":self.customerID,
                             "Token": stripeToken.tokenId,
                             "ClientType": "1",
                             "CountrySName":countryCode,
                             "CountryCurrency":countryPay]
                FPHttpManager().manager.post(MyWalletAddInterface, parameters: param, progress: nil, success: { (task, result) in
                    mbhud.hide(animated: true)
                    let json = JSON.init(result as Any)
                    print("doneButtonTapped\(json)")
                    if json["Code"].stringValue == "200" {
                        var createWallet = MyWalletInfo(jsonData: json["Data"])
                        let cardBrand: STPCardBrand = (stripeToken.card?.brand)!
                        createWallet.TopOne = 0
                        createWallet.CardNum = (stripeToken.card?.last4)!
                        if cardBrand == STPCardBrand.visa {
                            createWallet.CardType = "Visa"
                        } else if cardBrand == STPCardBrand.masterCard {
                            createWallet.CardType = "MasterCard"
                        } else if cardBrand == STPCardBrand.amex {
                            createWallet.CardType = "American Express"
                        } else if cardBrand == STPCardBrand.JCB {
                            createWallet.CardType = "JCB"
                        } else if cardBrand == STPCardBrand.discover {
                            createWallet.CardType = "Discover"
                        } else if cardBrand == STPCardBrand.dinersClub {
                            createWallet.CardType = "Diners Club"
                        }
                        if self.createNewWalletBlock != nil {
                            self.createNewWalletBlock!(createWallet)
                        }
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        let string = json["Msg"].stringValue
                        showToast(string)
                        return
                    }
                    
                }) { (task, error) in
                    mbhud.hide(animated: true)
                }
            }, failure: { (task, error) in
                mbhud.mode = .text
                mbhud.label.text = kCISaveFailureString
                mbhud.hide(animated: true, afterDelay: 1.0)
            })
        }
    }
    
    @objc func addCardForMercado() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        if emailField.text == "" || cardField.text == "" || fullNameField.text == "" || mmField.text == "" || yyField.text == "" || secureField.text == "" {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = .text
            hud.detailsLabel.text = kPMEnterCorrectCardString//"请输入信用卡信息"
            hud.hide(animated: true, afterDelay: 1.0)
            return
        }
        var mmString = ""
        if let mmFieldString = mmField.text {
            if mmFieldString.count == 1 && Int(mmFieldString)! < 10 {
                mmString = "0\(mmFieldString)"
            } else {
                mmString = mmFieldString
            }
        }
        let yyString = yyField.text
        
        let userID = UserManager.shareUserManager.getUserID()
        let param = ["UserID": userID]
        //获取PublishKey
        FPHttpManager().manager.post(MercadoPublishKeyInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            print("configMercadoCheckOut\(json)")
            if json["Code"].stringValue == "200" {
                let publishKey = json["Data"]["MPPublishableKey"].stringValue
                //获取token
                let MercadoTokenInterface = "https://api.mercadopago.com/v1/card_tokens?public_key=\(publishKey)"
                let tokenParams = ["card_number":"5474925432670366",
                                   "security_code":"366",
                                   "expiration_month":11,
                                   "expiration_year":2020,
                                   "cardholder":["name":"test"]] as [String : Any]
                print("toke para\(tokenParams)")
                //获取token
                let manage = FPHttpManager().manager
                manage.requestSerializer = AFJSONRequestSerializer()
                manage.responseSerializer.acceptableContentTypes = NSSet(array: ["text/html","application/json","text/json","text/plain","text/javascript"]) as? Set<String>
                manage.post(MercadoTokenInterface, parameters: tokenParams, progress: nil, success: { (task, result) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    let json = JSON.init(result as Any)
                    print("MercadoTokenInterface\(json)")
                    let mpToken = json["id"].stringValue
                    //获取customID
                    let customerIDParams = ["UserID": "eq4256",
                                            "UserEmail":"test_user_76209764@testuser.com"]
                    FPHttpManager().manager.post(MercadoCustomIDInterface, parameters: customerIDParams, progress: nil, success: { (task, result) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                        let json = JSON.init(result as Any)
                        print("MercadoCustomIDInterface\(json)")
                        if json["Code"].stringValue == "200" {
                            let customerID = json["Data"]["MPCustomerId"].stringValue
                            let createCardParams = ["UserID": "eq4256",
                                                    "MPCustomerId":customerID,
                                                    "MPToken": mpToken,
                                                    "ClientType": "1"]
                            FPHttpManager().manager.post(MercadoCreateCardInterface, parameters: createCardParams, progress: nil, success: { (task, result) in
                                MBProgressHUD.hide(for: self.view, animated: true)
                                let json = JSON.init(result as Any)
                                print("MercadoCreateCardInterface\(json)")
                                if json["Code"].stringValue == "200" {
                                    //let cardID = json["Data"]["MPCardId"].stringValue
                                    
                                } else {
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                }
                            }) { (task, error) in
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                        } else {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                    }) { (task, error) in
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                }) { (task, error) in
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func logAddedPaymentInfoEvent(success : Bool) {
        let tipNo = success ? 1 : 0
        let params = [FBSDKAppEventParameterNameSuccess : tipNo]
        FBSDKAppEvents.logEvent(FBSDKAppEventNameAddedPaymentInfo, parameters: params)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension CardInputViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == cardField {
            if range.location >= 16 {
                cvcField.becomeFirstResponder()
                return false
            }
        }
        
        if textField == cvcField {
            if range.location >= 3 {
                mmField.becomeFirstResponder()
                return false
            }
        }
        if textField == mmField {
            if range.location >= 2 {
                yyField.becomeFirstResponder()
                return false
            }
        }
        if textField == yyField {
            if range.location >= 4 {
                zipField.becomeFirstResponder()
                return false
            }
        }
        return true
    }
}

