//
//  MyWalletViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/13.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import MJRefresh

class MyWalletViewController: BaseViewController {
    var pageIndex = 1
    var headerView: UIView!
    var tableView: UITableView!
    var walletArr = [MyWalletInfo]()
    var selectedButtonArr = [Int]()
    let cellIdentifier = "MyWalletTableViewCell"
    var customerID = ""
   
    var isCheckoutPush = false //true 从支付页面进入，false从管理页面进入
    var selectedWalletID = 0 // 0什么都没有 1->选择的是PayPal 有任何的值就是指定的选择的cardID
    
    // selectedPayWay 0 什么也没有 1 Paypal 2 stripe 3 货到付款
    typealias SelectPaymentWayBlock = (_ paymentInfo: MyWalletInfo,_ selectedPayWay: Int) -> Void
    var selectPaymentWayBlock: SelectPaymentWayBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kMyWalletTitleString
        view.backgroundColor = UIColor.white
        if self.isCheckoutPush {
            let selectedCountryCode = getCurrentCountryCodeString()
            if selectedCountryCode == "IN" {
                //货到付款 印度
                var cashlWallet = MyWalletInfo(jsonData: [])
                if self.selectedWalletID == 3 {
                    cashlWallet.isPaySelect = true
                }
                self.walletArr.insert(cashlWallet, at: 0)
                self.selectedButtonArr.insert(0, at: 0)
            } else if selectedCountryCode != "MY" && selectedCountryCode != "AR"{// 马来西亚 阿根廷 只有stripe支付
                //Paypal
                var paypalWallet = MyWalletInfo(jsonData: [])
                if self.selectedWalletID == 1 {
                    paypalWallet.isPaySelect = true
                }
                self.walletArr.insert(paypalWallet, at: 0)
                self.selectedButtonArr.insert(0, at: 0)
            }
            
            configCommonEventMonitor(monitorString: kSelectPaymentInfoPage, type: 1)
        } else {
            configCommonEventMonitor(monitorString: kPayOptionManagerPage, type: 1)
        }
        setupTableView()
        requestWalletListFromServer()
        
    }
    func setupTableView() {
        tableView = UITableView(frame: view.frame, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tableView.rowHeight = 55
        tableView.separatorStyle = .none
        tableView.separatorColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.tableFooterView = UIView()
        view.addSubview(tableView)
        setupHeaderView()
        //setupRefreshHandler()
    }
    func setupHeaderView() {
        headerView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 76))
        headerView.backgroundColor = UIColor.white
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(headerViewTapped))
        headerView.addGestureRecognizer(tapGesture)
        
        let headImg = UIImageView(frame: CGRect(x: 20, y: headerView.frame.size.height / 2.0 - 22 / 2.0, width: 22, height: 22))
        headImg.image = UIImage(named: "addnew_icon")
        headerView.addSubview(headImg)
        
        let notiseLabel = UILabel(frame: CGRect(x: headImg.frame.maxX + 20, y: 0, width: kScreenWidth - 70, height: headerView.frame.size.height - 1))
        notiseLabel.text = kMWAddnewCardString
        notiseLabel.textAlignment = .left
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        notiseLabel.font = UIFont(name: kHelveticaRegular, size: 18)
        headerView.addSubview(notiseLabel)
        
        let lineView = UIView(frame: CGRect(x: 0, y: headerView.frame.size.height - 1, width: kScreenWidth, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        headerView.addSubview(lineView)
        
        tableView.tableHeaderView = headerView
    }
    
    @objc func headerViewTapped() {
        let inputWalletVC = CardInputViewController()
        inputWalletVC.customerID = self.customerID
        inputWalletVC.createNewWalletBlock = { (walletInfo: MyWalletInfo) -> Void in
            self.walletArr.append(walletInfo)
            if self.isCheckoutPush {
                configCommonEventMonitor(monitorString: kSaveCardEvent, type: 2)
            } else {
                configCommonEventMonitor(monitorString: kAccountSaveCardEvent, type: 2)
            }
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(inputWalletVC, animated: true)
    }
//    func setupRefreshHandler() {
//        //下拉刷新
//        self.tableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
//            self.pageIndex = 1
//            self.walletArr.removeAll()
//            self.requestWalletListFromServer()
//        })
//        //上拉加载更多
//        self.tableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {
//            self.pageIndex += 1
//            self.requestWalletListFromServer()
//        })
//    }
    func requestWalletListFromServer() {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID": userID,
                     "Index": self.pageIndex,
                     "Count": 10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(MyWalletListInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("requestWalletListFromServer\(json)")
            for item in json["Data"]["UserCardInfoList"].arrayValue {
                var wallet = MyWalletInfo(jsonData: item)
                if wallet.CardInfoID == self.selectedWalletID {
                    wallet.isPaySelect = true
                }
                self.walletArr.append(wallet)
                if wallet.TopOne == 0 {
                    self.selectedButtonArr.append(0)
                } else {
                    if self.isCheckoutPush {
                        self.selectedButtonArr.append(0)
                    } else {
                        self.selectedButtonArr.append(1)
                    }
                }
            }
            let customerID: String = json["Data"]["CustomerID"].stringValue
            self.customerID = json["Data"]["CustomerID"].stringValue
            UserDefaults.standard.set(customerID, forKey: "CustomerID")
            UserDefaults.standard.synchronize()
            self.tableView.reloadData()
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func setSelectedWalletToDefultPay(button: UIButton,selectedIndex: Int) {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        if button.isSelected {
            if isCheckoutPush {
                var selectedPayWay = 0 //没有支付方式
                let tmpCarInfo = self.walletArr[selectedIndex]
                if selectedIndex == 0 {
                    let selectedCountryCode = getCurrentCountryCodeString()
                    if selectedCountryCode == "IN" {
                        //货到付款
                        selectedPayWay = 3 //货到付款
                        self.selectedWalletID = 3
                        configCommonEventMonitor(monitorString: kSelectCashEvent, type: 2)
                    } else if selectedCountryCode == "MY" || selectedCountryCode == "AR" {
                        selectedPayWay = 2 //stripe
                        self.selectedWalletID = tmpCarInfo.CardInfoID
                        configCommonEventMonitor(monitorString: kSelectStripeEvent, type: 2)
                        
                    } else {
                        self.selectedWalletID = 1
                        selectedPayWay = 1 //paypal
                        configCommonEventMonitor(monitorString: kSelectPayPalEvent, type: 2)
                    }
                } else {
                    selectedPayWay = 2 //stripe
                    self.selectedWalletID = tmpCarInfo.CardInfoID
                    configCommonEventMonitor(monitorString: kSelectStripeEvent, type: 2)
                }
                self.tableView.reloadData()
                if selectPaymentWayBlock != nil {
                    selectPaymentWayBlock!(tmpCarInfo,selectedPayWay)
                    navigationController?.popViewController(animated: true)
                    return
                }
            }
            var cardInfo = self.walletArr[selectedIndex]
            let tmpArr = Array.init(repeating: 0, count: self.walletArr.count)
            for num in 0 ..< tmpArr.count {
                if num == selectedIndex {
                    cardInfo.TopOne = 1
                    self.walletArr.remove(at: selectedIndex)
                    self.walletArr.insert(cardInfo, at: selectedIndex)
                    self.selectedButtonArr.remove(at: num)
                    self.selectedButtonArr.insert(1, at: num)
                } else {
                    var defultCard = self.walletArr[num]
                    defultCard.TopOne = 0
                    self.walletArr.remove(at: num)
                    self.walletArr.insert(defultCard, at: num)
                    self.selectedButtonArr.remove(at: num)
                    self.selectedButtonArr.insert(1, at: num)
                }
            }
            self.tableView.reloadData()
            let countryCode = getCurrentCountryCodeString()
            let countryPay = getCurrentPayCharacter()
            let param = ["UserID": userID,
                         "CardInfoID": cardInfo.CardInfoID,
                         "CountrySName":countryCode,
                         "CountryCurrency":countryPay]
            FPHttpManager().manager.post(MyWalletSetDefultPayInterface, parameters: param, progress: nil, success: { (task, result) in
                let json = JSON(result as! Data)
                print("setSelectedWalletToDefultPay\(json)")
                if json["Code"].stringValue == "400" {
                    let msgString = json["Msg"].stringValue
                    showToast(msgString)
                    return
                }
            }) { (task, error) in
                button.isSelected = false
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension MyWalletViewController: UITableViewDelegate,UITableViewDataSource {
    //MARK: - tableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return walletArr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MyWalletTableViewCell
        cell.selectionStyle = .none
        if isCheckoutPush {
            let walletInfo = walletArr[indexPath.row]
            if indexPath.row == 0 {
                let selectedCountryCode = getCurrentCountryCodeString()
                if selectedCountryCode == "IN" {
                    cell.configCashCell(walletInfo: walletInfo, selectedPayID: selectedWalletID)
                } else if selectedCountryCode == "MY" || selectedCountryCode == "AR" {
                    cell.configWalletCell(walletInfo: walletInfo, selectedPayID: self.selectedWalletID, isCheckOut: true)
                } else {
                    cell.configPaypalCell(walletInfo: walletInfo, selectedPayID: self.selectedWalletID)
                }
            } else {
                cell.configWalletCell(walletInfo: walletInfo, selectedPayID: self.selectedWalletID, isCheckOut: true)
            }
        } else {
            let walletInfo = walletArr[indexPath.row]
            cell.configWalletCell(walletInfo: walletInfo, selectedPayID: 0, isCheckOut: false)
        }
        cell.selectedButtonTapped = { (button: UIButton) -> Void in
            self.setSelectedWalletToDefultPay(button: button, selectedIndex: indexPath.row)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
}

