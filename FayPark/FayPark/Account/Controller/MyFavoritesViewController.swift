//
//  MyFavoritesViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/14.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import AppsFlyerLib
import MJRefresh

class MyFavoritesViewController: BaseViewController {

    //页面索引
    var pageIndex = 1
    //收藏列表
    var likeArray = [BrowseActivityInfo]()

    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = kMyFavoriteTitleString
        
        configCommonEventMonitor(monitorString: kCollectPage, type: 1)
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        self.automaticallyAdjustsScrollViewInsets = false
        collectionViewInit()
        //请求收藏列表
        requestFavoriteListFromServer()
    }
    func collectionViewInit() {
      
        let layout = ZJFlexibleLayout(delegate: self)
       
//        let collectionFrame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
//        collectionView = UICollectionView(frame: collectionFrame, collectionViewLayout: layout)
        collectionView.collectionViewLayout = layout
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "FayParkCollectionCell", bundle: nil), forCellWithReuseIdentifier: "fayparkcell")
        view.addSubview(collectionView)
      
        //添加刷新控件
        self.addRefreshControl()
    }
    
    // MARK: - 添加刷新控件
    func addRefreshControl() {
        
        //下拉刷新
        self.collectionView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.pageIndex = 1
            self.likeArray.removeAll()
            self.requestFavoriteListFromServer()
        })
        //上拉加载更多
        self.collectionView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {
            self.pageIndex += 1
            self.requestFavoriteListFromServer()
        })
    }
    
    //MARK: - 请求收藏列表
    func requestFavoriteListFromServer() {
        
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        MBProgressHUD.showAdded(to: view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "Index":self.pageIndex,
                     "Count":10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(FavoriteListInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("requestFavoriteListFromServer\(json)")
            if json["Code"].stringValue == "400" {
                if self.pageIndex == 1 {
                    let newView = NoContentView.init(frame: self.view.bounds, type: 1)
                    newView.selectAction(block: {
                        let tabbarVC = self.parent?.parent as! UITabBarController
                        tabbarVC.selectedIndex = 0
                        self.navigationController?.popViewController(animated: false)
                    })
                    self.view.addSubview(newView)
                    self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                    return
                } else {
                    self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                    return
                }
            }
            for item in json["Data"]["MyFavoritesList"].arrayValue {
                let likeItem = BrowseActivityInfo.init(jsonData: item)
                self.likeArray.append(likeItem)
            }
            self.collectionView.reloadData()
            self.collectionView.mj_footer.endRefreshing()
            self.collectionView.mj_header.endRefreshing()
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.collectionView.mj_footer.endRefreshing()
            self.collectionView.mj_header.endRefreshing()
        }
    }
    //MARK: 删除所选中的商品
    func deleteSelectedFavoriteProductFromServer(selectedIndex: Int) {
        
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let selectedProduct = self.likeArray[selectedIndex]
        MBProgressHUD.showAdded(to: view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "ID":selectedProduct.ID,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(FavoriteDeleteInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            if json["Code"].stringValue == "400" {
                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                return
            }
            self.likeArray.remove(at: selectedIndex)
            if self.likeArray.count == 0 {
                let newView = NoContentView.init(frame: self.view.bounds, type: 1)
                newView.selectAction(block: {
                    let tabbarVC = self.parent?.parent as! UITabBarController
                    tabbarVC.selectedIndex = 0
                    self.navigationController?.popViewController(animated: false)
                })
                self.view.addSubview(newView)
                self.collectionView.mj_footer.endRefreshingWithNoMoreData()
                self.collectionView.mj_footer.endRefreshing()
                self.collectionView.mj_header.endRefreshing()
                return
            }
            self.collectionView.reloadData()
            
            self.collectionView.mj_footer.endRefreshing()
            self.collectionView.mj_header.endRefreshing()
            
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

//MARK: - CollectionView 数据源
extension MyFavoritesViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return likeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fayparkcell", for: indexPath) as! FayParkCollectionCell
        let pro = likeArray[indexPath.item]
        cell.imgUrl = URL.init(string: pro.MainImgLink)
        cell.configMyFavoriteCollectionCell(productInfo: pro)
        cell.hotLable.isHidden = true
        cell.almostView.isHidden = true
        cell.deleteButtonTapped = { (cell) -> Void in
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            let topModel = AlertButtonModel(title: kYesString, buttonType: .AlertButtonTypeNormal)
            let downModel = AlertButtonModel(title: kNoString, buttonType: .AlertButtonTypeRed)
            alertView.showWithAlertView(title: kAreYouSureString, detailString: kMFDeleteNotiseString, buttonArr: [topModel,downModel],cancel: false)
            alertView.bottomButtonTapped = { (button: UIButton) -> Void in
                if button.tag == 100 {
                    self.deleteSelectedFavoriteProductFromServer(selectedIndex: indexPath.row)
                }
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productVC = NewProductDetailViewController()
        productVC.requestProID = likeArray[indexPath.item].ProductID
        navigationController?.pushViewController(productVC, animated: true)
    }
}

//MARK: - 瀑布流代理
extension MyFavoritesViewController: ZJFlexibleDataSource {
    
    func sizeOfItemAtIndexPath(at indexPath: IndexPath) -> CGSize {
        let pro = likeArray[indexPath.item]
        let itemWidth = (UIScreen.main.bounds.width - 24) / 2
        let proString = pro.Proportion
        var proFloat: CGFloat = 0
        if let doubleValue = Double(proString){
            proFloat = CGFloat(doubleValue)
        }
        let itemHeight = itemWidth / proFloat
        return CGSize(width: itemWidth, height: 150 + itemHeight)
    }
    
    func heightOfAdditionalContent(at indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func numberOfCols(at section: Int) -> Int {
        return 2
    }
    
    func spaceOfCells(at section: Int) -> CGFloat{
        return 13
    }
    
    func sectionInsets(at section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 13, left: 13, bottom: 80, right: 13)
    }
}


