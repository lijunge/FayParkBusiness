//
//  RefundViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/9.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import FBSDKCoreKit
import MBProgressHUD
import AppsFlyerLib
import MessageUI

class RefundViewController: BaseViewController {
    
    @IBOutlet var refundBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    //订单ID
    var OrderID: Int!
    
    let cellIndentifier = "RefundTableViewCell"
    let refundReasonArr = [kRDReasonString1, kRDReasonString2, kRDReasonString3,
                           kRDReasonString4, kRDReasonString5, kRDReasonString6,
                           kRDReasonString7]
    var selectIndexPath = IndexPath(row: 0, section: 0) //设置一个默认值
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kRefundTitleString
        configCommonEventMonitor(monitorString: kRefundPage, type: 1)
        self.refundBtn.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 40))
        tableView.register(UINib.init(nibName: cellIndentifier, bundle: nil), forCellReuseIdentifier: cellIndentifier)
        tableView.rowHeight = 42
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        setupTableHeaderView()
        setupTableFooterView()
        if self.tableView.delegate != nil && self.tableView.delegate!.responds(to: #selector(tableView(_:didSelectRowAt:))) {
            self.tableView.delegate!.tableView!(self.tableView, didSelectRowAt: (NSIndexPath(row: 0, section: 0) as IndexPath) as IndexPath)
        }
    }
    func setupTableHeaderView() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 49))
        headerView.backgroundColor = UIColor.white
        
        let headerLabel = UILabel(frame: CGRect(x: 16, y: 0, width: KScreenWidth - 32, height: 49))
        headerLabel.text = kRDHeaderTitleString
        headerLabel.font = UIFont(name: kHelveticaRegular, size: 16)
        headerLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        headerView.addSubview(headerLabel)
        
        tableView.tableHeaderView = headerView
    }
    func setupTableFooterView() {
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(footerViewSingleTapped))
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 125))
        footerView.backgroundColor = UIColor.white
        footerView.addGestureRecognizer(tapgesture)
        
        let frontString = kRDFooterTitleString
        
        let frontAttriString = NSMutableAttributedString.init(string: frontString)
        frontAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0x7c7c7c), NSAttributedStringKey.font : UIFont(name: kGothamRoundedLight, size: 15) ?? UIFont.systemFont(ofSize: 15)], range: NSRange(location: 0, length: frontAttriString.length))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 11
        //        paragraphStyle.firstLineHeadIndent = 60
        //        paragraphStyle.headIndent = 60
        //        paragraphStyle.tailIndent = 10
        //        paragraphStyle.baseWritingDirection = .rightToLeft
        paragraphStyle.alignment = .center
        
        frontAttriString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: frontAttriString.length))
        let lastAttriString = NSMutableAttributedString.init(string: orderFayParkEmailString)
        lastAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0x9bbc65), NSAttributedStringKey.font : UIFont(name: kMSReference, size: 15) ?? UIFont.systemFont(ofSize: 15)], range: NSRange(location: 0, length: lastAttriString.length))
        lastAttriString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: lastAttriString.length))
        let mutableAttributeString = NSMutableAttributedString.init()
        mutableAttributeString.append(frontAttriString)
        mutableAttributeString.append(lastAttriString)
        
        let notiseLabel = UILabel(frame: CGRect(x: 50, y: 0, width: kScreenWidth - 100, height: 125))
        notiseLabel.textAlignment = .center
        notiseLabel.numberOfLines = 0
        notiseLabel.lineBreakMode = .byWordWrapping
        notiseLabel.attributedText = mutableAttributeString
        footerView.addSubview(notiseLabel)
        tableView.tableFooterView = footerView
    }
    
    @objc func footerViewSingleTapped() {
        guard MFMailComposeViewController.canSendMail() else {
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            let detailString = kRDMailDetailString
            let topModel = AlertButtonModel(title: kYesString, buttonType: .AlertButtonTypeNormal)
            alertView.showWithAlertView(title: kRDNotGetMailString, detailString: detailString, buttonArr: [topModel],cancel: false)
            return
        }
        
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setSubject("") //主题
        mailVC.setToRecipients([orderFayParkEmailString]) //收件人
        mailVC.setCcRecipients([""]) //抄送
        mailVC.setBccRecipients([""]) //密松
        mailVC.setMessageBody("", isHTML: false) //内容，允许使用html内容
        if let image = UIImage(named: "qq") {
            if let data = UIImagePNGRepresentation(image) {
                //添加文件
                mailVC.addAttachmentData(data, mimeType: "image/png", fileName: "qq")
            }
        }
        self.present(mailVC, animated: true) {
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func fundButtonTapped(_ sender: UIButton) {
        refundSelectProduct()
    }
    func refundSelectProduct() {
        //提交退款请求
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let reasonString = refundReasonArr[selectIndexPath.row]
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "OrderID":self.OrderID,
                     "RefundReason":reasonString,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FPHttpManager().manager.post(RefundInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON.init(result as Any)
            MBProgressHUD.hide(for: self.view, animated: true)
            if json["Code"].stringValue == "400" {
                let msgString = json["Msg"].stringValue
                showToast(msgString)
                return
            }
            if json["Msg"].stringValue == "Success" {
                let time: TimeInterval = 1.0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time) {
                    //code
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            configCommonEventMonitor(monitorString: kOrderRefundSuccess, type: 3)
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
}

extension RefundViewController: UITableViewDataSource,UITableViewDelegate {
    //MARK: - tableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return refundReasonArr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! RefundTableViewCell
        cell.selectionStyle = .none
        cell.reasonLabel.text = refundReasonArr[indexPath.row]
        if indexPath == selectIndexPath {
            cell.checkImg.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let lastCell = tableView.cellForRow(at: selectIndexPath) as! RefundTableViewCell
        lastCell.checkImg.isHidden = true
        lastCell.reasonLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        let currentCell = tableView.cellForRow(at: indexPath) as! RefundTableViewCell
        currentCell.checkImg.isHidden = false
        currentCell.reasonLabel.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        selectIndexPath = indexPath
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: selectIndexPath) as! RefundTableViewCell
        currentCell.checkImg.isHidden = true
        currentCell.reasonLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        headerView.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
}

extension RefundViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        guard error == nil else {
            return
        }
        //发送状态
        switch result {
        case .cancelled:
            print("send result: canceld")
        case .failed:
            print("send result: failed")
        case .saved:
            print("send result: saved")
        case .sent:
            print("send result: sent")
        }
    }
}
