//
//  ChangePasswordController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/8.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import AppsFlyerLib

class ChangePasswordController: BaseViewController {
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var currentPwd: UITextField!
    @IBOutlet weak var newPwd: UITextField!
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBAction func submitClick(_ sender: UIButton) {
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate
        //找回密码
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "CurrentPwd":currentPwd.text!,
                     "NewPwd":newPwd.text!,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        
        FPHttpManager().manager.post(ChangePasswordInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON.init(result as Any)
            hud.mode = .text
            hud.label.text = json["Msg"].stringValue
            hud.hide(animated: true, afterDelay: 2.0)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (task, error) in
            hud.mode = .text
            hud.label.text = kCPErrorString
            hud.hide(animated: true, afterDelay: 2.0)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kChangePasswordTitleString
    
        //点击键盘结束编辑
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
        self.submitBtn.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 40))
        configCommonEventMonitor(monitorString: kChangePasswordPage, type: 1)
    
        if let emailAddress = UserDefaults.standard.object(forKey: "kEmail") {
            let emailString = emailAddress as! String
            emailLabel.text = "\(kCPEmailString): \(emailString)"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
