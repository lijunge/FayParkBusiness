//
//  ContactUsViewController.swift
//  FayPark
//
//  Created by faypark on 2018/7/1.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!

    let contactArr = [businessFayParkEmailString,
                      marketFayParkEmailString,
                      suggestionFayParkEmailString]
    var contactTitleArr:Array<String>!
    let cellIdentifier = "ContactUsTableViewCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kContactUsTitleString
        contactTitleArr = [kBusinessCooprationString,kMarketPRString,kSuggestionString]
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
        tableView.rowHeight = 45
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        tableView.separatorStyle = .none
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func didSelectContactUs(selectedIndex: IndexPath) {
        guard MFMailComposeViewController.canSendMail() else {
            let alertView = CustomAlertView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            let detailString = kCUNotEmailNotiseString
            let topModel = AlertButtonModel(title: kYesString, buttonType: .AlertButtonTypeNormal)
            alertView.showWithAlertView(title: kCUNotGetEmailString, detailString: detailString, buttonArr: [topModel],cancel: false)
            return
        }
        let emailArr = [supportFayParkEmailString,
                        businessFayParkEmailString,
                        marketFayParkEmailString,
                        suggestionFayParkEmailString]
        let emailString = emailArr[selectedIndex.row]
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setSubject("") //主题
        mailVC.setToRecipients([emailString]) //收件人
        mailVC.setCcRecipients([""]) //抄送
        mailVC.setBccRecipients([""]) //密松
        mailVC.setMessageBody("", isHTML: false) //内容，允许使用html内容
        if let image = UIImage(named: "qq") {
            if let data = UIImagePNGRepresentation(image) {
                //添加文件
                mailVC.addAttachmentData(data, mimeType: "image/png", fileName: "qq")
            }
        }
        self.present(mailVC, animated: true) {
        }
    }

}
extension ContactUsViewController: UITableViewDataSource,UITableViewDelegate {
    //MARK: - tableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contactArr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = contactTitleArr[indexPath.row]
        cell.textLabel?.textColor = kRGBColorFromHex(rgbValue: 0x000000)
        cell.textLabel?.font = UIFont(name: kGothamRoundedLight, size: 14)
        let frontString = cell.textLabel?.text
        
        let frontAttriString = NSMutableAttributedString.init(string: frontString!)
        frontAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0x000000), NSAttributedStringKey.font : UIFont(name: kGothamRoundedLight, size: 14) ?? UIFont.systemFont(ofSize: 14)], range: NSRange(location: 0, length: frontAttriString.length))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 11
      
        paragraphStyle.alignment = .left
        frontAttriString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: frontAttriString.length))
        let lastAttriString = NSMutableAttributedString.init(string: contactArr[indexPath.row])
        lastAttriString.addAttributes([NSAttributedStringKey.foregroundColor : kRGBColorFromHex(rgbValue: 0x000000),kCTUnderlineStyleAttributeName as NSAttributedStringKey: NSUnderlineStyle.styleSingle.rawValue , NSAttributedStringKey.font : UIFont(name: kGothamRoundedLight, size: 14) ?? UIFont.systemFont(ofSize: 14)], range: NSRange(location: 0, length: lastAttriString.length))
        lastAttriString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: lastAttriString.length))
        
        let mutableAttributeString = NSMutableAttributedString.init()
        mutableAttributeString.append(frontAttriString)
        mutableAttributeString.append(lastAttriString)
        
        cell.textLabel?.attributedText = mutableAttributeString
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        didSelectContactUs(selectedIndex: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
   
}

extension ContactUsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        guard error == nil else {
            print("error")
            return
        }
        //发送状态
        switch result {
        case .cancelled:
            print("send result: canceld")
        case .failed:
            print("send result: failed")
        case .saved:
            print("send result: saved")
        case .sent:
            print("send result: sent")
        }
    }
}
