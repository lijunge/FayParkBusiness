//
//  ProfileViewController.swift
//  FayPark
//  个人详细信息界面
//  Created by 陈小奔 on 2018/1/8.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import AppsFlyerLib

class ProfileViewController: BaseViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var userHeadButton: UIButton!
    @IBOutlet var genderBtn: UIButton!
    @IBOutlet var birthBtn: UIButton!
    
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var firstField: UITextField!
    @IBOutlet weak var lastField: UITextField!
  
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet var genderBtnArray: [UIButton]!
    
    var genderData = [kPFMaleString, kPFFemaleString]
    var genderString = ""
    //选择性别
    @IBAction func genderClick(_ sender: UIButton) {
        UsefulPickerView.showSingleColPicker(kPFChooseSexString, data: genderData, defaultSelectedIndex: 0) {[unowned self] (selectedIndex, selectedValue) in
            self.genderBtn.setTitle(selectedValue, for: .normal)
        }
    }
    
    //选择生日
    @IBAction func birthClick(_ sender: UIButton) {
        UsefulPickerView.showDatePicker("") {[unowned self] ( selectedDate) in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let string = formatter.string(from: selectedDate)
            self.birthBtn.setTitle(string, for: .normal)
        }
    }
    
    @IBAction func userHeadButtonTapped(_ sender: UIButton) {
        let alertVC = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
        //拍照
        let takePhotoAction = UIAlertAction.init(title: kPFPhotoString, style: .default) { (action) in
            self.gotoImagePicker(camera: true)
        }
        alertVC.addAction(takePhotoAction)
        
        //上传图片
        let upImageAction = UIAlertAction.init(title: kPFAlbumString, style: .default) { (action) in
            self.gotoImagePicker(camera: false)
        }
        alertVC.addAction(upImageAction)
        
        //取消
        let cancelAction = UIAlertAction.init(title: kCancelString, style: .cancel) { (action) in
            
        }
        alertVC.addAction(cancelAction)
        
        present(alertVC, animated: true, completion: nil)
    }
    //点击更新
    @IBAction func updateClick(_ sender: UIButton) {
        
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        guard firstField.text != "" && lastField.text != "" && birthBtn.currentTitle != "" else {
            showToast(kPleaseEnterInformationString)
            return
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.mode = .indeterminate
        
        let firstName = firstField.text
        let lastName = lastField.text
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "FirstName":firstName!,
                     "LastName":lastName!,
                     "Gender":genderString,
                     "BirthDate":birthBtn.currentTitle!,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(UpdateProfileInterface, parameters: param, progress: nil, success: { (task, result) in
            let json = JSON.init(result as Any)
            hud.hide(animated: true, afterDelay: 2.0)
            if json["Code"].stringValue == "400" {
                let messageString = json["Msg"].stringValue
                showToast(messageString)
                return
            }
            if json["Code"].stringValue == "200" {
                hud.mode = .text
                hud.label.text = json["Msg"].stringValue
                hud.hide(animated: true, afterDelay: 2.0)
                UserDefaults.standard.set(json["Data"]["UserList"][0]["UserName"].stringValue, forKey: "kUserName")
                UserDefaults.standard.set(json["Data"]["UserList"][0]["FirstName"].stringValue, forKey: "kFirstName")
                UserDefaults.standard.set(json["Data"]["UserList"][0]["LastName"].stringValue, forKey: "kLastName")
                
                UserDefaults.standard.set(json["Data"]["UserList"][0]["Gender"].stringValue,forKey: "kGender")
                UserDefaults.standard.set(json["Data"]["UserList"][0]["Birthday"].stringValue, forKey: "kBirthday")
                self.navigationController?.popViewController(animated: true)
            }
        }) { (task, error) in
            hud.mode = .text
            hud.label.text = "Error!"
            hud.hide(animated: true, afterDelay: 2.0)
        }
    }

    //图片选择器
    func gotoImagePicker(camera: Bool) -> Void {
        if camera {
            //拍照
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let cameraVC = UIImagePickerController()
                cameraVC.sourceType = .camera
                cameraVC.delegate = self
                cameraVC.allowsEditing = true
                present(cameraVC, animated: true, completion: nil)
            }
        } else {
            //相册
            let photoVC = UIImagePickerController()
            photoVC.sourceType = .photoLibrary
            photoVC.delegate = self
            photoVC.allowsEditing = true
            present(photoVC, animated: true, completion: nil)
        }
    }
    
    @objc func genderBtnClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        for (index,item) in genderBtnArray.enumerated() {
            let selectedTextColor = kRGBColorFromHex(rgbValue: 0xffffff)
            let selectedBackColor = kRGBColorFromHex(rgbValue: 0xe9668f)
            let normalTextColor = kRGBColorFromHex(rgbValue: 0x333333)
            sender.layer.borderColor = UIColor.white.cgColor
            sender.layer.borderWidth = 0
            if item != sender {
                item.isSelected = false
                item.setTitleColor(normalTextColor, for: .normal)
                item.backgroundColor = .white
            } else {
                genderString = (index == 0) ? "1":"2"
                item.setTitleColor(selectedTextColor, for: .normal)
                item.backgroundColor = selectedBackColor
            }
        }
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = kProfileTitleString
        self.femaleBtn.layer.borderWidth = kSizeFrom750(x: 1)
        self.femaleBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
        self.maleBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
        self.maleBtn.layer.borderWidth = kSizeFrom750(x: 1)
        configCommonEventMonitor(monitorString: kChangeProfilePage, type: 1)
        
        setupSubViews()
        
        //点击键盘结束编辑
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
        
    }
   
    func setupSubViews() {
       
        userHeadButton.layer.cornerRadius = 50
        userHeadButton.layer.masksToBounds = true
        
        firstField.text = UserManager.shareUserManager.getFirstName()
        lastField.text = UserManager.shareUserManager.getLastName()
        birthBtn.setTitle(UserManager.shareUserManager.getBirthday(), for: .normal)
        
        self.updateBtn.layer.borderWidth = 1.0
        self.updateBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x333333).cgColor
        
        nameLabel.text = UserManager.shareUserManager.getUserName()
        emailLabel.text = UserManager.shareUserManager.getEmail()

        genderString = UserManager.shareUserManager.getGender()
        if genderString == "1" {
            //Male
            genderBtnArray.first?.isSelected = true
        } else  {
            //FeMale
            genderBtnArray.last?.isSelected = true
        }
        for (_,item) in genderBtnArray.enumerated() {
            item.addTarget(self, action: #selector(genderBtnClick(_:)), for: .touchUpInside)
            let selectedTextColor = kRGBColorFromHex(rgbValue: 0xffffff)
            let normalTextColor = kRGBColorFromHex(rgbValue: 0x333333)
            item.titleLabel?.textAlignment = .left
            if item.isSelected == true {
                item.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
                item.setTitleColor(selectedTextColor, for: .normal)
                item.layer.borderWidth = kSizeFrom750(x: 0)
                item.layer.borderColor = UIColor.white.cgColor
            } else {
                item.setTitleColor(normalTextColor, for: .normal)
                item.backgroundColor = .white
            }
        }
        if UserManager.shareUserManager.getUserImg().isEmpty {
            let userName = UserManager.shareUserManager.getFirstName()
            let string = getFirstLetterFromString(aString: userName)
            setHeadImgFromFistNameCharacter(character: string)
        } else {
            //本地存储的有后台连接
            let string = UserManager.shareUserManager.getUserImg()
            userHeadButton.sd_setImage(with: URL(string: string), for: .normal, placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
            setHeadImgWithImage()
        }
    }
    func setHeadImgFromFistNameCharacter(character: String) {
        userHeadButton.layer.borderWidth = 0.5
        userHeadButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0xababab).cgColor
        userHeadButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xcdcdcd)
        var titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
        if character == "A" || character == "B" || character == "C" || character == "D" || character == "E" {
            titleColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        } else if character == "F" || character == "G" || character == "H" || character == "I" || character == "J" {
            titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
        } else if character == "K" || character == "L" || character == "M" || character == "N" || character == "O" {
            titleColor = kRGBColorFromHex(rgbValue: 0x00acf0)
        } else if character == "P" || character == "Q" || character == "R" || character == "S" || character == "T" {
            titleColor = kRGBColorFromHex(rgbValue: 0xc45cac)
        } else {
            titleColor = kRGBColorFromHex(rgbValue: 0xf97f46)
        }
        userHeadButton.setTitleColor(titleColor, for: .normal)
        userHeadButton.setTitle(character, for: .normal)
        userHeadButton.titleLabel?.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 82))
    }
    func setHeadImgWithImage() {
        userHeadButton.layer.borderWidth = 0.0
        userHeadButton.backgroundColor = UIColor.white
        userHeadButton.setTitleColor(UIColor.white, for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //获取选择的原图
        let pickedImage: UIImage? = info[UIImagePickerControllerEditedImage] as? UIImage
        
        var imageData = UIImageJPEGRepresentation(pickedImage!, 1.0)
        while imageData!.count / 1024 > 100 {
            imageData = UIImageJPEGRepresentation(pickedImage!,0.5);// 压缩比例在0~1之间
        }
        let userID = UserDefaults.standard.object(forKey: "kUserID")
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let params = ["UserID": userID,
                      "Data": imageData,
                      "CountrySName":countryCode,
                      "CountryCurrency":countryPay]
        FPHttpManager().manager.post(UploadUserHeadImgInterface, parameters: params, constructingBodyWith: { (formData) in
            formData.appendPart(withFileData: imageData!, name: "file", fileName: "file.jpg", mimeType: "image")
        }, progress: { (uploadProgress) in
        }, success: { (task, responseObj) in
            let responseObjJson = JSON(responseObj!)
            print("json\(responseObjJson)")
            let messageString = responseObjJson["Msg"].stringValue
            if responseObjJson["Code"].stringValue == "400" {
                showToast(messageString)
            }
            let headImagePath = responseObjJson["Data"]["ImgUrl"].stringValue
            UserManager.shareUserManager.setUserImg(userImage: headImagePath)
        }, failure: { (task, error) in
            print("uploadImgFail \(String(describing: task)),\(error)")
        })
        //图片控制器退出
        picker.dismiss(animated: true) {
            self.userHeadButton.setImage(pickedImage, for: .normal)
        }
    }
}
