//
//  OrderHistoryDetailViewController.swift
//  FayPark
//  历史订单详情页
//  Created by 陈小奔 on 2018/1/6.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import FBSDKCoreKit
import AppsFlyerLib

class OrderHistoryDetailViewController: BaseViewController {

    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView : UITableView!
    
    @IBOutlet weak var bottomView: UIView!
    var headView: UIView!
    
    var footerView: CartFooterView!
    
    //订单详情数据
    var orderID: Int!
    
    //订单详情中商品数
    var orderDetailArray = [MyOrderDetailInfo]()
    
    //订单中第一个商品
    var orderFirstPro: MyOrderDetailInfo!
    

    var orderStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kOrderHistoryTitleString

        //请求购物车数据
        requestOrderDetailFormServer()
        
        tableView.register(UINib.init(nibName: "OrderDetailCell", bundle: nil), forCellReuseIdentifier: "orderdetail")
        tableView.separatorStyle = .none
        tableView.separatorColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //清空数组
        self.orderDetailArray.removeAll()
        
    }
    // MARK: - 请求订单详情数据
    func requestOrderDetailFormServer(){
        MBProgressHUD.showAdded(to: view, animated: true)
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "ID":orderID,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(OrderDetailInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            print("requestOrderDetailFormServer\(json)")
            for item in json["Data"]["OrderDetailList"].arrayValue {
                let orderItem = MyOrderDetailInfo.init(jsonData: item)
                self.orderDetailArray.append(orderItem)
            }
            //订单中第一个商品
            self.orderFirstPro = self.orderDetailArray[0]
            //UI填充数据
            self.setupTableHeadView()
            self.setupFooterView()
            self.setupBottomView()
            let titleString = "\(kOrderDetailPage)\(self.orderFirstPro.ProductID)"
            configCommonEventMonitor(monitorString: titleString, type: 1)
            self.tableView.reloadData()
            
        }) { (task, error) in
             MBProgressHUD.hide(for: self.view, animated: true)
        }
    }

    // MARK: - 设置headview
    func setupTableHeadView() {
        //头部view
        headView = UIView.init(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 160))
        headView.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        
        let tmpView = UIView(frame: CGRect(x: 0, y: 6, width: kScreenWidth, height: 100))
        tmpView.backgroundColor = .white
        headView.addSubview(tmpView)
        
        let iconView = UIImageView.init(image: UIImage.init(named: "order_location_icon"))
        iconView.frame = CGRect(x: 14, y: tmpView.frame.size.height / 2.0 - 26 / 2.0, width: 22, height: 26)
        tmpView.addSubview(iconView)
        
        let namePhoneLabel = UILabel.init(frame: CGRect(x: iconView.frame.maxX + 10, y: 10, width: view.frame.width - 60, height: 20))
        namePhoneLabel.text = "\((orderFirstPro?.Name)!)  \(orderFirstPro.Phone)"
        namePhoneLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        namePhoneLabel.font = UIFont.init(name: kHelveticaRegular, size: 14.0)
        tmpView.addSubview(namePhoneLabel)
        
        let addressLabel = UILabel.init(frame: CGRect(x: iconView.frame.maxX + 10, y: namePhoneLabel.frame.maxY, width: view.frame.width - 60, height: 70))
        let selectedLaunage = LanguageHelper.standardLanguager().currentLanguage
        var addressStr = ""
        if selectedLaunage == "hi" {
            addressStr = "\(orderFirstPro.Apt), \(orderFirstPro.Address), \(orderFirstPro.City), \(orderFirstPro.Postal_code),\(orderFirstPro.State), \(orderFirstPro.Country) "
        } else {
            addressStr = "\(orderFirstPro.Apt), \(orderFirstPro.Street), \(orderFirstPro.City), \(orderFirstPro.Postal_code), \(orderFirstPro.State), \(orderFirstPro.Country)"
        }
        addressLabel.text = addressStr
        addressLabel.numberOfLines = 0
        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        addressLabel.font = UIFont.init(name: kMSReference, size: 13.0)
        tmpView.addSubview(addressLabel)
        
        let orderView = UIView.init(frame: CGRect(x: 0, y: 112, width: kScreenWidth, height: 42))
        orderView.backgroundColor = UIColor.white
        
        let logLabel = UILabel(frame: CGRect(x: 14, y: 0, width: kScreenWidth, height: 42))
        if orderFirstPro.LogisticsCode.isEmpty {
            logLabel.text = kOHNoTrackingNumberString
        } else {
            logLabel.text = "\(kOHTrackingNumberString): \(orderFirstPro.LogisticsCode)"
        }
        logLabel.font = UIFont(name: kHelveticaRegular, size: 14)
        logLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        orderView.addSubview(logLabel)
        
        headView.addSubview(orderView)
        tableView.tableHeaderView = headView
    }
    
    func setupFooterView() {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 240))
        footerView.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        
        let topView = UIView(frame: CGRect(x: 0, y: 6, width: kScreenWidth, height: 145))
        topView.backgroundColor = UIColor.white
        footerView.addSubview(topView)
        
        let labelW = (kScreenWidth - 32) / 2.0
        let labelF = UIFont(name: kMSReference, size: 14)
        let labelC = kRGBColorFromHex(rgbValue: 0x494949)
        let textF = UIFont(name: kHelveticaRegular, size: 14)
        let textC = kRGBColorFromHex(rgbValue: 0xa6a6a6)
        
        let handleLabel = UILabel(frame: CGRect(x: 16, y: 10, width: labelW, height: 30))
        handleLabel.text = kOHRetailPriceString
        handleLabel.font = labelF
        handleLabel.textColor = labelC
        topView.addSubview(handleLabel)
        
        let handleString = String(format: "%@%.2f",orderFirstPro.Currency,orderFirstPro.Msrp)
        let str2 = NSMutableAttributedString.init(string: handleString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        let handleText = UILabel(frame: CGRect(x: handleLabel.frame.maxX, y: 10, width: labelW, height: 30))
        handleText.attributedText = str2
        handleText.font = textF
        handleText.textAlignment = .right
        handleText.textColor = textC
        topView.addSubview(handleText)
        
        let actualPayLabel = UILabel(frame: CGRect(x: 16, y: handleLabel.frame.maxY, width: labelW, height: 30))
        actualPayLabel.text = kOHItemTotalString
        actualPayLabel.font = labelF
        actualPayLabel.textColor = labelC
        topView.addSubview(actualPayLabel)
        
        let actualText = UILabel(frame: CGRect(x: actualPayLabel.frame.maxX, y: handleLabel.frame.maxY, width: labelW, height: 30))
        actualText.textColor = labelC
        actualText.font = textF
        actualText.textAlignment = .right
        actualText.text = "\(orderFirstPro.Currency)\(orderFirstPro.Total)"
        topView.addSubview(actualText)
        
        let shippingLabel = UILabel(frame: CGRect(x: 16, y: actualPayLabel.frame.maxY, width: 70, height: 30))
        shippingLabel.text = kOHShippingString
        shippingLabel.textColor = labelC
        shippingLabel.font = labelF
        topView.addSubview(shippingLabel)
        
        let shTimeLabel = UILabel(frame: CGRect(x: shippingLabel.frame.maxX, y: actualPayLabel.frame.maxY, width: 100, height: 30))
        shTimeLabel.text = "(\(getCurrentShippingDaysString()))"
        shTimeLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        shTimeLabel.font = UIFont(name: kGothamRoundedLight, size: 14)
        topView.addSubview(shTimeLabel)
        
        let shippingText = UILabel(frame: CGRect(x: actualPayLabel.frame.maxX, y: actualPayLabel.frame.maxY, width: labelW, height: 30))
        shippingText.textColor = labelC
        shippingText.font = textF
        shippingText.textAlignment = .right
        shippingText.text = "\(orderFirstPro.Currency)\(orderFirstPro.Shipping)"
        topView.addSubview(shippingText)
        
        let lineView = UIView(frame: CGRect(x: 16, y: shippingLabel.frame.maxY, width: kScreenWidth - 32, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        topView.addSubview(lineView)
        
        let totalLabel = UILabel(frame: CGRect(x: 16, y: lineView.frame.maxY, width: labelW, height: 44))
        totalLabel.font = UIFont(name: kMSReference, size: 17)
        totalLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        totalLabel.text = kOHOrderTotalString
        topView.addSubview(totalLabel)
        
        let totalText = UILabel(frame: CGRect(x: totalLabel.frame.maxX, y: lineView.frame.maxY, width: labelW, height: 44))
        totalText.text = "\(orderFirstPro.Currency)\(orderFirstPro.OrderTotal)"
        totalText.textAlignment = .right
        totalText.font = UIFont(name: kHelveticaRegular, size: 19)
        totalText.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        topView.addSubview(totalText)
        
        let bottomView = UIView(frame: CGRect(x: 0, y: topView.frame.maxY + 6, width: kScreenWidth, height: 78))
        bottomView.backgroundColor = UIColor.white
        
        let notiseLabel = UILabel(frame: CGRect(x: 16, y: 10, width: kScreenWidth - 32, height: 30))
        notiseLabel.text = "\(kOHReviewsString) (\(orderFirstPro.CommentCount))"
        notiseLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 34))
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        bottomView.addSubview(notiseLabel)
        
        var score: Float = 0
        if let tmp = Float(orderFirstPro.CommentAvg) {
            score = tmp
        }
        let startView = FPStartRateView(frame: CGRect(x: 16, y: 47, width: 90, height: 15), starCount: 5, score: score)
        startView.delegate = self
        startView.isUserInteractionEnabled = false//不支持用户操作
        startView.usePanAnimation = true
        startView.allowUserPan = true//滑动评星
       // starView.allowUnderCompleteStar = false // 完整星星
        bottomView.addSubview(startView)
        
        let scoreLabel = UILabel(frame: CGRect(x: startView.frame.maxX + 5, y: 42, width: 100, height: 30))
        scoreLabel.font = UIFont(name: kGothamRoundedLight, size: 13)
        scoreLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        scoreLabel.text = String(format: "%.1f", score)
        bottomView.addSubview(scoreLabel)
        
        let rightImg = UIImageView(frame: CGRect(x: KScreenWidth - 13 - 15, y: bottomView.frame.size.height / 2.0 - 13 / 2.0, width: 13, height: 13))
        rightImg.image = UIImage(named: "arrowrighticon")
        bottomView.addSubview(rightImg)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(evaluationViewTapped))
        bottomView.addGestureRecognizer(gesture)
        footerView.addSubview(bottomView)
        
        tableView.tableFooterView = footerView
    }
    
    func setupBottomView() {
       
        let lineView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        bottomView.addSubview(lineView)
        
        /**
         *  2 已发货 3 已收货
         *  isReund 是否可以退款 0 不可以退款 1 可以退款
         *  isComment 是否可以评论。1 可以评论 2 不可以评论
         *  orderType 商品的类型。  1 产品    2 freegift  如果是freegift，免费买的话是不可以退款的，不是免费买的可以退款
         */
        let buttonH: CGFloat = 45
        var refundX: CGFloat = 0
        var commentX: CGFloat = 0
        var buttonW: CGFloat = 0
        var refundIsHidden = false
        var commentIsHidden = false
        if self.orderFirstPro.Status == 2 || self.orderFirstPro.Status == 3 { //已发货 已收货
            if self.orderFirstPro.IsRefund == 1 && self.orderFirstPro.IsComment == 1 {
                //可以评论 可以退款
                if self.orderFirstPro.PaymentType == "3" {
                    //货到付款 不可以退款 可以评论
                    buttonW = KScreenWidth - 32
                    refundX = 0
                    refundIsHidden = true
                    commentIsHidden = false
                    commentX = 16
                } else {
                    if self.orderFirstPro.OrderType == 2 && self.orderFirstPro.Shipping == 0{ //FreeGift
                        //不可以退款 免费购买的Freegift
                        buttonW = KScreenWidth - 32
                        refundX = 0
                        refundIsHidden = true
                        commentIsHidden = false
                        commentX = 16
                    } else {
                        //可以退款 可以评论
                        buttonW = (kScreenWidth - 16 * 3) / 2
                        refundX = 16
                        commentX = 32 + buttonW
                        refundIsHidden = false
                        commentIsHidden = false
                    }
                }
            } else if self.orderFirstPro.IsRefund == 0 && self.orderFirstPro.IsComment == 1 {
                //不可以退款 可以评论
                buttonW = KScreenWidth - 32
                refundX = 0
                refundIsHidden = true
                commentIsHidden = false
                commentX = 16
            } else if self.orderFirstPro.IsRefund == 1 && self.orderFirstPro.IsComment == 2 {
                //可以退款 不可以评论
                if self.orderFirstPro.PaymentType == "3" {
                    // 1Paypal 2Stripe 3货到付款不能退款
                    //不可以退款 货到付款
                    hideBottomView()
                    return
                }
                if self.orderFirstPro.OrderType == 2 && self.orderFirstPro.Shipping == 0{
                    //不可以退款 免费购买的Freegift 不可以评论
                    hideBottomView()
                    return
                } else {
                    buttonW = KScreenWidth - 32
                    refundX = 16
                    refundIsHidden = false
                    commentIsHidden = true
                }
                
            } else if self.orderFirstPro.IsRefund == 0 && self.orderFirstPro.IsComment == 2 {
                //不可以评论 不可以退款
                hideBottomView()
                return
            }
            bottomView.isHidden = false
            bottomView.removeConstraint(self.bottomHeightConstraint)
            self.bottomHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 75)
            bottomView.addConstraint(self.bottomHeightConstraint)
            
            let leftButton = UIButton(frame: CGRect(x: refundX, y: bottomView.frame.size.height / 2.0 - buttonH / 2.0, width: buttonW, height: buttonH))
            leftButton.setTitle(kOHRequestRefundString, for: .normal)
            leftButton.setTitleColor(kRGBColorFromHex(rgbValue: 0x000000), for: .normal)
            leftButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: 16)
            leftButton.layer.borderWidth = 1.0
            leftButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
            leftButton.backgroundColor = UIColor.white
            leftButton.tag = 100
            leftButton.isHidden = refundIsHidden
            leftButton.addTarget(self, action: #selector(refundSelectProduct), for: .touchUpInside)
            bottomView.addSubview(leftButton)
            
            let rightButton = UIButton(frame: CGRect(x: commentX, y: bottomView.frame.size.height / 2.0 - buttonH / 2.0, width: buttonW, height: buttonH))
            rightButton.setTitle(kOHCommentString, for: .normal)
            rightButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xe9668f), for: .normal)
            rightButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: 16)
            rightButton.layer.borderWidth = 1.0
            rightButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0xe9668f).cgColor
            rightButton.backgroundColor = UIColor.white
            rightButton.tag = 101
            rightButton.isHidden = commentIsHidden
            rightButton.addTarget(self, action: #selector(commentSelectOrder), for: .touchUpInside)
            bottomView.addSubview(rightButton)
            
        } else if self.orderFirstPro.Status == 1 { //未发货
            if self.orderFirstPro.IsRefund == 0 { //不可以退款
                bottomView.isHidden = true
                bottomView.removeConstraint(self.bottomHeightConstraint)
                self.bottomHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 0)
                bottomView.addConstraint(self.bottomHeightConstraint)
            } else { //可以退款
                if self.orderFirstPro.OrderType == 2 && self.orderFirstPro.Shipping == 0{
                    //不可以退款 免费购买的Freegift
                    hideBottomView()
                    return
                }
                if self.orderFirstPro.PaymentType == "3" {
                    // 1Paypal 2Stripe 3货到付款不能退款
                    //不可以退款 货到付款
                    hideBottomView()
                    return
                }
                bottomView.isHidden = false
                bottomView.removeConstraint(self.bottomHeightConstraint)
                self.bottomHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 75)
                bottomView.addConstraint(self.bottomHeightConstraint)
                
                let leftButton = UIButton(frame: CGRect(x: 16, y: bottomView.frame.size.height / 2.0 - buttonH / 2.0, width: KScreenWidth - 32, height: buttonH))
                leftButton.setTitle(kOHRequestRefundString, for: .normal)
                leftButton.setTitleColor(kRGBColorFromHex(rgbValue: 0x000000), for: .normal)
                leftButton.titleLabel?.font = UIFont(name: kGothamRoundedLight, size: 16)
                leftButton.layer.borderWidth = 1.0
                leftButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
                leftButton.backgroundColor = UIColor.white
                leftButton.tag = 100
                leftButton.isHidden = refundIsHidden
                leftButton.addTarget(self, action: #selector(refundSelectProduct), for: .touchUpInside)
                bottomView.addSubview(leftButton)
            }
            
        }  else {
            bottomView.isHidden = true
            bottomView.removeConstraint(self.bottomHeightConstraint)
            self.bottomHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 0)
            bottomView.addConstraint(self.bottomHeightConstraint)
        }

    }
    func hideBottomView() {
        bottomView.isHidden = true
        bottomView.removeConstraint(self.bottomHeightConstraint)
        self.bottomHeightConstraint = NSLayoutConstraint(item: self.bottomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 0)
        bottomView.addConstraint(self.bottomHeightConstraint)
    }

    @objc func evaluationViewTapped() {
        if !orderFirstPro.CommentAvg.isEmpty {
            let commentVC = CommentViewController()
            commentVC.productID = orderFirstPro.ProductID
            self.navigationController?.present(commentVC, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refundSelectProduct() {
        configCommonEventMonitor(monitorString: kOrderRefundEvent, type: 2)
        let refundVC = RefundViewController()
        refundVC.OrderID = orderFirstPro.ID
        self.navigationController?.pushViewController(refundVC, animated: true)
        
    }
    
    @objc func commentSelectOrder() {
        let window = UIApplication.shared.keyWindow
        let inputCommentView = InputCommentView(frame: UIScreen.main.bounds)
        inputCommentView.addProductCommentInfoBlock = { (param: [String: Any]) -> Void in
            self.addPieceCommentInfoToServer(param: param)
        }
        window!.addSubview(inputCommentView)
        window!.bringSubview(toFront: inputCommentView)
    }
    
    func addPieceCommentInfoToServer(param: [String: Any]) {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let contentString = param["commentContent"]
        let level = param["commentScore"]
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let params = ["UserID":userID,
                     "OrderNo":orderFirstPro.OrderNO,
                     "CommentContent":contentString,
                     "CommentLevel":level,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: view, animated: true)
        FPHttpManager().manager.post(AddProductCommentInfoInterface, parameters: params, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            print("addPieceCommentInfoToServer\(json)")
            let msgString = json["Msg"].stringValue
            showToast(msgString)
            self.hideBottomView()
            return
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            print(error)
        }
    }
    
}
//MARK:TableView Datasource
extension OrderHistoryDetailViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderDetailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetail", for: indexPath) as! OrderDetailCell
        let orderItem = orderDetailArray[indexPath.row]
        cell.configOrderDetailCell(orderDetail: orderItem)
        cell.proImgView.sd_setImage(with: URL.init(string: orderItem.MainImgLink), placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        cell.isUserInteractionEnabled = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}

extension OrderHistoryDetailViewController: FPStarReteViewDelegate {
    //MARK: - 协议代理
    func starRate(view starRateView: FPStartRateView, score: Float) {
        print(score)
    }
}
