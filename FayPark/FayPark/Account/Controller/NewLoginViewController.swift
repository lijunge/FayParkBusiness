//
//  NewLoginViewController.swift
//  FayPark
//
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import FBSDKCoreKit
import AppsFlyerLib

class NewLoginViewController: BaseViewController {

    var idfv = UIDevice.current.identifierForVendor?.uuidString
    var selView:NewLoginView!
    var selectBtn = UIButton()
    
    @objc func sigupSeePassword(btn:UIButton){
        if btn.isSelected == false {
            btn.isSelected = !btn.isSelected
        }else{
            btn.isSelected = !btn.isSelected
        }
        selView.scrollView.passwordTField.isSecureTextEntry = !btn.isSelected
    }
    @objc func loginSeePassword(btn:UIButton){
        if btn.isSelected == false {
            btn.isSelected = !btn.isSelected
        }else{
            btn.isSelected = !btn.isSelected
        }
        selView.scrollView.loginPasswordTField.isSecureTextEntry = !btn.isSelected
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selView = NewLoginView.init(frame: self.view.bounds, type: "2")
        selView.scrollView.forgotBtn.addTarget(self, action: #selector(forgot), for: .touchUpInside)
        self.view = selView
        self.view.backgroundColor = UIColor.white
        selView.headerLiftView.addTarget(self, action: #selector(backBtn), for: .touchUpInside)
        selView.scrollView.passwordSwitchingLoginBtn.addTarget(self, action: #selector(loginSeePassword), for: .touchUpInside)
        selView.scrollView.passwordSwitchingBtn.addTarget(self, action: #selector(sigupSeePassword), for: .touchUpInside)
        selView.selectLoginBlock = { (type) in
            /*
             *type == 1 登陆
             *type == 2 注册
             */
            if (type as! String) == "1"{
                configCommonEventMonitor(monitorString: kLoginPage, type: 1)
                self.login()
            }else{
                configCommonEventMonitor(monitorString: kRegisterPage, type: 1)
                self.sigUP()
            }
        }
        //MARK:facebook授权登陆---------------
        selView.selectAuthorizedBlock = { () in
            configCommonEventMonitor(monitorString: kFacebookRegisterEvent, type: 2)
            self.getUserInfoForPlatform(platformType: .facebook)
        }
        configCommonEventMonitor(monitorString: kLoginPage, type: 1)
    }

    @objc func backBtn(){
        self.dismiss(animated: true, completion: nil)
    }
    @objc func forgot() {//找回密码
        self.present(NewForgitPasswordViewController(), animated: true, completion: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: 登陆方法---------------------------
    func login(){
        configCommonEventMonitor(monitorString: kLoginEvent, type: 2)
        let userID = UserManager.shareUserManager.getUserID()
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let emailString = selView.scrollView.loginEmailTField.text!
        let passString = selView.scrollView.loginPasswordTField.text!
        let loginDic = ["Email":emailString,
                        "PassWord":passString,
                        "UserIdTourist":userID,
                        "CountrySName":countryCode,
                        "CountryCurrency":countryPay] as [String : Any]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FPHttpManager().manager.post(LoginInterface, parameters: loginDic, progress: { (progress) in
            
        }, success: { (task, data) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.configLoginSuccessFayPark()
            //请求成功
            let json = JSON(data as! Data)
            print(json)
            if json["Code"].intValue == 200 {
                //初始化user模型
                var user = User.init(jsonData: json["Data"]["UserList"][0])
                user.CarProductCount = json["Data"]["CarProductCount"].intValue
                //缓存用户登录信息
                UserManager.shareUserManager.setUser(user: user)

                //发送通知,让首页Browse刷新
                NotificationCenter.default.post(name: Notification.Name(rawValue: "alreadyLogin"), object: nil, userInfo: nil)
                self.dismiss(animated: true, completion: nil)
            } else {
                //登录失败
                showToast(json["Msg"].string!)
            }
            
            
        }) { (task, error) in
            //请求失败
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    //MARK: 注册方法---------------------------
    func sigUP() {
        configCommonEventMonitor(monitorString: kRegisterEvent, type: 2)
        let UserIdTouristId = UserManager.shareUserManager.getUserID()
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let parm = ["Email":self.selView.scrollView.emailTField.text!, "PassWord":self.selView.scrollView.passwordTField.text!, "LastName":self.selView.scrollView.lastNameTField.text!, "FirstName":self.selView.scrollView.firstNameTField.text!,
            "RegisterType":"1",
            "UniqueValue":self.idfv as Any,
            "Gender":"0",
            "UserIdTourist":UserIdTouristId,
            "CountrySName":countryCode,
            "CountryCurrency":countryPay] as [String : Any]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FPHttpManager().manager.post(RegisterInterface, parameters: parm, progress: { (progress) in
            
        }, success: { (task, data) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(data as! Data)
            if json["Code"].intValue == 200 {
                //注册成功
                self.configRegisterSuccessFayPark()
                //初始化user模型
                var user = User.init(jsonData: json["Data"]["UserList"][0])
                user.CarProductCount = json["Data"]["CarProductCount"].intValue
                let tabbarVC = UIApplication.shared.keyWindow?.rootViewController
                if json["Data"]["CarProductCount"] == 0 {
                    tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "1"
                } else {
                    tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "\(json["Data"]["CarProductCount"].intValue+1)"
                }
                if #available(iOS 10.0, *) {
                    tabbarVC?.childViewControllers[2].tabBarItem.badgeColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
                }
                //缓存用户登录信息
                UserManager.shareUserManager.setUser(user: user)
                self.dismiss(animated: true, completion: nil)//弹出控制器
                //发送通知,让首页Browse刷新
                NotificationCenter.default.post(name: Notification.Name(rawValue: "alreadyLogin"), object: nil, userInfo: nil)
                
            } else {
                //注册失败
                showToast(json["Msg"].string!)
            }
            
            
        }) { (task, error) in
            //请求失败
            //print(error)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserInfoForPlatform(platformType: UMSocialPlatformType) {
        let mbpro = MBProgressHUD.showAdded(to: self.view, animated: true)
        mbpro.mode = .indeterminate
        //mbpro.label.text = "Loading..."
        MBProgressHUD.showAdded(to: self.view, animated: true)
        UMSocialManager.default().getUserInfo(with: platformType, currentViewController: self) { (result, error) in
            guard let resp: UMSocialUserInfoResponse = result as? UMSocialUserInfoResponse else {
                //print("facebook授权拒绝")
                MBProgressHUD.hide(for: self.view, animated: true)
                mbpro.mode = .text
                mbpro.hide(animated: true, afterDelay: 1.0)
                mbpro.label.text = kFaceBookAuthCancelString
                return
            }
            MBProgressHUD.hide(for: self.view, animated: true)
            self.configRegisterSuccessFaceBook()
            let userName = resp.name
            var firstName = ""
            var lastName = ""
            //判断有无空格
            if let index = userName?.index(of: " ") {
                firstName = String(userName![..<index])
                lastName = String(userName![index...])
            } else {
                firstName = userName!
                lastName = userName!
            }
            let UserIdTouristId = UserManager.shareUserManager.getUserID()
            let countryCode = getCurrentCountryCodeString()
            let countryPay = getCurrentPayCharacter()
            let parm = ["Token": "\(resp.uid!)",
                "UserName":"\(resp.name!)",
                "FirstName":firstName,
                "LastName":lastName,
                "Gender":"\(resp.gender!)",
                "ImgUrl":"\(resp.iconurl!)",
                "RegisterType":"1",
                "UniqueValue":self.idfv!,
                "UserIdTourist":UserIdTouristId,
                "CountrySName":countryCode,
                "CountryCurrency":countryPay]
            
            FPHttpManager().manager.post(FacebookRegInterface, parameters: parm, progress: { (progress) in
            }, success: { (task, data) in
                let json = JSON(data as! Data)
                print(json)
                if json["Code"].intValue == 200 {
                    
                    self.configLoginSuccessFaceBook()
                    //初始化user模型
                    var user = User.init(jsonData: json["Data"]["UserList"][0])
                    user.CarProductCount = json["Data"]["CarProductCount"].intValue
                    //缓存用户登录信息
                    UserManager.shareUserManager.setUser(user: user)
                    //弹出控制器
                    self.dismiss(animated: true, completion: nil)//弹出控制器
                    
                    //发送通知,让首页Browse刷新
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "alreadyLogin"), object: nil, userInfo: nil)
                    
                    let tabbarVC = UIApplication.shared.keyWindow?.rootViewController
                    if json["Data"]["CarProductCount"] == 0 {
                        tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "1"
                    } else {
                        tabbarVC?.childViewControllers[2].tabBarItem.badgeValue = "\(json["Data"]["CarProductCount"].intValue+1)"
                    }
                    if #available(iOS 10.0, *) {
                        tabbarVC?.childViewControllers[2].tabBarItem.badgeColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
                    }
                } else {
                    //登录失败
                    let mb = MBProgressHUD.showAdded(to: self.view, animated: true)
                    mb.mode = MBProgressHUDMode.text
                    mb.label.text = json["Msg"].string
                    mb.hide(animated: true, afterDelay: 2.0)
                }
                
            }) { (task, error) in
                print(error)
            }
            
        }
    }
    //Facebook 注册
    func configRegisterSuccessFaceBook() {
        //FBS
        let params = [FBSDKAppEventParameterNameRegistrationMethod : kRegisterFacebookSuccess]
        FBSDKAppEvents.logEvent(FBSDKAppEventNameCompletedRegistration, parameters: params)
        AppsFlyerTracker.shared().trackEvent(kRegisterFacebookSuccess, withValues: [:])
        FPHttpManager.init().fayParkMonitor(userID: UserManager.shareUserManager.getUserID(), staicsType: "3", name: kRegisterFacebookSuccess, productID: "0")
    }
    //Facebook 登陆
    func configLoginSuccessFaceBook() {
        FBSDKAppEvents.logEvent(kLoginFacebookSuccess)
        //Apps
        AppsFlyerTracker.shared().trackEvent(kLoginFacebookSuccess, withValues: [:])
        //FP
        FPHttpManager.init().fayParkMonitor(userID: UserManager.shareUserManager.getUserID(), staicsType: "3", name: kLoginFacebookSuccess, productID: "0")
    }
    //Faypark 注册
    func configRegisterSuccessFayPark() {
        //FBS
        let params = [FBSDKAppEventParameterNameRegistrationMethod : kRegisterFayParkSuccess]
        FBSDKAppEvents.logEvent(FBSDKAppEventNameCompletedRegistration, parameters: params)
        AppsFlyerTracker.shared().trackEvent(kRegisterFayParkSuccess, withValues: [:])
        FPHttpManager.init().fayParkMonitor(userID: UserManager.shareUserManager.getUserID(), staicsType: "3", name: kRegisterFayParkSuccess, productID: "0")
    }
    //Faypark 登陆
    func configLoginSuccessFayPark() {
        //FayPark
        FPHttpManager.init().fayParkMonitor(userID: UserManager.shareUserManager.getUserID(), staicsType: "3", name: kLoginFayParkSuccess, productID: "0")
        //Fbs
        FBSDKAppEvents.logEvent(kLoginFayParkSuccess)
        //Apps
        AppsFlyerTracker.shared().trackEvent(kLoginFayParkSuccess, withValues: [:])
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "alreadyLogin"), object: nil)/// 移除通知
    }
}
