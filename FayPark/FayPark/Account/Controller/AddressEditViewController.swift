//
//  AddressEditViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/12.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
class AddressEditViewController: BaseViewController {

    var backScrollView: UIScrollView!
    var addressInfo: MyAddressInfo!
    var isFirstEditAddress = false //是否是第一次没有地址时进入的页面
    var isPayEditAddress = false   //是否是支付页面添加进入的
    //新增 或者是 编辑
    typealias AddAddressBlock = ((_ addressInfo: MyAddressInfo,_ isUpdate: Bool) -> Void)
    var addAddressInfoBlock: AddAddressBlock?
    
    //第一次支付
    //底部支付信息
    var paymentInfo: CartPaymentInfo?
    //创建订单需要的信息
    var payPramsInfo: CreateTmpPayOrderInfo?
    //创建订单的返回信息
    var tmpOrderSuccessInfo: TmpPayOrderSuccessInfo?
    
    var itemParamsArr = [[String: Any]]() //监测使用到的item
    var itemProductIDString = "" //监测使用到的productID组合
    var itemProductCount = 0     //监测使用到的支付个数
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFirstEditAddress {
            title = localized("kPMAddressEditTitle")
        } else {
            title = kAddressEditTitleString
        }
        setupBackScrollView()
        configCommonEventMonitor(monitorString: kShippingAddressPage, type: 1)
    }

    func setupBackScrollView() {
        
        backScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        backScrollView.backgroundColor = UIColor.white
        backScrollView.isScrollEnabled = true
        backScrollView.showsVerticalScrollIndicator = false
        backScrollView.showsHorizontalScrollIndicator = false
        view.addSubview(backScrollView)
        
        let editAddressView = EditAddressView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight + 150))
        editAddressView.isFirstEdit = isFirstEditAddress
        editAddressView.selectAddressInfo = addressInfo
        //编辑。或者是 新增
        editAddressView.doneButtonTappedBlock = { (addressInfo: MyAddressInfo,isUpdate: Bool) -> Void in
            self.updateNewAddressToServer(addressInfo: addressInfo,isUpdate: isUpdate)
        }
        backScrollView.addSubview(editAddressView)
        backScrollView.contentSize = CGSize(width: kScreenWidth, height: kScreenHeight + 150)
    }
    
    func updateNewAddressToServer(addressInfo: MyAddressInfo,isUpdate: Bool) {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "AddrID": addressInfo.AddrID,
                     "Address": addressInfo.Address,
                     "Apt": addressInfo.Apt,
                     "City": addressInfo.City,
                     "Name": addressInfo.Name,
                     "ZipCode": addressInfo.ZipCode,
                     "Phone": addressInfo.Phone,
                     "Contry": addressInfo.Contry,
                     "States": addressInfo.States,
                     "CollectEmail": addressInfo.CollectEmail,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        FPHttpManager().manager.post(AccountEditAddressInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            print("updateNewAddressToServer\(json)")
            var updateInfo = addressInfo
            let addrID: Int = json["Data"]["AddrId"].intValue
            updateInfo.AddrID = addrID
            if self.isFirstEditAddress {
                configCommonEventMonitor(monitorString: kFirstCheckOutSaveShippingAddressEvent, type: 2)
                //首次支付没有地址的时候进入，地址更新完毕开始进入信用卡信息填写
                let shippingAddressVC = ShippingAddressViewController()
                shippingAddressVC.addressInfo = updateInfo
                shippingAddressVC.payOrderInfo = self.payPramsInfo
                shippingAddressVC.paymentInfo = self.paymentInfo
                shippingAddressVC.tmpOrderSuccessInfo = self.tmpOrderSuccessInfo
                shippingAddressVC.itemProductIDString = self.itemProductIDString
                shippingAddressVC.itemProductCount = self.itemProductCount
                shippingAddressVC.itemParamsArr = self.itemParamsArr
                self.navigationController?.pushViewController(shippingAddressVC, animated: true)
            } else {
                if self.addAddressInfoBlock != nil {
                    self.addAddressInfoBlock!(updateInfo,isUpdate)
                }
                if self.isPayEditAddress {
                    //checkout页面进入的保存地址
                    configCommonEventMonitor(monitorString: kCheckOutSaveShippingAddressEvent, type: 2)
                } else {
                    configCommonEventMonitor(monitorString: kAccountSaveShippingAddressEvent, type: 2)
                }
                self.navigationController?.popViewController(animated: true)
            }
            
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    override func leftBackButtonTapped() {
        if self.isFirstEditAddress {
            configCommonEventMonitor(monitorString: kFirstShippingBackEvent, type: 2)
        }
        navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
