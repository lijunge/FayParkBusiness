//
//  CommentViewController.swift
//  FayPark
//
//  Created by faypark on 2018/6/29.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import MJRefresh
class CommentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
   
    let cellIndentifier = "CommentTableViewCell"
    //页面索引
    var pageIndex = 1
    var productCommentInfo: ProductCommentList?
    var commentListArr = [CommentInfo]()
    var productID: String? {
        didSet {
            if let tmpProductID = productID {
                requestCommentListFromServer(productID: tmpProductID)
            }
        }
    }
    var rightButton: UIButton = {
        let rightButton = UIButton(type: UIButtonType.custom)
        rightButton.setImage(UIImage(named: "cart_icon_close"), for: .normal)
        rightButton.titleLabel?.isHidden = true
        rightButton.contentHorizontalAlignment = .center
        let backW: CGFloat = kScreenWidth > 375.0 ? 50 : 44
        rightButton.frame = CGRect(x: KScreenWidth - 10 - backW, y: 10, width: backW, height: 40)
        rightButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        return rightButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: rightButton)
        addRefreshControl()
    }
    func setupTableHeaderView(productCommentInfo: ProductCommentList) {

        let headerView = UIView(frame: CGRect(x: 0, y: 20, width: KScreenWidth, height: 105))
        headerView.backgroundColor = UIColor.white
        
        let reviewLabel = UILabel(frame: CGRect(x: 14, y: 10, width: KScreenWidth - 100, height: 40))
        reviewLabel.text = String(format: "%@(%d)",kCommentReviewsString, productCommentInfo.CommentCount)
        reviewLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        reviewLabel.font = UIFont(name: kHelveticaRegular, size: kSizeFrom750(x: 40))
        headerView.addSubview(reviewLabel)
        
        let avageLabel = UILabel(frame: CGRect(x: 14, y: reviewLabel.frame.maxY + 10, width: 110, height: 30))
        avageLabel.textColor = kRGBColorFromHex(rgbValue: 0x444444)
        avageLabel.font = UIFont(name: kGothamRoundedLight, size: 14)
        avageLabel.text = kCommnetAverageRatingString
        headerView.addSubview(avageLabel)
        
        var commentVV: Float = 0.0
        if let commentVg = Float(productCommentInfo.CommentAvg) {
            commentVV = commentVg
        }
        let rateView = FPStartRateView(frame: CGRect(x: avageLabel.frame.maxX + 5, y: reviewLabel.frame.maxY + 10 + 7, width: 90, height: 15), starCount: 5, score: commentVV)
        rateView.delegate = self
        rateView.isUserInteractionEnabled = false//不支持用户操作
        rateView.usePanAnimation = true
        rateView.allowUserPan = true//滑动评星
        headerView.addSubview(rateView)
        
        let countLabel = UILabel(frame: CGRect(x: rateView.frame.maxX + 5, y: reviewLabel.frame.maxY + 12, width: 100, height: 30))
        countLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        countLabel.font = UIFont(name: kGothamRoundedLight, size: 14)
        countLabel.text = productCommentInfo.CommentAvg
        headerView.addSubview(countLabel)
        
        let lineView = UIView(frame: CGRect(x: 0, y: countLabel.frame.maxY + 15, width: KScreenWidth , height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        headerView.addSubview(lineView)
        
        headerView.addSubview(self.rightButton)
        tableView.tableHeaderView = headerView
     
    }
    // MARK: - 添加刷新控件
    func addRefreshControl() {
        self.tableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.pageIndex = 1
            self.commentListArr = [CommentInfo]()
            if let new = self.productID {
                self.productID = new
                return
            }
            self.tableView.mj_header.endRefreshing()
        })
        //上拉加载更多
        self.tableView.mj_footer = MJRefreshAutoFooter.init(refreshingBlock: {
            self.pageIndex += 1
            if let new = self.productID {
                self.productID = new
                return
            }
            self.tableView.mj_footer.endRefreshing()
        })
    }
    func requestCommentListFromServer(productID: String) {
        MBProgressHUD.showAdded(to: view, animated: true)
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["ProductID":productID,
                     "Index":self.pageIndex,
                     "Count":10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay] as [String : Any]
        FPHttpManager().manager.post(ProductCommentListInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            print("requestCommentListFromServer\(json)")
            if json["Code"].stringValue == "400" {
                self.tableView.mj_header.endRefreshing()
                self.tableView.mj_footer.endRefreshing()
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                return
            }
            self.productCommentInfo = ProductCommentList(json: json["Data"])
            if let productInfo = self.productCommentInfo {
                for data in productInfo.ProductCommentList {
                    self.commentListArr.append(data)
                }
                self.setupTableHeaderView(productCommentInfo: productInfo)
            }
            
            self.tableView.reloadData()
            self.tableView.mj_header.endRefreshing()
            self.tableView.mj_footer.endRefreshing()
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tableView.reloadData()
            self.tableView.mj_header.endRefreshing()
            self.tableView.mj_footer.endRefreshing()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @objc func dismissButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    

}
extension CommentViewController: UITableViewDelegate,UITableViewDataSource {
    //MARK: - tableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentListArr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NewProductReviewsCell.ReviewsCell(tableView: tableView)
        cell.selectionStyle = .none
        if self.commentListArr.count != 0{
            let commentInfo = self.commentListArr[indexPath.row]
            cell.updateCommentCellData(comment: commentInfo)
            //_ = cell.configCommentCell(comment: commentInfo)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.commentListArr.count != 0 {
            return NewProductReviewsCell().getCommentCellHeight(comment: self.commentListArr[indexPath.row])
            //return NewProductReviewsCell().configCommentCell(comment: self.commentListArr[indexPath.row])
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
}
extension CommentViewController: FPStarReteViewDelegate {
    
}
