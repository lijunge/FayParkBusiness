//
//  OrderHistoryViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/6.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import MJRefresh
import FBSDKCoreKit
import AppsFlyerLib
import SDCycleScrollView
import JTSImageViewController
import AFNetworking
import SwiftyJSON
import SDWebImage
import FBSDKCoreKit

class OrderHistoryViewController: BaseViewController {
    //页面索引
    var pageIndex = 1
    //订单数组
    var historyOrderArr: [MyHistoryListInfo] = Array()
    var tableView: UITableView!
    
    let orderCellIndetifier = "OrderHistoryCell"
    override func viewDidLoad() {
        super.viewDidLoad()
        title = kOrderTitleString
        
        configCommonEventMonitor(monitorString: kOrderHistoryPage, type: 1)
      
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight), style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib.init(nibName: "OrderHistoryCell", bundle: nil), forCellReuseIdentifier: orderCellIndetifier)
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tableView.tableFooterView = UIView()
        view.addSubview(tableView)
    
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        
        requestOrderListFromServer()
        
        //添加刷新控件
        addRefreshControl()
    }
    
    // MARK: - 添加刷新控件
    func addRefreshControl() {
        self.tableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.pageIndex = 1
            self.historyOrderArr.removeAll()
            self.requestOrderListFromServer()
        })
        //上拉加载更多
        self.tableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {
            self.pageIndex += 1
            self.requestOrderListFromServer()
        })
    }
    
    // MARK: - 请求历史订单列表数据
    func requestOrderListFromServer() {
        
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "Index":self.pageIndex,
                     "Count":10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: view, animated: true)
        FPHttpManager().manager.post(MyHistoryOrderListInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            print("requestOrderListFromServer\(json)")
            if json["Code"].stringValue == "400" {
                if self.pageIndex == 1 {
                    let newView = NoContentView.init(frame: self.view.bounds, type: 0)
                    newView.selectAction(block: {
                        let tabbarVC = self.parent?.parent as! UITabBarController
                        tabbarVC.selectedIndex = 0
                        self.navigationController?.popViewController(animated: false)
                    })
                    self.view.addSubview(newView)
                }
                self.tableView.mj_header.endRefreshing()
                self.tableView.mj_footer.endRefreshing()
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                return
            }
            
            for item in json["Data"]["PaymentBatchList"].arrayValue {
                let orderList = MyHistoryListInfo(jsonData: item)
                self.historyOrderArr.append(orderList)
            }
            self.tableView.reloadData()
            self.tableView.mj_header.endRefreshing()
            self.tableView.mj_footer.endRefreshing()
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tableView.reloadData()
            self.tableView.mj_header.endRefreshing()
            self.tableView.mj_footer.endRefreshing()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //物流追踪
    func trackingSelectedOrder(selectOrder: MyOrderInfo,sender: UIButton) {
        let orderNoString = selectOrder.LogisticsCode
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            let webVC = SupportWebController()
            webVC.urlString = "\(ProductTrackingURLString)\(orderNoString)"
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    //确认收货
    func confirmReceiveProductForOrder(order: MyOrderInfo,sender: UIButton,selectedIndex: IndexPath) {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let orderID = order.ID
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "OrderID":orderID,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: view, animated: true)
        FPHttpManager().manager.post(ConfirmReceiveProductInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            print("confirmReceiveProductForOrder\(json)")
            if json["Code"].stringValue == "400" {
                return
            }
            var historyListInfo = self.historyOrderArr[selectedIndex.section]
            var orderListArr = historyListInfo.OrderList
            var orderItem = orderListArr[selectedIndex.row]
            orderItem.Status = 3
            orderListArr.remove(at: selectedIndex.row)
            orderListArr.insert(orderItem, at: selectedIndex.row)
            historyListInfo.OrderList = orderListArr
            self.historyOrderArr.remove(at: selectedIndex.section)
            self.historyOrderArr.insert(historyListInfo, at: selectedIndex.section)
            self.tableView.reloadData()

            let jsonString = json["Msg"].stringValue
            showToast(jsonString)
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    //评论
    func commentSelectedOrder(selectOrder: MyOrderInfo,sender: UIButton,selectedIndex: IndexPath) {
        let window = UIApplication.shared.keyWindow
        let inputCommentView = InputCommentView(frame: UIScreen.main.bounds)
        inputCommentView.addProductCommentInfoBlock = { (param: [String: Any]) -> Void in
            self.addPieceCommentInfoToServer(param: param,orderNo: selectOrder.OrderNO,selectedIndex: selectedIndex)
        }
        window!.addSubview(inputCommentView)
        window!.bringSubview(toFront: inputCommentView)
    }
    func addPieceCommentInfoToServer(param: [String: Any],orderNo: String,selectedIndex: IndexPath) {
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let contentString = param["commentContent"]
        let level = param["commentScore"]
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let params = ["UserID":userID,
                      "OrderNo":orderNo,
                      "CommentContent":contentString,
                      "CommentLevel":level,
                      "CountrySName":countryCode,
                      "CountryCurrency":countryPay]
        MBProgressHUD.showAdded(to: view, animated: true)
        FPHttpManager().manager.post(AddProductCommentInfoInterface, parameters: params, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON.init(result as Any)
            print("addPieceCommentInfoToServer\(json)")
            let msgString = json["Msg"].stringValue
            showToast(msgString)
            var historyListInfo = self.historyOrderArr[selectedIndex.section]
            var orderListArr = historyListInfo.OrderList
            var orderItem = orderListArr[selectedIndex.row]
            orderItem.IsComment = 2
            orderListArr.remove(at: selectedIndex.row)
            orderListArr.insert(orderItem, at: selectedIndex.row)
            historyListInfo.OrderList = orderListArr
            self.historyOrderArr.remove(at: selectedIndex.section)
            self.historyOrderArr.insert(historyListInfo, at: selectedIndex.section)
            self.tableView.reloadData()
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            print(error)
        }
    }
}

extension OrderHistoryViewController: UITableViewDelegate,UITableViewDataSource {
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let orderListArr = historyOrderArr[section].OrderList
        return orderListArr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return historyOrderArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: orderCellIndetifier, for: indexPath) as! OrderHistoryCell
        cell.selectionStyle = .none
        let orderListArr = historyOrderArr[indexPath.section].OrderList
        let orderItem = orderListArr[indexPath.row]
        cell.configOrderCell(orderInfo: orderItem)
        cell.configButtonTapped = { (sender: UIButton) -> Void in
            if sender.tag == 100 {
                //左边的
                self.trackingSelectedOrder(selectOrder: orderItem,sender: sender)
            } else {
                //右边的 2->confirm recepipt 3->comments
                if orderItem.Status == 2 {
                    self.confirmReceiveProductForOrder(order: orderItem,sender: sender,selectedIndex: indexPath)
                } else {
                    self.commentSelectedOrder(selectOrder: orderItem,sender: sender,selectedIndex: indexPath)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let orderListArr = historyOrderArr[indexPath.section].OrderList
        let orderItem = orderListArr[indexPath.row]
        if orderItem.Status == 1 || orderItem.Status == 9 {
            return 180
        }
        return 230
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //跳转到订单详情
        let orderListArr = historyOrderArr[indexPath.section].OrderList
        let orderDetailHis = OrderHistoryDetailViewController()
        orderDetailHis.orderID = orderListArr[indexPath.row].ID
        orderDetailHis.orderStatus = orderListArr[indexPath.row].Status
        navigationController?.pushViewController(orderDetailHis, animated: true)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let historyInfo = historyOrderArr[section]
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 98))
        headerView.backgroundColor = UIColor.white
        
        let timeLabel = UILabel(frame: CGRect(x: 20, y: 8, width: kScreenWidth - 30, height: 20))
        timeLabel.text = "\(historyInfo.OrderTime) \(kODStoreString)"
        timeLabel.font = UIFont(name: kHelveticaRegular, size: 14)
        timeLabel.textColor = kRGBColorFromHex(rgbValue: 0x444444)
        headerView.addSubview(timeLabel)
        
        let textFont = UIFont(name: kMSReference, size: 14)
        let textColor = kRGBColorFromHex(rgbValue: 0x494949)
        let labelWidth = (kScreenWidth - 36) / 2.0
        let paymentLabel = UILabel(frame: CGRect(x: 20, y: timeLabel.frame.maxY + 10, width: labelWidth, height: 20))
        paymentLabel.text = kODSubTotalString
        paymentLabel.font = textFont
        paymentLabel.textColor = textColor
        headerView.addSubview(paymentLabel)
        
        let paymentTextLabel = UILabel(frame: CGRect(x: paymentLabel.frame.maxX, y: timeLabel.frame.maxY + 10, width: labelWidth, height: 20))
        paymentTextLabel.font = UIFont(name: kHelveticaRegular, size: 15)
        paymentTextLabel.textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        paymentTextLabel.text = "\(historyInfo.Currency)\(historyInfo.OrderPayTotal)"
        paymentTextLabel.textAlignment = .right
        headerView.addSubview(paymentTextLabel)
        
        let shippingLabel = UILabel(frame: CGRect(x: 20, y: paymentLabel.frame.maxY + 10, width: 70, height: 20))
        shippingLabel.text = kODShippingString
        shippingLabel.font = textFont
        shippingLabel.textColor = textColor
        headerView.addSubview(shippingLabel)
        
        let dayLabel = UILabel(frame: CGRect(x: shippingLabel.frame.maxX, y: paymentLabel.frame.maxY + 10, width: 100, height: 20))
        if historyInfo.Currency == "Rs." {
            //印度
             dayLabel.text = String(format: "(%@)", localized("kCODDayString"))
        } else {
             dayLabel.text = String(format: "(%@)", localized("kShippingDayString"))
        }
        dayLabel.font = UIFont(name: kGothamRoundedLight, size: 14)
        dayLabel.textColor = kRGBColorFromHex(rgbValue: 0x999999)
        headerView.addSubview(dayLabel)
        
        let shippingTextLabel = UILabel(frame: CGRect(x: KScreenWidth - 16 - labelWidth, y: paymentLabel.frame.maxY + 10, width: labelWidth, height: 20))
        shippingTextLabel.font = UIFont(name: kHelveticaRegular, size: 15)
        shippingTextLabel.textColor = textColor
        shippingTextLabel.text = "\(historyInfo.Currency)\(historyInfo.ShippingTotal)"
        shippingTextLabel.textAlignment = .right
        headerView.addSubview(shippingTextLabel)
        var lineViewY: CGFloat = 97
        if historyInfo.PaymentMethod == "3" { //货到付款
            let codLabel = UILabel(frame: CGRect(x: 20, y: shippingLabel.frame.maxY + 10, width: 70, height: 20))
            codLabel.text = localized("kOHCODString")
            codLabel.font = textFont
            codLabel.textColor = textColor
            headerView.addSubview(codLabel)
            
            let codMoneyLabel = UILabel(frame: CGRect(x: KScreenWidth - 16 - 100, y: shippingLabel.frame.maxY + 10, width: 100, height: 20))
            codMoneyLabel.font = UIFont(name: kHelveticaRegular, size: 15)
            codMoneyLabel.textColor = textColor
            let priceString = "\(historyInfo.Currency)\(kCashServiceString)"
            let str2 = NSMutableAttributedString.init(string: priceString)
            str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
            codMoneyLabel.attributedText = str2
            codMoneyLabel.textAlignment = .right
            headerView.addSubview(codMoneyLabel)
            lineViewY = 127
        }
        let lineView = UIView(frame: CGRect(x: 0, y: lineViewY, width: KScreenWidth, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        headerView.addSubview(lineView)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let historyInfo = historyOrderArr[section]
        if historyInfo.PaymentMethod == "3" {
            return 128
        }
        return 98
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        headerView.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sectionHeaderHeight: CGFloat = 128
        if scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0 {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0)
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0)
        }
    }
    
}
