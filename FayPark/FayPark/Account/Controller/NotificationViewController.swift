//
//  NotificationViewController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/9.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import MJRefresh
import FBSDKCoreKit
import AppsFlyerLib

class NotificationViewController: BaseViewController {
    //请求商品分类页数
    var pageIndex = 1
    //消息数组
    var notiArray: [NotiMessage] = Array()
    
    var tableView: UITableView!
    var noDataView = UIView()
    
    let notificationCellIdentifier = "NotificationCell"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.init(rawValue: 0)
        title = kNotificationTitleString
        view.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        
        configCommonEventMonitor(monitorString: kNotificationPage, type: 1)
        
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight), style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib.init(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: notificationCellIdentifier)
        tableView.rowHeight = 109
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        tableView.tableFooterView = UIView()
        view.addSubview(tableView)
        
        //请求消息中心
        requestNotiCenterFromServer()
        //添加上拉下拉
        addRefreshControl()
    }
    func setupNoDataView() {
        
        noDataView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        noDataView.backgroundColor = UIColor.white
        view.addSubview(noDataView)
        
        let lineView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        noDataView.addSubview(lineView)
        
        let tmpView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 220))
        tmpView.backgroundColor = UIColor.white
        tmpView.center = self.view.center
        noDataView.addSubview(tmpView)
        
        let imageView = UIImageView(frame: CGRect(x: kScreenWidth / 2.0 - 126 / 2.0, y: 0, width: 127, height: 127))
        imageView.image = UIImage(named: "notifications_icon_no")
        tmpView.addSubview(imageView)
        
        let messageLabel = UILabel(frame: CGRect(x: 0, y: imageView.frame.maxY + 22, width: kScreenWidth, height: 18))
        messageLabel.text = kNCNoMessageString
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: kGothamRoundedLight, size: 18)
        messageLabel.textColor = kRGBColorFromHex(rgbValue: 0xb3b3b3)
        tmpView.addSubview(messageLabel)
        
        let detailLabel = UILabel(frame: CGRect(x: 0, y: messageLabel.frame.maxY + 22, width: kScreenWidth, height: 30))
        detailLabel.text = kNCDetailMessageString
        detailLabel.font = UIFont(name: kGothamRoundedLight, size: 14)
        detailLabel.textColor = kRGBColorFromHex(rgbValue: 0xb3b3b3)
        detailLabel.numberOfLines = 0
        detailLabel.textAlignment = .center
        tmpView.addSubview(detailLabel)
    }
    // MARK: - 添加刷新控件
    func addRefreshControl() {
        //下拉刷新
        self.tableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: {
            self.tableView.reloadData()
            self.tableView.mj_header.endRefreshing()
        })
        self.tableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {
            self.pageIndex += 1
            self.requestNotiCenterFromServer()
        })
    }
    
    //MARK: - 请求消息中心数据
    func requestNotiCenterFromServer() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        guard let userID = UserDefaults.standard.object(forKey: "kUserID") else {
            return
        }
        let countryCode = getCurrentCountryCodeString()
        let countryPay = getCurrentPayCharacter()
        let param = ["UserID":userID,
                     "Index":self.pageIndex,
                     "Count":10,
                     "CountrySName":countryCode,
                     "CountryCurrency":countryPay]
        
        FPHttpManager().manager.post(NotiCenterInterface, parameters: param, progress: nil, success: { (task, result) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let json = JSON(result as! Data)
            let tmpArr = json["Data"]["NotificationsList"]
            if json["Code"].stringValue == "400" {
                self.tableView.mj_footer.endRefreshingWithNoMoreData()
                return
            }
            if tmpArr.count == 0 {
                if self.pageIndex == 1 {
                    self.setupNoDataView()
                    self.noDataView.isHidden = false
                    self.tableView.isHidden = true
                } else {
                    self.noDataView.isHidden = true
                    self.tableView.isHidden = false
                }
            }
            for item in json["Data"]["NotificationsList"].arrayValue {
                let noti = NotiMessage.init(jsonData: item)
                self.notiArray.append(noti)
            }
            //刷新tableView
            self.tableView.reloadData()
            self.tableView.mj_footer.endRefreshing()
            
        }) { (task, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension NotificationViewController: UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notiArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: notificationCellIdentifier, for: indexPath) as! NotificationCell
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        
        let notiData = notiArray[indexPath.row]
        cell.dateLabel.text = notiData.CreateDate
        cell.notiLabel.text = notiData.Body
        
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        headerView.backgroundColor = kRGBColorFromHex(rgbValue:0xf5f5f5)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
}
