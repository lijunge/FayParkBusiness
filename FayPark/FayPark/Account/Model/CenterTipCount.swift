//
//  CenterTipCount.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/29.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CenterTipCount {
    //消息数
    var NotificationsCount: Int
    //购物车内数
    var CarProductCount: Int
    //我的收藏数
    var MyFavoritesCount: Int
    //订单数
    var OrderCount: Int

    init(jsonData: JSON) {
        self.NotificationsCount = jsonData["NotificationsCount"].intValue
        self.CarProductCount = jsonData["CarProductCount"].intValue
        self.MyFavoritesCount = jsonData["MyFavoritesCount"].intValue
        self.OrderCount = jsonData["OrderCount"].intValue
    }
}
