//
//  NotiMessage.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

class NotiMessage {
    //消息ID
    var ID: Int
    //消息内容
    var Body: String
    //创建时间
    var CreateDate: String
    //已读/未读
    var State: String
    
    init(jsonData: JSON) {
        self.ID = jsonData["ID"].intValue
        self.Body = jsonData["Body"].stringValue
        self.CreateDate = jsonData["CreateDate"].stringValue
        self.State = jsonData["State"].stringValue
    }
}
