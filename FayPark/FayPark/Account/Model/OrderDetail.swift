//
//  OrderDetail.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/30.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

//class OrderDetail: MyOrderInfo {
//    //产品总价
//    var Total: Double
//    //运费价格
//    var Shipping: Double
//    //折扣
//    var Discount: Double
//    //订单总价
//    var OrderTotal: Double
//    //用户名字
//    var Name: String
//    //电话号码
//    var Phone: String
//    //街道
//    var Street: String
//    //城市
//    var City: String
//    //州
//    var State: String
//    //国家
//    var Country: String
//    //邮编
//    var Postal_code: String
//    //门牌号
//    var Apt: String
//    //付款方式
//    var PaymentType: String
//    //订单状态 1：未发货 2：已发货 9：已经退款
//    var Status: Int
//    //付款码
//    var PayCode: String
//    //物流单号
//    var LogisticsCode: String
//    //是否退款 0：不可以退款 1：可以退款
//    var IsRefund: Int
//    
//    override init(jsonData: JSON) {
//        self.Total = jsonData["Total"].doubleValue
//        self.Shipping = jsonData["Shipping"].doubleValue
//        self.Discount = jsonData["Discount"].doubleValue
//        self.OrderTotal = jsonData["OrderTotal"].doubleValue
//        self.Name = jsonData["Name"].stringValue
//        self.Phone = jsonData["Phone"].stringValue
//        self.Street = jsonData["Street"].stringValue
//        self.City = jsonData["City"].stringValue
//        self.State = jsonData["State"].stringValue
//        self.Country = jsonData["Country"].stringValue
//        self.Postal_code = jsonData["Postal_code"].stringValue
//        self.Apt = jsonData["Apt"].stringValue
//        self.PaymentType = jsonData["PaymentType"].stringValue
//        self.PayCode = jsonData["PayCode"].stringValue
//        self.LogisticsCode = jsonData["LogisticsCode"].stringValue
//        self.Status = jsonData["Status"].intValue
//        self.IsRefund = jsonData["IsRefund"].intValue
//        
//        super.init(jsonData: jsonData)
//    }
//}
