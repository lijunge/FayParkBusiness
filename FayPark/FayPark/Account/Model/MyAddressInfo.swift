//
//  MyAddressInfo.swift
//  FayPark
//
//  Created by faypark on 2018/6/12.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MyAddressInfo {
    var AddrID: Int
    var Address: String
    var Apt: String
    var City: String
    var Name: String
    var ZipCode: String
    var Phone: String
    var Contry: String
    var States: String
    var CollectEmail: String
    var TopOne: Int
    var isPaySelect = false
    init(jsonData: JSON) {
        AddrID = jsonData["AddrID"].intValue
        Address = jsonData["Address"].stringValue
        Apt = jsonData["Apt"].stringValue
        City = jsonData["City"].stringValue
        Name = jsonData["Name"].stringValue
        ZipCode = jsonData["ZipCode"].stringValue
        Phone = jsonData["Phone"].stringValue
        Contry = jsonData["Contry"].stringValue
        States = jsonData["States"].stringValue
        CollectEmail = jsonData["CollectEmail"].stringValue
        TopOne = jsonData["TopOne"].intValue
    }
    
}
