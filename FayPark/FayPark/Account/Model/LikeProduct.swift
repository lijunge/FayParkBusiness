//
//  LikeProduct.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/31.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct LikeProduct {
    var ID: Int
    var SellingPrice: Double
    var Msrp: Double
    var ProductID: String
    var MainImgLink: String
    var ProductTitle: String
    var PageNO: String
    
    init(jsonData: JSON) {
        self.ID = jsonData["ID"].intValue
        self.SellingPrice = jsonData["SellingPrice"].doubleValue
        self.Msrp = jsonData["Msrp"].doubleValue
        self.ProductID = jsonData["ProductID"].stringValue
        self.MainImgLink = jsonData["MainImgLink"].stringValue
        self.ProductTitle = jsonData["ProductTitle"].stringValue
        self.PageNO = jsonData["PageNO"].stringValue
    }
}
