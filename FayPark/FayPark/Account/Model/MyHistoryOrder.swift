//
//  OrderBase.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/30.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MyHistoryListInfo {
    var OrderTime: String
    var OrderPayTotal: Double
    var ShippingTotal: Double
    var OrderList: Array<MyOrderInfo>
    var Currency: String //货币单位
    var PaymentMethod: String //1：paypal 2：stripe 3. 货到付款   20180804新增
    var IndiaCOD: Double //货到付款服务费
    init(jsonData: JSON) {
        OrderTime = jsonData["OrderTime"].stringValue
        OrderPayTotal = jsonData["OrderPayTotal"].doubleValue
        ShippingTotal = jsonData["ShippingTotal"].doubleValue
        Currency = jsonData["Currency"].stringValue
        PaymentMethod = jsonData["PaymentMethod"].stringValue
        IndiaCOD = jsonData["IndiaCOD"].doubleValue
        OrderList = Array()
        for jsonItem in jsonData["OrderList"].arrayValue {
            OrderList.append(MyOrderInfo.init(jsonData: jsonItem))
        }
    }
}

struct MyOrderInfo {
    //订单ID
    var ID: Int
    //订单编号
    var OrderNO: String
    //产品SKUID
    var ProductSKUID: String
    //产品主图
    var MainImgLink: String
    //产品标题
    var ProductTitle: String
    //产品尺寸
    var Size: String
    //产品颜色
    var Color: String
    //产品数量
    var Count: Int
    //产品单价
    var Price: Double
    //产品市场价
    var Msrp: Double
    //订单创建日期
    var CreateDate: String
    //订单状态 1 未发货 2已发货 3 已收货 9 已经退款
    var Status: Int
    //评论状态 1未评论 2 已评论（已经评论的不能再次评论）
    var IsComment: Int
    //c产品ID
    var ProductID: Int
    //物流单号
    var LogisticsCode: String
    var CurrencyType: String
    init(jsonData: JSON) {
        self.ID = jsonData["ID"].intValue
        self.OrderNO = jsonData["OrderNO"].stringValue
        self.ProductSKUID = jsonData["ProductSKUID"].stringValue
        self.MainImgLink = jsonData["MainImgLink"].stringValue
        self.ProductTitle = jsonData["ProductTitle"].stringValue
        self.Size = jsonData["Size"].stringValue
        self.Color = jsonData["Color"].stringValue
        self.Count = jsonData["Count"].intValue
        self.Price = jsonData["Price"].doubleValue
        self.Msrp = jsonData["Msrp"].doubleValue
        self.CreateDate = jsonData["CreateDate"].stringValue
        Status = jsonData["Status"].intValue
        IsComment = jsonData["IsComment"].intValue
        ProductID = jsonData["ProductID"].intValue
        LogisticsCode = jsonData["LogisticsCode"].stringValue
        CurrencyType = jsonData["CurrencyType"].stringValue
    }
}

struct MyOrderDetailInfo {
    //订单ID
    var ID: Int
    //订单编号
    var OrderNO: String
    //产品SKUID
    var ProductSKUID: String
    //产品主图
    var MainImgLink: String
    //产品标题
    var ProductTitle: String
    //产品尺寸
    var Size: String
    //产品颜色
    var Color: String
    //产品数量
    var Count: Int
    //产品单价
    var Price: Double
    //产品市场价
    var Msrp: Double
    //订单创建日期
    var CreateDate: String
    //订单状态 1 未发货 2已发货 3 已收货 9 已经退款
    var Status: Int
   
    //评论状态 1未评论 2 已评论（已经评论的不能再次评论）
    var IsComment: Int
    //产品总价
    var Total: Double
    //运费价格
    var Shipping: Double
    //折扣
    var Discount: Double
    //订单总价
    var OrderTotal: Double
    //用户名字
    var Name: String
    //电话号码
    var Phone: String
    //街道
    var Street: String
    //城市
    var City: String
    //州
    var State: String
    //国家
    var Country: String
    //邮编
    var Postal_code: String
    //门牌号
    var Apt: String
    //付款方式
    var PaymentType: String
    //付款码
    var PayCode: String
    //物流单号
    var LogisticsCode: String
    //是否退款 0：不可以退款 1：可以退款
    var IsRefund: Int
    //评论总数
    var CommentCount: Int
    //评论均分
    var CommentAvg: String
    var Address: String
    
    //ProductID
    var ProductID: String
    var OrderType: Int
    var Currency: String
    init(jsonData: JSON) {
        
        self.ID = jsonData["ID"].intValue
        self.OrderNO = jsonData["OrderNO"].stringValue
        self.ProductSKUID = jsonData["ProductSKUID"].stringValue
        self.MainImgLink = jsonData["MainImgLink"].stringValue
        self.ProductTitle = jsonData["ProductTitle"].stringValue
        self.Size = jsonData["Size"].stringValue
        self.Color = jsonData["Color"].stringValue
        self.Count = jsonData["Count"].intValue
        self.Price = jsonData["Price"].doubleValue
        self.Msrp = jsonData["Msrp"].doubleValue
        self.CreateDate = jsonData["CreateDate"].stringValue
        
        self.Total = jsonData["Total"].doubleValue
        self.Shipping = jsonData["Shipping"].doubleValue
        self.Discount = jsonData["Discount"].doubleValue
        self.OrderTotal = jsonData["OrderTotal"].doubleValue
        self.Name = jsonData["Name"].stringValue
        self.Phone = jsonData["Phone"].stringValue
        self.Street = jsonData["Street"].stringValue
        self.City = jsonData["City"].stringValue
        self.State = jsonData["State"].stringValue
        self.Country = jsonData["Country"].stringValue
        self.Postal_code = jsonData["Postal_code"].stringValue
        self.Apt = jsonData["Apt"].stringValue
        self.PaymentType = jsonData["PaymentType"].stringValue
        self.PayCode = jsonData["PayCode"].stringValue
        self.LogisticsCode = jsonData["LogisticsCode"].stringValue
        self.IsRefund = jsonData["IsRefund"].intValue
        
        Status = jsonData["Status"].intValue
        IsComment = jsonData["IsComment"].intValue
        CommentCount = jsonData["CommentCount"].intValue
        CommentAvg = jsonData["CommentAvg"].stringValue
        ProductID = jsonData["ProductID"].stringValue
        OrderType = jsonData["OrderType"].intValue
        Address = jsonData["Address"].stringValue
        Currency = jsonData["Currency"].stringValue
    }
}
/*
 *  评论
 */
struct CommentInfo {
    var UserName: String
    var UserImg: String
    var CommentContent: String
    var CommentLevel: Int //评论等级（12345级）
    var CommentTime: String
    init(json: JSON) {
        UserName = json["UserName"].stringValue
        UserImg = json["UserImg"].stringValue
        CommentContent = json["CommentContent"].stringValue
        CommentLevel = json["CommentLevel"].intValue
        CommentTime = json["CommentTime"].stringValue
    }
}
/**
 * 评论集合
 */
struct ProductCommentList {
    var CommentCount: Int //评论总数
    var CommentAvg: String //评论均分
    var ProductCommentList: [CommentInfo]
    init(json: JSON) {
        ProductCommentList = [CommentInfo]()
        CommentCount = json["CommentCount"].intValue
        CommentAvg = json["CommentAvg"].stringValue
        for item in json["ProductCommentList"].arrayValue {
            ProductCommentList.append(CommentInfo(json: item))
        }
        
    }
}
