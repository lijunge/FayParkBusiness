//
//  MyWalletInfo.swift
//  FayPark
//
//  Created by faypark on 2018/6/13.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MyWalletInfo {
    var CardInfoID: Int
    var CardType: String
    var CardNum: String
    var TopOne: Int
    var isPaySelect = false
    init(jsonData: JSON) {
        CardInfoID = jsonData["CardInfoID"].intValue
        CardType = jsonData["CardType"].stringValue
        CardNum = jsonData["CardNum"].stringValue
        TopOne = jsonData["TopOne"].intValue
    }
}
