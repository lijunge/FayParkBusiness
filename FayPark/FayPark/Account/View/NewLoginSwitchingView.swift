//
//  NewLoginSwitchingView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/20.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NewLoginSwitchingView: UIScrollView {
    var swicthType:String!//MARK:判断是登录还是注册
    init(frame: CGRect,switchingType:String) {
        super.init(frame: frame)
        self.swicthType = switchingType
        self.contentSize = CGSize.init(width: KScreenWidth*2, height: self.frame.size.height)
        if switchingType == "1" {
            self.contentOffset.x = KScreenWidth
        }else{
            self.contentOffset.x = 0
        }
        self.showsVerticalScrollIndicator = false
        self.bounces = false
        self.isPagingEnabled = true
        //MARK:加载注册视图
//        self.addSubview(self.firstNameLable)
//        self.addSubview(self.lastNameLable)
        self.addSubview(self.firstNameTField)
        self.addSubview(self.lastNameTField)
        //self.addSubview(self.emailLable)
        self.addSubview(self.emailTField)
        //self.addSubview(self.passwordLable)
        self.addSubview(self.passwordTField)
        self.addSubview(self.forgotBtn)
        self.addSubview(self.signUPBtn)
        self.addSubview(self.facebookBtn)
        self.passwordTField.rightView = self.passwordSwitchingBtn
        self.loginPasswordTField.rightView = self.passwordSwitchingLoginBtn
        self.addSubview(self.passwordSwitchingLoginBtn)
        self.addSubview(self.passwordSwitchingBtn)
        
        
        //MARK:加载登陆时图
        self.addSubview(self.loginPasswordTField)
        self.addSubview(self.loginEmailTField)
//        self.addSubview(self.passwordLoginLable)
//        self.addSubview(self.emailLoginlable)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //MARK:注册时图布局
//        self.firstNameLable.mas_makeConstraints { (mark) in
//            mark?.top.equalTo()(self.mas_top)?.setOffset(kSizeFrom750(x: 14))
//            mark?.left.equalTo()(self)?.setOffset(kSizeFrom750(x: 80))
//            mark?.height.equalTo()(kSizeFrom750(x: 30))
//            mark?.width.equalTo()(KScreenWidth/2-kSizeFrom750(x: 80+25))
//        }
//        self.lastNameLable.mas_makeConstraints { (mark) in
//            mark?.top.height().width().equalTo()(self.firstNameLable)
//            mark?.left.equalTo()(self.firstNameLable.mas_right)?.offset()(kSizeFrom750(x: 50))
//        }
        
        self.firstNameTField.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.mas_top)?.setOffset(kSizeFrom750(x: 14))
            mark?.left.equalTo()(self)?.setOffset(kSizeFrom750(x: 80))
           // mark?.top.equalTo()(self.firstNameLable.mas_bottom)
           // mark?.left.right().equalTo()(self.firstNameLable)
            mark?.height.equalTo()(kSizeFrom750(x: 75))
            mark?.width.equalTo()(KScreenWidth/2-kSizeFrom750(x: 80+25))
        }
        self.lastNameTField.mas_makeConstraints { (mark) in
            //mark?.top.equalTo()(self.lastNameLable.mas_bottom)
            //mark?.top.equalTo()(self.mas_top)?.setOffset(kSizeFrom750(x: 14))
            //mark?.height.equalTo()(kSizeFrom750(x: 75))
            mark?.top.height().width().equalTo()(self.firstNameTField)
            mark?.left.equalTo()(self.firstNameTField.mas_right)?.offset()(kSizeFrom750(x: 50))
           // mark?.left.right().equalTo()(self.lastNameLable)
        }
        
//        self.emailLable.mas_makeConstraints { (mark) in
//            mark?.top.equalTo()(self.firstNameTField.mas_bottom)?.offset()(kSizeFrom750(x: 25))
//            mark?.left.equalTo()(self)?.setOffset(kSizeFrom750(x: 80))
//            mark?.width.equalTo()(KScreenWidth - kSizeFrom750(x: 80)*2)
//            mark?.height.equalTo()(self.firstNameLable)
//        }
        self.emailTField.mas_makeConstraints { (mark) in
            //mark?.left.width().equalTo()(self.emailLable)
            mark?.width.equalTo()(KScreenWidth - kSizeFrom750(x: 80)*2)
            mark?.left.equalTo()(self)?.setOffset(kSizeFrom750(x: 80))
            mark?.top.equalTo()(self.lastNameTField.mas_bottom)?.offset()(kSizeFrom750(x: 25))
            mark?.height.equalTo()(kSizeFrom750(x: 75))
        }
        
//        self.passwordLable.mas_makeConstraints { (mark) in
//            mark?.top.equalTo()(self.emailTField.mas_bottom)?.offset()(kSizeFrom750(x: 25))
//            mark?.left.equalTo()(self)?.setOffset(kSizeFrom750(x: 80))
//            mark?.width.height().equalTo()(self.emailLable)
//        }
        self.passwordTField.mas_makeConstraints { (mark) in
            //mark?.top.equalTo()(self.passwordLable.mas_bottom)
            mark?.top.equalTo()(self.emailTField.mas_bottom)?.offset()(kSizeFrom750(x: 25))
            mark?.left.height().equalTo()(self.emailTField)
            mark?.width.equalTo()(KScreenWidth - kSizeFrom750(x: 80)*2)
        }

        //MARK登陆布局---------------------------------------------------------
        
//        self.emailLoginlable.mas_makeConstraints { (mark) in
//            mark?.top.equalTo()(self.mas_top)?.setOffset(kSizeFrom750(x: 14))
//            mark?.left.equalTo()(self)?.setOffset(KScreenWidth + kSizeFrom750(x: 80))
//            mark?.height.equalTo()(kSizeFrom750(x: 30))
//            mark?.width.equalTo()(KScreenWidth-kSizeFrom750(x: 80)*2)
//        }
        
        self.loginEmailTField.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.mas_top)?.setOffset(kSizeFrom750(x: 14))
            //mark?.top.equalTo()(self.emailLoginlable.mas_bottom)
            mark?.left.equalTo()(self)?.setOffset(KScreenWidth + kSizeFrom750(x: 80))
            //mark?.width.left().equalTo()(self.emailLoginlable)
            mark?.width.equalTo()(KScreenWidth-kSizeFrom750(x: 80)*2)
            mark?.height.equalTo()(kSizeFrom750(x: 75))
        }
        
//        self.passwordLoginLable.mas_makeConstraints { (mark) in
//            mark?.top.equalTo()(self.loginEmailTField.mas_bottom)?.setOffset(kSizeFrom750(x: 25))
//            mark?.height.left().width().equalTo()(self.emailLoginlable)
//        }
        self.loginPasswordTField.mas_makeConstraints { (mark) in
           // mark?.top.equalTo()(self.passwordLoginLable.mas_bottom)
            mark?.top.equalTo()(self.loginEmailTField.mas_bottom)?.setOffset(kSizeFrom750(x: 25))
            mark?.left.height().width().width().equalTo()(self.loginEmailTField)
        }

        //MARK: 登陆注册 都会走到方法
        //MARK: 登陆注册 都会走到方法
        if self.swicthType == "1"{
            self.forgotBtn.mas_remakeConstraints { (mark) in
                mark?.top.equalTo()(self.loginPasswordTField.mas_bottom)?.setOffset(kSizeFrom750(x: 28))
                mark?.width.left().equalTo()(self.loginPasswordTField)
                mark?.height.equalTo()(kSizeFrom750(x: 35))
            }
            
        }else{
            self.forgotBtn.mas_remakeConstraints { (mark) in
                mark?.top.equalTo()(self.passwordTField.mas_bottom)?.setOffset(kSizeFrom750(x: 28))
                mark?.width.left().equalTo()(self.passwordTField)
                mark?.height.equalTo()(kSizeFrom750(x: 35))
            }
        }
//        self.forgotBtn.titleLabel?.textAlignment = .right
        self.signUPBtn.mas_remakeConstraints { (mark) in
            mark?.top.equalTo()(self.forgotBtn.mas_bottom)?.setOffset(kSizeFrom750(x: 70))
            mark?.left.width().equalTo()(self.forgotBtn)
            mark?.height.equalTo()(kSizeFrom750(x: 90))
        }
        self.facebookBtn.mas_remakeConstraints { (mark) in
            mark?.top.equalTo()(self.signUPBtn.mas_bottom)?.offset()(kSizeFrom750(x: 30))
            mark?.height.right().width().equalTo()(self.signUPBtn)
        }
        self.passwordSwitchingBtn.mas_remakeConstraints { (mark) in
            mark?.width.height().equalTo()(kSizeFrom750(x: 50))
            mark?.centerX.equalTo()(self.passwordTField.mas_centerX)
        }
        
        self.passwordSwitchingLoginBtn.mas_remakeConstraints { (mark) in
            mark?.width.height().equalTo()(kSizeFrom750(x: 50))
            mark?.centerX.equalTo()(self.loginPasswordTField.mas_centerX)
        }
    }
    
    //MARK:初始化注册时图-------------------------------------
//    lazy var firstNameLable: UILabel = {
//        let tempFirstNameLable = UILabel.init()
//        return lableStyle(lable: tempFirstNameLable, title: "First Name")
//
//    }()
//    lazy var lastNameLable: UILabel = {
//        let tempLastNameLable = UILabel.init()
//        return lableStyle(lable: tempLastNameLable, title: "Last Name")
//    }()
//
//    lazy var emailLable: UILabel = {
//        let tempEmailLable = UILabel.init()
//        return lableStyle(lable: tempEmailLable, title: "Email")
//    }()
//
//    lazy var passwordLable: UILabel = {
//        let tempPasswordLable = UILabel.init()
//        return lableStyle(lable: tempPasswordLable, title: "Password")
//    }()
    
    lazy var forgotBtn: UIButton = {
        let tempForgotBtn = UIButton.init()
        tempForgotBtn.titleLabel?.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        tempForgotBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0xb3b3b3), for: .normal)
        tempForgotBtn.setTitle(kLGForgetPasswordString, for: .normal)
        tempForgotBtn.contentHorizontalAlignment = .right
        return tempForgotBtn
        
    }()
    
    lazy var firstNameTField: TextFieldStly = {
        let tempFirstNameTField = TextFieldStly.init()
        tempFirstNameTField.placeholder = kLGFirstNameString
        return textFieldStyle(textF: tempFirstNameTField )
    }()
    
    lazy var lastNameTField: TextFieldStly = {
        let tempLastNameTField = TextFieldStly.init()
        tempLastNameTField.placeholder = kLGLastNameString
        return textFieldStyle(textF: tempLastNameTField )
    }()
    
    lazy var emailTField: TextFieldStly = {
        let tempEmailTField = TextFieldStly.init()
        tempEmailTField.placeholder = kLGEmailString
        return textFieldStyle(textF: tempEmailTField)
    }()
    
    lazy var passwordTField: TextFieldStly = {
        let tempPasswordTField = TextFieldStly.init()
        tempPasswordTField.tag = 11110000
        tempPasswordTField.placeholder = kLGPasswordString
        return textFieldStyle(textF: tempPasswordTField)
    }()
    lazy var signUPBtn: UIButton = {
        let tempSignBtn = UIButton.init()
        return btnStyle(btn: tempSignBtn, title: kLoginSignUpString)
    }()
    
    lazy var facebookBtn: UIButton = {
        let tempFacbookBtn = UIButton.init()
        return faceBookBtnStyle(btn: tempFacbookBtn)
    }()
    //MARK:登陆时图初始化-------------------------------------------
    
//    lazy var emailLoginlable: UILabel = {
//        let tempLable = UILabel.init()
//
//        return lableStyle(lable: tempLable, title: "Email")
//    }()
    
    lazy var loginEmailTField: TextFieldStly = {
        let tempLoginTF = TextFieldStly.init()
        tempLoginTF.placeholder = kLGEmailString
        return textFieldStyle(textF: tempLoginTF)
    }()
    
//    lazy var passwordLoginLable: UILabel = {
//        let tempPassword = UILabel.init()
//        return lableStyle(lable: tempPassword, title: "Password")
//    }()

    lazy var loginPasswordTField: TextFieldStly = {
        let tempLogin = TextFieldStly.init()
        tempLogin.placeholder = kLGPasswordString
        tempLogin.tag = 11110000
        return textFieldStyle(textF: tempLogin)
    }()
    
    lazy var passwordSwitchingBtn:UIButton = {
        let tempPasswordSwitchingBtn = UIButton.init()
        tempPasswordSwitchingBtn.setImage(kImage(iconName: "view_normal"), for: .normal)
        tempPasswordSwitchingBtn.setImage(kImage(iconName: "view_pressed"), for: .selected)
        return tempPasswordSwitchingBtn
    }()
    
    lazy var passwordSwitchingLoginBtn:UIButton = {
        let tempPasswordSwitchingBtn = UIButton.init()
        tempPasswordSwitchingBtn.setImage(kImage(iconName: "view_normal"), for: .normal)
        tempPasswordSwitchingBtn.setImage(kImage(iconName: "view_pressed"), for: .selected)
        return tempPasswordSwitchingBtn
    }()
    
    //MARK:登陆/注册 BTN 样式
    func btnStyle(btn:UIButton,title:String)->UIButton {
        btn.backgroundColor = UIColor.white
        btn.setTitle(title, for: .normal)
        btn.setTitleColor(kRGBColorFromHex(rgbValue: 0x000000), for: .normal)
        btn.layer.borderWidth = kSizeFrom750(x: 1)
        btn.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
        btn.titleLabel?.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 40))
        return btn
    }
    
    func faceBookBtnStyle(btn:UIButton) -> UIButton {
        btn.setTitle(kLGWithFaceBookString, for: .normal)
        btn.backgroundColor = kRGBColorFromHex(rgbValue: 0x3b5998)
        btn.titleLabel?.font = UIFont.init(name: kGothamRoundedLight, size: kSizeFrom750(x: 40 ))
        btn.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        btn.setImage(kImage(iconName: "facebookicon"), for: .normal)
        return btn
    }
    //MARK:TextField 样式---------------------------------------
    func textFieldStyle(textF:TextFieldStly)->TextFieldStly{
        textF.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 30))
        textF.textColor = kRGBColorFromHex(rgbValue: 0x333333)
//        textF.backgroundColor = UIColor.red
        textF.tintColor = kRGBColorFromHex(rgbValue: 0x333333)
        if textF.tag == 11110000 {
            textF.isSecureTextEntry = true
            textF.rightViewMode = .always
//            textF.rightView = self.passwordSwitchingBtn
        }
        return textF
    }
    //LableMARK: 样式------------------------------------------
    func lableStyle(lable:UILabel,title:String) -> UILabel {
        lable.textColor = kRGBColorFromHex(rgbValue: 0xb3b3b3)
        lable.font = UIFont.init(name: kMSReference, size: kSizeFrom750(x: 30))
        lable.textAlignment = .left
        lable.text = title
        return lable
    }
}

class TextFieldStly: UITextField {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if  let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(kRGBColorFromHex(rgbValue: 0xe2e2e2).cgColor)
            context.fill(CGRect(x: 0, y: self.frame.size.height - kSizeFrom750(x: 1), width: self.frame.size.width, height: kSizeFrom750(x: 1)))
        }
    }
}
