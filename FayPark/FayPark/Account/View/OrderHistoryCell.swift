//
//  OrderHistoryCell.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/6.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class OrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var orderNumLabel: UILabel!
    @IBOutlet weak var proTitleLabel: UILabel!
    @IBOutlet weak var sizeColorLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var proImgView: UIImageView!
    @IBOutlet weak var originalPriceLabel: UILabel!
    @IBOutlet weak var shipLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var reviewButton: UIButton!
    
    typealias OrderConfigButtonTappedBlock = (_ sender: UIButton) -> Void
    var configButtonTapped: OrderConfigButtonTappedBlock?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        leftButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
        leftButton.layer.borderWidth = kSizeFrom750(x: 1)
        
        reviewButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0xe9668f).cgColor
        reviewButton.layer.borderWidth = kSizeFrom750(x: 1)
        
        reviewButton.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 30))
        leftButton.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 30))

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func configOrderButtonTapped(_ sender: UIButton) {
        if configButtonTapped != nil {
            configButtonTapped!(sender)
        }
    }
    
    func configOrderCell(orderInfo: MyOrderInfo) {
        
        orderNumLabel.text = "\(kOHOrderNumberString): \(orderInfo.OrderNO)"
        proTitleLabel.text = orderInfo.ProductTitle
        if orderInfo.Color.isEmpty {
            if orderInfo.Size.isEmpty {
                sizeColorLabel.text = ""
            } else {
                sizeColorLabel.text = "\(kPDSizeString):\(orderInfo.Size)"
            }
        } else {
            if orderInfo.Size.isEmpty {
                sizeColorLabel.text = "\(kPDColorString):\(orderInfo.Color)"
            } else {
                sizeColorLabel.text = "\(kPDSizeString):\(orderInfo.Size) \(kPDColorString):\(orderInfo.Color)"
            }
        }
        
        countLabel.text = "X \(orderInfo.Count)"
        proImgView.sd_setImage(with: URL.init(string: orderInfo.MainImgLink), placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        priceLabel.text = String(format: "%@%.2f", orderInfo.CurrencyType,orderInfo.Price)
        
        let oldString = String(format: "%@%.2f",orderInfo.CurrencyType,orderInfo.Msrp)
        let str2 = NSMutableAttributedString.init(string: oldString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        originalPriceLabel.attributedText = str2
        //1 未发货 2已发货 3 已收货 9 已经退款
        if orderInfo.Status == 1 {
            shipLabel.text = kODPreparingString
            leftButton.isHidden = true
            reviewButton.isHidden = true
        } else if orderInfo.Status == 2 {
            shipLabel.text = kODShippedString
            leftButton.isHidden = false
            reviewButton.isHidden = false
            leftButton.setTitle(kODTrackingOrderString, for: .normal)
            reviewButton.setTitle(kODConfirmReceiptString, for: .normal)
            reviewButton.isEnabled = true
            reviewButton.alpha = 1
        } else if orderInfo.Status == 3 {
            shipLabel.text = kODReceivedString
            leftButton.isHidden = false
            reviewButton.isHidden = false
            leftButton.setTitle(kODTrackingOrderString, for: .normal)
            if orderInfo.IsComment == 1 { //未评论
                reviewButton.setTitle(kODCommentString, for: .normal)
                reviewButton.isEnabled = true
                reviewButton.alpha = 1
            } else {  //已评论
                reviewButton.setTitle(kODHaveCommnetsString, for: .normal)
                reviewButton.isEnabled = false
                reviewButton.alpha = 0.4
            }
        } else if orderInfo.Status == 9 {
            shipLabel.text = kODRefundedString
            leftButton.isHidden = true
            reviewButton.isHidden = true
        }
    }
}
