//
//  CategoriesLiftView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/19.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
class CategoriesLiftView: UITableView,UITableViewDelegate,UITableViewDataSource {
    var liftTitleArray = Array<JSON>.init()
    let liftImageArray = ["browse_earrings_icon","browse_necklaces_icon","browse_rings_icon","browse_bracelets_icon","browse_hairaccessories_icon","browse_watches_icon","browse_anklets_icon","browse_other_icon"]
    
    
    var selectIndx:Bool = false
    
    typealias SelectliftView = (Int) -> Void
    var select:SelectliftView?
    
    func selectAction(block:@escaping SelectliftView) {
        self.select = block;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return liftTitleArray.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kSizeFrom750(x: 120)
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.white
        if (self.select != nil) {//传值
            self.select!(indexPath.row)
        }
        if self.selectIndx == true {
            let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0))
            cell?.contentView.backgroundColor = kRGBColorFromHex(rgbValue: 0xfcfcfc)
            self.selectIndx = false
        }
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = kRGBColorFromHex(rgbValue: 0xfcfcfc)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LiftCell.liftCell(tableView: tableView)
        cell?.categoriesTitle.text = liftTitleArray[indexPath.row]["OneTypeName"].stringValue
        cell?.categoriesImage.image = kImage(iconName: self.liftImageArray[indexPath.row])
        cell?.categoriesTitle.tag = indexPath.row
        return cell!
    }
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .grouped)
        self.dataSource = self
        self.delegate = self
        self.estimatedSectionFooterHeight = 0;
        self.estimatedSectionHeaderHeight = 0;
        self.separatorStyle = .none
    }
    
    public func setSelectedFirstCell() {
        
        if self.delegate != nil && self.delegate!.responds(to: #selector(tableView(_:didSelectRowAt:))) {
            self.delegate!.tableView!(self, didSelectRowAt: NSIndexPath(row: 0, section: 0) as IndexPath)
            self.selectIndx = true
            
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
