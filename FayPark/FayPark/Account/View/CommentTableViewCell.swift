//
//  CommentTableViewCell.swift
//  FayPark
//
//  Created by faypark on 2018/6/29.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var headImgButton: UIButton!
   // @IBOutlet weak var leftHeadImg: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var commentDetailLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var rateView: FPStartRateView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        headImgButton.layer.cornerRadius = headImgButton.frame.width / 2.0
        headImgButton.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configCommentCell(comment: CommentInfo) {
        if comment.UserImg.isEmpty {
            headImgButton.layer.borderWidth = 0.5
            headImgButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0xababab).cgColor
            headImgButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xcdcdcd)
            let character = getFirstLetterFromString(aString: comment.UserName)
            var titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
            if character == "A" || character == "B" || character == "C" || character == "D" || character == "E" {
                titleColor = kRGBColorFromHex(rgbValue: 0xe9668f)
            } else if character == "F" || character == "G" || character == "H" || character == "I" || character == "J" {
                titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
            } else if character == "K" || character == "L" || character == "M" || character == "N" || character == "O" {
                titleColor = kRGBColorFromHex(rgbValue: 0x00acf0)
            } else if character == "P" || character == "Q" || character == "R" || character == "S" || character == "T" {
                titleColor = kRGBColorFromHex(rgbValue: 0xc45cac)
            } else {
                titleColor = kRGBColorFromHex(rgbValue: 0xf97f46)
            }
            headImgButton.setTitleColor(titleColor, for: .normal)
            headImgButton.setTitle(character, for: .normal)
            headImgButton.titleLabel?.font = UIFont(name: kHelveticaRegular, size: 25)
        } else {
            headImgButton.layer.borderWidth = 0.0
            headImgButton.backgroundColor = UIColor.white
            headImgButton.setTitleColor(UIColor.white, for: .normal)
            headImgButton.sd_setImage(with: URL(string: comment.UserImg), for: .normal, placeholderImage: kPlaceHolderImage, options: .init(rawValue: 0), completed: nil)
        }
        
        userNameLabel.text = comment.UserName
        commentDetailLabel.text = comment.CommentContent
        timeLabel.text = comment.CommentTime
        let score = comment.CommentLevel
        rateView = FPStartRateView(frame: CGRect(x: KScreenWidth - 90 - 14, y: 14, width: 90, height: 15), starCount: 5, score: Float(score))
        rateView.delegate = self
        rateView.isUserInteractionEnabled = false//不支持用户操作
        rateView.usePanAnimation = true
        rateView.allowUserPan = true//滑动评星
        contentView.addSubview(rateView)
    }
}

extension CommentTableViewCell: FPStarReteViewDelegate {
    //MARK: - 协议代理
    func starRate(view starRateView: FPStartRateView, score: Float) {
        print(score)
    }
}
