//
//  SelectedColumnView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/19.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
class SelectedColumnView: UIScrollView,UIScrollViewDelegate {
    
    typealias SelectColumnView = (Any,_ selectNameString: String) -> Void
    var selectBlock:SelectColumnView?
    var selectBtn = UIButton()
    var redIndicator = UIView()
    var liftTitleArray = Array<JSON>.init()
    var pointLine:CGFloat!
    
    
    
    func textHeight(title:String,classWidth:CGFloat)->CGSize{
        let size = CGSize(width: CGFloat(MAXFLOAT), height: classWidth)
        let textFont = UIFont.systemFont(ofSize: kSizeFrom750(x: 27))
        let textLabelSize = textSize(text:title , font: textFont, maxSize: size)
        return textLabelSize
    }
    
    func textSize(text : String , font : UIFont , maxSize : CGSize) -> CGSize{
        
        let options : NSStringDrawingOptions = .usesLineFragmentOrigin
        let dic = NSDictionary(object: font, forKey: NSAttributedStringKey.font as NSCopying)
        return text.boundingRect(with: maxSize, options: options, attributes: dic as? [NSAttributedStringKey : Any], context: nil).size
    }
    
    lazy var cateScrollView: UIScrollView = {
        let tempCateScrollView = UIScrollView.init()
        return tempCateScrollView
    }()
    
    
    
    init(frame: CGRect,title:Array<JSON>,touchNumber:Int) {
        
        super.init(frame: frame)
        self.addSubview(self.cateScrollView)
        self.liftTitleArray = title
        self.showsHorizontalScrollIndicator = false
        self.bounces = false
        
        var btnWidth:CGFloat = 0
        let redIndicator = UIView.init()
        redIndicator.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        self.redIndicator = redIndicator
        self.layer.borderColor = kSpacingColor.cgColor
        self.layer.borderWidth = kSizeFrom750(x: 1)
        self.delegate = self
        
        for indx in 0..<liftTitleArray.count {
            
            let title = liftTitleArray[indx]["SecondTypeName"].stringValue
            let size = textHeight(title: title, classWidth: kSizeFrom750(x: 86))
            let btn = UIButton.init()
            btn.frame = CGRect.init(x: btnWidth, y: kSizeFrom750(x: 2), width: size.width+kSizeFrom750(x: 60), height: kSizeFrom750(x: 86))
            btnWidth += size.width+kSizeFrom750(x: 60)
            btn.backgroundColor = UIColor.white
            btn.setTitle(title as String, for: .normal)
            btn.setTitleColor(kRGBColorFromHex(rgbValue: 0x666666), for: .normal)
            btn.setTitleColor(kRGBColorFromHex(rgbValue: 0xe9668f), for: .selected)
            btn.titleLabel?.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 27))
            btn.addTarget(self, action: #selector(btnClick(btn:)), for: .touchUpInside)
            self.addSubview(btn)
    
            btn.tag = liftTitleArray[indx]["SecondTypeId"].intValue
            if indx == touchNumber {
                print(btn.mj_w)
                redIndicator.frame = CGRect.init(x: 0, y: kSizeFrom750(x: 54), width: 22, height: kSizeFrom750(x: 4))
                redIndicator.center = CGPoint(x: btn.center.x, y:kSizeFrom750(x: 96) )
                self.contentOffset.x = redIndicator.center.x-btn.frame.width
                btnClick(btn: btn)
            }
        }
        print(btnWidth)
        self.contentSize = CGSize.init(width: btnWidth, height: self.frame.size.height)
        self.addSubview(redIndicator)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func btnClick(btn:UIButton) {
        
        selectBtn.isSelected = false
        selectBtn = btn
        btn.isSelected = true
        UIView.animate(withDuration: 0.25) {
            self.redIndicator.bounds = CGRect(x: 0, y: 0, width: btn.frame.width, height: kSizeFrom750(x: 4))
            self.redIndicator.center = CGPoint(x: btn.center.x, y: kSizeFrom750(x: 96))
        }
        
        if btn.frame.maxX > KScreenWidth {
            self.cateScrollView.contentOffset.x = self.cateScrollView.center.x
        }
        if (self.selectBlock != nil) {//传值
            if let titleString = btn.titleLabel?.text {
                if !titleString.isEmpty {
                    self.selectBlock!(btn.tag,titleString)
                }
            }
        }
    }
    
    func selectBlockAction(block:@escaping SelectColumnView) {
        self.selectBlock = block;
    }
    
}
