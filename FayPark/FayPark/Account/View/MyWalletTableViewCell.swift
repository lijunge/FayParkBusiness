//
//  MyWalletTableViewCell.swift
//  FayPark
//
//  Created by faypark on 2018/6/13.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class MyWalletTableViewCell: UITableViewCell {

    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var walletImgView: UIImageView!
    @IBOutlet weak var walletLabel: UILabel!
   
    @IBOutlet weak var leftImageWidthConstraint: NSLayoutConstraint!
    typealias SelectedButtonTapped = (_ button : UIButton) -> Void
    var selectedButtonTapped: SelectedButtonTapped?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func checkButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if selectedButtonTapped != nil {
            selectedButtonTapped!(sender)
        }
    }
    
    func configWalletCell(walletInfo: MyWalletInfo,selectedPayID: Int,isCheckOut: Bool) {
        walletLabel.text = "*\(walletInfo.CardNum)"
        if isCheckOut {
            if selectedPayID == walletInfo.CardInfoID {
                self.checkButton.isSelected = true
                self.checkButton.setImage(UIImage(named: "circle_radio_ok_icon"), for: .selected)
            } else {
                self.checkButton.isSelected = false
                self.checkButton.setImage(UIImage(named: "circle_radio_normal_icon"), for: .normal)
            }
            
        } else {
            if walletInfo.TopOne == 0 {
                self.checkButton.isSelected = false
                self.checkButton.setImage(UIImage(named: "circle_radio_normal_icon"), for: .normal)
            } else {
                self.checkButton.isSelected = true
                self.checkButton.setImage(UIImage(named: "circle_radio_ok_icon"), for: .selected)
            }
        }
        
        if walletInfo.CardType == "Visa" {
            self.walletImgView.image = UIImage(named: "card_visa_icon")
        } else if walletInfo.CardType == "MasterCard" {
            self.walletImgView.image = UIImage(named: "card_mastercard_icon")
        } else if walletInfo.CardType == "American Express" {
            self.walletImgView.image = UIImage(named: "card_americanexpress_icon")
        } else if walletInfo.CardType == "JCB" {
            self.walletImgView.image = UIImage(named: "card_jcb_icon")
        } else if walletInfo.CardType == "Discover" {
            self.walletImgView.image = UIImage(named: "card_discover_icon")
        } else if walletInfo.CardType == "Diners Club" {
            self.walletImgView.image = UIImage(named: "card_dinersclub_icon")
        }
        walletImgView.removeConstraint(leftImageWidthConstraint)
        leftImageWidthConstraint = NSLayoutConstraint(item: walletImgView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 35)
        walletImgView.addConstraint(leftImageWidthConstraint)
    }
    func configPaypalCell(walletInfo: MyWalletInfo,selectedPayID: Int) {
        if selectedPayID == 1 {
            self.checkButton.isSelected = true
            self.checkButton.setImage(UIImage(named: "circle_radio_ok_icon"), for: .selected)
        } else {
            self.checkButton.isSelected = false
            self.checkButton.setImage(UIImage(named: "circle_radio_normal_icon"), for: .normal)
        }
        self.walletImgView.image = UIImage(named: "address_paypal")
        self.walletImgView.contentMode = .scaleAspectFit
        walletImgView.removeConstraint(leftImageWidthConstraint)
        leftImageWidthConstraint = NSLayoutConstraint(item: walletImgView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 100)
        walletImgView.addConstraint(leftImageWidthConstraint)
        walletLabel.text = ""
    }
    func configCashCell(walletInfo: MyWalletInfo,selectedPayID: Int) {
        if selectedPayID == 3 {
            self.checkButton.isSelected = true
            self.checkButton.setImage(UIImage(named: "circle_radio_ok_icon"), for: .selected)
        } else {
            self.checkButton.isSelected = false
            self.checkButton.setImage(UIImage(named: "circle_radio_normal_icon"), for: .normal)
        }
        self.walletImgView.image = UIImage(named: "address_cashondelivery")
        self.walletImgView.contentMode = .scaleAspectFit
        walletImgView.removeConstraint(leftImageWidthConstraint)
        leftImageWidthConstraint = NSLayoutConstraint(item: walletImgView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 35)
        walletImgView.addConstraint(leftImageWidthConstraint)
        walletLabel.text = "Cash on Delivery"
        walletLabel.font = UIFont(name: kGothamRoundedBold, size: kSizeFrom750(x: 30))
        walletLabel.textColor = kRGBColorFromHex(rgbValue: 0x28558f)
        
    }
}
