//
//  OrderDetailCell.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/8.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class OrderDetailCell: UITableViewCell {
    @IBOutlet weak var proTitleLabel: UILabel!
    @IBOutlet weak var sizeColorLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var proImgView: UIImageView!

    @IBOutlet weak var purchaseDateLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    
    @IBOutlet weak var shipLabel: UILabel!
    @IBOutlet weak var originalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configOrderDetailCell(orderDetail: MyOrderDetailInfo) {
        
        orderNumberLabel.text = "\(kOHOrderNumberString): \(orderDetail.OrderNO)"
        purchaseDateLabel.text = "\(kOHPurchaseDateString): \(orderDetail.CreateDate)"
        proTitleLabel.text = orderDetail.ProductTitle
        
        if orderDetail.Color.isEmpty {
            if orderDetail.Size.isEmpty {
                sizeColorLabel.text = ""
            } else {
                sizeColorLabel.text = "\(kPDSizeString):\(orderDetail.Size)"
            }
        } else {
            if orderDetail.Size.isEmpty {
                sizeColorLabel.text = "\(kPDColorString):\(orderDetail.Color)"
            } else {
                sizeColorLabel.text = "\(kPDSizeString):\(orderDetail.Size) \(kPDColorString):\(orderDetail.Color)"
            }
        }
        countLabel.text = "X \(orderDetail.Count)"
        
        priceLabel.text = String(format: "%@%.2f",orderDetail.Currency,orderDetail.Price)
        let oldString = String(format: "%@%.2f",orderDetail.Currency,orderDetail.Msrp)
        let str2 = NSMutableAttributedString.init(string: oldString)
        str2.addAttributes([NSAttributedStringKey.strikethroughStyle : NSNumber.init(value: 1), NSAttributedStringKey.strikethroughColor : UIColor.lightGray], range: NSRange(location: 0, length: str2.length))
        originalLabel.attributedText = str2
        
        if orderDetail.Status == 1 {
            shipLabel.text = kOHPreparingString
        } else if orderDetail.Status == 2 {
            shipLabel.text = kOHShippedString
        } else if orderDetail.Status == 3 {
            shipLabel.text = kOHReceivedString
        } else if orderDetail.Status == 9 {
            shipLabel.text = kOHRefundString
        }
    }
    
}
