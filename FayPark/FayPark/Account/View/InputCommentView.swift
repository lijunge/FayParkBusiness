//
//  InputCommentView.swift
//  FayPark
//
//  Created by faypark on 2018/6/29.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class InputCommentView: UIView {

    @IBOutlet weak var notiseLabel: UILabel!
    @IBOutlet var View: UIView!
    @IBOutlet weak var centerView: UIView!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var inputTextView: UITextView!
    
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var placeHolderLabel: UILabel!
    
    var rateView: FPStartRateView!
    var rateScore = 0
    
    typealias AddProductCommentInfoBlock = (_ param: [String: Any]) -> Void
    var addProductCommentInfoBlock: AddProductCommentInfoBlock?
    override init(frame: CGRect) {
        super.init(frame: frame)
        View = Bundle.main.loadNibNamed("InputCommentView", owner: self, options: nil)?.first as! UIView
        View.frame = self.bounds
        View.backgroundColor = UIColor(white: 0, alpha: 0.4)
        addSubview(View)

        
        submitButton.backgroundColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        submitButton.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 40))
        
        inputTextView.delegate = self
        
        let rateX: CGFloat = notiseLabel.frame.maxX + 20
        rateView = FPStartRateView(frame: CGRect(x: rateX, y: 40, width: 150, height: 25), starCount: 5, score: 0)
        rateView.delegate = self
        rateView.isUserInteractionEnabled = true//不支持用户操作
        rateView.usePanAnimation = true
        rateView.allowUserPan = true//滑动评星
        centerView.addSubview(rateView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        dismissFromSuperView()
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        var param = [String:Any].init()
        if let commentContent = inputTextView.text {
            param = ["commentContent": commentContent,"commentScore": rateScore] as [String : Any]
            
        } else {
            param = ["commentContent": "You can write your comment here.","commentScore": rateScore] as [String : Any]
        }
        
        if addProductCommentInfoBlock != nil {
            addProductCommentInfoBlock!(param)
            dismissFromSuperView()
        }
        
    }
    func dismissFromSuperView() {
        UIView.animate(withDuration: 1, animations: {
            self.alpha = 0.0
        }) { (isRemove) in
            self.removeFromSuperview()
        }
    }
    
}

extension InputCommentView: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.placeHolderLabel.isHidden = true
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.placeHolderLabel.isHidden = false
        } else {
            self.placeHolderLabel.isHidden = true
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension InputCommentView: FPStarReteViewDelegate {
    //MARK: - 协议代理
    func starRate(view starRateView: FPStartRateView, score: Float) {
        print(score)
        rateScore = Int(score)
    }
}
