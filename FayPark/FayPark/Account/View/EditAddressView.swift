//
//  EditAddressView.swift
//  FayPark
//
//  Created by faypark on 2018/6/13.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
class EditAddressView: UIView {

    @IBOutlet var contentView: UIView!
   
    @IBOutlet weak var emailInputField: UITextField!
    
    @IBOutlet weak var fullNameInputField: UITextField!
    
    @IBOutlet weak var streetAddressField: UITextField!
    
    @IBOutlet weak var aptSuitField: UITextField!
    
    @IBOutlet weak var cityInputField: UITextField!
    
    @IBOutlet weak var zipCodeField: UITextField!
    
    @IBOutlet weak var phoneInputField: UITextField!
    
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var unitStateButton: UIButton!
    
    @IBOutlet weak var stateButton: UIButton!
   
    typealias DoneBlock = (_ addressInfo: MyAddressInfo,_ isUpdate: Bool) -> Void
    var doneButtonTappedBlock: DoneBlock?
   
    var stateFlag = 0
    var selectedUniteIndex: Int = 0
    var selectedStateIndex: Int = 0
    let allStateArr = [["Alabama","Alaska","Arizona","Arkansas","California","Colorado",
                        "Connecticut","Delaware","District of Columbia","Florida","Georgia",
                        "Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky",
                        "Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota",
                        "Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire",
                        "New Jersey","New Mexico","New York","North Carolina","North Dakota",
                        "Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina",
                        "South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington",
                        "West Virginia","Wisconsin","Wyoming"],
                       ["Afghanistan"],["Aland lslands"],["Albania"],["Algeria"],["American Samoa"],
                       ["Andorra"],["Angola"],["Anguilla"],["Antarctica"],["Antigua and Barbuda"],
                       ["Buenos Aires","Catamarca","Córdoba","Chaco","Chubut","Ciudad Autónoma de Buenos Aires",
                        "Corrientes","Entre Ríos","Formosa","Jujuy","La Rioja","La Pampa","Mendoza",
                        "Misiones","Neuquén","Río Negro","Salta","San Juan","San Luis","Santa Cruz",
                        "Santa Fe","Santiago del Estero","Tucumán","Tierra del Fuego"],
                       ["Armenia"],["Aruba"],["Ascension Island"],
                       ["Canberra", "New South Wales", "Northern Territory", "Queensland", "South Australia", "Tasmania", "Victoria", "Western Australia"],
                       ["Andalusia","Asturias","Balearic Islands","Basque Country","Galicia","Canary Islands",
                        "Cantabria","Castile and León","Castilla-La Mancha","Catalonia","Community of Madrid",
                        "Extremadura","La Rioja","Navarre","Region of Murcia","Valencian Community"],
                       ["Azerbaijan"],["Bahamas"],["Bahrain"],["Bangladesh"],
                       ["Barbados"],["Belgium"],["Belize"],["Benin"],["Bermuda"],["Bhutan"],
                       ["Bolivia"],["Bosnia and Herzegovina"],["Botswana"],["Bouvet Island"],
                       ["Acre","Alagoas","Amapá","Amazonas","Bahia","Ceará","Distrito Federal",
                        "Espírito Santo","Goiás","Maranhão","Mato Grosso","Mato Grosso do Sul",
                        "Minas Gerais","Pará","Paraíba","Paraná","Pernambuco","Piauí",
                        "Rio de Janeiro","Rio Grande do Norte","Rio Grande do Sul","Rondônia",
                        "Roraima","Santa Catarina","São Paulo","Sergipe","Tocantins"],
                       ["British Indian Ocean Territory"],["Brunei"],["Bulgaria"],
                       ["Burkina Faso"],["Burundi"],["Cambodia"],["Cameroon"],["Canada"],
                       ["Cape Verde"],["Cayman Islands"],["Central African Republic"],["Chad"],
                       ["Chile"],["Beijing","Chongqing","Fujian","Gansu","Guangdong","Guangxi",
                                  "Guizhou","Hainan","Hebei","Heilongjiang","Henan","Hongkong SAR",
                                  "Hubei","Hunan","Inner Mongolia","Jiangsu","Jiangxi","Jilin",
                                  "Liaoning","Macao SAR","Ningxia","Qinghai","Shaanxi","Shandong",
                                  "Shanghai","Shanxi","Sichuan","Taiwan","Tianjin","Tibet",
                                  "Xinjiang","Yunnan","Zhejiang"],["Christmas Island"],
                        ["Cocos(Keeling)Islands"],["Columbia"],["Comoros"],["Congo"],["Congo(DRC)"],
                        ["Cook Islands"],["Costa Rica"],["Cote d'Ivoire"],["Croatia"],["Cuba"],
                        ["Cyprus"],["Czech Republic"],["Denmark"],["Djibouti"],["Dominica"],
                        ["Dominican Republic"],["Ecuador"],["Egypt"],["El Salvador"],["Eritrea"],
                        ["Estonia"],["Ethiopia"],["Falkland Islands"],["Faroe Islands"],
                        ["Fiji Islands"],["Finland"],["Alsace","Aquitaine","Auvergne","Auvergne",
                                                      "Burgundy","Brittany","Centre-Val de Loire",
                                                      "Champagne-Ardenne","Corsica","Franche-Comté",
                                                      "Upper Normandy","Île-de-France","Languedoc-Roussillon",
                                                      "Limousin","Lorraine","Midi-Pyrénées","Nord-Pas-de-Calais",
                                                      "Pays de la Loire","Picardy","Poitou-Charentes",
                                                      "Provence-Alpes-Côte d'Azur","Rhône-Alpes"],
                        ["Frech Polynesia"],
                        ["French Guiana"],["French Southern and Antarctic Lands"],["Gabon"],
                        ["Gambia"],["Georgia"],["Germany"],["Ghana"],["Gibraltar"],["Greece"],
                        ["Greenland"],["Grenada"],["Guadeloupe"],["Guam"],["Guatemala"],["Guernsey"],
                        ["Guinea"],["Guinea-Bissau"],["Guyana"],["Haiti"],
                        ["Heard Island and McDonald Islands"],["Honduras"],["Hungary"],["Iceland"],
                        ["Andaman and Nicobar Islands","Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chandigarh",
                         "Chhattisgarh","Dadra and Nagar Haveli","Daman and Diu","Delhi","Goa","Gujarat","Haryana",
                         "Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Madhya Pradesh",
                         "Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Odisha","Puducherry","Punjab",
                         "Rajasthan","Sikkim","Tamil Nadu","Telangana","Tripura","Uttar Pradesh","Uttarakhand",
                         "West Bengal"],
                        ["Indonesia"],["Iran"],["Iraq"],["Ireland"],["Isle of Man"],["Israel"],
                        ["Abruzzo","Basilicata","Calabria","Campania","Emilia-Romagna",
                                                    "Friuli-Venezia Giulia","Lazio","Liguria","Lombardia",
                                                    "Marche","Molise","Piemonte","Puglia","Sardegna","Sicilia",
                                                    "Toscana","Trentino-Alto Adige","Umbria","Valle d'Aosta",
                                                    "Veneto"],
                        ["Jamaica"],["Aichi","Akita","Aomori","Chiba","Ehime","Fukui","Fukucka",
                                     "fukushima","Gifu","Gumma","Hiroshima","Hokkaido","Hyogo",
                                     "Ibaraki","Ishikawa","Iwata","Kagawa","Kagoshima","Kanagawa",
                                     "Kochi","Kumamota","Kyoto","Mie","Miyagi","Miyazaki","Nagano",
                                     "Nagasaki","Nara","Niigata","Oita","Okayama","Okinawa","Osaka",
                                     "Saga","Saitama","Shiga","Shimane","Shizouka","Tochigi",
                                     "Tokushima","Tokyo","Tottori","Toyama","Wakayama","Yamagata",
                                     "Yamaguchi","Yamanashi"],
                        ["Jersey"],["Jordan"],
                        ["Kazakhstan"],["Kenya"],["Kiribati"],["Busan","Chungcheongbuk-do",
                                                               "Chungcheongnam-do","Daegu",
                                                               "Daejeon","Gangwon-do","Gwangju",
                                                               "Jeju-do","Gyeongsangbuk-do",
                                                               "Gyeongsangnam-do","Incheon",
                                                               "Gyeonggi-do","Jeollabuk-do",
                                                               "Jeollanam-do","Seoul","Ulsan"],
                        ["Kuwait"],["Kyrgyzstan"],["Laos"],["Latvia"],["Lebanon"],["Lesotho"],
                        ["Liberia"],["Libya"],["Liechtenstein"],["Lithuania"],["Luxembourg"],
                        ["Macedonia,Former Yugoslav Republic of"],["Madagascar"],["Malawi"],
                        ["Johor","Kedah","Kelantan","Kuala Lumpur","Labuan","Malacca",
                         "Negeri Sembilan","Pahang","Perak","Perlis","Pulau Pinang","Sabah",
                         "Sarawak","Selangor","Terengganu"],["Maldives"],["Mali"],["Malta"],
                        ["Marshall Islands"],["Martinique"],["Mauritania"],["Mauritius"],["Mayotte"],
                        ["Aguascalientes","Baja California","Baja California Sur","Campeche",
                         "Chiapas","Chihuahua","Coahuila de Zaragoza","Colima","Durango",
                         "Guanajuato","Guerrero","Hidalgo","Jalisco","Michoacán de Ocampo",
                         "Morelos","México","Nayarit","Nuevo León","Oaxaca","Puebla","Querétaro de Arteaga",
                         "Quintana Roo","San Luis Potosí","Sinaloa","Sonora","Tabasco","Tamaulipas",
                         "Tlaxcala","Veracruz","Yucatán","Zacatecas"],
                        ["Micronesia"],["Moldova"],["Monaco"],["Mongolia"],["Montserrat"],
                        ["Morocco"],["Mozambique"],["Myanmar"],["Namibia"],["Nauru"],["Nepal"],
                        ["Netherlands"],["Netherlands Antilles"],["New Caledonia"],["New Zealand"],
                        ["Nicaragua"],["Niger"],["Nigeria"],["Niue"],["Norfolk Island"],
                        ["North Korea"],["Northern Mariana Islands"],["Norway"],["Oman"],
                        ["Pakistan"],["Palau"],["Palestinian Authority"],["Panama"],
                        ["Papua New Guinea"],["Paraguay"],["Peru"],["Philippines"],
                        ["Pitcairn Islands"],["Poland"],["Portugal"],["Puerto Rico"],["Qatar"],
                        ["Reunion"],["Romania"],["Russia"],["Rwanda"],["Samoa"],["San Marino"],
                        ["Sao Tome and Principe"],["Saudi Arabia"],["Senegal"],["Serbia,Montenegro"],
                        ["Seychelles"],["Sierra Leone"],["Singapore"],["Slovakia"],["Slovenia"],
                        ["Solomon Islands"],["Somalia"],["South Africa"],
                        ["South Georgia and South Sandwich Islands"],["Spain"],["Sri Lanka"],
                        ["St.Helena"],["St.Kitts and Nevis"],["St.Lucia"],["St.Pierre and Miquelon"],
                        ["St.Vincent and the Grenadines"],["Sudan"],["Suriname"],
                        ["Svalbard and Jan Mayen"],["Swaziland"],["Sweden"],["Switzerland"],
                        ["Syria"],["Tajikistan"],["Tanzania"],["Bangkok","Phatthaya","Amnat Charoen",
                                                               "Ang Thong","Bueng Kan","Buri Ram",
                                                               "Chachoengsao","Chai Nat","Chaiyaphum",
                                                               "Chanthaburi","Chiang Mai","Chiang Rai",
                                                               "Chon Buri","Chumphon","Kalasin",
                                                               "Kamphaeng Phet","Kanchanaburi","Khon Kaen",
                                                               "Krabi","Lampang","Lamphun","Loei",
                                                               "Lop Buri","Mae Hong Son","Maha Sarakham",
                                                               "Mukdahan","Nakhon Nayok","Nakhon Pathom",
                                                               "Nakhon Phanom","Nakhon Ratchasima","Nakhon Sawan",
                                                               "Nakhon Si Thammarat","Nan","Narathiwat",
                                                               "Nong Bua Lam Phu","Nong Khai","Nonthaburi",
                                                               "Pathum Thani","Pattani","Phangnga","Phatthalung",
                                                               "Phayao","Phetchabun","Phetchaburi","Phichit",
                                                               "Phitsanulok","Phrae","Phra Nakhon Si Ayutthaya",
                                                               "Phuket","Prachin Buri","Prachuap Khiri Khan",
                                                               "Ranong","Ratchaburi","Rayong","Roi Et","Sa Kaeo",
                                                               "Sakon Nakhon","Samut Prakan","Samut Sakhon",
                                                               "Samut Songkhram","Saraburi","Satun","Sing Buri",
                                                               "Si Sa Ket","Songkhla","Sukhothai","Suphan Buri",
                                                               "Surat Thani","Surin","Tak","Trang","Trat",
                                                               "Ubon Ratchathani","Udon Thani","Uthai Thani",
                                                               "Uttaradit","Yala","Yasothon"],
                        ["Timor-Leste"],["Togo"],
                        ["Tokelau"],["Tonga"],["Trinidad and Tobago"],["Tristan da Cunha"],
                        ["Tunisia"],["Turkey"],["Turkmenistan"],["Turks and Caicos Islands"],
                        ["Tuvalu"],["Uganda"],["Ukraine"],["United Arab Emirates"],["England",
                         "Northern Ireland", "Scotland","Wales"],
                        ["United States Minor Outlying Islands"],["Uruguay"],["Uzbekistan"],
                        ["Vanuatu"],["Vatican City"],["Venezuela"],["Vietnam"],["Virgin Islands"],
                        ["Virgin Islands,British"],["Wallis and Futuna"],["White Russia"],["Yemen"],
                        ["Zambia"],["Zimbabwe"]]
    var isFirstEdit: Bool? {
        didSet {
            if let isFirstEdit = isFirstEdit {
                if isFirstEdit {
                    self.doneButton.setTitle(kNextString, for: .normal)
                } else {
                    self.doneButton.setTitle(kDoneString, for: .normal)
                }
            }
        }
    }
    var selectAddressInfo: MyAddressInfo? {
        didSet {
            if let addressInfo: MyAddressInfo = selectAddressInfo {
                setupEditAddressInfo(addressInfo: addressInfo)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialFromXib()
    }
    
    func initialFromXib() {
        
        let view = Bundle.main.loadNibNamed("EditAddressView", owner: self, options: nil)?.first as! UIView;
        view.frame = self.bounds;
        addSubview(view);

        self.doneButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        self.doneButton.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 40))
        self.doneButton.layer.cornerRadius = 5.0
        self.unitStateButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.unitStateButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xc5d67b), for: .normal)
        self.stateButton.titleLabel?.adjustsFontSizeToFitWidth = true
        //self.unitStateButton.imageLabelEdgeInset(imageDirection: .Right, imageTitleSpace: 10)
        self.stateButton.imageLabelEdgeInset(imageDirection: .Right, imageTitleSpace: 10)
        self.emailInputField.text = UserDefaults.standard.object(forKey: "kEmail") as? String
        self.fullNameInputField.text = UserDefaults.standard.object(forKey: "kUserName") as? String
        emailInputField.delegate = self
        fullNameInputField.delegate = self
        streetAddressField.delegate = self
        aptSuitField.delegate = self
        cityInputField.delegate = self
        zipCodeField.delegate = self
        phoneInputField.delegate = self
        if let countryDic = UserDefaults.standard.value(forKey: kSelectedCountry) as? [String: String]{
            if let countryString = countryDic["CountryName"] {
                if !countryString.isEmpty {
                    if let selectedIndex = allCountryArr.index(of: countryString) {
                        self.selectedUniteIndex = selectedIndex
                        self.unitStateButton.setTitle(countryString, for: .normal)
                    }
                }
            }
        }
    }

    @IBAction func unitButtonTapped(_ sender: Any) {
        //提示用户回到首页进行国家的切换
        showToast(localized("kAEChangeCountryString"))
//        let unitPicker = CustomPickerView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
//        unitPicker.initWithPicker(data: allCountryArr, title: kCountryString, defaultSelectedIndex: 0)
//        unitPicker.selectBlock = { (selectedIndex: Int,selectedValue: String) -> Void in
//            self.unitStateButton.setTitle(selectedValue, for: .normal)
//            self.unitStateButton.imageLabelEdgeInset(imageDirection: .Right, imageTitleSpace: 10)
//            self.selectedUniteIndex = selectedIndex
//        }
    }
    
    @IBAction func stateButtonTapped(_ sender: Any) {
        stateFlag = 1
        let cityArr = self.allStateArr[self.selectedUniteIndex]
        let unitPicker = CustomPickerView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        unitPicker.initWithPicker(data: cityArr, title: kStateString, defaultSelectedIndex: 0)
        unitPicker.selectBlock = { (selectedIndex: Int,selectedValue: String) -> Void in
            self.stateButton.setTitle(selectedValue, for: .normal)
            self.stateButton.imageLabelEdgeInset(imageDirection: .Right, imageTitleSpace: 10)
            self.selectedStateIndex = selectedIndex
        }
    }
    @IBAction func doneButtonTapped(_ sender: Any) {
        if emailInputField.text == "" {
            showTextHudInWindow(showText: kAEEnterCorrectEmailString)
            return
        }
        
        if validateEmail(email: emailInputField.text!) == true {
            if fullNameInputField.text == "" {
                showTextHudInWindow(showText: kAEEnterYourNameString)
                return
            }
            if streetAddressField.text == "" {
                showTextHudInWindow(showText: kAEEnterAddressString)
                return
            }
            if stateFlag == 0 {
                showTextHudInWindow(showText: kAEChooseCountryString)
                return
            }
            if cityInputField.text == "" {
                showTextHudInWindow(showText: kAEEnterCityString)
                return
            }
            if zipCodeField.text == "" {
                showTextHudInWindow(showText: kAEEnterZipCodeString)
                return
            }
            if phoneInputField.text == "" {
                showTextHudInWindow(showText: kAEEnterPhoneNumberString)
                return
            }
            let emailString = emailInputField.text!
            let fullNameString = fullNameInputField.text!
            let streetString = streetAddressField.text!
            let aptString = aptSuitField.text!
           
            let cityString = cityInputField.text!
            let zipString = zipCodeField.text!
            let phoneString = phoneInputField.text!

            let unitString = unitStateButton.titleLabel!.text!
            let stateString = stateButton.titleLabel!.text!
            var addrID = 0
            var topOne = 0
            var isUpdate = false
            if let addressInfo: MyAddressInfo = selectAddressInfo {
                addrID = addressInfo.AddrID
                topOne = addressInfo.TopOne
                isUpdate = true
            }
            let jsonData = ["AddrID": addrID,"Address": streetString,"Apt": aptString,"City":cityString,"Name": fullNameString,"ZipCode": zipString,"Phone": phoneString,"Contry": unitString,"States": stateString,"CollectEmail": emailString,"TopOne": topOne] as [String : Any]
            let json = JSON(jsonData)
            let addressInfo = MyAddressInfo(jsonData: json)
            if doneButtonTappedBlock != nil {
                doneButtonTappedBlock!(addressInfo,isUpdate)
            }
            if let first = self.isFirstEdit{
                if first {
                    configCommonEventMonitor(monitorString: kAddAddressEvent, type: 2)
                }
            }
        } else {
            showTextHudInWindow(showText: kAEEnterEmailAddressString)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialFromXib()
    }
    
    func validateEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    func setupEditAddressInfo(addressInfo: MyAddressInfo) {
        if addressInfo.States == "" {
            stateFlag = 0
        } else {
            stateFlag = 1
        }
        let selectedCountryString = addressInfo.Contry
        if allCountryArr.contains(selectedCountryString) {
            if let selectedIndex = allCountryArr.index(of: selectedCountryString) {
                self.selectedUniteIndex = selectedIndex
            }
        }
        self.emailInputField.text = addressInfo.CollectEmail
        self.fullNameInputField.text = addressInfo.Name
        self.streetAddressField.text = addressInfo.Address
        self.aptSuitField.text = addressInfo.Apt
        self.cityInputField.text = addressInfo.City
        self.zipCodeField.text = addressInfo.ZipCode
        self.phoneInputField.text = addressInfo.Phone
        self.unitStateButton.setTitle(addressInfo.Contry, for: .normal)
        self.unitStateButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xc5d67b), for: .normal)
        self.stateButton.setTitle(addressInfo.States, for: .normal)
        //self.unitStateButton.imageLabelEdgeInset(imageDirection: .Right, imageTitleSpace: 10)
        self.stateButton.imageLabelEdgeInset(imageDirection: .Right, imageTitleSpace: 10)
        
    }
    func showTextHudInWindow(showText: String) {
        let sharehud = MBProgressHUD.showAdded(to: contentView, animated: true)
        sharehud.mode = .text
        sharehud.label.text = showText
        sharehud.removeFromSuperViewOnHide = true
        contentView.addSubview(sharehud)
        sharehud.hide(animated: true, afterDelay: 2.0)
    }
    
}

extension EditAddressView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailInputField {
            fullNameInputField.becomeFirstResponder()
            return true
        } else if textField == fullNameInputField {
            streetAddressField.becomeFirstResponder()
            return true
        } else if textField == streetAddressField {
            aptSuitField.becomeFirstResponder()
            return true
        } else if textField == cityInputField {
            zipCodeField.becomeFirstResponder()
            return true
        } else if textField == zipCodeField {
            phoneInputField.becomeFirstResponder()
            return true
        }
        return false
    }
}
