//
//  RightCell.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/21.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class RightCell: UITableViewCell {

    class func liftCell(tableView: UITableView) -> RightCell! {
        let cell = RightCell.init(style: UITableViewCellStyle.default, reuseIdentifier: NSStringFromClass(self))
        cell.selectionStyle = .none
        return cell
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.categoriesTitle)
//        self.backgroundColor = kRGBColorFromHex(rgbValue: 0xfcfcfc)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        self.categoriesTitle.mas_makeConstraints { (mark) in
            mark?.centerY.equalTo()(self.mas_centerY)
            mark?.left.equalTo()(self.mas_left)?.setOffset(kSizeFrom750(x: 8))
            mark?.right.equalTo()(self)?.setOffset(-kSizeFrom750(x: 8))
        }
        self.categoriesTitle.setContentHuggingPriority(.required, for: .horizontal)
    }
    
    
   
    
    lazy var categoriesTitle: UILabel = {
        let tempCategoriesTitle = UILabel.init()
        tempCategoriesTitle.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 28))
        tempCategoriesTitle.textAlignment = .center
        tempCategoriesTitle.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempCategoriesTitle.numberOfLines = 0
        return tempCategoriesTitle
    }()

}
