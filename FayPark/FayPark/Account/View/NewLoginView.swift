//
//  NewLoginView.swift
//  FayPark
//
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import Masonry
let statusRect:CGFloat = UIApplication.shared.statusBarFrame.size.height + 44
class NewLoginView: UIView,UIScrollViewDelegate {
    var typeLogin:String!
    //MARK:注册-----------------------------
    //    typealias SelectSinUPView = (Any) -> Void
    typealias SelectAuthorizedView = () -> Void
    var selectAuthorizedBlock:SelectAuthorizedView?
    //MARK:登陆------------------------------
    typealias SelectLoginView = (Any) -> Void
    var selectLoginBlock:SelectLoginView?
   
    
    func selectLoginBlockAction(block:@escaping SelectLoginView) {
        self.selectLoginBlock = block;
    }

    func selectAuthorizedAction(block:@escaping SelectAuthorizedView) {
        self.selectAuthorizedBlock = block;
    }
    
    var loginView:NewProductDetailViewController!
    init(frame: CGRect,type:String) {
        super.init(frame: frame)
        self.addSubview(self.iocnView)
        self.addSubview(self.infoLable)
        self.typeLogin = type
        self.addSubview(loginBtn)
        self.addSubview(signUPBtn)
        self.addSubview(self.lineView)
        self.addSubview(self.scrollView)
        self.loginView = NewProductDetailViewController()
        self.addSubview(self.headerView)
        self.headerView.addSubview(self.headerLiftView)
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.headerView.mas_makeConstraints { (mark) in
            mark?.top.left().right().equalTo()(self)
            mark?.height.equalTo()(statusRect)
        }
        self.headerLiftView.mas_makeConstraints { (mark) in
            mark?.bottom.equalTo()(self.headerView)
            mark?.left.equalTo()(self.headerView)?.offset()(kSizeFrom750(x: 56))
            mark?.width.height().equalTo()(kSizeFrom750(x: 55))
        }
        
        self.iocnView.mas_makeConstraints { (mark)  in
            mark?.top.equalTo()(self.headerView.mas_bottom)?.setOffset(kSizeFrom750(x: 0))
            mark?.width.height().equalTo()(kSizeFrom750(x: 100))
            mark?.centerX.equalTo()(self)
        }
        
        self.infoLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.iocnView.mas_bottom)?.setOffset(kSizeFrom750(x: 50))
            mark?.right.left().equalTo()(self)
        }
        self.infoLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.signUPBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.infoLable.mas_bottom)?.offset()(kSizeFrom750(x: 53))
            mark?.height.equalTo()(kSizeFrom750(x: 50))
            mark?.width.equalTo()(kSizeFrom750(x: 132))
            mark?.centerX.equalTo()(self)?.setOffset(-kSizeFrom750(x: 130))
        }
        
        self.loginBtn.mas_makeConstraints { (mark) in
            mark?.top.with().height().equalTo()(self.signUPBtn)
            mark?.centerX.equalTo()(self)?.setOffset(kSizeFrom750(x: 130) )
        }
        if self.typeLogin == "2"{
            self.lineView.mas_remakeConstraints { (mark) in
                mark?.top.equalTo()(self.signUPBtn.mas_bottom)?.setOffset(kSizeFrom750(x: 12))
                mark?.width.left().equalTo()(self.signUPBtn)
                mark?.height.equalTo()(kSizeFrom750(x: 4))
            }
        }else{
            self.lineView.mas_remakeConstraints { (mark) in
                mark?.top.equalTo()(self.loginBtn.mas_bottom)?.setOffset(kSizeFrom750(x: 12))
                mark?.width.left().equalTo()(self.loginBtn)
                mark?.height.equalTo()(kSizeFrom750(x: 4))
            }
        }
    }
    
    @objc func moreViewTitleBtnClick(send:UIButton) {
        let lineY = self.lineView.center.y
        self.scrollView.signUPBtn.setTitle(send.titleLabel?.text, for: .normal)
        self.lineView.mj_w = send.mj_w
        if send.titleLabel?.text == kLoginSignUpString  {
            self.signUPBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x9bbc65), for: .normal)
            self.loginBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
            self.typeLogin = "2"
            self.scrollView.contentOffset.x = 0
            UIView.animate(withDuration: 0.1) {
                self.lineView.center = CGPoint(x: send.center.x, y: lineY)
            }
        }else{
            self.typeLogin = "1"
            self.scrollView.contentOffset.x = KScreenWidth
            self.loginBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x9bbc65), for: .normal)
            self.signUPBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
            UIView.animate(withDuration: 0.1) {
                self.lineView.center = CGPoint(x: send.center.x, y: lineY)
            }
        }
        
    }
    
    lazy var headerView: UIView = {
        let tempView = UIView.init()
        return tempView
    }()
    
    lazy var headerLiftView: UIButton = {
        let tempImg = UIButton.init()
        tempImg.setImage(kImage(iconName: "arrow_left_icon"), for: .normal)
        tempImg.contentMode = .scaleAspectFit
        return tempImg
    }()
    
    lazy var iocnView: UIImageView = {
        let tempIocnView = UIImageView.init()
        tempIocnView.image = kImage(iconName: "login_logo_icon")
        tempIocnView.contentMode = .scaleAspectFit
        return tempIocnView
    }()
    
    lazy var infoLable: UILabel = {
        let tempInfoLable = UILabel.init()
        tempInfoLable.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        tempInfoLable.text = kWelcomFayParkString
        tempInfoLable.font = UIFont.init(name: kMSReference, size: kSizeFrom750(x: 46))
        tempInfoLable.textAlignment = .center
        return tempInfoLable
    }()
    
    lazy var signUPBtn: UIButton = {
        let tempSignUPBtn = UIButton.init()
        tempSignUPBtn.setTitle(kLoginSignUpString, for: .normal)
        tempSignUPBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x9bbc65), for: .normal)
        tempSignUPBtn.titleLabel?.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 34))
        tempSignUPBtn.addTarget(self, action: #selector(moreViewTitleBtnClick), for: .touchUpInside)
        return tempSignUPBtn
    }()
    
    lazy var loginBtn: UIButton = {
        let tempLoginBtn = UIButton.init()
        tempLoginBtn.setTitle(kLoginString, for: .normal)
        tempLoginBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
        tempLoginBtn.titleLabel?.font = UIFont.init(name: kHelveticaRegular, size: kSizeFrom750(x: 34))
        tempLoginBtn.addTarget(self, action: #selector(moreViewTitleBtnClick), for: .touchUpInside)
        return tempLoginBtn
    }()
    
    lazy var lineView: UIView = {
        let tempLineView = UIView.init()
        tempLineView.backgroundColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        return tempLineView
    }()
    
    lazy var scrollView: NewLoginSwitchingView = {
        let tempScrollView = NewLoginSwitchingView.init(frame: CGRect.init(x: 0, y: kSizeFrom750(x: 400)+statusRect, width: KScreenWidth, height: kScreenHeight-kSizeFrom750(x: 400)-statusRect),switchingType:self.typeLogin )
        tempScrollView.delegate = self
        tempScrollView.signUPBtn.addTarget(self, action: #selector(login), for: .touchUpInside)
        tempScrollView.facebookBtn.addTarget(self, action: #selector(facebook), for: .touchUpInside)
        return tempScrollView
    }()
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let lineY = self.lineView.center.y
        if scrollView.contentOffset.x == KScreenWidth {
            self.loginBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x9bbc65), for: .normal)
            self.signUPBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
            self.scrollView.signUPBtn.setTitle(kLoginString, for: .normal)
            self.scrollView.swicthType = "1"
            self.lineView.mj_w = self.loginBtn.mj_w
            UIView.animate(withDuration: 0.1) {
                self.lineView.center = CGPoint(x: self.loginBtn.center.x, y: lineY)
            }
        }else if scrollView.contentOffset.x == 0 {
            self.scrollView.swicthType = "2"
            self.lineView.mj_w = self.signUPBtn.mj_w
            self.signUPBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x9bbc65), for: .normal)
            self.loginBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0x333333), for: .normal)
            self.scrollView.signUPBtn.setTitle(kLoginSignUpString, for: .normal)
            UIView.animate(withDuration: 0.1) {
                self.lineView.center = CGPoint(x: self.signUPBtn.center.x, y: lineY)
            }
        }
    }
    
    //MARK:d登陆授权登陆点击事件-----------------------------------------------------------------------
    @objc func login() {
        
        if self.scrollView.swicthType == "1"{
            if (self.scrollView.loginEmailTField.text?.isEmpty)!{
                showToast(kLGEnterCorrectEmailString)
                return
            }
            if (self.scrollView.loginPasswordTField.text?.isEmpty)! {
                showToast(kLGEnterCorrectPasswordString)
                return
            }
        }else{
            if (self.scrollView.firstNameTField.text?.isEmpty)!{
                showToast(kLGEnterFirstNameString)
                return
            }
            if (self.scrollView.lastNameTField.text?.isEmpty)!{
                showToast(kLGEnterLastNameString)
                return
            }
            if (self.scrollView.emailTField.text?.isEmpty)!{
                showToast(kLGEnterCorrectEmailString)
                return
            }
            if (self.scrollView.passwordTField.text?.isEmpty)! {
                showToast(kLGEnterCorrectPasswordString)
                return
            }
        }
        
        if (self.selectLoginBlock != nil) {//传值
            self.selectLoginBlock!(self.scrollView.swicthType)
        }
    }
    @objc func facebook()  {
        if (self.selectAuthorizedBlock != nil) {//传值
            self.selectAuthorizedBlock!()
        }
    }
}
