//
//  AccountHeadView.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/3.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class AccountHeadView: UIView {
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var headButton: UIButton!
 
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        view = Bundle.main.loadNibNamed("AccountHeadView", owner: self, options: nil)?.first as! UIView;
        view.frame = self.bounds;
        addSubview(view);
        
        setupSubViews()
    }
    
    func setupSubViews() {
        
        nameLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        nameLabel.font = UIFont(name: kHelveticaRegular, size: 20)
        nameLabel.textAlignment = .left
        
        headButton.layer.cornerRadius = 30
        headButton.layer.masksToBounds = true
    }
    
    func setHeadImgFromFistNameCharacter(character: String) {
        headButton.layer.borderWidth = 0.5
        headButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0xababab).cgColor
        headButton.backgroundColor = kRGBColorFromHex(rgbValue: 0xcdcdcd)
        var titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
        if character == "A" || character == "B" || character == "C" || character == "D" || character == "E" {
            titleColor = kRGBColorFromHex(rgbValue: 0xe9668f)
        } else if character == "F" || character == "G" || character == "H" || character == "I" || character == "J" {
            titleColor = kRGBColorFromHex(rgbValue: 0xffffff)
        } else if character == "K" || character == "L" || character == "M" || character == "N" || character == "O" {
            titleColor = kRGBColorFromHex(rgbValue: 0x00acf0)
        } else if character == "P" || character == "Q" || character == "R" || character == "S" || character == "T" {
            titleColor = kRGBColorFromHex(rgbValue: 0xc45cac)
        } else {
            titleColor = kRGBColorFromHex(rgbValue: 0xf97f46)
        }
        headButton.setTitleColor(titleColor, for: .normal)
        headButton.setTitle(character, for: .normal)
        headButton.titleLabel?.font = UIFont(name: kHelveticaRegular, size: 25)
    }
    func setHeadImgWithImage() {
        headButton.layer.borderWidth = 0.0
        headButton.backgroundColor = UIColor.white
        headButton.setTitleColor(UIColor.white, for: .normal)
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
