//
//  RefundTableViewCell.swift
//  FayPark
//
//  Created by faypark on 2018/6/29.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class RefundTableViewCell: UITableViewCell {

    @IBOutlet weak var checkImg: UIImageView!
    @IBOutlet weak var reasonLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reasonLabel.textColor = kRGBColorFromHex(rgbValue: 0x000000)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
