//
//  AccountTableViewCell.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/3.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellIco: UIImageView!
    @IBOutlet weak var badgeButton: UIButton!
    
    @IBOutlet weak var badgeHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var badgeWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setBadgeString(badgeString: String) {
        // 0(点)  1-99 99+
        badgeButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xffffff), for: .normal)
        badgeButton.backgroundColor = kRGBColorFromHex(rgbValue: 0x9bbc65)
        badgeButton.titleLabel?.font = UIFont(name: kHelveticaRegular, size: 10)
        badgeButton.removeConstraint(badgeHeightConstraint)
        badgeButton.removeConstraint(badgeWidthConstraint)
        
        if Int(badgeString)! == 0 {
            badgeButton.layer.cornerRadius = 4
            badgeButton.layer.masksToBounds = true
            badgeButton.setTitle("", for: .normal)
            badgeWidthConstraint = NSLayoutConstraint(item: badgeButton, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 8)
            badgeHeightConstraint = NSLayoutConstraint(item: badgeButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 8)
        } else if Int(badgeString)! <= 99 {
            badgeButton.layer.cornerRadius = 8
            badgeButton.layer.masksToBounds = true
            badgeButton.setTitle(badgeString, for: .normal)
            badgeWidthConstraint = NSLayoutConstraint(item: badgeButton, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 16)
            badgeHeightConstraint = NSLayoutConstraint(item: badgeButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 16)
        } else {
            badgeButton.layer.cornerRadius = 8
            badgeButton.layer.masksToBounds = true
            badgeButton.setTitle("99+", for: .normal)
            badgeWidthConstraint = NSLayoutConstraint(item: badgeButton, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 30)
            badgeHeightConstraint = NSLayoutConstraint(item: badgeButton, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 16)
        }
        badgeButton.addConstraint(badgeWidthConstraint)
        badgeButton.addConstraint(badgeHeightConstraint)
    }
}
