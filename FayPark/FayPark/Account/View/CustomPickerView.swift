//
//  CustomPickerView.swift
//  FayPark
//
//  Created by faypark on 2018/6/13.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class CustomPickerView: UIView {
    let cellIdentifier = "CustomPickerCellIdentifier"
    public typealias SelectDoneAction = (_ selectedIndex: Int, _ selectedValue: String) -> Void
    var selectBlock: SelectDoneAction?
    var tableView: UITableView!
    var dataSourceArr = [String]()
    var titleString: String!
    override init(frame: CGRect) {
        super.init(frame: frame)
        let window = UIApplication.shared.keyWindow
        guard let currentWindow = window else { return }
        currentWindow.addSubview(self)
        currentWindow.bringSubview(toFront: self)
        UIView.animate(withDuration: 0.25, animations: {[unowned self] in
            self.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.1)
            
        }, completion: nil)
        // 点击背景移除self
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFromSuperView))
        tap.delegate = self
        self.addGestureRecognizer(tap)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func initWithPicker(data: [String],title: String,defaultSelectedIndex: Int?) {
        self.dataSourceArr = data
        self.titleString = title
        commonInit()
    }
    
    func commonInit() {
        
        tableView = UITableView(frame: CGRect(x: 25, y: 30, width: kScreenWidth - 50, height: kScreenHeight - 60), style: .plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 55
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.separatorStyle = .none
        tableView.separatorColor = .clear
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = UIColor.white
        tableView.tableFooterView = UIView()
        addSubview(tableView)
        setupHeaderView()
    }
    
    func setupHeaderView() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
        let notiseLabel = UILabel(frame: CGRect(x: 14, y: 30, width: headerView.frame.size.width, height: 20))
        notiseLabel.text = self.titleString
        notiseLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        notiseLabel.font = UIFont(name: kHelveticaRegular, size: 20)
        headerView.addSubview(notiseLabel)
        
        let lineView = UIView(frame: CGRect(x: 0, y: 59, width: tableView.frame.size.width, height: 1))
        lineView.backgroundColor = kRGBColorFromHex(rgbValue: 0xe2e2e2)
        headerView.addSubview(lineView)
        
        tableView.tableHeaderView = headerView
    }
    @objc func dismissFromSuperView() {
        UIView.animate(withDuration: 0.25, animations: {
            self.backgroundColor = UIColor.clear
        }) { (hidden) in
            self.removeFromSuperview()
        }
    }
}
extension CustomPickerView: UITableViewDelegate,UITableViewDataSource {
    //MARK: - tableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArr.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //MARK: - tableView Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.text = dataSourceArr[indexPath.row]
        cell.textLabel?.font = UIFont(name: kHelveticaRegular, size: 16)
        cell.textLabel?.textColor = kRGBColorFromHex(rgbValue: 0x757575)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.selectBlock != nil {
            let string = dataSourceArr[indexPath.row]
            self.selectBlock!(indexPath.row,string)
            self.dismissFromSuperView()
        }
    }
 
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let sectionHeaderHeight: CGFloat = 10
        if scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0 {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0)
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0)
        }
    }
}

extension CustomPickerView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isKind(of: UITableView.classForCoder()))! {
            return true
        }
        return false
    }
}
