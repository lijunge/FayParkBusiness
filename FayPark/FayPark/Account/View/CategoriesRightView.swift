//
//  CategoriesRightView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/6/19.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import SwiftyJSON
class CategoriesRightView: UITableView,UITableViewDataSource,UITableViewDelegate {
    
    var liftTitleArray = Array<JSON>.init()
    typealias didSelectColumnView = (Int) -> Void
    var selectBlock:didSelectColumnView?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.liftTitleArray.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kSizeFrom750(x: 120)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = RightCell.liftCell(tableView: tableView)
        print(liftTitleArray[indexPath.row])
        cell?.categoriesTitle.text = liftTitleArray[indexPath.row]["SecondTypeName"].stringValue
        cell?.categoriesTitle.tag = indexPath.row
        cell?.categoriesTitle.textAlignment = .center
        return cell!
    }
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .grouped)
        self.dataSource = self
        self.delegate = self
        self.estimatedSectionFooterHeight = 0;
        self.estimatedSectionHeaderHeight = 0;
        self.separatorStyle = .none
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        for lable  in (cell?.contentView.subviews)! {
            if (self.selectBlock != nil) {
                self.selectBlock!(indexPath.row)
            }
            if lable.tag == indexPath.row {
                (lable as! UILabel).textColor = kRGBColorFromHex(rgbValue: 0xe9668f)
            }
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        for lable  in (cell?.contentView.subviews)! {
            if lable.tag == indexPath.row {
                (lable as! UILabel).textColor = kRGBColorFromHex(rgbValue: 0x333333)
            }
        }
    }
    func selectBlockDid(block:@escaping didSelectColumnView) {
        self.selectBlock = block;
    }
    
}
