//
//  NoContentView.swift
//  FayPark
//
//  Created by sayingwhy on 2018/7/6.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class NoContentView: UIView {

    
    typealias SelectShoppingView = () -> Void
    var selectShopping:SelectShoppingView?
    
    func selectAction(block:@escaping SelectShoppingView) {
        self.selectShopping = block;
    }
    
    init(frame: CGRect,type:Int) {
        super.init(frame: frame)
        self.addSubview(self.contentBtn)
        self.addSubview(self.contentInfoLable)
        self.addSubview(self.contentImageView)
        self.backgroundColor = UIColor.white
        if type == 1 {
            self.contentInfoLable.text = kWishListEmptyNowString
            self.contentImageView.image = kImage(iconName: "wishlist_icon_no")
            
        }else if type == 0 {
            self.contentInfoLable.text = kOrderListEmptyNowString
            self.contentImageView.image = kImage(iconName: "orders_icon_no")
        }else {
            self.contentInfoLable.text = kCanNotFindPiecesString
            self.contentImageView.image = kImage(iconName: "categories_icon")
            self.contentBtn.isHidden = true
        }
    }
    @objc func goShoppingBtn(){
        if self.selectShopping != nil {
            self.selectShopping!()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentImageView.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self)?.offset()(kSizeFrom750(x: 198*2))
            mark?.size.equalTo()(CGSize.init(width: kSizeFrom750(x: 126*2), height: kSizeFrom750(x: 126*2)))
            mark?.centerX.equalTo()(self.mas_centerX)
        }
        self.contentInfoLable.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.contentImageView.mas_bottom)?.offset()(kSizeFrom750(x: 44))
            mark?.left.equalTo()(self)?.offset()(kSizeFrom750(x: 28))
            mark?.right.equalTo()(self)?.offset()(-kSizeFrom750(x: 28))
        }
        self.contentInfoLable.setContentHuggingPriority(.required, for: .horizontal)
        
        self.contentBtn.mas_makeConstraints { (mark) in
            mark?.top.equalTo()(self.contentInfoLable.mas_bottom)?.offset()(kSizeFrom750(x: 44))
            mark?.size.equalTo()(CGSize.init(width: kSizeFrom750(x: 410), height: kSizeFrom750(x: 100)))
            mark?.centerX.equalTo()(self.mas_centerX)
        }
    }
    lazy var contentImageView: UIImageView = {
        let tempImg = UIImageView.init()
        return tempImg
    }()
    
    lazy var contentInfoLable: UILabel = {
        let tempLable = UILabel.init()
        tempLable.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 34))
        tempLable.text = kYourWishListEmptyString
        tempLable.textColor = kRGBColorFromHex(rgbValue: 0xb3b3b3)
        tempLable.textAlignment = .center
        tempLable.numberOfLines = 0
        return tempLable
    }()
    
    lazy var contentBtn: UIButton = {
        let tempBtn = UIButton.init()
        tempBtn.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 40))
        tempBtn.setTitle(kGoShoppingString, for: .normal)
        tempBtn.setTitleColor(kRGBColorFromHex(rgbValue: 0xe9668f), for: .normal)
        tempBtn.layer.borderColor = kRGBColorFromHex(rgbValue: 0xe9668f).cgColor
        tempBtn.layer.borderWidth = kSizeFrom750(x: 1)
        tempBtn.addTarget(self, action: #selector(goShoppingBtn), for: .touchUpInside)
        return tempBtn
    }()
}
