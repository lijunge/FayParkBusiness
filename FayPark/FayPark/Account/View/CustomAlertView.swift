//
//  CustomAlertView.swift
//  FayPark
//
//  Created by faypark on 2018/6/15.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

enum AlertButtonType {
    case AlertButtonTypeNormal
    case AlertButtonTypeRed
}

struct AlertButtonModel {
    var title: String
    var buttonType: AlertButtonType
}

class CustomAlertView: UIView {
    
    var centerView = UIView()
    typealias BottomButtonTappedBlock = (_ button: UIButton) -> Void
    var bottomButtonTapped: BottomButtonTappedBlock?
    var tapgesture = UITapGestureRecognizer()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let window = UIApplication.shared.keyWindow
        guard let currentWindow = window else { return }
        currentWindow.addSubview(self)
        currentWindow.bringSubview(toFront: self)
        UIView.animate(withDuration: 0.25, animations: {[unowned self] in
            self.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
            }, completion: nil)
        // 点击背景移除self
        tapgesture = UITapGestureRecognizer(target: self, action: #selector(dismissFromSuperView))
        //tap.delegate = selft
        self.addGestureRecognizer(tapgesture)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func removeBackGesture() {
        self.removeGestureRecognizer(tapgesture)
    }
    func showWithAlertView(title: String,detailString: String,buttonArr:[AlertButtonModel],cancel: Bool) {
        
        let centerW = frame.size.width - 40
        centerView.frame = CGRect(x: 20, y: 0, width: centerW, height: 100)
        
        let cancelButton = UIButton(frame: CGRect(x: centerW - 50, y: 0, width: 40, height: 40))
        cancelButton.setImage(UIImage(named: "settings_icon_close"), for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        cancelButton.isHidden = cancel
        centerView.addSubview(cancelButton)
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 35, width: centerW, height: 30))
        titleLabel.text = title
        titleLabel.textColor = kRGBColorFromHex(rgbValue: 0x333333)
        titleLabel.font = UIFont(name: kGothamRoundedBold, size: 18)
        titleLabel.textAlignment = .center
        centerView.addSubview(titleLabel)
        
        let detailFont: UIFont = UIFont(name: kMSReference, size: 16)!
        let detailLabel = UILabel(frame: CGRect(x: 0, y: titleLabel.frame.maxY + 5, width: centerW, height: 20))
        detailLabel.text = detailString
        detailLabel.textColor = kRGBColorFromHex(rgbValue: 0x494949)
        detailLabel.font = detailFont
        detailLabel.numberOfLines = 0
        detailLabel.lineBreakMode = .byWordWrapping
        detailLabel.textAlignment = .center
        let detailH = detailString.getHeightWithConstrainedWidth(width: centerW, font: detailFont)
        detailLabel.frame = CGRect(x: 0, y: titleLabel.frame.maxY + 5, width: centerW, height: detailH)
        centerView.addSubview(detailLabel)
        
        let topMargin = detailLabel.frame.maxY + 20
        var buttonY = topMargin
        for i in 0 ..< buttonArr.count {
            let tmpButton = UIButton(frame: CGRect(x: 20 , y: topMargin + CGFloat(i) * 72, width: centerW - 40, height: 60))
            let alertButtonModel = buttonArr[i]
            tmpButton.setTitle(alertButtonModel.title, for: .normal)
            tmpButton.titleLabel?.font = kGothamRoundedFontSize(size: kSizeFrom750(x: 40))
            if alertButtonModel.buttonType == .AlertButtonTypeNormal {
                tmpButton.setTitleColor(kRGBColorFromHex(rgbValue: 0x000000), for: .normal)
                tmpButton.layer.borderWidth = kSizeFrom750(x: 1)
                tmpButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0x000000).cgColor
            } else {
                tmpButton.setTitleColor(kRGBColorFromHex(rgbValue: 0xe9668f), for: .normal)
//                tmpButton.layer.borderWidth = 1.0
//                tmpButton.layer.borderColor = kRGBColorFromHex(rgbValue: 0xe9668f).cgColor
            }
            tmpButton.tag = 100 + i
            tmpButton.addTarget(self, action: #selector(bottomButtonTapped(button:)), for: .touchUpInside)
            centerView.addSubview(tmpButton)
            buttonY = tmpButton.frame.maxY
        }
        let maxY = buttonY + 28
        centerView.frame = CGRect(x: 20, y: 0, width: centerW, height: maxY)
        centerView.center = self.center
        centerView.backgroundColor = UIColor.white
        self.addSubview(centerView)
    }
    @objc func dismissFromSuperView() {
        UIView.animate(withDuration: 0.25, animations: {
            self.backgroundColor = UIColor.clear
        }) { (hidden) in
            self.removeFromSuperview()
        }
    }
    @objc func cancelButtonTapped() {
        dismissFromSuperView()
    }
    @objc func bottomButtonTapped(button: UIButton) {
        if bottomButtonTapped != nil {
            dismissFromSuperView()
            bottomButtonTapped!(button)
        }
    }
}

//extension CustomAlertView: UIGestureRecognizerDelegate {
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if (touch.view?.isKind(of: UITableView.classForCoder()))! {
//            return true
//        }
//        return false
//    }
//}
