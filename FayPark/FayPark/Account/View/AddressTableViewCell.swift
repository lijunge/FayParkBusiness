//
//  AddressTableViewCell.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/10.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    var editAddress : ((_ cell : AddressTableViewCell) -> Void)!
    var deleteAddress : ((_ cell : AddressTableViewCell) -> Void)!
    typealias SelectedButtonTapped = (_ button : UIButton) -> Void
    var selectedButtonTapped: SelectedButtonTapped?
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func setDefalutClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if selectedButtonTapped != nil {
            selectedButtonTapped!(sender)
        }
//        if !sender.isSelected {
//            sender.isSelected = !sender.isSelected
//            if selectedButtonTapped != nil {
//                selectedButtonTapped!(sender)
//            }
//        }
    }
    
    @IBAction func editBtnClick(_ sender: Any) {
        //跳转到编辑地址页面
        editAddress(self)
    }
    
    @IBAction func deleteBtnClick(_ sender: Any) {
        //删除地址条目
        deleteAddress(self)
    }
    
    func configCell(addressInfo: MyAddressInfo,isPayment: Bool) {
        self.nameLabel.text = addressInfo.Name
        self.phoneLabel.text = addressInfo.Phone
        
        //邮寄地址填写完成的显示设置应从小到大（房间号，门牌号，街道名，区，城市，省市，国家，邮编）
        self.addressLabel.text = "\(addressInfo.Apt), \(addressInfo.Address), \(addressInfo.City), \(addressInfo.States), \(addressInfo.Contry),\(addressInfo.ZipCode) "
        if isPayment {
            //是从支付页面过来的，则根据传过来的字段判断是否被选中，否则以默认地址为选中
            if addressInfo.isPaySelect {
                self.selectButton.isSelected = true
                self.selectButton.setImage(UIImage(named: "circle_radio_ok_icon"), for: .selected)
            } else {
                self.selectButton.isSelected = false
                self.selectButton.setImage(UIImage(named: "circle_radio_normal_icon"), for: .normal)
            }
        } else {
            if addressInfo.TopOne == 0 {
                self.selectButton.isSelected = false
                self.selectButton.setImage(UIImage(named: "circle_radio_normal_icon"), for: .normal)
            } else {
                self.selectButton.isSelected = true
                self.selectButton.setImage(UIImage(named: "circle_radio_ok_icon"), for: .selected)
            }
        }
    }
    
}
