//
//  SupportData.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/29.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SupportData {
    var itemID: Int
    var NewsBody: String
    var NewsTitle: String
    
    init(jsonData: JSON) {
        self.itemID = jsonData["ID"].intValue
        self.NewsBody = jsonData["NewsBody"].stringValue
        self.NewsTitle = jsonData["NewsTitle"].stringValue
    }
}
