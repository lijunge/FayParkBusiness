//
//  SupportDetailInfo.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/9.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class SupportDetailInfo {
    var title : String
    var subtitle : String
    
    init(dict : Dictionary<String, Any>) {
        title = dict["title"] as! String
        subtitle = dict["subtitle"] as! String
    }
    
}
