//
//  SupportInfo.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/9.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import Foundation

class SupportInfo {
    var name : String
    var data : [Dictionary<String, Any>]
    
    init(dict : Dictionary<String, Any>) {
        name = dict["name"] as! String
        data = dict["data"] as! Array
    }
}
