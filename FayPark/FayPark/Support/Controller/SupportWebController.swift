//
//  SupportWebController.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/4/11.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit
import MBProgressHUD
class SupportWebController: UIViewController, UIWebViewDelegate {
   
    @IBOutlet weak var webView: UIWebView!
    
    var isLoad = true
    
    var fayRequest: URLRequest!
    
    var urlString: String?
    var titleString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = titleString
        
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.setImage(UIImage(named: "arrow_left_icon"), for: .normal)
        backButton.titleLabel?.isHidden = true
        backButton.contentHorizontalAlignment = .center
        backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        let backW: CGFloat = kScreenWidth > 375.0 ? 50 : 44
        backButton.frame = CGRect(x: 0, y: 0, width: backW, height: 40)
        backButton.addTarget(self, action: #selector(letBackButtonTapped), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backButton)
        
        if let tmpUrlString = urlString {
            let request = URLRequest(url: URL(string: tmpUrlString)!)
            webView.loadRequest(request)
            webView.delegate = self
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    @objc func letBackButtonTapped() {
        if webView.canGoBack {
            webView.goBack()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
//    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        //isLoad = false
//        //print("第二个获取title\(webView.stringByEvaluatingJavaScript(from: "document.title"))")
//        if !isLoad {
//            let vc = SupportWebDetailController()
//            vc.fayRequest = request
//            vc.title = self.title
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        return isLoad
//    }

//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        isLoad = false
//        self.title = webView.stringByEvaluatingJavaScript(from: "document.title")
//    }
}
