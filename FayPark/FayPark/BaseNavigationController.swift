//
//  FPNavigationController.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/26.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {
    
    var backButton: UIButton = {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.setImage(UIImage(named: "arrow_left_icon"), for: .normal)
        backButton.titleLabel?.isHidden = true
        backButton.contentHorizontalAlignment = .center
        backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        let backW: CGFloat = kScreenWidth > 375.0 ? 50 : 44
        backButton.frame = CGRect(x: 0, y: 0, width: backW, height: 40)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        return backButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCommentNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @objc func backButtonTapped() {
        popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        viewController.navigationItem.hidesBackButton = true
        if (self.childViewControllers.count > 0) { // 非根控制器
            viewController.hidesBottomBarWhenPushed = true
        }
       // viewController.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backButton)
        super.pushViewController(viewController, animated: animated)
    }
    func setupCommentNavigationBar() {
        //设置全局navigationBar
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: kRGBColorFromHex(rgbValue: 0x333333), NSAttributedStringKey.font: UIFont(name: kMSReference, size: 17) ?? UIFont.systemFont(ofSize: 17)]
        self.navigationBar.barTintColor = UIColor.white
        //设置App全局navigationBar色调
        //        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = false
    }

}
