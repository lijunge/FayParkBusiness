//
//  CheckOutButton.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/3/16.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class CheckOutButton: UIButton {

    let selectView = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView?.frame = CGRect(x: 0, y: 0, width: frame.width * 0.5, height: frame.height - 10)
        imageView?.contentMode = .scaleAspectFit
        imageView?.center = CGPoint(x: frame.width * 0.5, y: frame.height * 0.5)
        
        selectView.frame = CGRect(x: 10, y: frame.height - 2, width: frame.width - 20, height: 2)
        selectView.backgroundColor = kRGBColorFromHex(rgbValue: 0xE9668F)
        addSubview(selectView)

    }
}
