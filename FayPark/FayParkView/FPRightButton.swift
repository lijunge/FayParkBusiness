//
//  FPRightButton.swift
//  FayPark
//
//  Created by 陈小奔 on 2018/1/15.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class FPRightButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView?.frame = CGRect(x: frame.width - 30, y: 0, width: 20, height: 20)
        imageView?.center.y = frame.height * 0.5
        imageView?.contentMode = .scaleAspectFit
        
        titleLabel?.frame = CGRect(x: 10, y: 0, width: frame.width - 40, height: 30)
        titleLabel?.center.y = frame.height * 0.5
        titleLabel?.textAlignment = .left
        titleLabel?.textColor = .darkGray
        
    }
    
    override func setBackgroundImage(_ image: UIImage?, for state: UIControlState) {
        let newImage = image?.stretchableImage(withLeftCapWidth: Int.init((image?.size.width)! * 0.5), topCapHeight: Int.init((image?.size.height)! * 0.5))
        
        super.setBackgroundImage(newImage, for: state)
    }

}
