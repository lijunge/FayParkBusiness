//
//  FPPullDownView.swift
//  FayPark
//
//  Created by 陈小奔 on 2017/12/29.
//  Copyright © 2017年 cray.chen. All rights reserved.
//

import UIKit

protocol FPPullDownMenuDelegate {
    func downMenuWillShow(menu : FPPullDownMenu)
    func downMenuDidShow(menu : FPPullDownMenu)
    func downMenuWillHide(menu : FPPullDownMenu)
    func downMenuDidHide(menu : FPPullDownMenu)
    func downMenu(menu : FPPullDownMenu,selectNum : Int)
}


class FPPullDownMenu: UIView,UITableViewDelegate,UITableViewDataSource{
    var mainBtn : UIButton?
    var delegate : FPPullDownMenuDelegate?
    
    var titlesArray :[String]?
    var rowHeight : CGFloat = 0
    
    var listView = UIView()
    var tableView :UITableView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        //初始化mainBtn
        mainBtn = UIButton()
        mainBtn?.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        mainBtn?.setTitle("国家", for: .normal)
        mainBtn?.contentHorizontalAlignment = .left
        mainBtn?.setTitleColor(.black, for: .normal)
//        mainBtn?.layer.borderColor = UIColor.black.cgColor
//        mainBtn?.layer.borderWidth = 1
        mainBtn?.addTarget(self, action: #selector(clickMainBtn(sender:)), for: .touchUpInside)
        if let _ = mainBtn {
            addSubview(mainBtn!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //初始化mainBtn
        mainBtn = UIButton()
        mainBtn?.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        mainBtn?.setTitle("国家", for: .normal)
       // mainBtn?.setTitleColor(.black, for: .normal)
//        mainBtn?.layer.borderColor = UIColor.black.cgColor
//        mainBtn?.layer.borderWidth = 1
        mainBtn?.addTarget(self, action: #selector(clickMainBtn(sender:)), for: .touchUpInside)
        if let _ = mainBtn {
            addSubview(mainBtn!)
        }
    }
    
    //设置下拉框的选项
    func setMenuTitles(titles : [String],rowHeight : CGFloat) -> Void {
        self.titlesArray = Array(titles)
        self.rowHeight = rowHeight
        let statusHeight = UIApplication.shared.statusBarFrame.height + 44
        listView = UIView()
        listView.frame = CGRect(x: KScreenWidth - 170, y: statusHeight + 10, width: 150, height: 0)
        listView.backgroundColor = .red
        listView.clipsToBounds = true
        
        tableView = UITableView()
        tableView?.frame = CGRect(x: 0, y: 0, width: (listView.frame.width), height: (listView.frame.height))
        tableView?.dataSource = self
        tableView?.delegate = self
        listView.addSubview(tableView!)
        
    }
    
    //点击mainBtn
    @objc func clickMainBtn(sender:UIButton) {
        let window = UIApplication.shared.keyWindow!
        window.addSubview(listView)
        //self.superview?.addSubview(listView!)
        if sender.isSelected {
            hideDownMenu()
        } else {
            showDownMenu()
        }
    }
    
    //隐藏下拉
    func hideDownMenu() {
        delegate?.downMenuWillHide(menu: self)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.listView.frame = CGRect(x: (self.listView.frame.origin.x), y: (self.listView.frame.origin.y), width: (self.listView.frame.width), height:0)
            
            self.tableView?.frame = CGRect(x: 0, y: 0, width: (self.listView.frame.width), height: 0)
        }) { (finished) in
            self.delegate?.downMenuDidHide(menu: self)
        }
        
        mainBtn?.isSelected = false
    }
    
    //显示下拉
    func showDownMenu() {
        delegate?.downMenuWillShow(menu: self)
        UIView.animate(withDuration: 0.25, animations: {
            let count = CGFloat((self.titlesArray?.count)!)
            let listHeight = self.rowHeight * count + 20
            self.listView.frame = CGRect(x: (self.listView.frame.origin.x), y: (self.listView.frame.origin.y), width: (self.listView.frame.width), height:listHeight)
            self.tableView?.frame = CGRect(x: 0, y: 0, width: (self.listView.frame.width), height: listHeight)
        }) { (finished) in
            self.delegate?.downMenuDidShow(menu: self)
        }
        mainBtn?.isSelected = true
    }
    
    //MARK: tableview代理方法
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (titlesArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init()
        cell.textLabel?.text = titlesArray?[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectCell = tableView.cellForRow(at: indexPath)
        
        delegate?.downMenu(menu: self, selectNum: indexPath.row)
        mainBtn?.setTitle(selectCell?.textLabel?.text, for: .normal)
        
        
        selectCell?.imageView?.image = UIImage(named: "OtherIco")
    
        hideDownMenu()
    }
}
