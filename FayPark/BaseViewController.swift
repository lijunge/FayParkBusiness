//
//  BaseViewController.swift
//  FayPark
//
//  Created by faypark on 2018/7/25.
//  Copyright © 2018年 cray.chen. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
                
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.setImage(UIImage(named: "arrow_left_icon"), for: .normal)
        backButton.titleLabel?.isHidden = true
        backButton.contentHorizontalAlignment = .center
        backButton.contentEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        let backW: CGFloat = kScreenWidth > 375.0 ? 50 : 44
        backButton.frame = CGRect(x: 0, y: 0, width: backW, height: 40)
        backButton.addTarget(self, action: #selector(leftBackButtonTapped), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: backButton)
    }
    
    @objc func leftBackButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
